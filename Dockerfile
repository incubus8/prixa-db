FROM golang:1.14-alpine as builder
RUN apk --no-cache add make git gcc libtool musl-dev
WORKDIR /
COPY . /
RUN make build

FROM alpine:latest
RUN apk --no-cache add ca-certificates && \
    rm -rf /var/cache/apk/* /tmp/*
COPY data /data
COPY --from=builder /prixa-db .
COPY --from=builder /prixa-db-hospital-ingestion .
COPY --from=builder /prixa-db-nalar-ingestion .
COPY --from=builder /prixa-db-nalar-enrichment .
COPY --from=builder /prixa-db-siloam-ingestion .
ENTRYPOINT ["/prixa-db"]

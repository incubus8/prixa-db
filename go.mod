module prixa.ai/prixa-db

go 1.14

require (
	cloud.google.com/go v0.56.0
	cloud.google.com/go/logging v1.0.0
	cloud.google.com/go/storage v1.6.0
	contrib.go.opencensus.io/exporter/stackdriver v0.13.1
	github.com/ClickHouse/clickhouse-go v1.4.0
	github.com/PaesslerAG/jsonpath v0.1.1
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/apache/beam v2.20.0+incompatible
	github.com/armon/go-metrics v0.0.0-20190430140413-ec5e00d3c878 // indirect
	github.com/ashulepov/gocron-graceful v0.0.0-20190417134532-0ec856145c65
	github.com/catinello/base62 v0.0.0-20160325105823-e0daaeb631c9
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/crisp-im/go-crisp-api v3.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.5.1
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/go-resty/resty/v2 v2.1.0
	github.com/go-sql-driver/mysql v1.4.0
	github.com/goccy/go-yaml v1.4.3
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/protobuf v1.4.0
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/graarh/golang-socketio v0.0.0-20170510162725-2c44953b9b5f // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.14.4
	github.com/hashicorp/consul/api v1.4.0
	github.com/hashicorp/go-msgpack v0.5.5 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20200323211822-1a413f9a41a2
	github.com/joho/godotenv v1.3.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/lib/pq v1.3.0
	github.com/lytics/base62 v0.0.0-20180808010106-0ee4de5a5d6d
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/meilisearch/meilisearch-go v0.12.1
	github.com/microcosm-cc/bluemonday v1.0.2
	github.com/minio/minio-go/v6 v6.0.55
	github.com/mitchellh/mapstructure v1.2.2
	github.com/mwitkow/go-proto-validators v0.3.0 // indirect
	github.com/oleiade/reflections v1.0.0 // indirect
	github.com/olivere/elastic/v7 v7.0.11
	github.com/opentracing/opentracing-go v1.1.0
	github.com/prixa-ai/gosocial v0.0.0-20200406075154-52cf85d4ba75
	github.com/prixa-ai/logrus-sentry-hook v0.1.3
	github.com/prixa-ai/prixa-proto v0.2.21-0.20200930061152-e2596f63a256
	github.com/qntfy/jsonparser v1.0.2 // indirect
	github.com/qntfy/kazaam v3.4.8+incompatible
	github.com/rs/cors v1.7.0
	github.com/sendgrid/rest v2.4.1+incompatible
	github.com/sendgrid/sendgrid-go v3.5.0+incompatible
	github.com/shuoli84/sqlm v0.0.0-20161018142151-867c99438774
	github.com/sirupsen/logrus v1.5.0
	github.com/slack-go/slack v0.6.4
	github.com/spf13/cast v1.3.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	github.com/stretchr/testify v1.5.1
	github.com/umahmood/haversine v0.0.0-20151105152445-808ab04add26
	github.com/yuin/gopher-lua v0.0.0-20190514113301-1cd887cd7036 // indirect
	go.opencensus.io v0.22.3
	golang.org/x/crypto v0.0.0-20200206161412-a0c6ece9d31a
	golang.org/x/net v0.0.0-20200501053045-e0ff5e5a1de5 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	google.golang.org/api v0.22.0
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/genproto v0.0.0-20200430143042-b979b6f78d84
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.21.0
)

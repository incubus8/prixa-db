package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v7"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/telemedicine"
)

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-agent-allocation"); err != nil {
		logrus.Warnf("init error: %v", err)
	}
}

func main() {
	ctx := context.Background()
	publisher, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error when setup RabbitMQ")
	}
	redisClient := db.NewRedisClient(ctx)
	_, err = redisClient.Ping().Result()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error redis connection")
	}

	engine := telemedicine.NewDoctorAllocationEngine(ctx, redisClient, publisher)
	engine.StartDoctorAllocationEngine()
	engine.AutomaticResolveIdleChat()
	gracefulShutdown(publisher, redisClient, engine)
}

func gracefulShutdown(publisher *messaging.RabbitMQPublisher, redisClient *redis.Client, engine *telemedicine.DoctorAllocationEngine) {
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)
	<-osSignals
	logrus.Infoln("osSignal Interrupt trigerred")
	_ = engine.Close()
	_ = redisClient.Close()
	_ = publisher.Close()
}

package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/golang/protobuf/ptypes/any"
	"github.com/golang/protobuf/ptypes/timestamp"
	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"github.com/streadway/amqp"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/messaging"
)

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-nalar-enrichment"); err != nil {
		logrus.Warnf("init error: %v", err)
	}
}

type geoReversedData struct {
	KodeKabupaten string `json:"kode_kab"`
	KodeKecamatan string `json:"kode_kec"`
	KodeKelurahan string `json:"kode_kel"`
	NamaKabupaten string `json:"nama_kab"`
	NamaKecamatan string `json:"nama_kec"`
	NamaKelurahan string `json:"nama_kel"`
}

func getGeoReversedData(lat, lon float32) (geoReversedData, error) {
	resp, err := http.Get("https://covid19-public.digitalservice.id/api/v1/location-detail?api_key=y7SRQ0UZWXgdMqgd5NrECahbxBZ2HK74&lat=" + cast.ToString(lat) + "&long=" + cast.ToString(lon))
	if err != nil {
		logrus.WithError(err).Errorln()
		return geoReversedData{
			NamaKabupaten: "",
			NamaKecamatan: "",
			NamaKelurahan: "",
		}, err
	}

	returnedData := geoReversedData{}
	respBody, _ := ioutil.ReadAll(resp.Body)
	errUnmarshal := json.Unmarshal(respBody, &returnedData)
	if errUnmarshal != nil {
		return geoReversedData{
			NamaKabupaten: "",
			NamaKecamatan: "",
			NamaKelurahan: "",
		}, errUnmarshal
	}
	return returnedData, nil
}

func main() {
	ctx := context.Background()
	// consumer, err := stream.NewNATSConsumer()
	consumer, err := messaging.NewRabbitMQConsumer()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error upon making NATS Consumer")
	}
	// natsPublisher, err := stream.NewNATSPublisher()
	rmq, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error upon making NATS Publisher")
	}
	publisher := messaging.Publisher(rmq)
	handler := func(msgs <-chan amqp.Delivery) {
		ci := bot.ConversationInteraction{}
		go func() {
			for msg := range msgs {
				err := json.Unmarshal(msg.Body, &ci)
				if err != nil {
					logrus.WithError(err).Errorln()
				}

				geoReversed, errGetGeoReversedData := getGeoReversedData(ci.Latitude, ci.Longitude)
				if errGetGeoReversedData != nil {
					logrus.WithError(errGetGeoReversedData).WithContext(ctx).Errorln()
				}
				reply, errMarshal := json.Marshal(ci.Reply)
				if errMarshal != nil {
					logrus.WithError(errMarshal).Errorln()
				}

				result, errMarshal := json.Marshal(ci.Result)
				if errMarshal != nil {
					logrus.WithError(errMarshal).Errorln()
				}
				publisher.Publish(&analyticGRPC.NalarEvent{
					EventLog: &analyticGRPC.EventLog{
						PartnerID:    ci.PartnerID,
						PartnerAppID: ci.ApplicationID,
						Date:         &timestamp.Timestamp{Seconds: time.Now().Unix()},
					},
					SessionID: ci.SessionID,
					Latitude:  ci.Latitude,
					Longitude: ci.Longitude,
					Kabupaten: geoReversed.NamaKabupaten,
					Kecamatan: geoReversed.NamaKecamatan,
					Kelurahan: geoReversed.NamaKelurahan,
					PrevState: ci.PreviousState,
					NextState: ci.NextState,
					Reply: &any.Any{
						Value: reply,
					},
					Result: &any.Any{
						Value: result,
					},
				}, messaging.QueueGeoLocationEnrichment, "")
			}
		}()
	}
	consumer.ConsumeQueue(messaging.QueueNalarEvent, handler)

	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	<-osSignals

	_ = rmq.Close()
	_ = consumer.Stop()

	logrus.Infoln("exiting nalar enrichment...")
}

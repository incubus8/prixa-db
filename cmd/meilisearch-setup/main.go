package main

import (
	"context"
	"encoding/json"
	"flag"
	"strings"
	"time"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/sdk"
)

var (
	action      = flag.String("action", "help", "action for meilisearch setup")
	path        = flag.String("path", "/meili-indexes", "indexes source for meilisearch")
	consulValue json.RawMessage
)

func init() {
	flag.Parse()
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config"); err != nil {
		logrus.Warnf("init error: %v", err)
	}

	consulValue, err = cfgManager.GetConfiguration(*path)
	if err != nil {
		logrus.WithError(err).Fatalln()
	}
}

type actionHandler func(client *meili.Client, index string, setting sdk.MeiliIndexSetting) error

func getActionHandler() map[string]actionHandler {
	return map[string]actionHandler{
		"create": sdk.CreateIndex,
		"delete": sdk.DeleteIndex,
	}
}

func main() {
	client := sdk.NewMeilisearchClient()

	var indexes sdk.MeiliIndexes
	err := json.Unmarshal(consulValue, &indexes)
	if err != nil {
		logrus.WithError(err).Fatalln()
	}

	handler, ok := getActionHandler()[strings.ToLower(*action)]
	if !ok {
		logrus.WithField("warning", "action must be `create` or `delete`").Fatalln()
	}
	for ind, setting := range indexes.Nalar {
		err = handler(client, ind, setting)
		if err != nil {
			logrus.WithError(err).Errorln()
		}
		/*
			Meilisearch seems unable to handle index create in a very short interval.
			The effect of this seems to completely shutdown/freeze meilisearch server.
			Therefore, time.Sleep(duration) is use in an attempt to handle this behaviour.
		*/
		time.Sleep(time.Second)
	}
	for ind, setting := range indexes.Covid {
		err = handler(client, ind, setting)
		if err != nil {
			logrus.WithError(err).Errorln()
		}
		/*
			Meilisearch seems unable to handle index create in a very short interval.
			The effect of this seems to completely shutdown/freeze meilisearch server.
			Therefore, time.Sleep(duration) is use in an attempt to handle this behaviour.
		*/
		time.Sleep(time.Second)
	}
}

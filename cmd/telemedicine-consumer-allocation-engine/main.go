package main

import (
	"context"
	"database/sql"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-redis/redis/v7"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/repository"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/store/postgreslayer"
	"prixa.ai/prixa-db/pkg/store/redislayer"
	"prixa.ai/prixa-db/pkg/util"
)

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-consumer-agent-allocation"); err != nil {
		logrus.Warnf("init error: %v", err)
	}
}

func initConsumerRepository(store store.Store) map[string]messaging.ConsumerRepository {
	partners := getListPartner()
	consumers := make(map[string]messaging.ConsumerRepository, len(partners))
	for _, partner := range partners {
		consumers[partner] = messaging.ConsumerRepository{
			ExchangeName:     os.Getenv("RABBITMQ_TELEMEDICINE_ALLOCATION_EXCHANGE"),
			RoutingKey:       "telemed:partner:" + partner,
			BindingKey:       "telemed." + partner + ".*",
			ConsumerCallback: repository.NewAgentAllocationConsumerService(store),
			Args: map[string]interface{}{
				"x-message-ttl": 60000,
			},
		}
	}
	consumers["ResolvedChat"] = messaging.ConsumerRepository{
		RoutingKey:       os.Getenv("RABBITMQ_TELEMEDICINE_RESOLVE_CHAT_ROUTINGKEY"),
		ConsumerCallback: repository.NewResolveChatConsumerService(store),
	}
	consumers["DoctorStatus"] = messaging.ConsumerRepository{
		RoutingKey:       os.Getenv("RABBITMQ_TELEMEDICINE_SET_DOCTOR_STATUS_ROUTINGKEY"),
		ConsumerCallback: repository.NewSetDoctorStatusConsumerService(store),
	}
	return consumers
}

func main() {
	defer util.RecoverWithSentry()
	ctx := context.Background()
	redisClient := db.NewRedisClient(ctx)
	_, err := redisClient.Ping().Result()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error redis connection")
	}

	pgClient, err := sdk.SetupPostgreClient()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error postgres connection")
	}

	rmq, err := messaging.NewRabbitMQConsumer()
	if err != nil {
		logrus.WithError(err).Fatalln("failed to connect to RabbitMQ")
	}

	store := initStore(redisClient, pgClient)
	store.SetContext(ctx)
	rmq.SetConsumerRepository(initConsumerRepository(store))

	err = rmq.StartConsumingAllQueues()
	if err != nil {
		logrus.WithError(err).Fatalln()
	}

	gracefulShutdown(rmq, redisClient)
}

func gracefulShutdown(rmq *messaging.RabbitMQConsumer, redisClient *redis.Client) {
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)
	<-osSignals

	logrus.Infof("osSignal Interrupt trigerred")
	_ = redisClient.Close()
	_ = rmq.Close()
}

func getListPartner() []string {
	return []string{
		"SHDP", "SHMT", "MRCCC", "SHCN", "SH ASRI", "SHBG", "SHKJ", "SHLC", "SHLV", "SHTB", "SHCB", "SHSR", "SHSB",
		"SHYG", "SHBP", "SHPR", "SHMK", "SHMN", "SHPD", "SHBB", "SHJB", "SHMD", "SHPL", "SHLL", "SHJR",
	}
}

func initStore(redis *redis.Client, pgClient *sql.DB) store.Store {
	rootStore := store.NewAppStore()
	redisStore := redislayer.NewRedisStore(rootStore, redis, nil)
	appStore := postgreslayer.NewPostgresStore(redisStore, pgClient)
	return appStore
}

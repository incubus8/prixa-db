package main

import (
	"context"

	graceful "github.com/ashulepov/gocron-graceful"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/sdk/siloam"
	"prixa.ai/prixa-db/pkg/util"
)

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-siloam-ingestion"); err != nil {
		logrus.Warnf("init error: %v", err)
	}
}

func main() {
	defer util.RecoverWithSentry()
	graceful.Worker("task For Ingest Siloam API data and Publish to RabbitMQ", siloam.TaskPipeliningSiloamAPIToRabbitMQ)
}

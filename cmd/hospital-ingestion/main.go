package main

import (
	"context"

	graceful "github.com/ashulepov/gocron-graceful"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/sdk/siloam"
)

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-hospital-ingestion"); err != nil {
		logrus.Warnf("init error: %v", err)
	}
}

func main() {
	graceful.Worker("task For Ingest Hospital data and Save to ElasticSearch", siloam.StartIngestHospitalDataFromAirtable)
}

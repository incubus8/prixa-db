package main

import (
	"context"
	"encoding/json"
	"flag"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/pipelining"
	"prixa.ai/prixa-db/pkg/util"
)

func init() {
	flag.Parse()

	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config-nalar-ingestion"); err != nil {
		logrus.Warnf("init error: %v", err)
	}

	yamlConfig, err = cfgManager.GetConfiguration(*yamlPath)
	if err != nil {
		logrus.WithError(err).Fatalln()
	}
}

var (
	yamlPath   = flag.String("yaml", "", "relative path yaml file in consul to run pipelining")
	yamlConfig json.RawMessage
)

func main() {
	defer util.RecoverWithSentry()
	ctx := context.Background()
	err := pipelining.RunPrixaPipelining(ctx, yamlConfig)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Fatalln()
	}
}

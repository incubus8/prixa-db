Partner-dataflow
=======

This is a brief description on usage and the function of each scripts in partner-dataflow for prixa.ai.
This folder contains:

Dockerfile
-----------
A dockerfile to be used to build docker image that must be done in order to run the stream pipeline in kubernetes pods.
The image will contains all script in the folder. If the included file want to be specified, make sure to include streamline_data.py and creds.py.

requirement.txt
-----------
A python requirement for the libraries which will be used in streamline_data.py.

schedule_crons.py
-----------
A python script which will be used to automatically schedule all cronjobs for each partner listed in backend database.
The cript will automatically produce a cronfile with a name "dynamicCron.yaml" which then will be applied using kubectl command.
Schedule crontab is set to 16:57:00 UTC.
Activate port forwarding to consul server before usage.

cronfile.yaml
-----------
A yaml file contains the details of the job schedule, containers and its command that will be executed in the time specified in crontab format.
the job name will dynamically changed based on partner specified using schedule_cron.py.


creds.py
-----------
A python script to generate creds.json file for SA authentication using gcloud command.

streamline_data.py
-----------
The pipeline script to activate pipeline in dataflow which will produce 4 pipelines that process data and store metrics csv data.


=======
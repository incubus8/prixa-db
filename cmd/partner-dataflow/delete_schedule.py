import json
import os
import requests
import consul # pylint: disable=import-error


client = consul.Consul(host="127.0.0.1", port="8500", dc="primarydc")
index, data_consul = client.kv.get('dataflow/covid19_engine_creds')
creds = json.loads(data_consul['Value'].decode('utf8').replace("'", '"'))

url = creds['url']

header = {
        'content-type' : 'application/json',
        'X-Prixa-API-Key' : creds['X-Prixa-API-Key'],
        'x-partner-app-id' : creds['x-partner-app-id'],
        'x-partner-id' : creds['x-partner-id']
    }

data = {
        "authData": {
            "email": creds['authData']['email'],
            "password": creds['authData']['password']
            }
    }

req = requests.post(url, json=data, headers=header )
retrieved = req.json()

url_get = creds['url-get']

header_get = {
        'X-Prixa-API-Key' : creds['X-Prixa-API-Key'],
        'Authorization' : retrieved['loginToken'],

    }

req_get = requests.get(url_get, headers=header_get)  
json_data = req_get.json()
total_pages = (json_data['page'])['totalPages']

datas = []
for i in range(1, total_pages+1):
    req_get_all = requests.get(url_get+"?page=" + str(i), headers=header_get)
    json_data_all = req_get_all.json()
    for j in json_data_all['data']:
        datas.append([j['name'], j['id']])

for data in datas:
    file = open("cronfile.yaml", "r")
    job = "partner-" + ((data[0].lower()).replace(' ', '')).replace("+", "plus")
    cron = (file.read().replace("namapartner", data[0] )).replace("namajob", job)
    os.system("kubectl delete cronjob " + job)

import consul
import json
import os

client = consul.Consul(host='consul-consul-server.consul', port=8500, dc="primarydc")
index, data = client.kv.get('dataflow/sa')
creds = json.loads(data['Value'].decode('utf8').replace("'", '"'))

with open('creds.json', 'w') as outfile:
    json.dump(creds, outfile)
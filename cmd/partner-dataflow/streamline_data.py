import argparse
import json
import os
import time
import consul # pylint: disable=import-error
import pandas as pd # pylint: disable=import-error
import psycopg2 # pylint: disable=import-error
import requests
import apache_beam as beam # pylint: disable=import-error
from apache_beam.options.pipeline_options import PipelineOptions # pylint: disable=import-error
from apache_beam.options.pipeline_options import StandardOptions # pylint: disable=import-error
from apache_beam.options.pipeline_options import GoogleCloudOptions # pylint: disable=import-error

def argumentIn():

    """
    Get and parse arguments inputed from command line.

    Args:

    Returns:
      args (str) : contains date target for the query and partner name.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--date_file", help="Data date")
    parser.add_argument("-p", "--partner", help="Partner name")

    args = parser.parse_args()

    print("Data date : {} \nPartner : {}  ".format(
        args.date_file,
        args.partner))
    return args

def get_json_data():
    """
    Get json data contains all partners data from backend.

    Args:

    Returns:
      datas (str) : partner's name and partner's id.
    """

    #connecting to consul
    consul_client = consul.Consul(host="consul-consul-server.consul", port=8500)
    index, data_consul = consul_client.kv.get('dataflow/covid19_engine_creds')
    creds = json.loads(data_consul['Value'].decode('utf8').replace("'", '"'))
    url = creds['url']
    header = {
        'content-type' : 'application/json',
        'X-Prixa-API-Key' : creds['X-Prixa-API-Key'],
        'x-partner-app-id' : creds['x-partner-app-id'],
        'x-partner-id' : creds['x-partner-id']}
    data = {
        "authData":
            {
                "email": creds['authData']['email'],
                "password": creds['authData']['password']
            }
        }
    access = False
    while(access == False):
        #login
        req = requests.post(url, json=data, headers=header)
        retrieved = req.json()
        url_get = creds['url-get']
        header_get = {
            'X-Prixa-API-Key' : creds['X-Prixa-API-Key'],
            'Authorization' : retrieved['loginToken'],
            }
        #get a json data to retrieve page totals
        req_get = requests.get(url_get, headers=header_get)
        json_data = req_get.json()
        if 'page' in json_data:
            total_pages = (json_data['page'])['totalPages']
            datas = []
            for i in range(1, total_pages+1):
                req_get_all = requests.get(url_get + "?page=" + str(i), headers=header_get)
                json_data_all = req_get_all.json()
                #get json data that contains key 'data'
                while 'data' not in json_data_all:
                    req = requests.post(url, json=data, headers=header)
                    retrieved = req.json()
                    header_get['Authorization'] = retrieved['loginToken']
                    req_get_all = requests.get(url_get + "?page=" + str(i), headers=header_get)
                    json_data_all = req_get_all.json()
                for j in json_data_all['data']:
                    datas.append([j['name'], j['id']])
            access = True

    return datas

def get_partner_id(partner_name, partner_datas):

    """
    Get partner id by iterating through json data retrieved from backend.
    The function will be used inside getPartnerData function.

    Args:
      partner_name (str) : exact partner name in backend partner database.
      dataPage1 (json) : page 1 of json  partner data retrieved from backend.
      dataPage2 (json) : page 2 of json  partner data retrieved from backend.

    Returns:
      find (bool) : data find status.
      partnerId (str) : partner id. If not found will return empty string.
    """

    partner_id = ''
    find = False

    for data in partner_datas:
        if partner_name == data[0]:
            partner_id = data[1]
            find = True
            print("Partner %s found" %(partner_name))
            break      

    if find is False:
        print("Partner %s not found. Either non-existent, inactive, or deleted." %(partner_name))

    return find, partner_id

def get_partners_data(partner):
    """
    Get partner json data retrieved from backend and put it
    in get_partner_id function to get find status and partner id.

    Args:
      partner(str) : partner name that is requested.

    Returns:
      find (bool) : data find status.
      partnerId (str) : partner id. If not found will return empty string.
    """

    datas = get_json_data()

    if partner == 'all':
        return datas
    return get_partner_id(partner, datas)


def main(date_file, partner, partner_id_req, single):

    """
    Main function, if single is False then will run pipeline for all partners
    else will run on specified partner.

    Args:
      date_file (str) : desired date target.
      partner (str) : desired partner name.
      partner_id_req (str) : desired partner id.
      single (bool) : as an indicator wether the run function will be run for one partner or all.

    Returns:
    """

    class Process(beam.DoFn):
        def process(self, element):
            """
            Process the retrieved data from retrieve_data function into suitable format.
            Used in pipeline directly to process the return value of retrieve_data.

            Args:
              element (PCollection) : data input in apache beam pipeline

            Returns:

            """
            data = element
            data = map(lambda x: x.__str__(), element)
            data = (", ").join(data)
            print(data)

            return [data]

    #beam pipeline
    def run(partner, partner_id_req, date_file):
        """
          Running the whole pipeline process.

          Args:
            partner (str) : desired partner name.
            pIdReq (str) : desired partner id.
            date_file = desired date targer.

          Returns:

          """

        def get_col_header(argument):

            """
            Get column header for the csv file output of the pipeline by matching the argument with the table available

            Args:
              argument (str) : table name or metric requested

            Returns:
              (str) : column header
            """
            switcher = {
                "user_conversion": "date, initial, askPrecondition, showComplaint, chooseComplaint, fin",
                "symptoms": "date, chiefComplaintUser, count",
                "diseases": "date, diseaseName, count",
                "gender": "date, female, male",
                "age": "age, count",
                "RDT" : "date, session_id, nama, no_hp, gender, age, kabupaten, kecamatan, kelurahan, diabetes, hipertensi, gagal_ginjal, kanker, CVD, contact_history, local_transmission, pain_chest_severe, shortness_of_breath, cough, fever, runny_nose, sore_throat, disease_name"
            }

            return switcher.get(argument, "")

        def retrieve_query(table):
            """
            function to get query based on metric choosen
            """
            index, query_byte = client.kv.get("dataflow/queries/"+table)
            if query_byte is not None:
                query = query_byte['Value'].decode("utf-8")
                return query

            print("query does not exist.")
            return ''

        def adjust_query(query, date, partner_id):
            """
            Adjust query by changing dateTarget and partner_id with the desired date target and partner id.

            Args:
              query (str) : query of the desired metric.
              date (str) : desired date target
              partner_id (str) : desired partner Id

            Returns:
              (str) : changed query
            """

            return (query.replace('dateTarget', date)).replace('partnerId', partner_id)
        
        def insert_data(metric, data, partner_id):
            """
            Inserting data to nalar-report db.

            Args:
              metric (str) : metric name.
              data  : desired date target.
              partner_id (str) : desired partner Id.
            """
            print("Inserting to reporting db has started")
            #get creds for db prixa dev nalar-reporting from consul
            client = consul.Consul(host='consul-consul-server.consul', port=8500, dc="primarydc")
            index, creds_data = client.kv.get("dataflow/postgres-dev")
            dev_creds = json.loads(creds_data['Value'].decode('utf8').replace("'", '"'))

            #get insertion query correspond the metric's kind
            client = consul.Consul(host='consul-consul-server.consul', port=8500, dc="primarydc")
            index, insert_data = client.kv.get("dataflow/queries/insert/" + metric)
            insert_query = insert_data['Value'].decode('utf8')

            try:
                connection = psycopg2.connect(user=dev_creds['user'],
                                              password=dev_creds['password'],
                                              host=dev_creds['hostname'],
                                              port=dev_creds['port'],
                                              database=dev_creds['database'])

                cursor = connection.cursor()
                # Print PostgreSQL Connection properties
                print(connection.get_dsn_parameters(), "\n")

                print("Executing insertion query...")

                if "user_conversion" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, (str(d[0]), str(partner_id), 
                            str(int(d[1])), str(int(d[2])), str(int(d[3])), str(int(d[4])),  str(int(d[5]))))
                            connection.commit()
                        except:
                            continue
                elif "gender" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, (str(d[0]), str(partner_id), str(int(d[1])), str(int(d[2]))))
                            connection.commit()
                        except:
                            continue
                elif "symptoms" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, (str(d[0]), str(partner_id), str(d[1]), str(int(d[2]))))
                            connection.commit()
                        except:
                            continue
                elif "age" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, (str(d[0]), str(partner_id), str(d[1]), str(d[2])))
                            connection.commit()
                        except:
                            continue
                elif "diseases" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, (str(d[0]), str(partner_id), str(d[1]), str(int(d[2]))))
                            connection.commit()
                        except:
                            continue
                elif "rdt" in metric:
                    for d in data:
                        try:
                            cursor.execute(insert_query, ( str(d[0]), str(d[1]), str(partner_id),  str(d[2]), str(d[3]),
                            str(d[4]), str(d[5]), str(d[6]), str(d[7]), str(d[8]), str(int(d[9])), str(int(d[10])),
                            str(int(d[11])), str(int(d[12])), str(int(d[13])), str(int(d[14])), str(int(d[15])), str(int(d[16])), str(int(d[17])),
                            str(int(d[18])), str(int(d[19])), str(int(d[20])), str(int(d[21])), str(d[22]) ))
                            connection.commit()
                        except:
                            continue
                print("Insertion query execution done.")
                
            except (Exception, psycopg2.Error) as error :
                print ("Error while connecting to PostgreSQL", error)
            finally:
            #closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    print("PostgreSQL connection is closed")

        def retrieve_data(query, creds, metric, partner_id):
            """
              Retrieve data by running query to postgre production database.

              Args:
                query (str) : query of the metric.
                creds (json) : json data contains credentials.

              Returns:
                record (list) : list of retrieved data.
              """

            try:
                connection = psycopg2.connect(user=creds['user'],
                                              password=creds['password'],
                                              host=creds['hostname'],
                                              port=creds['port'],
                                              database=creds['database'])

                cursor = connection.cursor()
                # Print PostgreSQL Connection properties
                print(connection.get_dsn_parameters(), "\n")

                # get the data by executing query
                print("Executing query...")
                cursor.execute(query)
                print("Query execution done.")

                #convert to dataframe and get the retrieved data shape
                record = pd.DataFrame(cursor.fetchall())

                #make a dataframe copy for data insertion to reporting db
                data_to_insert = record.copy()
                data_to_insert = data_to_insert.fillna(0)
                print(data_to_insert.head())
                data_to_insert[0] = data_to_insert[0].astype(str)
                data_to_insert = data_to_insert.values.tolist()
                print("Record's shape : ", record.shape)

                #convert record to list
                record = record.values.tolist()

            except (Exception, psycopg2.Error) as error:
                print("Error while connecting to PostgreSQL", error)
                return None

            finally:
                #closing database connection.
                if connection:
                    cursor.close()
                    connection.close()
                    print("PostgreSQL connection is closed")
                    insert_data(metric, data_to_insert, partner_id)
                    return record

        def dataflow_pipeline(partner_id, date_file, project,
                              job_name, region, metric, query, col_header, key_value):
            """
            Set up and run the pipeline using dataflow as a runner.
            Data output in the form of csv, contains metric data retrieved from query.
            Data output will be stored in GCS.

            Args:
              partner_id (str) : desired partner id.
              date_file (str) : desired date target.
              project (str) : GCP project name.
              jobName (str) : name for dataflow job.
              region (str) : GCP region.
              metric (str) : desired metric name.
              query (str) : metric query
              colHeader (str) : strings contains column header for the outpur file.
              keyValue (json) : json data contains creds for postgre db in retrieve_data function

            Returns:

            """
            options = PipelineOptions()
            options.view_as(StandardOptions).runner = 'DataflowRunner'
            google_cloud_options = options.view_as(GoogleCloudOptions)
            google_cloud_options.project = project
            google_cloud_options.job_name = job_name
            google_cloud_options.staging_location = 'gs://prixa-analytic/binaries'
            google_cloud_options.temp_location = 'gs://prixa-analytic/temp'
            google_cloud_options.region = region

            with beam.Pipeline(options=options) as pipe:

                try:
                    time_run = time.strftime("%Y%m%d%H%M")
                    print("Running pipeline job: ", job_name)
                    #pipeline
                    lines = (
                        pipe | 'get data'>>beam.Create(retrieve_data(query, key_value, metric, partner_id)) 
                        | 'process to suitable format' >> beam.ParDo(Process())
                        | 'out' >>  beam.io.WriteToText('gs://prixa-analytic/'+ partner_id +
                                                        '/' + metric + '-' + date_file.replace('-', '') + '-' + time_run,
                                                        file_name_suffix='.csv', header=col_header, num_shards=1))

                except:
                    print("Error while running pipeline")

        #get creds for db from consul
        client = consul.Consul(host='consul-consul-server.consul', port=8500, dc="primarydc")
        index, data = client.kv.get("dataflow/postgres")
        key_values = json.loads(data['Value'].decode('utf8').replace("'", '"'))

        #get column headers
        col_header_user = get_col_header("user_conversion")
        col_header_symp = get_col_header("symptoms")
        col_header_disease = get_col_header("diseases")
        col_header_age = get_col_header("age")
        col_header_gender = get_col_header("gender")
        col_header_rdt = get_col_header("RDT")


        #get queries
        query_user_conv = adjust_query(retrieve_query("user_conversion"), date_file, partner_id_req)
        query_symptoms = adjust_query(retrieve_query("symptoms"), date_file, partner_id_req)
        query_diseases = adjust_query(retrieve_query("diseases"), date_file, partner_id_req)
        query_age = adjust_query(retrieve_query("age"), date_file, partner_id_req)
        query_gender = adjust_query(retrieve_query("gender"), date_file, partner_id_req)
        query_rdt = adjust_query(retrieve_query("RDT"), date_file, partner_id_req)

        # running the pipelines
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-userconversion-' + date_file.replace('-', ''),
                          'us-central1', 'user_conversion', query_user_conv,
                          col_header_user, key_values)
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-symptoms-' + date_file.replace('-', ''),
                          'us-central1', 'symptoms', query_symptoms,
                          col_header_symp, key_values)
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-diseases-' + date_file.replace('-', ''),
                          'us-central1', 'diseases', query_diseases,
                          col_header_disease, key_values)
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-age-' + date_file.replace('-', ''),
                          'us-central1', 'age', query_age,
                          col_header_age, key_values)
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-gender-' + date_file.replace('-', ''),
                          'us-central1', 'gender', query_gender,
                          col_header_gender, key_values)
        dataflow_pipeline(partner_id_req, date_file, 'prixa-dev',
                          'dataflow-'+((partner.lower()).replace(' ', '')).replace('+', 'plus') +
                          '-rdt-' + date_file.replace('-', ''),
                          'us-central1', 'rdt', query_rdt,
                          col_header_rdt, key_values)

    #check if the requested partner is specified or all partners
    if single:
        #run pipeline for single partner
        run(partner, partner_id_req, date_file)

    else:
        data_partners = get_partners_data('all')

        #run pipeline for all partner
        for data_partner in data_partners:
            try:
                print("Running pipelines for ", data_partner[0])
                run(data_partner[0], data_partner[1], date_file)
            except:
                print("Error pipelines execution for ", data_partner[0])
                os.system("gcloud auth list")
                continue



if __name__ == '__main__':

    args = argumentIn()
    if args.date_file is None or args.partner is None:
        print("Please specify argument : -d [YYYY-MM-DD] -p [partner name]")

    elif args.partner == 'all':
        main(args.date_file, '', '', False)

    else:
        #get partner id
        result_status, partner_id_result = get_partners_data(args.partner)

        if result_status:
            print("Run pipeline")
            main(args.date_file, args.partner, partner_id_result, True)

        else:
            print("Pipeline canceled")
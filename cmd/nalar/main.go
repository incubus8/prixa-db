package main

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/grpc"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/sdk"
	siloamSDK "prixa.ai/prixa-db/pkg/sdk/siloam"
	"prixa.ai/prixa-db/pkg/user"
)

func getMessagesData() (result map[string]string) {
	pwd, _ := os.Getwd()
	jsonFile, err := os.Open(pwd + "/data/text.json") // #nosec
	if err != nil {
		logrus.WithError(err).Fatalln("error opening data/text.json file")
	}

	defer func() {
		_ = jsonFile.Close()
	}()

	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		logrus.WithError(err).Fatalln("cannot i/o read on data/text.json")
	}
	if err = json.Unmarshal(byteValue, &result); err != nil {
		logrus.WithError(err).Fatalln("cannot unmarshal on data/text.json")
	}
	return result
}

func init() {
	cfgManager, err := config.NewConsulManager(context.Background())
	if err != nil {
		logrus.Warnf("init error: %v", err)
		return
	}

	if err := cfgManager.Init("config"); err != nil {
		logrus.Fatalf("init error: %v", err)
	}
}

func main() {
	ctx := context.Background()

	redisClient := db.NewRedisClient(ctx)
	baymaxRedisClient := db.NewBaymaxRedisClient(ctx)

	c := core.New()

	rmq, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error when setup RabbitMQ")
	}

	myBot := bot.NewBot(c, getMessagesData(), rmq)

	meiliClient := sdk.NewMeilisearchClient()

	pgClient, err := sdk.SetupPostgreClient()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error when setup Postgres client")
	}

	mysqlClient, err := sdk.SetupMysqlClient()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error when setup MySQL client")
	}

	crispData := user.CrispData(sdk.NewCrispClient())
	siloamAPI := siloamSDK.NewMySiloamAPI(4, 3, 4)

	// App initialization
	app := &grpc.App{
		Ctx:           ctx,
		Core:          c,
		Bot:           myBot,
		Redis:         redisClient,
		Publisher:     rmq,
		MsClient:      meiliClient,
		PostgreClient: pgClient,
		CrispData:     crispData,
		SiloamAPI:     siloamAPI,
		MysqlClient:   mysqlClient,
		BaymaxRedis:   baymaxRedisClient,
	}

	app.OnStart().
		OnShutdown(func() {
			_ = rmq.Close()
			_ = redisClient.Close()
		})
}


## Configure systemctl Prixa data consumer service
- Create configuration {service-name}.service file on /etc/systemd/system/
- Add the service to systemd
  * systemctl enable {service-name}.service
- Activate the service
  * systemctl start {service-name}.service
- Check if systemd started it.  
  * systemctl status {service-name}.service

## Additional Notes
- repository prixa-db located in folder /opt/
- Run go build ./cmd/nalar-ingestion/main.go
- ExecStart specifies the full path and the arguments of the command to be executed to start the process.
- ensure the yaml file exist and the path location in accordance with the path that has been configured in the service file

# Nalar Diagnosis Engine

**Nalar Engine** to diagnose symptoms based on SOCRATES (Site, Onset, Characteristic, Radiation, Timing, Severity) algorithm.

## Overview

Prixa-DB runs entirely on Google Kubernetes Engine (GKE). There are 3 environments: dev, staging and production.
Each of these environments has their own Memory Storage instance with Redis installed.

## Prerequisites

- Internet connection
- Golang version 1.14 or later
- Python ver. 3.7
- Redis v4 or above
- RabbitMQ version 3.7.14 or later
- Meilisearch 0.14.0
- DGraph v20.03.1
- Consul 7.4
- Postgres 11
- NATS Streaming
- Insomnia

## Environments

### Running with .env

To run locally, create `.env` file in the root directory with this entry.

### Running with Consul

To run with Consul, you need to perform port forwarding from dev to your local.

## Port Forward Service

The port forward is the simplified approach to run backend service in your local.

### Redis

We recommend using default Redis port: `6379`.

```shell script
$ kubectl port-forward svc/keydb 6379:6379 -n default
```

If you need to test against your own partner, session, etc you may want to setup your own Redis. 

### RabbitMQ

We recommend using default Redis port: `5672`.

```shell script
$ kubectl port-forward svc/prixa-rabbitmq 5672:5672 -n default
```

If you need to check RabbitMQ management you can do:

```shell script
$ kubectl port-forward svc/prixa-rabbitmq-headless 15672:15672
```

Then, in your browser go to `localhost:15672`

### Meilisearch

The default port of Meilisearch is `7700`. 

```shell script
$ kubectl port-forward svc/meili-meilisearch 7700:9200 -n prixa
```

You may want to setup your own Meilisearch if you want to test data pipeline on local environment.

### NATS Streaming

The default for NATS streaming is `4222`.

```shell script
$ kubectl port-forward svc/nats-headless 4222:4222 -n default
```

You may want to set up your own NATS Streaming if you do local development. 

### Consul

Consul used to manage environment variables. After you start Nalar, the program will pull configuration from Consul, 
then set the environment variables. It will override. 

### Airtable

We used Airtable to control Diagnostic data and Hospitals (+ specialization) data. 

For access to Airtable, please ask Rheza.

### DGraph

Dgraph used to represent Diagnostic data.

To run DGraph UI (Dgraph Ratel):
```shell script
$ kubectl port-forward svc/dgraph-dgraph-ratel 8000:8000
```

To run DGraph Server (DGraph Alpha):
```shell script
$ kubectl port-forward svc/dgraph-dgraph-alpha 8080:8080
```

You may want to set up your own DGraph, if you try to develop data changes.

## Development

We use `Makefile` to manage dependencies. Please make sure you have pass linter and other checks, run `make check`.

## Starting

First run the engine by executing `make build`, then open the API docs inside `docs` directory, by using Insomnia, then create the following in order:
1. Obtain `superadmin` user JWT token from Login API
2. Create new Partner
3. Create new Partner Application

## Testing

### Unit Test

Run all unit test by executing `make test`

## Hot Issues :fire: :fire: :fire:

Protobuf is still using ver 1.0, couldn't be upgraded due to block on GRPC PR https://github.com/grpc/grpc-go/pull/3453

## License and Copyright

PT. Pintar Data Group (c) 2019-2020

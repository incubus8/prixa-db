import argparse
import logging
import os
import sys
from pylint.lint import Run

def standarizeFilepath(filename):
    """
    standarizeFilepath standarize filename for filepath,
    by adding slash "/" before the filename
    """
    return "/" + filename

def getArguments():
    """
    getArguments gets all the needed arguments when running the python code
    """
    logging.getLogger().setLevel(logging.INFO)

    parser = argparse.ArgumentParser(prog="LINT")

    parser.add_argument('-p',
                        '--path',
                        help='path to directory you want to run pylint | '
                             'Default: %(default)s | '
                             'Type: %(type)s ',
                        default='./cmd/partner-dataflow',
                        type=str)

    parser.add_argument('-t',
                        '--threshold',
                        help='score threshold to fail pylint runner | '
                             'Default: %(default)s | '
                             'Type: %(type)s ',
                        default=7,
                        type=float)
    return parser.parse_args()

def main():
    """
    main function
    """
    args = getArguments()
    path = str(args.path)
    threshold = float(args.threshold)

    logging.info('PyLint Starting | '
                 'Path: {} | '
                 'Threshold: {} '.format(path, threshold))

    filespath = []
    for filename in os.listdir(path):
        if ".py" in filename:
            filepath = path + standarizeFilepath(filename)
            command = "autopep8 --in-place --aggressive " + filepath
            os.system(command)
            filespath.append(filepath)

    results = Run(filespath, do_exit=False)

    final_score = results.linter.stats['global_note']

    if final_score < threshold:

        message = ('PyLint Failed | '
                   'Score: {} | '
                   'Threshold: {} '.format(final_score, threshold))

        logging.error(message)
        raise Exception(message)
    else:
        message = ('PyLint Passed | '
                   'Score: {} | '
                   'Threshold: {} '.format(final_score, threshold))

        logging.info(message)

        sys.exit()

if __name__ == "__main__":
    main()
    
package queuelayer

import (
	"os"

	"prixa.ai/prixa-db/pkg/store"
)

// QueueLabtestStore is
type QueueLabtestStore struct {
	store.LabtestStore
	rootStore *QueueStore
}

// SaveCouponToDB is
func (s *QueueLabtestStore) SaveCouponToDB(payload interface{}) {
	publishKeySurvey, _ := os.LookupEnv("RABBITMQ_LABTEST_COUPON_QUEUE")
	s.rootStore.publisher.Publish(payload, publishKeySurvey, "")
}

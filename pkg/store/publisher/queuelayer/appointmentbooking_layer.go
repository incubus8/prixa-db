package queuelayer

import (
	"os"

	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/store"
)

// QueueAppointmentBookingStore is
type QueueAppointmentBookingStore struct {
	store.AppointmentBookingStore
	rootStore *QueueStore
}

// Save is
func (s *QueueAppointmentBookingStore) Save(payload interface{}) {
	publishKey, _ := os.LookupEnv("RABBITMQ_TELEMEDICINE_APPOINTMENT_QUEUE")
	s.rootStore.publisher.Publish(payload, publishKey, "")
}

// CancelAppointmentBooking tracks appointment booking activity
func (s *QueueAppointmentBookingStore) CancelAppointmentBooking(data *store.Param) {
	s.rootStore.publish(messaging.QueueAppointmentBookingEvent, data)
}

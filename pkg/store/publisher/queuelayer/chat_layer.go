package queuelayer

import (
	"os"
	"strings"

	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

// QueueChatRequestStore is
type QueueChatRequestStore struct {
	store.ChatRequestStore
	rootStore *QueueStore
}

// PublishChatRequest publish chat request to publisher
func (s *QueueChatRequestStore) PublishChatRequest(payload *model.ChatRequest) {
	exchangeName := os.Getenv("RABBITMQ_TELEMEDICINE_ALLOCATION_EXCHANGE")
	routingKey := "telemed." + payload.Partner + "." + payload.Speciality
	s.rootStore.publisher.Publish(payload, strings.ToLower(routingKey), exchangeName)
}

// PublishResolvedChatRequest publish resolved chat to publisher
func (s *QueueChatRequestStore) PublishResolvedChatRequest(payload *model.ResolveChatRequest) {
	s.rootStore.publisher.Publish(payload, os.Getenv("RABBITMQ_TELEMEDICINE_RESOLVE_CHAT_ROUTINGKEY"), "")
}

// PublishSetDoctorStatusRequest publish resolved chat to publisher
func (s *QueueChatRequestStore) PublishSetDoctorStatusRequest(payload *model.SetDoctorStatusRequest) {
	s.rootStore.publisher.Publish(payload, os.Getenv("RABBITMQ_TELEMEDICINE_SET_DOCTOR_STATUS_ROUTINGKEY"), "")
}

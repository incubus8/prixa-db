package queuelayer

import (
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/store"
)

// QueueAnalyticStore is
type QueueAnalyticStore struct {
	store.AnalyticStore
	rootStore *QueueStore
}

// TrackEvent tracks event from Frontend
func (s *QueueAnalyticStore) TrackEvent(data *store.Param) {
	s.rootStore.publish(messaging.QueueTrackEvent, data)
}

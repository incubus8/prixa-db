package queuelayer

import (
	"os"

	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/store"
)

// QueueDiagnosticStore is
type QueueDiagnosticStore struct {
	store.DiagnosticStore
	rootStore *QueueStore
}

// SaveSurvey saves diagnostic survey
func (s *QueueDiagnosticStore) SaveSurvey(payload interface{}) {
	publishKeySurvey, _ := os.LookupEnv("RABBITMQ_FEEDBACK_QUEUE")
	s.rootStore.publisher.Publish(payload, publishKeySurvey, "")
}

// SaveFeedbackQuestion saves feedback question
func (s *QueueDiagnosticStore) SaveFeedbackQuestion(payload interface{}) {
	publishKeySurvey, _ := os.LookupEnv("RABBITMQ_FEEDBACK_QUESTION_QUEUE")
	s.rootStore.publisher.Publish(payload, publishKeySurvey, "")
}

// SaveCovid19Form save covid19 form submission
func (s *QueueDiagnosticStore) SaveCovid19Form(data *store.Param) {
	data.EventType = store.SuccessEvent
	s.rootStore.publish(messaging.QueueCovidForm, data)
}

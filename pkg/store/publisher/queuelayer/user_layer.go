package queuelayer

import (
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"
)

// QueueUserStore is
type QueueUserStore struct {
	store.UserStore
	rootStore *QueueStore
}

// TrackUserActivity is
func (s *QueueUserStore) TrackUserActivity(data *store.Param) {
	s.rootStore.publish(messaging.QueueUserEvent, data)
}

// SaveUserPreconditions saves user preconditions data
func (s *QueueUserStore) SaveUserPreconditions(data *store.Param) {
	data.EventType = store.SuccessEvent
	if core.IsForCovid19() {
		s.rootStore.publish(messaging.CovidPrefix+"."+messaging.QueueUserPreconditions, data)
	}
	s.rootStore.publish(messaging.QueueUserPreconditions, data)
}

// SaveAssessmentResult saves the diagnosis result from engine to elasticsearch via NATS
func (s *QueueUserStore) SaveAssessmentResult(properties userprofile.ProfileProperties) {
	var chiefComplaint string
	for _, symptom := range properties.DiagnosisResult.Symptoms {
		if symptom.Chief {
			chiefComplaint = symptom.SymptomName
		}
	}

	var gender string
	for _, profile := range properties.DiagnosisResult.Profiles {
		if profile.Type == bot.ProfileTypeGender {
			gender = profile.NameIndo
		}
	}
	historyData := &userGRPC.AssessmentHistory{
		Email: properties.Email,
		Datetime: &timestamp.Timestamp{
			Seconds: time.Now().Unix(),
		},
		ChiefComplaint: chiefComplaint,
		Name:           properties.Name,
		Age:            properties.DiagnosisResult.UserDetails.AgeYear,
		Gender:         gender,
	}
	if core.IsForCovid19() {
		s.rootStore.publisher.Publish(historyData, messaging.CovidPrefix+"."+messaging.QueueUserAssessmentHistory, "")
	} else {
		s.rootStore.publisher.Publish(historyData, messaging.QueueUserAssessmentHistory, "")
	}
}

package queuelayer

import (
	"os"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// QueueStore is
type QueueStore struct {
	store.Store
	publisher          messaging.Publisher
	diagnostic         *QueueDiagnosticStore
	appointmentBooking *QueueAppointmentBookingStore
	chatRequest        *QueueChatRequestStore
	labtest            *QueueLabtestStore
	user               *QueueUserStore
	analytic           *QueueAnalyticStore
}

// NewQueueStore is
func NewQueueStore(baseStore store.Store, publisher messaging.Publisher) store.Store {
	queueStore := QueueStore{
		Store:     baseStore,
		publisher: publisher,
	}
	queueStore.diagnostic = &QueueDiagnosticStore{DiagnosticStore: baseStore.Diagnostic(), rootStore: &queueStore}
	queueStore.appointmentBooking = &QueueAppointmentBookingStore{AppointmentBookingStore: baseStore.AppointmentBooking(), rootStore: &queueStore}
	queueStore.chatRequest = &QueueChatRequestStore{ChatRequestStore: baseStore.ChatRequest(), rootStore: &queueStore}
	queueStore.labtest = &QueueLabtestStore{LabtestStore: baseStore.Labtest(), rootStore: &queueStore}
	queueStore.user = &QueueUserStore{UserStore: baseStore.User(), rootStore: &queueStore}
	queueStore.analytic = &QueueAnalyticStore{AnalyticStore: baseStore.Analytic(), rootStore: &queueStore}

	return queueStore
}

func (s *QueueStore) publish(queueName string, param *store.Param) {
	payload := setupPayload(param)
	s.publisher.Publish(payload, queueName, "")
}

func setupPayload(data *store.Param) *analyticGRPC.EventLog {
	incomingMds := metautils.ExtractIncoming(data.Ctx)
	payload := &analyticGRPC.EventLog{
		LogID: util.GetRandomID(),

		Environment: os.Getenv("GO_ENV"),

		PrixaAPIKey:    incomingMds.Get("x-prixa-api-key"),
		PartnerID:      incomingMds.Get("x-partner-id"),
		PartnerName:    incomingMds.Get("partnerName"),
		PartnerAppID:   incomingMds.Get("x-partner-app-id"),
		PartnerAppName: incomingMds.Get("partnerAppName"),
		Ip:             incomingMds.Get("x-forwarded-for"),
		UserAgent:      incomingMds.Get("grpcgateway-user-agent"),
		Hostname:       incomingMds.Get("x-forwarded-host"),
		Email:          incomingMds.Get("email"),
		UserName:       incomingMds.Get("name"),

		Type:        data.EventType,
		Description: data.Description,
		Name:        data.EventName,
		SentAt:      data.SentAt,
		Context:     data.Context,
		Metadata:    data.Metadata,

		Date: &timestamp.Timestamp{Seconds: time.Now().Unix()},
	}

	if payload.Email == "" {
		payload.Email = data.Email
	}

	if payload.UserName == "" {
		payload.UserName = data.UserName
	}

	return payload
}

// Diagnostic is
func (s QueueStore) Diagnostic() store.DiagnosticStore {
	return s.diagnostic
}

// AppointmentBooking is
func (s QueueStore) AppointmentBooking() store.AppointmentBookingStore {
	return s.appointmentBooking
}

// ChatRequest is
func (s QueueStore) ChatRequest() store.ChatRequestStore {
	return s.chatRequest
}

// Labtest is
func (s QueueStore) Labtest() store.LabtestStore {
	return s.labtest
}

// User is
func (s QueueStore) User() store.UserStore {
	return s.user
}

// Analytic is
func (s QueueStore) Analytic() store.AnalyticStore {
	return s.analytic
}

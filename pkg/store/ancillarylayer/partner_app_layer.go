package ancillarylayer

import (
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"

	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/store"
)

// AncillaryPartnerAppStore is
type AncillaryPartnerAppStore struct {
	store.PartnerAppStore

	rootStore *AncillaryStore
}

var appointmentBookingMethod = map[string]bool{
	"/prixa.telemedicine.v1.TelemedicineService/GetAreaData":                  true,
	"/prixa.telemedicine.v1.TelemedicineService/GetAreasData":                 true,
	"/prixa.telemedicine.v1.TelemedicineService/GetSpecialityData":            true,
	"/prixa.telemedicine.v1.TelemedicineService/GetSpecialitiesData":          true,
	"/prixa.telemedicine.v1.TelemedicineService/GetHospitalData":              true,
	"/prixa.telemedicine.v1.TelemedicineService/GetHospitalsData":             true,
	"/prixa.telemedicine.v1.TelemedicineService/GetHospitalsDataByCoordinate": true,
	"/prixa.telemedicine.v1.TelemedicineService/GetDoctorData":                true,
	"/prixa.telemedicine.v1.TelemedicineService/GetDoctorsData":               true,
	"/prixa.telemedicine.v1.TelemedicineService/GetDoctorSchedulesData":       true,
	"/prixa.telemedicine.v1.TelemedicineService/GetDoctorTimeSlotsData":       true,
	"/prixa.telemedicine.v1.TelemedicineService/PostAppointmentBookingData":   true,
	// "/prixa.telemedicine.v1.TelemedicineService/GetAppointmentBookingData":    true,
	// "/prixa.telemedicine.v1.TelemedicineService/GetAppointmentBookingsData":   true,
	"/prixa.telemedicine.v1.TelemedicineService/GetValidPaymentMethodsData":   true,
	"/prixa.telemedicine.v1.TelemedicineService/CancelAppointmentBookingData": true,
}

var telemedicineMethod = map[string]bool{
	"/prixa.telemedicine.v1.TelemedicineService/InitConversation": true,
}

// IsValidAncillary is
func (s *AncillaryPartnerAppStore) IsValidAncillary() bool {
	incomingMds := metautils.ExtractIncoming(s.rootStore.Store.Context())
	pID := incomingMds.Get("x-partner-id")
	pAppID := incomingMds.Get("x-partner-app-id")
	method := incomingMds.Get("grpcMethod")

	ancillary := s.GetAncillaryControl(pID, pAppID)

	if appointmentBookingMethod[method] && !ancillary.AppointmentBooking {
		return false
	}
	if telemedicineMethod[method] && !ancillary.Telemedicine {
		return false
	}

	return true
}

// GetAncillaryControl is
func (s *AncillaryPartnerAppStore) GetAncillaryControl(partnerID, appID string) db.AncillaryControl {
	return s.rootStore.ancillary.ByPartnerIDAndAppID(partnerID, appID)
}

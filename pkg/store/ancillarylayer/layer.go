package ancillarylayer

import (
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/store"
)

// AncillaryStore is
type AncillaryStore struct {
	store.Store

	ancillary  db.AncillaryControlTable
	partnerApp *AncillaryPartnerAppStore
}

// NewAncillaryStore is
func NewAncillaryStore(baseStore store.Store, ancillary db.AncillaryControlTable) store.Store {
	as := AncillaryStore{
		Store:     baseStore,
		ancillary: ancillary,
	}
	as.partnerApp = &AncillaryPartnerAppStore{PartnerAppStore: baseStore.PartnerApp(), rootStore: &as}

	return as
}

// PartnerApp is
func (s AncillaryStore) PartnerApp() store.PartnerAppStore {
	return s.partnerApp
}

package postgreslayer

import (
	"context"
	"encoding/json"
	"errors"
	"math/rand"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/sdk/gdbc"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// PostgresChatResultStore is
type PostgresChatResultStore struct {
	store.ChatResultStore
	rootStore *PostgresStore
}

// CreateChatResult is
func (s *PostgresChatResultStore) CreateChatResult(id string, chatResult *model.ChatResult) error {
	sqlTx, err := s.rootStore.sqlDB.TxBegin()
	if err != nil {
		return err
	}
	return sqlTx.TxEnd(func() error {
		return createChatResult(s.rootStore.Context(), sqlTx, chatResult)
	})
}

// ProcessingChatRequest is
func (s *PostgresChatResultStore) ProcessingChatRequest(chatRequest *model.ChatRequest) error {
	sqlTx, err := s.rootStore.sqlDB.TxBegin()
	if err != nil {
		return err
	}
	return sqlTx.TxEnd(func() error {
		return startProcessingChatRequest(s.rootStore.Context(), sqlTx, chatRequest, s.rootStore.Store)
	})
}

func createChatResult(ctx context.Context, sqlTx gdbc.SQLGdbc, chatResult *model.ChatResult) error {
	marshalDataDoctor, err := json.Marshal(chatResult.Doctor)
	if err != nil {
		return err
	}
	_, err = sqlTx.ExecContext(ctx, `INSERT INTO telemedicine_sessionchat(
		sessionid, doctor, userid, status, message, timecreated, lastupdated)
		VALUES ($1, $2, $3, $4, $5, $6, $7)`, chatResult.SessionID, marshalDataDoctor, chatResult.User, chatResult.Status, chatResult.Message, chatResult.CreatedAt, time.Now().Format("2006-01-02T15:04:05.000Z"))
	if err != nil {
		return err
	}
	return nil
}

func startProcessingChatRequest(ctx context.Context, sqlTx gdbc.SQLGdbc, chatRequest *model.ChatRequest, store store.Store) error {
	doctors, err := findAllAvailableDoctor(ctx, sqlTx, chatRequest, true)

	message := ""
	status := "APPROVED"
	if err != nil {
		message = err.Error()
		status = "REJECTED"

	}

	if len(doctors) == 0 {
		status = "REJECTED"
		message = "no doctors available"
	}

	var doctorsWithMiniumumPatient []*model.DoctorChat
	limit := 9999
	for _, doctor := range doctors {
		if doctor.PatientHandled < limit {
			limit = doctor.PatientHandled
			doctorsWithMiniumumPatient = nil
			doctorsWithMiniumumPatient = append(doctorsWithMiniumumPatient, doctor)
		} else if doctor.PatientHandled == limit {
			doctorsWithMiniumumPatient = append(doctorsWithMiniumumPatient, doctor)
		}
	}

	rand.Seed(time.Now().UTC().UnixNano())
	selectedIndex := rand.Intn(len(doctorsWithMiniumumPatient))
	doctor := doctorsWithMiniumumPatient[selectedIndex]

	chatResult := &model.ChatResult{
		SessionID:   chatRequest.SessionID,
		Doctor:      doctor,
		CreatedAt:   chatRequest.CreatedAt,
		Partner:     chatRequest.Partner,
		Speciality:  chatRequest.Speciality,
		User:        chatRequest.User,
		LastUpdated: time.Now().Format(util.LayoutFormatDateTime),
		Message:     message,
		Status:      status,
	}
	err = createChatResult(ctx, sqlTx, chatResult)
	if err != nil {
		return err
	}
	if doctor != nil {
		err = incrementDoctorAllocation(ctx, sqlTx, doctor.ID)
		if err != nil {
			return err
		}
	}

	err = store.ChatResult().CreateChatResult(chatRequest.SessionID, chatResult)
	if err != nil {
		return err
	}

	return nil
}

func getPatientHandledLimit() int {
	defaultLimit := 2
	limitEnv := os.Getenv("TELEMEDICINE_PATIENT_HANDLED_LIMIT")
	if limitEnv == "" {
		return defaultLimit
	}

	limit, err := strconv.ParseInt(limitEnv, 10, 64)
	if err != nil {
		logrus.WithField("limit", limitEnv).WithError(err).Warningln()
		return defaultLimit
	}
	return int(limit)
}

// ProcessingResolveChatRequest is
func (s *PostgresChatResultStore) ProcessingResolveChatRequest(request *model.ResolveChatRequest) error {
	sqlTx, err := s.rootStore.sqlDB.TxBegin()
	if err != nil {
		return err
	}
	return sqlTx.TxEnd(func() error {
		return startProcessingResolveChatRequest(s.rootStore.Context(), sqlTx, request)
	})
}

func startProcessingResolveChatRequest(ctx context.Context, sqlTx gdbc.SQLGdbc, request *model.ResolveChatRequest) error {
	chat, err := findChatResultByID(ctx, sqlTx, request.SessionID, true)
	if err != nil {
		return err
	}

	if chat.Status == "RESOLVED" {
		logrus.WithContext(ctx).Warningln("chat " + chat.SessionID + " already resolved")
		return errors.New("chat " + chat.SessionID + " already resolved")
	}

	if chat.Status == "REJECTED" {
		logrus.WithContext(ctx).Warningln("chat " + chat.SessionID + " is rejected and can't be resolved")
		return errors.New("chat " + chat.SessionID + " is rejected and can't be resolved")
	}

	err = setChatResultResolved(ctx, sqlTx, request.SessionID, request.Information)
	if err != nil {
		return err
	}

	if chat.Doctor != nil {
		err = decrementDoctorAllocation(ctx, sqlTx, chat.Doctor.ID)
		if err != nil {
			return err
		}
	}

	return nil
}

func setChatResultResolved(ctx context.Context, sqlTx gdbc.SQLGdbc, sessionID string, information string) error {
	_, err := sqlTx.ExecContext(ctx, `Update telemedicine_sessionchat set status = 'RESOLVED', message = $1, lastUpdated = now()
	where sessionid = $2`, information, sessionID)
	return err
}

func findChatResultByID(ctx context.Context, sqlTx gdbc.SQLGdbc, sessionID string, locking bool) (*model.ChatResult, error) {
	query := "SELECT sessionid, doctor, userid, status, message FROM telemedicine_sessionchat where sessionid = $1"
	if locking {
		query += "FOR UPDATE"
	}

	var chat model.ChatResult
	var doctor json.RawMessage
	err := sqlTx.QueryRowContext(ctx, query, sessionID).Scan(&chat.SessionID, &doctor, &chat.User, &chat.Status, &chat.Message)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(doctor, &chat.Doctor)
	if err != nil {
		return nil, errors.New("failed to unmarshal doctor data in chat result")
	}
	return &chat, nil
}

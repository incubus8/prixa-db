package postgreslayer

import (
	"database/sql"

	"prixa.ai/prixa-db/pkg/sdk/gdbc"
	"prixa.ai/prixa-db/pkg/store"
)

// PostgresStore is
type PostgresStore struct {
	store.Store
	sqlDB gdbc.SQLGdbc

	chatResult       *PostgresChatResultStore
	doctorAllocation *PostgresDoctorAllocationStore
	labtest          *PostgresLabtestStore
	user             *PostgresUserStore
}

// NewPostgresStore is
func NewPostgresStore(baseStore store.Store, sql *sql.DB) store.Store {
	postgresStore := PostgresStore{
		Store: baseStore,
		sqlDB: &gdbc.SQLDBTx{DB: sql},
	}
	postgresStore.chatResult = &PostgresChatResultStore{rootStore: &postgresStore, ChatResultStore: baseStore.ChatResult()}
	postgresStore.doctorAllocation = &PostgresDoctorAllocationStore{rootStore: &postgresStore, DoctorAllocationStore: baseStore.DoctorAllocation()}
	postgresStore.labtest = &PostgresLabtestStore{rootStore: &postgresStore, LabtestStore: baseStore.Labtest()}
	postgresStore.user = &PostgresUserStore{rootStore: &postgresStore, UserStore: baseStore.User()}

	return postgresStore
}

// ChatResult is
func (s PostgresStore) ChatResult() store.ChatResultStore {
	return s.chatResult
}

// DoctorAllocation is
func (s PostgresStore) DoctorAllocation() store.DoctorAllocationStore {
	return s.doctorAllocation
}

// Labtest is
func (s PostgresStore) Labtest() store.LabtestStore {
	return s.labtest
}

// User is
func (s PostgresStore) User() store.UserStore {
	return s.user
}

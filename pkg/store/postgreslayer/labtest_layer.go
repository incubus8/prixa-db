package postgreslayer

import (
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

// PostgresLabtestStore is
type PostgresLabtestStore struct {
	store.LabtestStore
	rootStore *PostgresStore
}

// GetCouponFromDB is
func (s *PostgresLabtestStore) GetCouponFromDB(email string) (*model.CouponData, error) {
	query := "select * from triasse_coupon_codes where email=$1"
	res, err := s.rootStore.sqlDB.Query(query, email)
	if err != nil {
		return nil, err
	}

	var data model.CouponData
	err = res.Scan(&data.Code, &data.Email, &data.CreatedAt, &data.ExpiredAt)
	if err != nil {
		return nil, err
	}

	return &data, nil
}

package postgreslayer

import (
	"database/sql"
	"encoding/json"

	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"prixa.ai/prixa-db/pkg/store"
)

// PostgresUserStore is
type PostgresUserStore struct {
	rootStore *PostgresStore
	store.UserStore
}

// GetUserAppointmentBookingData is
func (s *PostgresUserStore) GetUserAppointmentBookingData(email, startDate, endDate string) ([]*telemedicineGRPC.AppointmentBookingData, error) {
	query := "select * from telemedicine_appointment where email=$1"
	if (startDate != "") && (endDate != "") {
		query = "select * from telemedicine_appointment where email=$1 and time_created between $2 and $3"
	}

	var res *sql.Rows
	var err error
	if (startDate != "") && (endDate != "") {
		res, err = s.rootStore.sqlDB.Query(query, email, startDate, endDate)
		if err != nil {
			return nil, err
		}
	} else {
		res, err = s.rootStore.sqlDB.Query(query, email)
		if err != nil {
			return nil, err
		}
	}
	defer func() {
		_ = res.Close()
	}()

	data := []*telemedicineGRPC.AppointmentBookingData{}
	for res.Next() {
		var (
			appointmentBooking telemedicineGRPC.AppointmentBookingData
			appointmentID      string
			timestamp          string
			doctor             string
			hospital           string
			doctorGRPC         telemedicineGRPC.DoctorData
			hospitalGRPC       telemedicineGRPC.HospitalData
		)
		err = res.Scan(
			&appointmentBooking.Id,
			&appointmentID,
			&appointmentBooking.Name,
			&appointmentBooking.Nik,
			&appointmentBooking.Email,
			&appointmentBooking.PhoneNumber,
			&appointmentBooking.PaymentMethod,
			&appointmentBooking.AppointmentDate,
			&appointmentBooking.AppointmentTime,
			&doctor,
			&hospital,
			&appointmentBooking.BookingStatus,
			&appointmentBooking.BirthDate,
			&appointmentBooking.ContactId,
			&appointmentBooking.ScheduleId,
			&appointmentBooking.ContactName,
			&appointmentBooking.CreatedTime,
			&timestamp,
		)
		if err != nil {
			return nil, err
		}

		err = json.Unmarshal([]byte(doctor), &doctorGRPC)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal([]byte(hospital), &hospitalGRPC)
		if err != nil {
			return nil, err
		}
		appointmentBooking.Hospital = &hospitalGRPC
		appointmentBooking.Doctor = &doctorGRPC

		data = append(data, &appointmentBooking)
	}

	return data, nil
}

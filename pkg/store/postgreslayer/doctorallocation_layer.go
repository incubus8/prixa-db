package postgreslayer

import (
	"context"
	"errors"

	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/sdk/gdbc"
	"prixa.ai/prixa-db/pkg/store"
)

// PostgresDoctorAllocationStore is
type PostgresDoctorAllocationStore struct {
	store.DoctorAllocationStore
	rootStore *PostgresStore
}

// SetDoctorStatus is
func (s *PostgresDoctorAllocationStore) SetDoctorStatus(request *model.SetDoctorStatusRequest) error {
	sqlTx, err := s.rootStore.sqlDB.TxBegin()
	if err != nil {
		return err
	}
	return sqlTx.TxEnd(func() error {
		return processSetDoctorStatus(s.rootStore.Context(), sqlTx, request)
	})
}

func processSetDoctorStatus(ctx context.Context, sqlTx gdbc.SQLGdbc, request *model.SetDoctorStatusRequest) error {
	if request.Status != "ONLINE" && request.Status != "OFFLINE" {
		logrus.WithContext(ctx).WithField("status", request.Status).Errorln("invalid doctor status, only allowed value are ONLINE or OFFLINE")
		return errors.New("invalid doctor status, only allowed value are ONLINE or OFFLINE")
	}

	doctor, err := findDoctorByID(ctx, sqlTx, request.DoctorID, true)
	if err != nil {
		return err
	}

	if doctor.Status == request.Status {
		logrus.WithContext(ctx).WithFields(logrus.Fields{
			"doctorID": request.DoctorID,
			"status":   request.Status,
		}).Errorln("doctor " + request.DoctorID + " is already " + request.Status)
		return errors.New("doctor " + request.DoctorID + " is already " + request.Status)
	}
	err = updateDoctorStatus(ctx, sqlTx, request.DoctorID, request.Status)
	if err != nil {
		return err
	}

	return nil
}

func updateDoctorStatus(ctx context.Context, sqlTx gdbc.SQLGdbc, doctorID string, status string) error {
	_, err := sqlTx.ExecContext(ctx, `Update telemedicine_doctor set status = $1, lastUpdated = now()
	where doctorid = $2`, status, doctorID)
	return err
}

func findDoctorByID(ctx context.Context, sqlTx gdbc.SQLGdbc, doctorID string, locking bool) (*model.DoctorChat, error) {
	query := "SELECT doctorid, doctorname, partner, specialityid, specialityname, status, patienthandled, rating, totalpatient FROM telemedicine_doctor where doctorID = $1 "
	if locking {
		query += "FOR UPDATE"
	}

	var doctor model.DoctorChat
	err := sqlTx.QueryRowContext(ctx, query, doctorID).Scan(&doctorID, &doctor.Name, &doctor.Partner, &doctor.SpecialityID, &doctor.SpecialityName, &doctor.Status, &doctor.PatientHandled, &doctor.Rating, &doctor.Totalpatient)
	if err != nil {
		return nil, err
	}
	return &doctor, nil
}

func findAllAvailableDoctor(ctx context.Context, sqlTx gdbc.SQLGdbc, chatRequest *model.ChatRequest, locking bool) ([]*model.DoctorChat, error) {
	patientHandledLimit := getPatientHandledLimit()
	query := "SELECT doctorid, doctorname, partner, specialityid, specialityname, patientHandled, rating from telemedicine_doctor where status = 'ONLINE' and patientHandled < $1 and partner = $2 and specialityId = $3 "
	if locking {
		query += "FOR UPDATE"
	}

	rows, err := sqlTx.QueryContext(ctx, query, patientHandledLimit, chatRequest.Partner, chatRequest.Speciality)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			_ = rows.Close()
		}
	}()

	doctors := make([]*model.DoctorChat, 0)
	for rows.Next() {
		var doctor model.DoctorChat
		err = rows.Scan(&doctor.ID, &doctor.Name, &doctor.Partner, &doctor.SpecialityID, &doctor.SpecialityName, &doctor.PatientHandled, &doctor.Rating)
		if err != nil {
			return nil, err
		}
		doctors = append(doctors, &doctor)
	}
	return doctors, nil
}

func incrementDoctorAllocation(ctx context.Context, sqlTx gdbc.SQLGdbc, doctorID string) error {
	_, err := sqlTx.ExecContext(ctx, `Update telemedicine_doctor set patienthandled = patienthandled + 1, totalpatient = totalpatient + 1, lastUpdated = now()
	where doctorId = $1`, doctorID)
	return err
}

func decrementDoctorAllocation(ctx context.Context, sqlTx gdbc.SQLGdbc, doctorID string) error {
	_, err := sqlTx.ExecContext(ctx, `Update telemedicine_doctor set patienthandled = patienthandled - 1, lastUpdated = now()
	where doctorId = $1`, doctorID)
	return err
}

package mysqllayer

import (
	"context"
	"database/sql"
	"errors"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/model/baymax"
	"prixa.ai/prixa-db/pkg/sdk/gdbc"
	"prixa.ai/prixa-db/pkg/store"
)

// MysqlBaymaxStore is
type MysqlBaymaxStore struct {
	rootStore *MysqlStore
	store.BaymaxStore
}

// IsDoctorExistInClient is
func (s *MysqlBaymaxStore) IsDoctorExistInClient(doctorID string, clientID string) (bool, error) {
	var id string
	query := `Select id from client_doctor where client_id = ? and doctor_id = ?`
	err := s.rootStore.sqlDB.QueryRow(query, clientID, doctorID).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// IsExternalUserExistInClient is
func (s *MysqlBaymaxStore) IsExternalUserExistInClient(userID string, clientID string) (bool, error) {
	query := `Select id from external_users where client_id = ? and id = ?`
	err := s.rootStore.sqlDB.QueryRow(query, clientID, userID).Scan(&userID)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// GetConversationData is
func (s *MysqlBaymaxStore) GetConversationData(conversationID string) (*baymax.Conversation, error) {
	query := `Select A.id, A.client_id, A.inbox_id, A.transaction_id, B.doctor_id, B.patient_id from conversations A
	LEFT JOIN transactions B on A.transaction_id = B.id
	 where A.id = ? and deleted_at is null`
	res := s.rootStore.sqlDB.QueryRow(query, conversationID)

	var data baymax.Conversation
	err := res.Scan(&data.ID, &data.ClientID, &data.InboxID, &data.TransactionID, &data.DoctorID, &data.PatientID)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("conversation id " + conversationID + " is not found")
		}
		return nil, err
	}
	return &data, nil
}

// GetConversationMessages is
func (s *MysqlBaymaxStore) GetConversationMessages(conversationID string, offset string) ([]*baymax.MessageData, error) {
	var createdAt string
	if offset != "" {
		msgOffset, err := s.getMessageData(offset)
		if err != nil {
			return []*baymax.MessageData{}, nil
		}
		createdAt = msgOffset.CreatedAt
	}

	query := `Select id, content, client_id, inbox_id, conversation_id, message_type, created_at, updated_at, private, doctor_id, status, 
	content_type, patient_id, message_subtype
	from messages where conversation_id = ? and deleted_at is null
	order by created_at desc limit 20`

	if createdAt != "" {
		query += " and B.created_at < '" + createdAt + "'"
	}

	rows, err := s.rootStore.sqlDB.Query(query, conversationID)
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	if err != nil {
		return nil, err
	}

	messages := make([]*baymax.MessageData, 0)
	for rows.Next() {
		var message baymax.MessageData
		err = rows.Scan(&message.ID, &message.Content, &message.ClientID, &message.InboxID, &message.ConversationID, &message.MessageType,
			&message.CreatedAt, &message.UpdatedAt, &message.Private, &message.DoctorID, &message.Status, &message.ContentType, &message.PatientID, &message.MessageSubType)

		if err != nil {
			return nil, err
		}
		messages = append(messages, &message)
	}

	reverseMessages := make([]*baymax.MessageData, 0)
	for i := len(messages) - 1; i >= 0; i-- {
		reverseMessages = append(reverseMessages, messages[i])
	}

	return reverseMessages, nil
}

// GetListOfPermission is
func (s *MysqlBaymaxStore) GetListOfPermission(userID string) ([]*baymax.Permission, error) {
	query := `Select model_id, C.id permissionID, C.name permissionName, C.guard_name from model_has_roles A 
	JOIN role_permission B on A.role_id = B.role_id
	JOIN permissions C on B.permission_id = C.id
	where model_id = ? `

	rows, err := s.rootStore.sqlDB.Query(query, userID)
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	if err != nil {
		return nil, err
	}

	permissions := make([]*baymax.Permission, 0)
	for rows.Next() {
		var permission baymax.Permission
		err = rows.Scan(&permission.ModelID, &permission.PermissionID, &permission.PermissionName, &permission.GuardName)

		if err != nil {
			return nil, err
		}
		permissions = append(permissions, &permission)
	}
	return permissions, nil
}

// ValidateConversationTransactionDoctor is
func (s *MysqlBaymaxStore) ValidateConversationTransactionDoctor(ctx context.Context, conversationID string) (*baymax.Conversation, *baymax.Transaction, *baymax.Doctor, error) {
	conversation, transaction, err := getConversationAndTransactionFromConversationID(ctx, s.rootStore.sqlDB, conversationID)
	if err != nil {
		return nil, nil, nil, err
	}

	meta := metautils.ExtractIncoming(ctx)
	userID := meta.Get("id")
	if transaction.DoctorID != userID {
		return nil, nil, nil, errors.New("user not allowed to send message to this conversation")
	}

	doctor, err := getDoctorFromDoctorID(ctx, s.rootStore.sqlDB, userID)
	if err != nil {
		return nil, nil, nil, err
	}

	if doctor.DoctorAvailability != "online" {
		return nil, nil, nil, errors.New("doctor is not online")
	}

	return conversation, transaction, doctor, nil
}

// ParseWebsiteTokenCwConversation is
func (s *MysqlBaymaxStore) ParseWebsiteTokenCwConversation(ctx context.Context, websiteToken string, cwConversation string) (*baymax.Conversation, *baymax.Transaction, error) {
	authTokenParam, err := parseAndVerifyCwConversation(cwConversation)
	if err != nil {
		return nil, nil, err
	}

	inboxID, err := getInboxIDFromWebsiteToken(ctx, s.rootStore.sqlDB, websiteToken)
	if err != nil {
		return nil, nil, err
	}

	if authTokenParam["inbox_id"] != inboxID {
		return nil, nil, errors.New("invalid token")
	}

	conversation, transaction, err := getConversationAndTransactionFromTransactionID(ctx, s.rootStore.sqlDB, cast.ToString(authTokenParam["transaction_id"]))
	if err != nil {
		return nil, nil, err
	}

	patientID, err := getPatientIDFromInboxIDAndSourceID(ctx, s.rootStore.sqlDB, inboxID, cast.ToString(authTokenParam["source_id"]))
	if err != nil {
		return nil, nil, err
	}

	if transaction.PatientID != patientID {
		return nil, nil, errors.New("invalid token")
	}

	if conversation.InboxID != inboxID {
		return nil, nil, errors.New("invalid token")
	}

	externalUserID, pubsubToken, err := getExternalUserIDAndPubsubTokenFromPatientID(ctx, s.rootStore.sqlDB, patientID)
	if err != nil {
		return nil, nil, err
	}

	if pubsubToken != authTokenParam["pubsub_token"] {
		return nil, nil, errors.New("invalid token")
	}

	meta := metautils.ExtractIncoming(ctx)
	if meta.Get("id") != externalUserID {
		return nil, nil, errors.New("unauthorized user")
	}

	return conversation, transaction, nil
}

// GetPatientData is
func (s *MysqlBaymaxStore) GetPatientData(patientID string) (*baymax.Patient, error) {
	query := `Select id, patient_f_name, patient_l_name, patient_card_number, patient_gender,
	client_app_id, patient_organization, patient_phone, patient_vip_status,
	patient_class, patient_height, patient_weight, patient_age, external_user_id, precondition_id, patient_address, patient_dob
	from patients where id = ? and deleted_at is null `
	res := s.rootStore.sqlDB.QueryRow(query, patientID)
	var data baymax.Patient
	err := res.Scan(&data.ID, &data.FirstName, &data.LastName, &data.CardNumber, &data.Gender,
		&data.ClientAppID, &data.Organization, &data.PhoneNumber, &data.VipStatus,
		&data.Class, &data.Height, &data.Weight, &data.Age, &data.ExternalUserID, &data.PreconditionID, &data.PatientAddress, &data.DOB)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("patient id " + patientID + " is not found")
		}
		return nil, err
	}
	return &data, nil
}

// GetActiveTransactionByPatient is
func (s *MysqlBaymaxStore) GetActiveTransactionByPatient(patientID string, transactionID string) ([]*baymax.ActiveTransaction, error) {
	query := `Select A.id patientId, A.patient_card_number, B.id transaction_id, B.state, B.doctor_id, B.chat_room_id, 
		C.id conversation_id, D.id inbox_id, E.website_token, F.pubsub_token, G.source_id, D.name, H.doctor_f_name, H.doctor_l_name
		from patients A
		Join transactions B on A.id = B.patient_id
		LEFT JOIN conversations C on B.id = C.transaction_id
		LEFT JOIN inboxes D on C.inbox_id = D.id
		LEFT JOIN channel_web_widgets E on D.channel_id = E.id
		LEFT JOIN pubsub_tokens F on A.id = F.morphable_id and F.morphable_type = 'patient'
		LEFT JOIN patient_inboxes G on G.patient_id = A.id and G.inbox_id = D.id
		LEFT JOIN doctors H on H.id = B.doctor_id
		where A.id = ? and B.state in ('in_progress', 'request') `

	if transactionID != "" {
		query += " and B.id = '" + transactionID + "'"
	}

	rows, err := s.rootStore.sqlDB.Query(query, patientID)
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	if err != nil {
		return nil, err
	}

	transactions := make([]*baymax.ActiveTransaction, 0)
	for rows.Next() {
		var transaction baymax.ActiveTransaction
		err = rows.Scan(&transaction.PatientID, &transaction.PatientCardNumber, &transaction.TransactionID, &transaction.State, &transaction.DoctorID, &transaction.ChatRoomID,
			&transaction.ConversationID, &transaction.InboxID, &transaction.WebsiteToken, &transaction.PatientToken, &transaction.SourceID,
			&transaction.InboxName, &transaction.DoctorFirstName, &transaction.DoctorLastName)

		if err != nil {
			return nil, err
		}
		transactions = append(transactions, &transaction)
	}
	return transactions, nil
}

func parseJWTToken(token string) (jwt.MapClaims, error) {
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_SIGNATURE_KEY")), nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok || !parsedToken.Valid {
		return nil, errors.New("invalid claims conversion")
	}

	return claims, nil
}

func parseAndVerifyCwConversation(cwConversation string) (jwt.MapClaims, error) {
	authTokenParam, err := parseJWTToken(cwConversation)
	if err != nil {
		return nil, err
	}

	if _, ok := authTokenParam["source_id"]; !ok {
		return nil, errors.New("invalid token")
	}

	if _, ok := authTokenParam["inbox_id"]; !ok {
		return nil, errors.New("invalid token")
	}

	if _, ok := authTokenParam["transaction_id"]; !ok {
		return nil, errors.New("invalid token")
	}

	if _, ok := authTokenParam["pubsub_token"]; !ok {
		return nil, errors.New("invalid token")
	}

	return authTokenParam, nil
}

func getInboxIDFromWebsiteToken(ctx context.Context, sql gdbc.SQLGdbc, websiteToken string) (string, error) {
	query := "SELECT inboxes.id FROM inboxes JOIN channel_web_widgets on inboxes.channel_id = channel_web_widgets.id WHERE channel_web_widgets.website_token = ?"
	rows, err := sql.QueryContext(ctx, query, websiteToken)
	if err != nil {
		return "", err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var inboxID string
	rows.Next()
	if err := rows.Scan(&inboxID); err != nil {
		return "", err
	}

	return inboxID, nil
}

func getConversationAndTransactionFromTransactionID(ctx context.Context, sql gdbc.SQLGdbc, transactionID string) (*baymax.Conversation, *baymax.Transaction, error) {
	query := "SELECT conversations.id, conversations.client_id, conversations.inbox_id, transactions.id, transactions.doctor_id, transactions.patient_id, transactions.state FROM conversations JOIN transactions ON conversations.transaction_id = transactions.id WHERE transactions.id = ?"
	rows, err := sql.QueryContext(ctx, query, transactionID)
	if err != nil {
		return nil, nil, err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var conversation baymax.Conversation
	var transaction baymax.Transaction
	rows.Next()
	if err := rows.Scan(&conversation.ID, &conversation.ClientID, &conversation.InboxID, &transaction.ID, &transaction.DoctorID, &transaction.PatientID, &transaction.State); err != nil {
		return nil, nil, err
	}

	return &conversation, &transaction, nil
}

func getPatientIDFromInboxIDAndSourceID(ctx context.Context, sql gdbc.SQLGdbc, inboxID string, sourceID string) (string, error) {
	query := "SELECT patient_id FROM patient_inboxes WHERE inbox_id = ? AND source_id = ?"
	rows, err := sql.QueryContext(ctx, query, inboxID, sourceID)
	if err != nil {
		return "", err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var patientID string
	rows.Next()
	if err := rows.Scan(&patientID); err != nil {
		return "", err
	}

	return patientID, nil
}

func getExternalUserIDAndPubsubTokenFromPatientID(ctx context.Context, sql gdbc.SQLGdbc, patientID string) (string, string, error) {
	query := "SELECT patients.external_user_id, t1.pubsub_token FROM patients JOIN (SELECT morphable_id AS id, pubsub_token FROM pubsub_tokens WHERE morphable_type = 'patient' AND morphable_id = ?) AS t1 ON patients.id = t1.id WHERE patients.id = ?"
	rows, err := sql.QueryContext(ctx, query, patientID, patientID)
	if err != nil {
		return "", "", err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var externalUserID string
	var pubsubToken string
	rows.Next()
	if err := rows.Scan(&externalUserID, &pubsubToken); err != nil {
		return "", "", err
	}

	return externalUserID, pubsubToken, nil
}

func getConversationAndTransactionFromConversationID(ctx context.Context, sql gdbc.SQLGdbc, conversationID string) (*baymax.Conversation, *baymax.Transaction, error) {
	query := "SELECT conversations.id, conversations.client_id, conversations.inbox_id, transactions.id, transactions.doctor_id, transactions.patient_id, transactions.state FROM conversations JOIN transactions ON conversations.transaction_id = transactions.id WHERE conversations.id = ?"
	rows, err := sql.QueryContext(ctx, query, conversationID)
	if err != nil {
		return nil, nil, err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var conversation baymax.Conversation
	var transaction baymax.Transaction
	rows.Next()
	if err := rows.Scan(&conversation.ID, &conversation.ClientID, &conversation.InboxID, &transaction.ID, &transaction.DoctorID, &transaction.PatientID, &transaction.State); err != nil {
		return nil, nil, err
	}

	return &conversation, &transaction, nil
}

func getDoctorFromDoctorID(ctx context.Context, sql gdbc.SQLGdbc, doctorID string) (*baymax.Doctor, error) {
	query := "SELECT id, doctor_availability FROM doctors WHERE id = ?"
	rows, err := sql.QueryContext(ctx, query, doctorID)
	if err != nil {
		return nil, err
	}
	defer func() {
		if rows != nil {
			_ = rows.Close()
		}
	}()

	var doctor baymax.Doctor
	rows.Next()
	if err := rows.Scan(&doctor.ID, &doctor.DoctorAvailability); err != nil {
		return nil, err
	}

	return &doctor, nil
}

func (s *MysqlBaymaxStore) getMessageData(messageID string) (*baymax.MessageData, error) {
	query := `Select id, content, client_id, inbox_id, conversation_id, message_type, created_at, updated_at, private, doctor_id, status, 
	content_type, patient_id, message_subtype
	from messages where id = ? and deleted_at is null`

	res := s.rootStore.sqlDB.QueryRow(query, messageID)

	var data baymax.MessageData
	err := res.Scan(&data.ID, &data.Content, &data.ClientID, &data.InboxID, &data.ConversationID, &data.MessageType,
		&data.CreatedAt, &data.UpdatedAt, &data.Private, &data.DoctorID, &data.Status, &data.ContentType, &data.PatientID, &data.MessageSubType)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("message id " + messageID + " is not found")
		}
		return nil, err
	}
	return &data, nil
}

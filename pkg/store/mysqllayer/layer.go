package mysqllayer

import (
	"database/sql"

	"prixa.ai/prixa-db/pkg/sdk/gdbc"
	"prixa.ai/prixa-db/pkg/store"
)

// MysqlStore is
type MysqlStore struct {
	store.Store
	sqlDB gdbc.SQLGdbc

	baymax *MysqlBaymaxStore
}

// NewMysqlStore is
func NewMysqlStore(baseStore store.Store, sql *sql.DB) store.Store {
	mysqlStore := MysqlStore{
		Store: baseStore,
		sqlDB: &gdbc.SQLDBTx{DB: sql},
	}

	mysqlStore.baymax = &MysqlBaymaxStore{rootStore: &mysqlStore, BaymaxStore: baseStore.Baymax()}

	return mysqlStore
}

// Baymax is
func (m MysqlStore) Baymax() store.BaymaxStore {
	return m.baymax
}

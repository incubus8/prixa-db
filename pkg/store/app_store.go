package store

import "context"

// AppStore is
type AppStore struct {
	// context describes per request context
	context          context.Context
	analytic         AnalyticStore
	appointment      AppointmentBookingStore
	diagnostic       DiagnosticStore
	diagnosticResult DiagnosticStore
	doctor           DoctorStore
	partner          PartnerStore
	partnerApp       PartnerAppStore
	user             UserStore
	crispUser        CrispUserStore
	hospital         HospitalStore
	chat             ChatRequestStore
	chatResult       ChatResultStore
	doctorAllocation DoctorAllocationStore
	labtest          LabtestStore
	baymax           BaymaxStore
}

// NewAppStore is
func NewAppStore() *AppStore {
	return &AppStore{}
}

// Analytic is
func (app AppStore) Analytic() AnalyticStore {
	return app.analytic
}

// AppointmentBooking is
func (app AppStore) AppointmentBooking() AppointmentBookingStore {
	return app.appointment
}

// Diagnostic is
func (app AppStore) Diagnostic() DiagnosticStore {
	return app.diagnostic
}

// DiagnosticResult is
func (app AppStore) DiagnosticResult() DiagnosticStore {
	return app.diagnosticResult
}

// Doctor is
func (app AppStore) Doctor() DoctorStore {
	return app.doctor
}

// Hospital is
func (app AppStore) Hospital() HospitalStore {
	return app.hospital
}

// User is
func (app AppStore) User() UserStore {
	return app.user
}

// Labtest is
func (app AppStore) Labtest() LabtestStore {
	return app.labtest
}

// CrispUser is
func (app AppStore) CrispUser() CrispUserStore {
	return app.crispUser
}

// Partner is
func (app AppStore) Partner() PartnerStore {
	return app.partner
}

// PartnerApp is
func (app AppStore) PartnerApp() PartnerAppStore {
	return app.partnerApp
}

// ChatRequest is
func (app AppStore) ChatRequest() ChatRequestStore {
	return app.chat
}

// ChatResult is
func (app AppStore) ChatResult() ChatResultStore {
	return app.chatResult
}

// DoctorAllocation is
func (app AppStore) DoctorAllocation() DoctorAllocationStore {
	return app.doctorAllocation
}

// Baymax is
func (app AppStore) Baymax() BaymaxStore {
	return app.baymax
}

// SetContext sets context for Store
func (app *AppStore) SetContext(context context.Context) {
	app.context = context
}

// Context gets context for Store
func (app AppStore) Context() context.Context {
	return app.context
}

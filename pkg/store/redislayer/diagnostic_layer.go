package redislayer

import (
	"encoding/json"
	"fmt"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
)

// RedisDiagnosticStore is
type RedisDiagnosticStore struct {
	query.GetSetQueryWithTTLRedisImpl
	store.DiagnosticStore
	rootStore *RedisStore
}

// GetSessionResult gets SessionResult from cache
func (s *RedisDiagnosticStore) GetSessionResult(id string) (bot.SessionResult, error) {
	sess := bot.SessionResult{}

	key := s.makeSessionResultKey(id)
	result, err := s.FindByID(key)
	if err != nil {
		return sess, fmt.Errorf("key not found: %v %v", key, err.Error())
	}

	err = json.Unmarshal([]byte(result.(string)), &sess)
	if err != nil {
		return sess, fmt.Errorf("failed to unmarshal SessionResult: %+v %v", sess, err.Error())
	}

	return sess, nil
}

// SaveSessionResult saves SessionResult into cache
func (s *RedisDiagnosticStore) SaveSessionResult(id string, sr *bot.SessionResult) error {
	val, err := json.Marshal(sr)
	if err != nil {
		return fmt.Errorf("failed to encode: %v", err.Error())
	}

	key := s.makeSessionResultKey(id)
	value := string(val)
	if err = s.Save(key, value); err != nil {
		return fmt.Errorf("save SessionResult to redis failede: %v", err.Error())
	}

	return nil
}

func (s *RedisDiagnosticStore) makeSessionResultKey(key string) string {
	return fmt.Sprintf("%v_sessionResult:%v", s.TableName(), key)
}

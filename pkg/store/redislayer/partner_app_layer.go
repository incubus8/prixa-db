package redislayer

import (
	"encoding/json"
	"time"

	"github.com/golang/protobuf/proto" //lint:ignore SA1019 GRPC not yet supported for Protobuf V2
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/partner"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// RedisPartnerAppStore is
type RedisPartnerAppStore struct {
	query.PageableQueryRedisImpl
	store.PartnerAppStore
	rootStore *RedisStore
}

// MakeKeyWithPartnerID makes key for later use, useful when finding partner app
func (s *RedisPartnerAppStore) MakeKeyWithPartnerID(partnerID, ID string) string {
	return "partner:" + partnerID + ":" + s.TableName() + ":" + ID
}

// SavePartnerApp persists partner app to disk
func (s *RedisPartnerAppStore) SavePartnerApp(partnerID string, partnerApp *partner.App) (*partner.App, error) {
	if partnerApp.ID == "" {
		partnerApp.ID = util.GetRandomID()
		partnerApp.SecretKey = util.RandString(16)
		partnerApp.CreatedAt = time.Now().String()
		partnerApp.DeletedAt = ""
		partnerApp.Status = "ACTIVE"
	}

	partnerApp.UpdatedAt = time.Now().String()

	keyID := s.MakeKeyWithPartnerID(partnerID, partnerApp.ID)

	if err := s.Save(keyID, partnerApp); err != nil {
		return nil, err
	}

	if err := s.GetRedis().SAdd("partner:"+partnerID+":appIds", partnerApp.ID).Err(); err != nil {
		return nil, err
	}

	return partnerApp, nil
}

// FindPartnerAppByID finds partner by ID
func (s *RedisPartnerAppStore) FindPartnerAppByID(key string) (*partner.App, error) {
	result, err := s.FindByID(key)
	if err != nil {
		return nil, err
	}

	var p *partner.App
	err2 := json.Unmarshal([]byte(result.(string)), &p)
	if err2 != nil {
		return nil, err2
	}

	return p, nil
}

// FindAllPartnerApps finds all partner apps data
func (s *RedisPartnerAppStore) FindAllPartnerApps(from, to int64) ([]*partner.App, error) {
	results, err := s.FindAll(from, to)
	if err != nil {
		return nil, err
	}

	partnerApps := make([]*partner.App, 0, len(results))
	for _, result := range results {
		var partnerApp *partner.App
		if err2 := json.Unmarshal([]byte(result.(string)), &partnerApp); err2 != nil {
			return nil, err2
		}

		partnerApps = append(partnerApps, partnerApp)
	}

	return partnerApps, nil
}

// UpdatePartnerApp updates partner app, needs partner id to work, will update updatedAt field accordingly
func (s *RedisPartnerAppStore) UpdatePartnerApp(partnerID string, partnerApp *partner.App) (*partner.App, error) {
	partnerApp.UpdatedAt = time.Now().String()

	keyID := s.MakeKeyWithPartnerID(partnerID, partnerApp.ID)
	if err := s.Save(keyID, partnerApp); err != nil {
		return nil, err
	}

	return partnerApp, nil
}

// SaveAppMetadata persists partner app metadata to disk
func (s *RedisPartnerAppStore) SaveAppMetadata(data partner.AppData) error {
	keyID := s.MakeKeyWithPartnerID(data.PartnerID, data.AppID) + ":metadata"
	protoByte, err := proto.Marshal(data.Metadata)
	if err != nil {
		return err
	}

	err = s.Save(keyID, protoByte)
	if err != nil {
		return err
	}

	return nil
}

// GetAppMetadata retrieve partner metadata
func (s *RedisPartnerAppStore) GetAppMetadata(key string) (*partner.AppData, error) {
	result, err := s.FindByID(key)
	if err != nil {
		return nil, err
	}

	var metadata partnerAppGRPC.AppMetadata
	if err = proto.Unmarshal([]byte(result.(string)), &metadata); err != nil {
		logrus.WithError(err).Errorln("cannot unmarshal")
		return nil, err
	}

	return &partner.AppData{Metadata: &metadata}, nil
}

// FindActivePartnerAppID finds ACTIVE partner application data from given partner id and partner app id
func (s *RedisPartnerAppStore) FindActivePartnerAppID(partnerID, partnerAppID string) (*partner.App, error) {
	keyID := s.MakeKeyWithPartnerID(partnerID, partnerAppID)
	foundPartnerApp, err := s.FindPartnerAppByID(keyID)
	if err != nil {
		return nil, partner.ErrNoAppIDFound
	}

	if foundPartnerApp.Status != "ACTIVE" {
		return nil, partner.ErrInactivePartnerApp
	}

	return foundPartnerApp, nil
}

// FindActivePartnerAndPartnerApp finds ACTIVE partner and its app data
func (s *RedisPartnerAppStore) FindActivePartnerAndPartnerApp(partnerID string, partnerAppID string) (*partner.Partner, *partner.App, error) {
	if partnerID == "" {
		return nil, nil, partner.ErrEmptyPartnerID
	}
	if partnerAppID == "" {
		return nil, nil, partner.ErrEmptyPartnerAppID
	}

	foundPartner, err := s.rootStore.partner.FindActivePartnerID(partnerID)
	if err != nil || foundPartner == nil {
		return nil, nil, err
	}

	foundPartnerApp, err := s.FindActivePartnerAppID(foundPartner.ID, partnerAppID)
	if err != nil {
		return nil, nil, err
	}

	return foundPartner, foundPartnerApp, nil
}

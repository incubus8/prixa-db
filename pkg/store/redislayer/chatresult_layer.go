package redislayer

import (
	"encoding/json"
	"errors"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

// RedisChatResultStore is
type RedisChatResultStore struct {
	store.ChatResultStore
	rootStore *RedisStore
	redis     *redis.Client
	name      string
}

// CreateChatResult is to save partner data
func (s *RedisChatResultStore) CreateChatResult(id string, chat *model.ChatResult) error {
	timeoutEnv := os.Getenv("AUTOMATIC_RESOLVE_TIMEOUT")
	timeout, err := strconv.ParseUint(timeoutEnv, 10, 64)
	if err != nil {
		logrus.WithField("timeout", timeoutEnv).WithError(err).Errorln()
		return err
	}
	duration := time.Duration(timeout) * time.Second

	tx := s.redis.TxPipeline()
	keyID := s.makeKeyWithID(id)
	marshalChat, err := json.Marshal(chat)
	if err != nil {
		return err
	}

	if err := tx.Set(keyID, marshalChat, 0).Err(); err != nil {
		return err
	}

	if chat.Doctor != nil {
		shadowKeyID := "shadow:" + keyID
		if err := tx.Set(shadowKeyID, chat.SessionID, duration).Err(); err != nil {
			return err
		}
	}

	if err := tx.ZAdd(s.name, &redis.Z{
		Score:  float64(time.Now().Unix()),
		Member: keyID,
	}).Err(); err != nil {
		return err
	}

	if _, err := tx.Exec(); err != nil {
		return err
	}

	return nil
}

// FindChatResultByID is
func (s *RedisChatResultStore) FindChatResultByID(key string) (*model.ChatResult, error) {
	keyID := s.makeKeyWithID(key)

	result, err := s.redis.Get(keyID).Result()
	if err != nil {
		return nil, errors.New("key not found " + key + " " + err.Error())
	}

	var sessionChat *model.ChatResult
	if err = json.Unmarshal([]byte(result), &sessionChat); err != nil {
		return nil, err
	}

	return sessionChat, nil
}

// ResetIdleTimeoutToDefault is
func (s *RedisChatResultStore) ResetIdleTimeoutToDefault(key string) error {
	timeoutEnv := os.Getenv("AUTOMATIC_RESOLVE_TIMEOUT")
	timeout, err := strconv.ParseUint(timeoutEnv, 10, 64)
	if err != nil {
		logrus.WithField("timeout", timeoutEnv).WithError(err).Errorln()
		return err
	}
	duration := time.Duration(timeout) * time.Second

	tx := s.redis.TxPipeline()
	keyID := s.makeKeyWithID(key)
	shadowKeyID := "shadow:" + keyID

	if err := tx.Expire(shadowKeyID, duration).Err(); err != nil {
		return err
	}
	if _, err := tx.Exec(); err != nil {
		return err
	}

	return nil
}

func (s *RedisChatResultStore) makeKeyWithID(ID string) string {
	return s.name + ":" + ID
}

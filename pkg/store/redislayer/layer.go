package redislayer

import (
	"time"

	"github.com/go-redis/redis/v7"

	"prixa.ai/prixa-db/pkg/store"
)

// RedisStore is
type RedisStore struct {
	store.Store
	partner          *RedisPartnerStore
	partnerApp       *RedisPartnerAppStore
	user             *RedisUserStore
	crispUser        *RedisCrispUserStore
	doctor           *RedisDoctorStore
	diagnostic       *RedisDiagnosticStore
	diagnosticResult *RedisDiagnosticResultStore
	chatResult       *RedisChatResultStore
	labtest          *RedisLabTestStore
	baymaxRedis      *RedisBaymaxStore
}

// NewRedisStore is
func NewRedisStore(baseStore store.Store, redis *redis.Client, baymaxRedis *redis.Client) store.Store {
	redisStore := RedisStore{
		Store: baseStore,
	}

	redisStore.partner = &RedisPartnerStore{PartnerStore: baseStore.Partner(), rootStore: &redisStore}
	redisStore.partner.SetRedis(redis)
	redisStore.partner.SetTableName("partner")
	redisStore.partner.SetPageSize(25)

	redisStore.partnerApp = &RedisPartnerAppStore{PartnerAppStore: baseStore.PartnerApp(), rootStore: &redisStore}
	redisStore.partnerApp.SetRedis(redis)
	redisStore.partnerApp.SetTableName("partner_app")
	redisStore.partnerApp.SetPageSize(25)

	redisStore.user = &RedisUserStore{UserStore: baseStore.User(), rootStore: &redisStore}
	redisStore.user.SetRedis(redis)
	redisStore.user.SetTableName("login")
	redisStore.user.SetTTL(0)
	redisStore.user.SetAutoRenewWhenSet(false)

	redisStore.crispUser = &RedisCrispUserStore{CrispUserStore: baseStore.CrispUser(), rootStore: &redisStore}
	redisStore.crispUser.SetRedis(redis)
	redisStore.crispUser.SetTableName("crispUser")
	redisStore.crispUser.SetTTL(time.Hour)
	redisStore.crispUser.SetAutoRenewWhenSet(false)

	redisStore.doctor = &RedisDoctorStore{DoctorStore: baseStore.Doctor(), rootStore: &redisStore}
	redisStore.doctor.SetRedis(redis)
	redisStore.doctor.SetTableName("doctor")
	redisStore.doctor.SetTTL(2 * time.Hour)
	redisStore.doctor.SetAutoRenewWhenSet(false)

	redisStore.diagnostic = &RedisDiagnosticStore{DiagnosticStore: baseStore.Diagnostic(), rootStore: &redisStore}
	redisStore.diagnostic.SetRedis(redis)
	redisStore.diagnostic.SetTableName("diagnostic")
	redisStore.diagnostic.SetTTL(40 * time.Minute)
	redisStore.diagnostic.SetAutoRenewWhenSet(true)

	redisStore.diagnosticResult = &RedisDiagnosticResultStore{DiagnosticStore: baseStore.Diagnostic(), rootStore: &redisStore}
	redisStore.diagnosticResult.SetRedis(redis)
	redisStore.diagnosticResult.SetTableName("diagnostic")
	redisStore.diagnosticResult.SetTTL(24 * time.Hour)
	redisStore.diagnosticResult.SetAutoRenewWhenSet(true)

	redisStore.labtest = &RedisLabTestStore{LabtestStore: baseStore.Labtest(), rootStore: &redisStore}
	redisStore.labtest.SetRedis(redis)
	redisStore.labtest.SetTableName("labtest")
	redisStore.labtest.SetTTL(30 * 24 * time.Hour)
	redisStore.labtest.SetAutoRenewWhenSet(true)

	redisStore.chatResult = &RedisChatResultStore{name: "chatresult", redis: redis, ChatResultStore: baseStore.ChatResult(), rootStore: &redisStore}

	redisStore.baymaxRedis = &RedisBaymaxStore{rootStore: &redisStore, BaymaxStore: baseStore.Baymax()}
	redisStore.baymaxRedis.SetRedis(baymaxRedis)
	redisStore.baymaxRedis.SetTTL(30 * time.Minute)
	redisStore.baymaxRedis.SetAutoRenewWhenSet(true)

	return redisStore
}

// Partner is
func (s RedisStore) Partner() store.PartnerStore {
	return s.partner
}

// PartnerApp is
func (s RedisStore) PartnerApp() store.PartnerAppStore {
	return s.partnerApp
}

// User is
func (s RedisStore) User() store.UserStore {
	return s.user
}

// CrispUser is
func (s RedisStore) CrispUser() store.CrispUserStore {
	return s.crispUser
}

// Doctor is
func (s RedisStore) Doctor() store.DoctorStore {
	return s.doctor
}

// Diagnostic is
func (s RedisStore) Diagnostic() store.DiagnosticStore {
	return s.diagnostic
}

// DiagnosticResult is
func (s RedisStore) DiagnosticResult() store.DiagnosticStore {
	return s.diagnosticResult
}

// ChatResult is
func (s RedisStore) ChatResult() store.ChatResultStore {
	return s.chatResult
}

// Labtest is
func (s RedisStore) Labtest() store.LabtestStore {
	return s.labtest
}

// Baymax is
func (s RedisStore) Baymax() store.BaymaxStore {
	return s.baymaxRedis
}

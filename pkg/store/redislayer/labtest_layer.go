package redislayer

import (
	"fmt"

	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
)

// RedisLabTestStore is
type RedisLabTestStore struct {
	query.GetSetQueryWithTTLRedisImpl
	store.LabtestStore
	rootStore *RedisStore
}

// SaveCouponCode is
func (s *RedisLabTestStore) SaveCouponCode(email, token string) error {
	key := s.makeKey(email)
	if err := s.Save(key, token); err != nil {
		return fmt.Errorf("save token to redis failed: %v", err.Error())
	}

	return nil
}

// GetCouponCode is
func (s *RedisLabTestStore) GetCouponCode(email string) (string, error) {
	key := s.makeKey(email)
	code, err := s.FindByID(key)
	if err != nil {
		return "", err
	}
	return cast.ToString(code), nil
}

func (s *RedisLabTestStore) makeKey(email string) string {
	return fmt.Sprintf("%v_coupon:%v", s.TableName(), email)
}

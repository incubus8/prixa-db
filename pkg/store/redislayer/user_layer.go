package redislayer

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"

	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"
)

// RedisUserStore is
type RedisUserStore struct {
	query.GetSetQueryWithTTLRedisImpl
	store.UserStore
	rootStore *RedisStore
}

// SaveToken saves user token
func (s *RedisUserStore) SaveToken(userData *userprofile.RedisData) error {
	key := s.makeKey(userData.Key)
	if err := s.Save(key, userData.Token); err != nil {
		logrus.WithError(err).WithField("userTokenKey", key).Errorln("Failed save user token")
		return err
	}

	return nil
}

// GetToken gets user token
func (s *RedisUserStore) GetToken(key string) (string, error) {
	result, err := s.FindByID(s.makeKey(key))
	if err != nil {
		logrus.WithError(err).WithField("userTokenKey", key).Errorln("Failed get user token")
		return "", err
	}

	return cast.ToString(result), nil
}

// DeleteToken deletes user token
func (s *RedisUserStore) DeleteToken(key string) error {
	if err := s.Delete(s.makeKey(key)); err != nil {
		logrus.WithError(err).WithField("userTokenKey", key).Errorln("Failed delete user token")
		return err
	}
	return nil
}

func (s *RedisUserStore) makeKey(key string) string {
	return s.TableName() + ":" + key
}

// SaveAdminPwd saves admin pwd
func (s *RedisUserStore) SaveAdminPwd(userData *userprofile.RedisData) error {
	key := s.makeAdminKey(userData.Key)
	if err := s.Save(key, userData.Token); err != nil {
		logrus.WithError(err).WithField("userTokenKey", key).Errorln("failed save admin data")
		return err
	}

	return nil
}

// GetAdminPwd gets admin password
func (s *RedisUserStore) GetAdminPwd(key string) (string, error) {
	result, err := s.FindByID(s.makeAdminKey(key))
	if err != nil {
		logrus.WithError(err).WithField("userTokenKey", s.makeAdminKey(key)).Errorln("failed get admin data")
		return "", err
	}

	return cast.ToString(result), nil
}

func (s *RedisUserStore) makeAdminKey(key string) string {
	return s.TableName() + ":" + key + ":superadmin"
}

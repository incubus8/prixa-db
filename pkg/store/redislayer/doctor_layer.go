package redislayer

import (
	"encoding/json"
	"time"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
)

// RedisDoctorStore is
type RedisDoctorStore struct {
	query.GetSetQueryWithTTLRedisImpl

	store.DoctorStore
	rootStore *RedisStore
}

// SaveDoctorLeavesData is to save DoctorLeaves data
func (s *RedisDoctorStore) SaveDoctorLeavesData(leavesData *model.DoctorLeaves) (*model.DoctorLeaves, error) {
	keyID := s.makeDoctorLeavesKeyWithID(leavesData.HospitalID + "-" + leavesData.DoctorID)
	if err := s.Save(keyID, leavesData); err != nil {
		logrus.WithError(err).WithField("keyId", keyID).WithField("doctorLeaves", leavesData).WithField("leavesQuery", s).Errorln()
		return nil, err
	}

	return leavesData, nil
}

// FindDoctorLeavesData finds DoctorLeaves data by doctorId and hospitalId
func (s *RedisDoctorStore) FindDoctorLeavesData(hospitalID, doctorID string) (*model.DoctorLeaves, error) {
	keyID := s.makeDoctorLeavesKeyWithID(hospitalID + "-" + doctorID)
	result, err := s.FindByID(keyID)
	if err != nil {
		logrus.WithError(err).WithField("keyId", keyID).WithField("leavesQuery", s).Errorln()
		return nil, err
	}

	var leavesData *model.DoctorLeaves
	if err = json.Unmarshal([]byte(result.(string)), &leavesData); err != nil {
		logrus.WithError(err).WithField("leavesData", result).Errorln("error during unmarshal doctor leaves")
		return nil, err
	}

	return leavesData, nil
}

func (s *RedisDoctorStore) makeDoctorLeavesKeyWithID(id string) string {
	return s.TableName() + "_leaves:" + id
}

// SaveDoctorTimeSlotsData is to save DoctorTimeSlot data
func (s *RedisDoctorStore) SaveDoctorTimeSlotsData(timeSlotData *model.DoctorTimeSlots) (*model.DoctorTimeSlots, error) {
	keyID := s.makeDoctorTimeSlotKeyWithID(timeSlotData.HospitalID + "-" + timeSlotData.DoctorID + "-" + timeSlotData.AppointmentDate.Format("2006/01/02"))
	if err := s.Save(keyID, timeSlotData); err != nil {
		logrus.WithError(err).WithField("timeslot", timeSlotData).WithField("keyId", keyID).Errorln()
		return nil, err
	}

	return timeSlotData, nil
}

// FindDoctorTimeSlotsData finds DoctorTimeSlot  data by doctorId and hospitalId
func (s *RedisDoctorStore) FindDoctorTimeSlotsData(hospitalID, doctorID string, appointmentDate *time.Time) (*model.DoctorTimeSlots, error) {
	keyID := s.makeDoctorTimeSlotKeyWithID(hospitalID + "-" + doctorID + "-" + appointmentDate.Format("2006/01/02"))
	result, err := s.FindByID(keyID)
	if err != nil {
		logrus.WithError(err).WithField("keyId", keyID).Errorln()
		return nil, err
	}

	var timeSlotData *model.DoctorTimeSlots
	if err = json.Unmarshal([]byte(result.(string)), &timeSlotData); err != nil {
		logrus.WithError(err).WithField("timeSlots", result).Errorln("error during unmarshal timeSlot")
		return nil, err
	}

	return timeSlotData, nil
}

// RemoveDoctorTimeSlotsData delete DoctorTimeSlot  data by doctorId and hospitalId
func (s *RedisDoctorStore) RemoveDoctorTimeSlotsData(hospitalID, doctorID string, appointmentDate *time.Time) error {
	keyID := s.makeDoctorTimeSlotKeyWithID(hospitalID + "-" + doctorID + "-" + appointmentDate.Format("2006/01/02"))
	if err := s.Delete(keyID); err != nil {
		logrus.WithError(err).WithField("keyId", keyID).Errorln("id not found or error delete data in redis")
		return err
	}

	return nil
}

func (s *RedisDoctorStore) makeDoctorTimeSlotKeyWithID(id string) string {
	return s.TableName() + "_timeslots:" + id
}

package redislayer

import (
	"encoding/json"
	"strconv"
	"time"

	"prixa.ai/prixa-db/pkg/partner"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// RedisPartnerStore is
type RedisPartnerStore struct {
	query.PageableQueryRedisImpl
	store.PartnerStore
	rootStore *RedisStore
}

// SavePartner is to save partner data
// Create new partner if missing partner id passed in param
func (s *RedisPartnerStore) SavePartner(partner *partner.Partner) (*partner.Partner, error) {
	if partner.ID == "" {
		partner.ID = util.GetRandomID()
		partner.CreatedAt = time.Now().String()
		partner.Status = "ACTIVE"
		partner.DeletedAt = ""
		partner.AppIds = []string{}
	}

	partner.UpdatedAt = time.Now().String()

	keyID := s.makeKeyWithID(partner.ID)

	if err := s.Save(keyID, partner); err != nil {
		return nil, err
	}

	return partner, nil
}

// UpdatePartner updates partner data
func (s *RedisPartnerStore) UpdatePartner(partner *partner.Partner) (*partner.Partner, error) {
	partner.UpdatedAt = time.Now().String()

	keyID := s.makeKeyWithID(partner.ID)
	if err := s.Save(keyID, partner); err != nil {
		return nil, err
	}

	return partner, nil
}

// FindPartnerByID finds partner by id
func (s *RedisPartnerStore) FindPartnerByID(id string) (*partner.Partner, error) {
	result, err := s.FindByID(s.makeKeyWithID(id))
	if err != nil {
		return nil, err
	}

	var partner *partner.Partner
	if err = json.Unmarshal([]byte(result.(string)), &partner); err != nil {
		return nil, err
	}

	partner.AppIds = s.GetAppIDs(partner.ID)

	return partner, nil
}

// FindActivePartnerID finds ACTIVE partner data from given partner id
func (s *RedisPartnerStore) FindActivePartnerID(partnerID string) (*partner.Partner, error) {
	found, err := s.FindPartnerByID(partnerID)
	if err != nil || found == nil {
		return nil, partner.ErrNoPartnerIDFound
	}

	if found.Status != "ACTIVE" {
		return nil, partner.ErrInactivePartner
	}

	return found, nil
}

// GetAppIDs get app IDs by partner ID
func (s *RedisPartnerStore) GetAppIDs(partnerID string) []string {
	val, err := s.GetRedis().SMembers("partner:" + partnerID + ":appIds").Result()
	if err != nil {
		return nil
	}

	return val
}

// FindAllPartners finds all partners
func (s *RedisPartnerStore) FindAllPartners(from, to int64) ([]*partner.Partner, error) {
	results, err := s.FindAll(from, to)
	if err != nil {
		return nil, err
	}

	partners := make([]*partner.Partner, 0, len(results))
	for _, result := range results {
		var partner *partner.Partner
		err2 := json.Unmarshal([]byte(result.(string)), &partner)
		if err2 != nil {
			return nil, err2
		}

		partner.AppIds = s.GetAppIDs(partner.ID)

		partners = append(partners, partner)
	}

	return partners, nil
}

// FindAllPartnersAtPage finds all partners with page no.
func (s *RedisPartnerStore) FindAllPartnersAtPage(pageNo int64) (*partner.PageablePartnerData, error) {
	results, err := s.GetDataAtPage(pageNo)
	if err != nil {
		return nil, err
	}

	partners := make([]*partner.Partner, 0, len(results))
	for _, result := range results {
		var prtner *partner.Partner
		err2 := json.Unmarshal([]byte(result.(string)), &prtner)
		if err2 != nil {
			return nil, err2
		}

		prtner.AppIds = s.GetAppIDs(prtner.ID)

		partners = append(partners, prtner)
	}

	partnerData := partner.PageablePartnerData{
		Partners: partners,
		Page: &query.PageData{
			PageNo:       strconv.FormatInt(pageNo+1, 10),
			TotalPages:   strconv.FormatInt(s.GetTotalPages()+1, 10),
			TotalRecords: strconv.FormatInt(s.Count(), 10),
		},
	}

	return &partnerData, nil
}

func (s *RedisPartnerStore) makeKeyWithID(ID string) string {
	return s.TableName() + ":" + ID
}

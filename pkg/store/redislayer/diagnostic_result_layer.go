package redislayer

import (
	"fmt"

	"github.com/golang/protobuf/proto" //lint:ignore SA1019 GRPC not yet supported for Protobuf V2
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
)

// RedisDiagnosticResultStore is
type RedisDiagnosticResultStore struct {
	query.GetSetQueryWithTTLRedisImpl
	store.DiagnosticStore
	rootStore *RedisStore
}

// SaveDiagnosisResult saves user assessment result to cache
func (s *RedisDiagnosticResultStore) SaveDiagnosisResult(diagResData *bot.DiagnosisResData) error {
	data, err := proto.Marshal(diagResData.Data)
	if err != nil {
		logrus.WithError(err).Errorln("cannot marshal")
		return err
	}

	key := s.makeDiagnosticResultKey(diagResData.Key)
	if err := s.Save(key, data); err != nil {
		logrus.WithError(err).Errorln("cannot save diagnostic result")
		return err
	}

	return nil
}

// GetDiagnosisResult gets user assessment result from the cache
func (s *RedisDiagnosticResultStore) GetDiagnosisResult(key string) (*bot.DiagnosisResData, error) {
	result, err := s.FindByID(s.makeDiagnosticResultKey(key))
	if err != nil {
		return nil, err
	}

	var data diagnosticGRPC.DiagnosisResultData
	if err = proto.Unmarshal([]byte(result.(string)), &data); err != nil {
		logrus.WithError(err).Errorln("cannot unmarshal")
		return nil, err
	}

	return &bot.DiagnosisResData{Key: key, Data: &data}, nil
}

func (s *RedisDiagnosticResultStore) makeDiagnosticResultKey(key string) string {
	return fmt.Sprintf("%v_diagnosticResult:%v", s.TableName(), key)
}

package redislayer

import (
	"encoding/json"

	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"
)

// RedisCrispUserStore is
type RedisCrispUserStore struct {
	query.GetSetQueryWithTTLRedisImpl
	store.CrispUserStore
	rootStore *RedisStore
}

// SavePersonProfile saves person profile to Crisp
func (s *RedisCrispUserStore) SavePersonProfile(crispUserData *userprofile.CrispUserRedisData) error {
	val, err := json.Marshal(crispUserData.User)
	if err != nil {
		logrus.WithError(err).WithField("personProfileKey", crispUserData).Errorln("Failed marshal save person profile")
		return err
	}

	key := s.makeKey(crispUserData.Key)
	if err := s.Save(key, val); err != nil {
		logrus.WithError(err).WithField("personProfileKey", key).Errorln("Failed save person profile")
		return err
	}

	return nil
}

// GetPersonProfile gets crisp user data from the cache
func (s *RedisCrispUserStore) GetPersonProfile(key string) (*userprofile.CrispUserRedisData, error) {
	result, err := s.FindByID(s.makeKey(key))
	if err != nil {
		logrus.WithError(err).WithField("personProfileKey", key).Errorln("Failed get person profile")
		return nil, err
	}

	var data userprofile.CrispUserData
	if err = json.Unmarshal([]byte(result.(string)), &data); err != nil {
		logrus.WithError(err).Errorln("Failed to unmarshal crisp response")
		return nil, err
	}

	return &userprofile.CrispUserRedisData{Key: key, User: data}, nil
}

// DeletePersonProfile delete crisp user data from the cache
func (s *RedisCrispUserStore) DeletePersonProfile(email string) error {
	key := s.makeKey(email)
	if err := s.Delete(key); err != nil {
		logrus.WithError(err).WithField("personProfileKey", key).Errorln("Failed to delete person profile")
		return err
	}
	return nil
}

func (s *RedisCrispUserStore) makeKey(key string) string {
	meta := metautils.ExtractIncoming(s.rootStore.Context())
	return meta.Get("x-partner-id") + ":" + meta.Get("x-partner-app-id") + ":" + s.TableName() + ":" + key
}

// SaveUserWebsiteID saves user websiteID to cache
func (s *RedisCrispUserStore) SaveUserWebsiteID(email, websiteID string) error {
	key := s.makeWebsiteIDKey(email)
	if err := s.Save(key, websiteID); err != nil {
		logrus.WithError(err).WithField("userWebsiteIDKey", key).Errorln("Failed save user websiteID")
		return err
	}

	return nil
}

// GetUserWebsiteID gets user websiteID from the cache
func (s *RedisCrispUserStore) GetUserWebsiteID(email string) (string, error) {
	key := s.makeWebsiteIDKey(email)
	websiteID, err := s.FindByID(email)
	if err != nil {
		logrus.WithError(err).WithField("userWebsiteIDKey", key).Errorln("Failed get user websiteID")
		return "", err
	}

	return websiteID.(string), nil
}

func (s *RedisCrispUserStore) makeWebsiteIDKey(key string) string {
	return s.makeKey(key) + ":" + "websiteID"
}

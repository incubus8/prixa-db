package redislayer

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/model/baymax"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// RedisBaymaxStore is
type RedisBaymaxStore struct {
	query.GetSetQueryWithTTLRedisImpl

	rootStore *RedisStore
	store.BaymaxStore
}

// SendMessage is
func (b *RedisBaymaxStore) SendMessage(ctx context.Context, data *baymax.MessageData) error {
	serialized := ""
	if cast.ToString(data.DoctorID) != "" && cast.ToString(data.PatientID) != "" {
		return errors.New("invalid data")
	} else if cast.ToString(data.DoctorID) != "" {
		serialized = "O:36:\"Illuminate\\Events\\CallQueuedListener\":8:{s:5:\"class\";s:33:\"App\\Listeners\\SendMessageListener\";s:6:\"method\";s:6:\"handle\";s:4:\"data\";a:1:{i:0;O:22:\"App\\Events\\SendMessage\":1:{s:7:\"payload\";a:7:{s:2:\"id\";s:" + strconv.Itoa(len(data.ID)) + ":\"" + data.ID + "\";s:7:\"content\";s:" + strconv.Itoa(len(data.Content)) + ":\"" + data.Content + "\";s:12:\"conversation\";s:" + strconv.Itoa(len(data.ConversationID)) + ":\"" + data.ConversationID + "\";s:11:\"messageType\";s:" + strconv.Itoa(len(data.MessageType)) + ":\"" + data.MessageType + "\";s:6:\"sender\";s:" + strconv.Itoa(len(data.Sender)) + ":\"" + data.Sender + "\";s:7:\"private\";b:" + strconv.Itoa(data.Private) + ";s:8:\"doctorId\";s:" + strconv.Itoa(len(cast.ToString(data.DoctorID))) + ":\"" + cast.ToString(data.DoctorID) + "\";}}}s:5:\"tries\";N;s:10:\"retryAfter\";N;s:9:\"timeoutAt\";N;s:7:\"timeout\";N;s:3:\"job\";N;}"
	} else if cast.ToString(data.PatientID) != "" {
		serialized = "O:36:\"Illuminate\\Events\\CallQueuedListener\":8:{s:5:\"class\";s:33:\"App\\Listeners\\SendMessageListener\";s:6:\"method\";s:6:\"handle\";s:4:\"data\";a:1:{i:0;O:22:\"App\\Events\\SendMessage\":1:{s:7:\"payload\";a:7:{s:2:\"id\";s:" + strconv.Itoa(len(data.ID)) + ":\"" + data.ID + "\";s:7:\"content\";s:" + strconv.Itoa(len(data.Content)) + ":\"" + data.Content + "\";s:12:\"conversation\";s:" + strconv.Itoa(len(data.ConversationID)) + ":\"" + data.ConversationID + "\";s:11:\"messageType\";s:" + strconv.Itoa(len(data.MessageType)) + ":\"" + data.MessageType + "\";s:6:\"sender\";s:" + strconv.Itoa(len(data.Sender)) + ":\"" + data.Sender + "\";s:7:\"private\";b:" + strconv.Itoa(data.Private) + ";s:9:\"patientId\";s:" + strconv.Itoa(len(cast.ToString(data.PatientID))) + ":\"" + cast.ToString(data.PatientID) + "\";}}}s:5:\"tries\";N;s:10:\"retryAfter\";N;s:9:\"timeoutAt\";N;s:7:\"timeout\";N;s:3:\"job\";N;}"
	}

	if serialized == "" {
		return errors.New("invalid data")
	}

	event := &baymax.SendMessageEvent{
		UuID:          util.GetRandomID(),
		DisplayName:   "App\\Listeners\\SendMessageListener",
		Job:           "Illuminate\\Queue\\CallQueuedHandler@call",
		MaxTries:      nil,
		MaxExceptions: nil,
		Delay:         nil,
		Timeout:       nil,
		TimeoutAt:     nil,
		ID:            util.RanStrWithLength(32),
		Attempts:      0,
		Data: &baymax.SendMessageEventData{
			CommandName: "Illuminate\\Events\\CallQueuedListener",
			Command:     serialized,
		},
	}

	encodedEvent, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = b.LPush("queues:baymax.send_message", encodedEvent)
	if err != nil {
		fmt.Println(err)
	}

	return nil
}

// HasPermissionViewConversationMessageInCache is
func (b *RedisBaymaxStore) HasPermissionViewConversationMessageInCache(userID string, conversationID string) (*baymax.PermissionViewConversationMessage, error) {
	keyID := b.makeKeyIDForCheckPermissionViewConvMessage(userID, conversationID)
	result, err := b.FindByID(keyID)
	if err != nil {
		logrus.WithError(err).WithField("keyId", keyID).Errorln()
		return nil, err
	}
	var permission *baymax.PermissionViewConversationMessage
	if err = json.Unmarshal([]byte(result.(string)), &permission); err != nil {
		logrus.WithError(err).WithField("timeSlots", result).Errorln("error during unmarshal timeSlot")
		return nil, err
	}
	return permission, nil
}

// SavePermissionViewConversationMessageInCache is
func (b *RedisBaymaxStore) SavePermissionViewConversationMessageInCache(permission *baymax.PermissionViewConversationMessage) error {
	keyID := b.makeKeyIDForCheckPermissionViewConvMessage(permission.UserID, permission.ConversationID)
	marshalPermission, err := json.Marshal(permission)
	if err != nil {
		logrus.WithError(err).WithField("permission", permission).Errorln("failed to marshal permission data")
		return err
	}
	if err := b.Save(keyID, marshalPermission); err != nil {
		logrus.WithError(err).WithField("permission", permission).WithField("keyId", keyID).Errorln()
		return err
	}

	return nil
}

func (b *RedisBaymaxStore) makeKeyIDForCheckPermissionViewConvMessage(userID string, conversationID string) string {
	return "permission:" + userID + "_" + "view_conversation:" + conversationID + "_message"
}

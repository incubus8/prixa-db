package store

import (
	"context"
	"time"

	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/model/baymax"
	"prixa.ai/prixa-db/pkg/partner"
	"prixa.ai/prixa-db/pkg/user/userprofile"
)

// Store defines application activities
type Store interface {
	Analytic() AnalyticStore
	AppointmentBooking() AppointmentBookingStore
	Diagnostic() DiagnosticStore
	DiagnosticResult() DiagnosticStore
	Doctor() DoctorStore
	Hospital() HospitalStore
	User() UserStore
	Labtest() LabtestStore
	CrispUser() CrispUserStore
	Partner() PartnerStore
	PartnerApp() PartnerAppStore
	ChatRequest() ChatRequestStore
	ChatResult() ChatResultStore
	DoctorAllocation() DoctorAllocationStore
	Baymax() BaymaxStore
	SetContext(context context.Context)
	Context() context.Context
}

// AnalyticStore is
type AnalyticStore interface {
	// TraceEvent tracks user event from FE
	TrackEvent(data *Param)
}

// AppointmentBookingStore is
type AppointmentBookingStore interface {
	Save(payload interface{})
	// CancelAppointmentBooking tracks appointment booking activity
	CancelAppointmentBooking(data *Param)
}

// DiagnosticStore is
type DiagnosticStore interface {
	// SaveCovid19Form saves covid19 form submission
	SaveCovid19Form(data *Param)

	// SaveSurvey performs survey saving
	SaveSurvey(payload interface{})

	// SaveFeedbackQuestion performs feedback question saving
	SaveFeedbackQuestion(payload interface{})

	SaveDiagnosisResult(diagResData *bot.DiagnosisResData) error
	GetDiagnosisResult(key string) (*bot.DiagnosisResData, error)

	SaveSessionResult(key string, sr *bot.SessionResult) error
	GetSessionResult(key string) (bot.SessionResult, error)
}

// UserStore is
type UserStore interface {
	// TrackUserActivity tracks user activity
	TrackUserActivity(data *Param)

	// SaveUserPreconditions saves user's precondition
	SaveUserPreconditions(data *Param)

	SaveToken(userData *userprofile.RedisData) error
	GetToken(key string) (string, error)
	DeleteToken(key string) error

	SaveAdminPwd(userData *userprofile.RedisData) error
	GetAdminPwd(key string) (string, error)

	SaveAssessmentResult(properties userprofile.ProfileProperties)
	GetAssessmentResult() *userGRPC.GetAssessmentHistoryResponse
	GetPreconditionsList(email string) []*diagnosticGRPC.PreconditionsData

	GetUserAppointmentBookingData(email, startDate, endDate string) ([]*telemedicineGRPC.AppointmentBookingData, error)
}

// LabtestStore is
type LabtestStore interface {
	SaveCouponCode(email, token string) error
	GetCouponCode(email string) (string, error)
	SaveCouponToDB(payload interface{})
	GetCouponFromDB(email string) (*model.CouponData, error)
}

// CrispUserStore implements crisp specific user store
type CrispUserStore interface {
	SavePersonProfile(crispUserData *userprofile.CrispUserRedisData) error
	GetPersonProfile(key string) (*userprofile.CrispUserRedisData, error)
	DeletePersonProfile(email string) error
	SaveUserWebsiteID(email, websiteID string) error
	GetUserWebsiteID(email string) (string, error)
}

// PartnerStore is
type PartnerStore interface {
	SavePartner(partner *partner.Partner) (*partner.Partner, error)
	UpdatePartner(partner *partner.Partner) (*partner.Partner, error)
	GetAppIDs(partnerID string) []string
	FindPartnerByID(key string) (*partner.Partner, error)
	FindActivePartnerID(partnerID string) (*partner.Partner, error)
	FindAllPartners(from, to int64) ([]*partner.Partner, error)
	FindAllPartnersAtPage(pageNo int64) (*partner.PageablePartnerData, error)
}

// PartnerAppStore is
type PartnerAppStore interface {
	MakeKeyWithPartnerID(partnerID, ID string) string
	SavePartnerApp(partnerID string, partnerApp *partner.App) (*partner.App, error)
	FindPartnerAppByID(key string) (*partner.App, error)
	FindActivePartnerAppID(partnerID, partnerAppID string) (*partner.App, error)
	FindActivePartnerAndPartnerApp(partnerID, partnerAppID string) (*partner.Partner, *partner.App, error)
	FindAllPartnerApps(from, to int64) ([]*partner.App, error)
	UpdatePartnerApp(partnerID string, partnerApp *partner.App) (*partner.App, error)
	SaveAppMetadata(data partner.AppData) error
	GetAppMetadata(key string) (*partner.AppData, error)
	IsValidAncillary() bool
	GetAncillaryControl(partnerID, appID string) db.AncillaryControl
}

// DoctorStore is
type DoctorStore interface {
	SaveDoctorLeavesData(leavesData *model.DoctorLeaves) (*model.DoctorLeaves, error)
	FindDoctorLeavesData(hospitalID, doctorID string) (*model.DoctorLeaves, error)

	SaveDoctorTimeSlotsData(timeSlotData *model.DoctorTimeSlots) (*model.DoctorTimeSlots, error)
	FindDoctorTimeSlotsData(hospitalID, doctorID string, appointmentDate *time.Time) (*model.DoctorTimeSlots, error)
	RemoveDoctorTimeSlotsData(hospitalID, doctorID string, appointmentDate *time.Time) error
}

// HospitalStore is
type HospitalStore interface {
	GetAreaData(areaID string) (*telemedicineGRPC.AreaData, error)
	GetAreasData(pageNo int) ([]*telemedicineGRPC.AreaData, error)
	GetDoctorData(doctorID string) (*telemedicineGRPC.DoctorData, error)
	GetDoctorsData(pageNo int, param *telemedicineGRPC.GetDoctorParamData) ([]*telemedicineGRPC.DoctorData, error)
	GetHospitalData(hospitalID string) (*telemedicineGRPC.HospitalData, error)
	GetHospitalsData(pageNo int, param *telemedicineGRPC.GetHospitalParamData) ([]*telemedicineGRPC.HospitalData, error)
	GetHospitalsDataFilterByDistance(pageNo int, latitude float32, longitude float32, filterKm float32) ([]*telemedicineGRPC.HospitalData, error)
	GetSpecialityData(specialityID string) (*telemedicineGRPC.SpecialityData, error)
	GetSpecialitiesData(pageNo int, name string) ([]*telemedicineGRPC.SpecialityData, error)
	GetSpecialitiesDataByParam(pageNo int, areaID string, hospitalID string, name string) ([]*telemedicineGRPC.SpecialityData, error)
	GetDoctorSchedulesData(pageNo int, doctorID string, hospitalID string) ([]*telemedicineGRPC.ScheduleData, error)
}

// ChatRequestStore is
type ChatRequestStore interface {
	PublishChatRequest(request *model.ChatRequest)
	PublishResolvedChatRequest(request *model.ResolveChatRequest)
	PublishSetDoctorStatusRequest(request *model.SetDoctorStatusRequest)
}

// ChatResultStore is
type ChatResultStore interface {
	ProcessingChatRequest(request *model.ChatRequest) error
	CreateChatResult(key string, chat *model.ChatResult) error
	FindChatResultByID(key string) (*model.ChatResult, error)
	ProcessingResolveChatRequest(request *model.ResolveChatRequest) error
	ResetIdleTimeoutToDefault(key string) error
}

// DoctorAllocationStore is
type DoctorAllocationStore interface {
	SetDoctorStatus(request *model.SetDoctorStatusRequest) error
}

// BaymaxStore is
type BaymaxStore interface {
	ValidateConversationTransactionDoctor(ctx context.Context, conversationID string) (*baymax.Conversation, *baymax.Transaction, *baymax.Doctor, error)
	ParseWebsiteTokenCwConversation(ctx context.Context, websiteToken string, cwConversation string) (*baymax.Conversation, *baymax.Transaction, error)
	GetPatientData(patientID string) (*baymax.Patient, error)
	GetActiveTransactionByPatient(patientID string, transactionID string) ([]*baymax.ActiveTransaction, error)
	SendMessage(ctx context.Context, data *baymax.MessageData) error
	GetListOfPermission(userID string) ([]*baymax.Permission, error)
	GetConversationMessages(conversationID string, offset string) ([]*baymax.MessageData, error)
	GetConversationData(conversationID string) (*baymax.Conversation, error)
	IsDoctorExistInClient(doctorID string, clientID string) (bool, error)
	IsExternalUserExistInClient(userID string, clientID string) (bool, error)
	SavePermissionViewConversationMessageInCache(permission *baymax.PermissionViewConversationMessage) error
	HasPermissionViewConversationMessageInCache(userID string, conversationID string) (*baymax.PermissionViewConversationMessage, error)
}

// Param is
type Param struct {
	Ctx         context.Context
	Email       string
	UserName    string
	Description string
	EventType   string
	EventName   string
	SentAt      string
	Context     *analyticGRPC.ContextFields
	Metadata    map[string]string
}

// SuccessEvent is
const SuccessEvent = "SUCCESS_EVENT"

// ErrorEvent is
const ErrorEvent = "ERROR_EVENT"

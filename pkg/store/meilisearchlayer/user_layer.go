package meilisearchlayer

import (
	"encoding/json"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	meili "github.com/meilisearch/meilisearch-go"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/store"
)

// MeiliUserStore is
type MeiliUserStore struct {
	store.UserStore
	rootStore *MeiliStore
}

const (
	// TableAssessment is
	TableAssessment = "user-assessment-history"

	// TableUserPreconditions is
	TableUserPreconditions = "user-preconditions"
)

// GetAssessmentResult is
func (ms *MeiliUserStore) GetAssessmentResult() *userGRPC.GetAssessmentHistoryResponse {
	ctx := ms.rootStore.Context()
	meta := metautils.ExtractIncoming(ctx)
	email := meta.Get("email")

	table := TableAssessment
	if core.IsForCovid19() {
		table = fmt.Sprintf("covid_%v", TableAssessment)
	}

	resp, err := ms.rootStore.msClient.Search(table).Search(meili.SearchRequest{
		Query:   email,
		Filters: fmt.Sprintf("email='%v'", email),
	})
	if err != nil {
		logrus.WithError(err).Errorln()
		return &userGRPC.GetAssessmentHistoryResponse{AssessmentHistory: nil}
	}

	assessmentHistory := []*userGRPC.AssessmentHistory{}
	for _, hit := range resp.Hits {
		history := mapToHistoryResponse(hit.(map[string]interface{}))
		assessmentHistory = append(assessmentHistory, history)
	}

	return &userGRPC.GetAssessmentHistoryResponse{
		AssessmentHistory: assessmentHistory,
	}
}

func mapToHistoryResponse(data map[string]interface{}) *userGRPC.AssessmentHistory {
	datetime := cast.ToStringMap(data["datetime"])
	return &userGRPC.AssessmentHistory{
		Email: cast.ToString(data["email"]),
		Datetime: &timestamp.Timestamp{
			Seconds: cast.ToInt64(datetime["seconds"]),
		},
		ChiefComplaint: cast.ToString(data["chiefComplaint"]),
		Name:           cast.ToString(data["name"]),
		Age:            cast.ToInt32(data["age"]),
		Gender:         cast.ToString(data["gender"]),
	}
}

// GetPreconditionsList get user preconditions
func (ms *MeiliUserStore) GetPreconditionsList(email string) []*diagnosticGRPC.PreconditionsData {
	table := TableUserPreconditions
	if core.IsForCovid19() {
		table = fmt.Sprintf("covid_%v", TableUserPreconditions)
	}
	resp, err := ms.rootStore.msClient.Search(table).Search(meili.SearchRequest{
		Query:   email,
		Filters: fmt.Sprintf("email='%v'", email),
	})
	if err != nil {
		logrus.WithError(err).Errorln()
		return nil
	}

	var preconditions []*diagnosticGRPC.PreconditionsData
	for _, hit := range resp.Hits {
		metadata := cast.ToStringMap(cast.ToStringMap(hit)["metadata"])
		err := json.Unmarshal([]byte(metadata["preconditions"].(string)), &preconditions)
		if err != nil {
			continue
		}
		break
	}
	return preconditions
}

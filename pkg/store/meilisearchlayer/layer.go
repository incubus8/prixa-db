package meilisearchlayer

import (
	"strings"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
)

// MeiliStore is
type MeiliStore struct {
	store.Store
	msClient *meili.Client
	pageSize int

	user     *MeiliUserStore
	hospital *MeiliHospitalStore
}

// NewMeiliStore instantiate new MeiliStore
func NewMeiliStore(baseStore store.Store, msClient *meili.Client) store.Store {
	store := MeiliStore{
		Store:    baseStore,
		msClient: msClient,
		pageSize: 25,
	}
	store.user = &MeiliUserStore{
		UserStore: baseStore.User(),
		rootStore: &store,
	}
	store.hospital = &MeiliHospitalStore{
		HospitalStore: baseStore.Hospital(),
		rootStore:     &store,
	}

	return store
}

func (s *MeiliStore) getOffsetNo(pageNo int) int {
	var from int
	if pageNo > 1 {
		from = (pageNo - 1) * s.pageSize
	}
	return from
}

func (s *MeiliStore) updateRankingRules(index string, newRules []string) error {
	_, err := s.msClient.Settings(index).UpdateRankingRules(append(newRules, sdk.DefaultRules...))
	if err != nil {
		return err
	}
	return nil
}

func (s *MeiliStore) search(index string, searchRequest meili.SearchRequest) (*meili.SearchResponse, error) {
	resp, err := s.msClient.Search(index).Search(searchRequest)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// getDocs with pagination of limit and offset
func (s *MeiliStore) getDocs(index string, offset int) ([]map[string]interface{}, error) {
	var fetchedDocs []map[string]interface{}
	err := s.msClient.Documents(strings.ToLower(index)).List(meili.ListDocumentsRequest{
		Limit:  cast.ToInt64(s.pageSize),
		Offset: cast.ToInt64(offset),
	}, &fetchedDocs)
	if err != nil {
		return nil, err
	}

	return fetchedDocs, nil
}

func (s *MeiliStore) getDocByID(index, id string) (map[string]interface{}, error) {
	var doc map[string]interface{}
	err := s.msClient.Documents(index).Get(id, &doc)
	if err != nil {
		return nil, err
	}
	return doc, nil
}

// User is
func (s MeiliStore) User() store.UserStore {
	return s.user
}

// Hospital is
func (s MeiliStore) Hospital() store.HospitalStore {
	return s.hospital
}

package meilisearchlayer

import (
	"fmt"
	"sort"
	"strings"

	meili "github.com/meilisearch/meilisearch-go"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"

	"prixa.ai/prixa-db/pkg/store"
)

const (
	tableSpecialities = "specialities"
	tableDoctors      = "doctors"
	tableAreas        = "areas"
	tableHospitals    = "hospitals"
	tableSchedules    = "doctorschedules"
)

// MeiliHospitalStore is
type MeiliHospitalStore struct {
	store.HospitalStore
	rootStore *MeiliStore
}

// GetAreaData is Get Area Data By its document ID
func (s *MeiliHospitalStore) GetAreaData(areaID string) (*telemedicineGRPC.AreaData, error) {
	ctx := s.rootStore.Store.Context()
	doc, err := s.rootStore.getDocByID(tableAreas, areaID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableAreas).WithField("areaId", areaID).Errorln()
		return nil, err
	}

	areaData := mapAreaData(doc)
	return areaData, nil
}

func mapAreaData(data map[string]interface{}) *telemedicineGRPC.AreaData {
	return &telemedicineGRPC.AreaData{
		Id:          cast.ToString(data["id"]),
		Name:        cast.ToString(data["name"]),
		ImageUrl:    cast.ToString(data["imageUrl"]),
		LastUpdated: cast.ToString(data["lastUpdated"]),
		Source:      mapSourceData(cast.ToStringMap(data["source"])),
	}
}

func mapSourceData(data map[string]interface{}) *telemedicineGRPC.SourceData {
	return &telemedicineGRPC.SourceData{
		SourceId:   cast.ToString(data["sourceId"]),
		OriginId:   cast.ToString(data["originId"]),
		SourceName: cast.ToString(data["sourceName"]),
	}
}

// GetAreasData is
func (s *MeiliHospitalStore) GetAreasData(pageNo int) ([]*telemedicineGRPC.AreaData, error) {
	ctx := s.rootStore.Store.Context()

	from := s.rootStore.getOffsetNo(pageNo)
	err := s.rootStore.updateRankingRules(tableAreas, []string{"desc(name)"})
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableAreas).Errorln()
		return nil, err
	}
	fetchedDocs, err := s.rootStore.getDocs(tableAreas, from)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableAreas).Errorln()
		return nil, err
	}

	areaList := []*telemedicineGRPC.AreaData{}
	for _, doc := range fetchedDocs {
		areaData := mapAreaData(doc)
		areaList = append(areaList, areaData)
	}
	_, err = s.rootStore.msClient.Settings(tableAreas).ResetRankingRules()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableAreas).Errorln("failed to reset ranking rule")
	}
	return areaList, nil
}

// GetDoctorData is Get Doctor Data By its document ID
func (s *MeiliHospitalStore) GetDoctorData(doctorID string) (*telemedicineGRPC.DoctorData, error) {
	ctx := s.rootStore.Store.Context()

	doc, err := s.rootStore.getDocByID(tableDoctors, doctorID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableDoctors).WithField("doctorID", doctorID).Errorln()
		return nil, err
	}

	doctorData := mapDoctorData(doc)
	return doctorData, err
}

func mapDoctorData(data map[string]interface{}) *telemedicineGRPC.DoctorData {
	hospitalsData := []*telemedicineGRPC.HospitalData{}
	hospitals := cast.ToSlice(data["hospitals"])
	for _, hospital := range hospitals {
		hospitalsData = append(hospitalsData, mapHospitalData(cast.ToStringMap(hospital)))
	}
	return &telemedicineGRPC.DoctorData{
		Id:          cast.ToString(data["id"]),
		Name:        cast.ToString(data["name"]),
		Speciality:  mapSpecialityData(cast.ToStringMap(data["speciality"])),
		Hospitals:   hospitalsData,
		Source:      mapSourceData(cast.ToStringMap(data["source"])),
		LastUpdated: cast.ToString(data["lastUpdated"]),
	}
}

func mapSpecialityData(data map[string]interface{}) *telemedicineGRPC.SpecialityData {
	return &telemedicineGRPC.SpecialityData{
		Id:          cast.ToString(data["id"]),
		Name:        cast.ToString(data["name"]),
		NameIndo:    cast.ToString(data["nameIndo"]),
		ImageUrl:    cast.ToString(data["imageUrl"]),
		LastUpdated: cast.ToString(data["lastUpdated"]),
		Source:      mapSourceData(cast.ToStringMap(data["source"])),
	}
}

func mapHospitalData(data map[string]interface{}) *telemedicineGRPC.HospitalData {
	specialitiesData := []*telemedicineGRPC.SpecialityData{}
	specialities := cast.ToSlice(data["specialities"])
	for _, speciality := range specialities {
		specialitiesData = append(specialitiesData, mapSpecialityData(cast.ToStringMap(speciality)))
	}
	return &telemedicineGRPC.HospitalData{
		Id:           cast.ToString(data["id"]),
		Name:         cast.ToString(data["name"]),
		Alias:        cast.ToString(data["nameIndo"]),
		Address:      mapHospitalAddressData(cast.ToStringMap(data["address"])),
		Contact:      mapHospitalContactData(cast.ToStringMap(data["contact"])),
		AreaId:       cast.ToString(data["areaId"]),
		Specialities: specialitiesData,
	}
}

func mapHospitalAddressData(data map[string]interface{}) *telemedicineGRPC.AddressData {
	return &telemedicineGRPC.AddressData{
		ZipCode:     cast.ToString(data["zipCode"]),
		Village:     cast.ToString(data["village"]),
		SubDistrict: cast.ToString(data["subDistrict"]),
		District:    cast.ToString(data["district"]),
		Province:    cast.ToString(data["province"]),
		Street:      cast.ToString(data["street"]),
	}
}

func mapHospitalContactData(data map[string]interface{}) *telemedicineGRPC.ContactData {
	return &telemedicineGRPC.ContactData{
		PhoneNumber: cast.ToString(data["phoneNumber"]),
		Email:       cast.ToString(data["email"]),
	}
}

// GetDoctorsData is
func (s *MeiliHospitalStore) GetDoctorsData(pageNo int, param *telemedicineGRPC.GetDoctorParamData) ([]*telemedicineGRPC.DoctorData, error) {
	from := s.rootStore.getOffsetNo(pageNo)
	queries := setQueryHospitals(param)

	queries.Limit = cast.ToInt64(s.rootStore.pageSize)
	queries.Offset = cast.ToInt64(from)

	ctx := s.rootStore.Store.Context()
	resp, err := s.rootStore.search(tableDoctors, queries)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableDoctors).WithField("queries", queries).Errorln()
		return nil, err
	}

	doctorsData := []*telemedicineGRPC.DoctorData{}
	for _, hit := range resp.Hits {
		doctorsData = append(doctorsData, mapDoctorData(cast.ToStringMap(hit)))
	}
	return doctorsData, nil
}

func setQueryHospitals(param *telemedicineGRPC.GetDoctorParamData) meili.SearchRequest {
	searchRequest := meili.SearchRequest{}
	var (
		filters      []string
		facetFilters []interface{}
	)

	if param == nil {
		/*
			Meilisearch uses Query as a mandatory field for searching.
			Leaving it empty and use filters only or in our case, our queries is impossible at the moment.
			Therefore, char "a" used as the query "hacks" instead.
		*/
		searchRequest.Query = "a"
		return searchRequest
	}
	if param.AreaId != "" {
		facetFilters = append(facetFilters, fmt.Sprintf("hospitalsAreaID:%v", param.AreaId))
	}
	if len(param.Specialities) > 0 {
		for _, speciality := range param.Specialities {
			filters = append(filters, fmt.Sprintf("specialityID = '%v'", speciality))
		}
	}
	if len(param.Hospitals) > 0 {
		var hospitalsFacet []string
		for _, hospital := range param.Hospitals {
			hospitalsFacet = append(hospitalsFacet, fmt.Sprintf("hospitalsID:%v", hospital))
		}
		facetFilters = append(facetFilters, hospitalsFacet)
	}

	if param.NameKeyword != "" {
		searchRequest.Query = param.NameKeyword
	} else {
		/*
			Meilisearch uses Query as a mandatory field for searching.
			Leaving it empty and use filters only or in our case, our queries is impossible at the moment.
			Therefore, char "a" used as the query "hacks" instead.
		*/
		searchRequest.Query = "a"
	}
	searchRequest.Filters = strings.Join(filters, " OR ")
	searchRequest.FacetFilters = facetFilters

	return searchRequest
}

// GetHospitalData is Get Hospital Data By its document ID
func (s *MeiliHospitalStore) GetHospitalData(hospitalID string) (*telemedicineGRPC.HospitalData, error) {
	ctx := s.rootStore.Store.Context()

	doc, err := s.rootStore.getDocByID(tableHospitals, hospitalID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableHospitals).WithField("doctorID", hospitalID).Errorln()
		return nil, err
	}

	hospitalData := mapHospitalData(doc)
	return hospitalData, nil
}

// GetHospitalsData is
func (s *MeiliHospitalStore) GetHospitalsData(pageNo int, param *telemedicineGRPC.GetHospitalParamData) ([]*telemedicineGRPC.HospitalData, error) {
	from := s.rootStore.getOffsetNo(pageNo)
	queries := meili.SearchRequest{}
	var (
		filters      string
		facetFilters []interface{}
	)

	queries.Query = "a"
	if param != nil {
		if param.AreaId != "" {
			filters = fmt.Sprintf("areaId = '%v'", param.AreaId)
		}
		if len(param.Specialities) > 0 {
			var specialitiesFacet []string
			for _, speciality := range param.Specialities {
				specialitiesFacet = append(specialitiesFacet, fmt.Sprintf("specialitiesID:%v", speciality))
			}
			facetFilters = append(facetFilters, specialitiesFacet)
		}
		if param.NameKeyword != "" {
			queries.Query = param.NameKeyword
		}
		queries.Filters = filters
		queries.FacetFilters = facetFilters
	}

	queries.Limit = cast.ToInt64(s.rootStore.pageSize)
	queries.Offset = cast.ToInt64(from)

	ctx := s.rootStore.Store.Context()
	resp, err := s.rootStore.search(tableHospitals, queries)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableHospitals).WithField("queries", queries).Errorln()
		return nil, err
	}

	hospitalData := []*telemedicineGRPC.HospitalData{}
	for _, hit := range resp.Hits {
		hospitalData = append(hospitalData, mapHospitalData(cast.ToStringMap(hit)))
	}
	return hospitalData, nil
}

// GetHospitalsDataFilterByDistance is
func (s *MeiliHospitalStore) GetHospitalsDataFilterByDistance(pageNo int, latitude float32, longitude float32, filterKm float32) ([]*telemedicineGRPC.HospitalData, error) {
	ctx := s.rootStore.Store.Context()
	from := s.rootStore.getOffsetNo(pageNo)

	fetchedDocs, err := s.rootStore.getDocs(tableHospitals, from)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableHospitals).Errorln()
		return nil, err
	}

	hospitalData := []*telemedicineGRPC.HospitalData{}
	for _, doc := range fetchedDocs {
		hospitalData = append(hospitalData, mapHospitalData(cast.ToStringMap(doc)))
	}
	return hospitalData, nil
}

// GetSpecialityData is Get Speciality Data By its document ID
func (s *MeiliHospitalStore) GetSpecialityData(specialityID string) (*telemedicineGRPC.SpecialityData, error) {
	ctx := s.rootStore.Store.Context()
	doc, err := s.rootStore.getDocByID(tableSpecialities, specialityID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableHospitals).WithField("specialityID", specialityID).Errorln()
		return nil, err
	}

	specialityData := mapSpecialityData(cast.ToStringMap(doc))
	return specialityData, nil
}

// GetSpecialitiesData is
func (s *MeiliHospitalStore) GetSpecialitiesData(pageNo int, name string) ([]*telemedicineGRPC.SpecialityData, error) {
	ctx := s.rootStore.Store.Context()
	from := s.rootStore.getOffsetNo(pageNo)

	if name == "" {
		name = "a"
	}
	searchRequest := meili.SearchRequest{
		Query:  strings.ToLower(name),
		Offset: cast.ToInt64(from),
		Limit:  cast.ToInt64(s.rootStore.pageSize),
	}

	resp, err := s.rootStore.search(tableSpecialities, searchRequest)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableSpecialities).WithField("queries", searchRequest).Errorln()
		return nil, err
	}

	specialitiesData := []*telemedicineGRPC.SpecialityData{}
	for _, hit := range resp.Hits {
		specialitiesData = append(specialitiesData, mapSpecialityData(cast.ToStringMap(hit)))
	}
	return specialitiesData, nil
}

// GetSpecialitiesDataByParam is
func (s *MeiliHospitalStore) GetSpecialitiesDataByParam(pageNo int, areaID, hospitalID, name string) ([]*telemedicineGRPC.SpecialityData, error) {
	from := s.rootStore.getOffsetNo(pageNo)
	searchRequest := meili.SearchRequest{
		Query:  strings.ToLower(name),
		Offset: cast.ToInt64(from),
		Limit:  cast.ToInt64(s.rootStore.pageSize),
	}

	ctx := s.rootStore.Store.Context()
	resp, err := s.rootStore.search(tableHospitals, searchRequest)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableSpecialities).WithField("queries", searchRequest).Errorln()
		return nil, err
	}

	var specialities []*telemedicineGRPC.SpecialityData
	for _, hit := range resp.Hits {
		hospitalData := mapHospitalData(cast.ToStringMap(hit))
		for _, speciality := range hospitalData.Specialities {
			if !strings.Contains(strings.ToLower(speciality.NameIndo), strings.ToLower(name)) {
				continue
			}
			var exist bool
			for _, existData := range specialities {
				if strings.EqualFold(speciality.Id, existData.Id) {
					exist = true
					break
				}
			}
			if !exist {
				specialities = append(specialities, speciality)
			}
		}
	}
	specialities = sortSpecialityByName(specialities, func(a, b *telemedicineGRPC.SpecialityData) bool {
		return a.NameIndo > b.NameIndo
	})
	if from > len(specialities) {
		return []*telemedicineGRPC.SpecialityData{}, nil
	}
	to := from + s.rootStore.pageSize
	if to > len(specialities) {
		to = len(specialities)
	}
	specialities = specialities[from:to]

	return specialities, nil
}

func sortSpecialityByName(s []*telemedicineGRPC.SpecialityData, fn func(*telemedicineGRPC.SpecialityData, *telemedicineGRPC.SpecialityData) bool) []*telemedicineGRPC.SpecialityData {
	sort.Slice(s, func(a, b int) bool {
		return fn(s[a], s[b])
	})

	return s
}

// GetDoctorSchedulesData is
func (s *MeiliHospitalStore) GetDoctorSchedulesData(pageNo int, doctorID string, hospitalID string) ([]*telemedicineGRPC.ScheduleData, error) {
	from := s.rootStore.getOffsetNo(pageNo)

	// search by coordinate not yet implemented
	searchRequest := meili.SearchRequest{
		Filters: fmt.Sprintf("hospitalID = '%v' AND doctorID = '%v'", hospitalID, doctorID),
		Query:   "a",
		Offset:  cast.ToInt64(from),
		Limit:   cast.ToInt64(s.rootStore.pageSize),
	}

	ctx := s.rootStore.Store.Context()
	resp, err := s.rootStore.search(tableSchedules, searchRequest)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableSchedules).WithField("queries", searchRequest).Errorln()
		return nil, err
	}

	schedulesData := []*telemedicineGRPC.ScheduleData{}
	for _, hit := range resp.Hits {
		schedulesData = append(schedulesData, mapScheduleData(cast.ToStringMap(hit)))
	}
	return schedulesData, nil
}

func mapScheduleData(data map[string]interface{}) *telemedicineGRPC.ScheduleData {
	return &telemedicineGRPC.ScheduleData{
		Id:       cast.ToString(data["id"]),
		Doctor:   mapDoctorData(cast.ToStringMap(data["id"])),
		Hospital: mapHospitalData(cast.ToStringMap(data["id"])),
		Day:      cast.ToInt32(data["day"]),
		FromTime: cast.ToString(data["fromTime"]),
		ToTime:   cast.ToString(data["toTime"]),
		Source:   mapSourceData(cast.ToStringMap(data["source"])),
	}
}

package gcloud

import (
	"context"
	"errors"
	"io/ioutil"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2/google"
	"golang.org/x/oauth2/jwt"
	iam "google.golang.org/api/iam/v1"
	"google.golang.org/api/option"
)

var (
	// IAMService is
	_ *iam.Service

	// UploadableBucket is the name of the bucket use in google cloud storage
	UploadableBucket = os.Getenv("UPLOAD_BUCKET_NAME")

	// SAConfig holds the value from the private key used in the data transaction in google Cloud
	SAConfig *jwt.Config

	// ErrInvalidContentType holds the error value for mismatch content type
	ErrInvalidContentType = errors.New("invalid content type")

	// SignedURLExpirationTime holds the expiration time for created signedURL
	SignedURLExpirationTime = time.Now().Add(10 * time.Minute)
)

// SetGoogleCloudCredentials sets gcloud credentials for google cloud functions
func SetGoogleCloudCredentials(ctx context.Context) {
	httpClient, err := google.DefaultClient(ctx, iam.CloudPlatformScope)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
	}

	_, err = iam.NewService(ctx, option.WithHTTPClient(httpClient))
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
	}

	filePath := "./data/key.json"
	saKey, errReadFile := ioutil.ReadFile(filePath)
	if errReadFile != nil {
		logrus.WithError(errReadFile).WithContext(ctx).Error()
	}

	SAConfig, err = google.JWTConfigFromJSON(saKey)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
	}
}

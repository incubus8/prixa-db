//revive:disable
//lint:file-ignore U1001 for Google Cloud integration, remove when already implemented
package gcloud

//revive:enable

import (
	"context"
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"

	"cloud.google.com/go/storage"
	"github.com/sirupsen/logrus"
)

// GCSEvent is a struct that holds the event info for triggered events
type GCSEvent struct {
	Bucket string `json:"bucket"`
	Name   string `json:"name"`
}

var (
	errRetryable     = errors.New("upload: retryable error")
	errLargeFileSize = errors.New("upload: image file is too large")
)

const mbSize = 3

func validate(ctx context.Context, obj *storage.ObjectHandle) error {
	attrs, err := obj.Attrs(ctx)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
		return errRetryable
	}

	if attrs.Size >= 1024*mbSize {
		logrus.WithError(errLargeFileSize).WithContext(ctx).Error()
		return errLargeFileSize
	}
	// Validates obj and returns true if it conforms supported image formats.
	if err := validateMIMEType(ctx, attrs, obj); err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
		return err
	}
	return nil
}

func validateMIMEType(ctx context.Context, attrs *storage.ObjectAttrs, obj *storage.ObjectHandle) error {
	r, err := obj.NewReader(ctx)
	if err != nil {
		return errRetryable
	}
	defer func() {
		errClose := r.Close()
		if errClose != nil {
			logrus.WithError(errClose).WithContext(ctx).Error()
		}
	}()

	if _, err := func(ct string) (image.Image, error) {
		switch ct {
		case "image/png":
			return png.Decode(r)
		case "image/jpeg", "image/jpg":
			return jpeg.Decode(r)
		case "image/gif":
			return gif.Decode(r)
		default:
			logrus.WithError(ErrInvalidContentType).WithContext(ctx).Error()
			return nil, ErrInvalidContentType
		}
	}(attrs.ContentType); err != nil {
		return err
	}
	return nil
}

// ValidateUploads validates the object and copy it into the distribution bucket.
func ValidateUploads(ctx context.Context, e GCSEvent) error {
	client, err := storage.NewClient(ctx)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Error()
		return err
	}
	defer func() {
		errClose := client.Close()
		if errClose != nil {
			logrus.WithError(errClose).WithContext(ctx).Error()
		}
	}()

	if err != storage.ErrObjectNotExist {
		return err
	}
	src := client.Bucket(e.Bucket).Object(e.Name)
	if err := validate(ctx, src); err != nil {
		if err == errRetryable {
			return err
		}
		if errDelete := src.Delete(ctx); errDelete != nil {
			return errDelete
		}
	}

	return nil
}

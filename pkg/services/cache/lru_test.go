package cache

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestLRU(t *testing.T) {
	l := NewLRU(&LRUOptions{
		Size:          128,
		DefaultExpiry: 0,
	})

	for i := 0; i < 256; i++ {
		err := l.Set(fmt.Sprintf("%d", i), i)
		require.Nil(t, err)
	}
	size, err := l.Len()
	require.Nil(t, err)
	require.Equalf(t, size, 128, "bad len: %v", size)

	keys, err := l.Keys()
	require.Nil(t, err)
	for i, k := range keys {
		var v int
		err = l.Get(k, &v)
		require.Nil(t, err, "bad key: %v", k)
		require.Equalf(t, fmt.Sprintf("%d", v), k, "bad key: %v", k)
		require.Equalf(t, i+128, v, "bad value: %v", k)
	}
	for i := 0; i < 128; i++ {
		var v int
		err = l.Get(fmt.Sprintf("%d", i), &v)
		require.Equal(t, ErrKeyNotFound, err, "should be evicted %v: %v", i, err)
	}
	for i := 128; i < 256; i++ {
		var v int
		err = l.Get(fmt.Sprintf("%d", i), &v)
		require.Nil(t, err, "should not be evicted %v: %v", i, err)
	}
	for i := 128; i < 192; i++ {
		l.Remove(fmt.Sprintf("%d", i))
		var v int
		err = l.Get(fmt.Sprintf("%d", i), &v)
		require.Equal(t, ErrKeyNotFound, err, "should be deleted %v: %v", i, err)
	}

	var v int
	err = l.Get("192", &v) // expect 192 to be last key in l.Keys()
	require.Nil(t, err, "should exist")
	require.Equalf(t, 192, v, "bad value: %v", v)

	keys, err = l.Keys()
	require.Nil(t, err)
	for i, k := range keys {
		require.Falsef(t, i < 63 && k != fmt.Sprintf("%d", i+193), "out of order key: %v", k)
		require.Falsef(t, i == 63 && k != "192", "out of order key: %v", k)
	}

	l.Purge()
	size, err = l.Len()
	require.Nil(t, err)
	require.Equalf(t, size, 0, "bad len: %v", size)
	err = l.Get("200", &v)
	require.Equal(t, err, ErrKeyNotFound, "should contain nothing")

	err = l.Set("201", 301)
	require.Nil(t, err)
	err = l.Get("201", &v)
	require.Nil(t, err)
	require.Equal(t, 301, v)
}

func TestLRUExpire(t *testing.T) {
	l := NewLRU(&LRUOptions{
		Size:          128,
		DefaultExpiry: 1 * time.Second,
	})

	l.SetWithDefaultExpiry("1", 1)
	l.SetWithExpiry("3", 3, 0*time.Second)

	time.Sleep(time.Second * 2)

	var r1 int
	err := l.Get("1", &r1)
	require.Equal(t, err, ErrKeyNotFound, "should not exist")

	var r2 int
	err2 := l.Get("3", &r2)
	require.Nil(t, err2, "should exist")
	require.Equal(t, 3, r2)
}

func TestLRUZeroExpire(t *testing.T) {
	l := NewLRU(&LRUOptions{
		Size:          128,
		DefaultExpiry: 1 * time.Second,
	})

	l.SetWithDefaultExpiry("1", 1)
	l.SetWithExpiry("3", 3, 0)

	time.Sleep(time.Second * 2)

	var r1 int
	err := l.Get("1", &r1)
	require.Equal(t, err, ErrKeyNotFound, "should not exist")

	var r2 int
	err2 := l.Get("3", &r2)
	require.Nil(t, err2, "should exist")
	require.Equal(t, 3, r2)
}

func TestInvalidValue(t *testing.T) {
	l := NewLRU(&LRUOptions{
		Size:          128,
		DefaultExpiry: 1 * time.Second,
	})

	err := l.Set("1", nil)
	require.Error(t, err, "gob: cannot encode nil value")
}

func TestLRUMarshalUnMarshal(t *testing.T) {
	l := NewLRU(&LRUOptions{
		Size:          1,
		DefaultExpiry: 0,
	})

	value1 := map[string]interface{}{
		"key1": 1,
		"key2": "value2",
	}
	err := l.Set("test", value1)

	require.Nil(t, err)

	var value2 map[string]interface{}
	err = l.Get("test", &value2)
	require.Nil(t, err)

	v1, ok := value2["key1"].(int)
	require.True(t, ok, "unable to cast value")
	require.Equal(t, 1, v1)

	v2, ok := value2["key2"].(string)
	require.True(t, ok, "unable to cast value")
	require.Equal(t, "value2", v2)
}

package filestore

import (
	"fmt"
	"io"
)

// ReadCloseSeeker is
type ReadCloseSeeker interface {
	io.ReadCloser
	io.Seeker
}

// FileBackend is
type FileBackend interface {
	TestConnection() error

	Reader(path string) (ReadCloseSeeker, error)
	ReadFile(path string) ([]byte, error)
	FileExists(path string) (bool, error)
	CopyFile(oldPath, newPath string) error
	MoveFile(oldPath, newPath string) error
	WriteFile(fr io.Reader, path string) (int64, error)
	RemoveFile(path string) error

	ListDirectory(path string) (*[]string, error)
	RemoveDirectory(path string) error
}

// FileSettings is
type FileSettings struct {
	EnableFileAttachments   bool
	EnableMobileUpload      bool
	EnableMobileDownload    bool
	MaxFileSize             int64
	DriverName              string
	Directory               string
	EnablePublicLink        bool
	PublicLinkSalt          string
	InitialFont             string
	AmazonS3AccessKeyID     string
	AmazonS3SecretAccessKey string
	AmazonS3Bucket          string
	AmazonS3Region          string
	AmazonS3Endpoint        string
	AmazonS3SSL             bool
	AmazonS3SignV2          bool
	AmazonS3SSE             bool
	AmazonS3Trace           bool
}

// NewFileBackend is
func NewFileBackend(settings *FileSettings) (FileBackend, error) {
	switch settings.DriverName {
	case "amazons3":
		return &S3FileBackend{
			endpoint:  settings.AmazonS3Endpoint,
			accessKey: settings.AmazonS3AccessKeyID,
			secretKey: settings.AmazonS3SecretAccessKey,
			secure:    settings.AmazonS3SSL,
			signV2:    settings.AmazonS3SignV2,
			region:    settings.AmazonS3Region,
			bucket:    settings.AmazonS3Bucket,
			encrypt:   settings.AmazonS3SSE,
			trace:     settings.AmazonS3Trace,
		}, nil
	case "local":
		return &LocalFileBackend{
			directory: settings.Directory,
		}, nil
	}

	return nil, fmt.Errorf("invalid filestore driver")
}

package filestore

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/util"
)

const (
	// TestFilePath defines test path
	TestFilePath = "/testfile"
)

// LocalFileBackend defines local file backend directory
type LocalFileBackend struct {
	directory string
}

// TestConnection tests connection
func (b *LocalFileBackend) TestConnection() error {
	f := bytes.NewReader([]byte("testingwrite"))
	if _, err := writeFileLocally(f, filepath.Join(b.directory, TestFilePath)); err != nil {
		return err
	}

	_ = os.Remove(filepath.Join(b.directory, TestFilePath))
	logrus.Debugln("Able to write files to local storage.")

	return nil
}

// Reader reads file
func (b *LocalFileBackend) Reader(path string) (ReadCloseSeeker, error) {
	f, err := os.Open(filepath.Join(b.directory, filepath.Clean(path)))
	if err != nil {
		return nil, err
	}
	return f, nil
}

// ReadFile reads file from a given path
func (b *LocalFileBackend) ReadFile(path string) ([]byte, error) {
	f, err := ioutil.ReadFile(filepath.Join(b.directory, filepath.Clean(path)))
	if err != nil {
		return nil, err
	}
	return f, nil
}

// FileExists checks if file exists in a given path
func (b *LocalFileBackend) FileExists(path string) (bool, error) {
	_, err := os.Stat(filepath.Join(b.directory, path))

	if os.IsNotExist(err) {
		return false, nil
	}

	if err != nil {
		return false, err
	}
	return true, nil
}

// CopyFile copies file from old path to new path
func (b *LocalFileBackend) CopyFile(oldPath, newPath string) error {
	return util.CopyFile(filepath.Join(b.directory, oldPath), filepath.Join(b.directory, newPath))
}

// MoveFile moves file from old path to new path
func (b *LocalFileBackend) MoveFile(oldPath, newPath string) error {
	if err := os.MkdirAll(filepath.Dir(filepath.Join(b.directory, newPath)), 0750); err != nil {
		return err
	}

	return os.Rename(filepath.Join(b.directory, oldPath), filepath.Join(b.directory, newPath))
}

// WriteFile writes file from a given path and io.Reader
func (b *LocalFileBackend) WriteFile(fr io.Reader, path string) (int64, error) {
	return writeFileLocally(fr, filepath.Join(b.directory, path))
}

func writeFileLocally(fr io.Reader, path string) (int64, error) {
	if err := os.MkdirAll(filepath.Dir(path), 0750); err != nil {
		directory, _ := filepath.Abs(filepath.Dir(path))
		return 0, fmt.Errorf("err write to dir: %v", directory)
	}
	fw, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return 0, err
	}

	defer func() {
		if e := fw.Close(); e != nil {
			err = e
		}
	}()

	written, err := io.Copy(fw, fr)
	if err != nil {
		return written, err
	}
	return written, nil
}

// RemoveFile removes file from a given path
func (b *LocalFileBackend) RemoveFile(path string) error {
	return os.Remove(filepath.Join(b.directory, path))
}

// ListDirectory lists directory from a given path
func (b *LocalFileBackend) ListDirectory(path string) (*[]string, error) {
	fileInfos, err := ioutil.ReadDir(filepath.Join(b.directory, path))
	paths := make([]string, len(fileInfos))
	if err != nil {
		if os.IsNotExist(err) {
			return &paths, nil
		}
		return nil, err
	}
	for _, fileInfo := range fileInfos {
		paths = append(paths, filepath.Join(path, fileInfo.Name()))
	}

	return &paths, nil
}

// RemoveDirectory removes directory from a given path
func (b *LocalFileBackend) RemoveDirectory(path string) error {
	return os.RemoveAll(filepath.Join(b.directory, path))
}

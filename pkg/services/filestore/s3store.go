package filestore

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	s3 "github.com/minio/minio-go/v6"
	"github.com/minio/minio-go/v6/pkg/credentials"
	"github.com/minio/minio-go/v6/pkg/encrypt"
	"github.com/sirupsen/logrus"
)

// S3FileBackend is
type S3FileBackend struct {
	endpoint  string
	accessKey string
	secretKey string
	secure    bool
	signV2    bool
	region    string
	bucket    string
	encrypt   bool
	trace     bool
}

// Similar to s3.New() but allows initialization of signature v2 or signature v4 client.
// If signV2 input is false, function always returns signature v4.
//
// Additionally this function also takes a user defined region, if set
// disables automatic region lookup.
func (b *S3FileBackend) s3New() (*s3.Client, error) {
	var creds *credentials.Credentials

	if b.accessKey == "" && b.secretKey == "" {
		creds = credentials.NewIAM("")
	} else if b.signV2 {
		creds = credentials.NewStatic(b.accessKey, b.secretKey, "", credentials.SignatureV2)
	} else {
		creds = credentials.NewStatic(b.accessKey, b.secretKey, "", credentials.SignatureV4)
	}

	s3Clnt, err := s3.NewWithCredentials(b.endpoint, creds, b.secure, b.region)
	if err != nil {
		return nil, err
	}

	if b.trace {
		s3Clnt.TraceOn(os.Stdout)
	}

	return s3Clnt, nil
}

// TestConnection tests connectivity
func (b *S3FileBackend) TestConnection() error {
	s3Clnt, err := b.s3New()
	if err != nil {
		return err
	}

	exists, err := s3Clnt.BucketExists(b.bucket)
	if err != nil {
		return err
	}

	if !exists {
		logrus.Warn("Bucket specified does not exist. Attempting to create...")
		err := s3Clnt.MakeBucket(b.bucket, b.region)
		if err != nil {
			logrus.Error("Unable to create bucket.")
			return err
		}
	}
	logrus.Debug("Connection to S3 or minio is good. Bucket exists.")
	return nil
}

// Reader caller must close the first return value
func (b *S3FileBackend) Reader(path string) (ReadCloseSeeker, error) {
	s3Clnt, err := b.s3New()
	if err != nil {
		return nil, err
	}
	minioObject, err := s3Clnt.GetObject(b.bucket, path, s3.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	return minioObject, nil
}

// ReadFile is
func (b *S3FileBackend) ReadFile(path string) ([]byte, error) {
	s3Clnt, err := b.s3New()
	if err != nil {
		return nil, err
	}
	minioObject, err := s3Clnt.GetObject(b.bucket, path, s3.GetObjectOptions{})
	if err != nil {
		return nil, err
	}

	defer func(obj *s3.Object) {
		_ = obj.Close()
	}(minioObject)

	return ioutil.ReadAll(minioObject)
}

// FileExists is
func (b *S3FileBackend) FileExists(path string) (bool, error) {
	s3Clnt, err := b.s3New()
	if err != nil {
		return false, err
	}
	_, err = s3Clnt.StatObject(b.bucket, path, s3.StatObjectOptions{})

	if err == nil {
		return true, nil
	}

	if err.(s3.ErrorResponse).Code == "NoSuchKey" {
		return false, nil
	}

	return false, err
}

// CopyFile is
func (b *S3FileBackend) CopyFile(oldPath, newPath string) error {
	s3Clnt, err := b.s3New()
	if err != nil {
		return err
	}

	source := s3.NewSourceInfo(b.bucket, oldPath, nil)
	destination, err := s3.NewDestinationInfo(b.bucket, newPath, encrypt.NewSSE(), nil)
	if err != nil {
		return err
	}

	return s3Clnt.CopyObject(destination, source)
}

// MoveFile is
func (b *S3FileBackend) MoveFile(oldPath, newPath string) error {
	s3Clnt, err := b.s3New()
	if err != nil {
		return err
	}

	source := s3.NewSourceInfo(b.bucket, oldPath, nil)
	destination, err := s3.NewDestinationInfo(b.bucket, newPath, encrypt.NewSSE(), nil)
	if err != nil {
		return err
	}
	if err = s3Clnt.CopyObject(destination, source); err != nil {
		return err
	}

	return s3Clnt.RemoveObject(b.bucket, oldPath)
}

// WriteFile is
func (b *S3FileBackend) WriteFile(fr io.Reader, path string) (int64, error) {
	s3Clnt, err := b.s3New()
	if err != nil {
		return 0, err
	}

	var contentType string
	if ext := filepath.Ext(path); IsFileExtImage(ext) {
		contentType = GetImageMimeType(ext)
	} else {
		contentType = "binary/octet-stream"
	}

	options := s3PutOptions(b.encrypt, contentType)
	var buf bytes.Buffer
	_, err = buf.ReadFrom(fr)
	if err != nil {
		return 0, err
	}
	written, err := s3Clnt.PutObject(b.bucket, path, &buf, int64(buf.Len()), options)
	if err != nil {
		return written, err
	}

	return written, nil
}

// RemoveFile is
func (b *S3FileBackend) RemoveFile(path string) error {
	s3Clnt, err := b.s3New()
	if err != nil {
		return err
	}

	return s3Clnt.RemoveObject(b.bucket, path)
}

func getPathsFromObjectInfos(in <-chan s3.ObjectInfo) <-chan string {
	out := make(chan string, 1)

	go func() {
		defer close(out)

		for {
			info, done := <-in

			if !done {
				break
			}

			out <- info.Key
		}
	}()

	return out
}

// ListDirectory is
func (b *S3FileBackend) ListDirectory(path string) (*[]string, error) {
	s3Clnt, err := b.s3New()
	if err != nil {
		return nil, err
	}

	doneCh := make(chan struct{})
	defer close(doneCh)

	if !strings.HasSuffix(path, "/") && len(path) > 0 {
		// s3Clnt returns only the path itself when "/" is not present
		// appending "/" to make it consistent across all filesstores
		path = path + "/"
	}

	objects := s3Clnt.ListObjects(b.bucket, path, false, doneCh)
	paths := make([]string, len(objects))
	for object := range objects {
		if object.Err != nil {
			return nil, object.Err
		}
		paths = append(paths, strings.Trim(object.Key, "/"))
	}

	return &paths, nil
}

// RemoveDirectory is
func (b *S3FileBackend) RemoveDirectory(path string) error {
	s3Clnt, err := b.s3New()
	if err != nil {
		return err
	}

	doneCh := make(chan struct{})

	for err := range s3Clnt.RemoveObjects(b.bucket, getPathsFromObjectInfos(s3Clnt.ListObjects(b.bucket, path, true, doneCh))) {
		if err.Err != nil {
			doneCh <- struct{}{}
			return err.Err
		}
	}

	close(doneCh)
	return nil
}

func s3PutOptions(encrypted bool, contentType string) s3.PutObjectOptions {
	options := s3.PutObjectOptions{}
	if encrypted {
		options.ServerSideEncryption = encrypt.NewSSE()
	}
	options.ContentType = contentType

	return options
}

//func CheckMandatoryS3Fields(settings *model.FileSettings)error {
//	if settings.AmazonS3Bucket == nil || len(*settings.AmazonS3Bucket) == 0 {
//		return errors.New("missing s3 bucket")
//		//return model.NewAppError("S3File", "api.admin.test_s3.missing_s3_bucket", nil, "", http.StatusBadRequest)
//	}
//
//	// if S3 endpoint is not set call the set defaults to set that
//	if settings.AmazonS3Endpoint == nil || len(*settings.AmazonS3Endpoint) == 0 {
//		settings.SetDefaults(true)
//	}
//
//	return nil
//}
var (
	ImageExtensions = [7]string{".jpg", ".jpeg", ".gif", ".bmp", ".png", ".tiff", "tif"}
	ImageMimeTypes  = map[string]string{".jpg": "image/jpeg", ".jpeg": "image/jpeg", ".gif": "image/gif", ".bmp": "image/bmp", ".png": "image/png", ".tiff": "image/tiff", ".tif": "image/tif"}
)

// IsFileExtImage is
func IsFileExtImage(ext string) bool {
	ext = strings.ToLower(ext)
	for _, imgExt := range ImageExtensions {
		if ext == imgExt {
			return true
		}
	}
	return false
}

// GetImageMimeType is
func GetImageMimeType(ext string) string {
	ext = strings.ToLower(ext)
	if len(ImageMimeTypes[ext]) == 0 {
		return "image"
	}

	return ImageMimeTypes[ext]
}

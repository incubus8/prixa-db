package tracing

import (
	"context"
	"strconv"
	"strings"

	"github.com/opentracing/opentracing-go/ext"
	"github.com/opentracing/opentracing-go/log"

	"github.com/go-redis/redis/v7"
	"github.com/opentracing/opentracing-go"
)

// reference: https://github.com/opentracing/specification/blob/master/semantic_conventions.md

// RedisHook is
type RedisHook struct {
	addrs    []string
	database int
}

// NewRedisHook is
func NewRedisHook(addrs []string, database int) RedisHook {
	return RedisHook{
		addrs,
		database,
	}
}

// BeforeProcess is a RedisHook before process.
func (h RedisHook) BeforeProcess(ctx context.Context, cmd redis.Cmder) (context.Context, error) {
	span, newCtx := opentracing.StartSpanFromContext(ctx, "redis:cmd")
	ext.DBType.Set(span, "redis")
	ext.DBInstance.Set(span, strconv.Itoa(h.database))
	ext.PeerAddress.Set(span, strings.Join(h.addrs, ", "))
	ext.PeerService.Set(span, "redis")
	ext.SpanKind.Set(span, ext.SpanKindEnum("client"))
	ext.DBStatement.Set(span, strings.ToUpper(cmd.Name()))
	return newCtx, nil
}

// AfterProcess is a RedisHook after process.
func (h RedisHook) AfterProcess(ctx context.Context, cmd redis.Cmder) error {
	span := opentracing.SpanFromContext(ctx)
	if span != nil {
		// if context is raised an error.
		if ctx.Err() != nil {
			ext.Error.Set(span, true)
			span.LogFields(log.Error(ctx.Err()))
		}
		span.Finish()
	}
	return nil
}

// BeforeProcessPipeline is a RedisHook before pipeline process.
func (h RedisHook) BeforeProcessPipeline(ctx context.Context, cmds []redis.Cmder) (context.Context, error) {
	span, newCtx := opentracing.StartSpanFromContext(ctx, "redis:pipeline:cmd")
	ext.DBType.Set(span, "redis")
	ext.DBInstance.Set(span, strconv.Itoa(h.database))
	ext.PeerAddress.Set(span, strings.Join(h.addrs, ", "))
	ext.PeerService.Set(span, "redis")
	ext.SpanKind.Set(span, ext.SpanKindEnum("client"))
	merge := make([]string, len(cmds))
	for i, cmd := range cmds {
		merge[i] = strings.ToUpper(cmd.Name())
	}
	ext.DBStatement.Set(span, strings.Join(merge, " --> "))
	return newCtx, nil
}

// AfterProcessPipeline is a RedisHook after pipeline process.
func (h RedisHook) AfterProcessPipeline(ctx context.Context, cmds []redis.Cmder) error {
	span := opentracing.SpanFromContext(ctx)
	if span != nil {
		// if context is raised an error.
		if ctx.Err() != nil {
			ext.Error.Set(span, true)
			span.LogFields(log.Error(ctx.Err()))
		}
		span.Finish()
	}
	return nil
}

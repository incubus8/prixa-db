package superadmin

import (
	"context"
	"os"
	"strings"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"
	"prixa.ai/prixa-db/pkg/util"
)

// Service defines super admin operations
type Service interface {
	Create()
	IsMatchPassword(challenge string) bool
	IsMatchEmail(email string) bool
}

// user defines super admin service implementation
type user struct {
	store store.Store
}

// New creates new super admin service
func New(store store.Store) Service {
	return user{
		store: store,
	}
}

const adminEmail = "superadmin@prixasuper.ai"

// Create creates new super admin user and its password, store it to redis
func (s user) Create() {
	pwd := os.Getenv("SUPER_ADMIN_PASSWORD")
	if pwd == "" {
		logrus.Infoln("SUPER_ADMIN_PASSWORD environment is empty. Generate new password...")
		pwd = util.RandString(16)
	}

	s.store.SetContext(context.Background())
	err := s.store.User().SaveAdminPwd(&userprofile.RedisData{
		Key:   adminEmail,
		Token: pwd,
	})
	if err != nil {
		logrus.WithError(err).Warnln("error during saving admin password")
	}

	logrus.WithField("user", adminEmail).
		WithField("password", pwd).
		Infoln("Admin Credentials")
}

// IsMatchPassword matches super admin user password
func (s user) IsMatchPassword(challenge string) bool {
	pwd, _ := s.store.User().GetAdminPwd(adminEmail)
	return pwd == challenge
}

// IsMatchEmail matches super admin user email
func (s user) IsMatchEmail(email string) bool {
	return strings.EqualFold(email, adminEmail)
}

package stackdriver

import (
	"context"
	"fmt"
	"os"

	"cloud.google.com/go/errorreporting"
	"cloud.google.com/go/logging"
	"contrib.go.opencensus.io/exporter/stackdriver"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/option"
)

// Stackdriver is
type Stackdriver struct {
	ctx       context.Context
	projectID string
	creds     *google.Credentials
}

// Service is
type Service interface {
	Trace() error
	Logging() (*logging.Client, error)
	ErrorReporting() (*errorreporting.Client, error)
}

// NewService creates Stackdriver service
func NewService(context context.Context, creds *google.Credentials) Service {
	return Stackdriver{
		ctx:       context,
		projectID: os.Getenv("GOOGLE_PROJECT_ID"),
		creds:     creds,
	}
}

// withClientOptions sets default opts
func (s Stackdriver) withClientOptions(svcName string) []option.ClientOption {
	reason := fmt.Sprintf("Authenticated for %v, service: %v", s.projectID, svcName)

	return []option.ClientOption{
		option.WithCredentials(s.creds),
		option.WithRequestReason(reason),
	}
}

// Trace inits Stackdriver trace
func (s Stackdriver) Trace() error {
	opts := stackdriver.Options{
		Context:                 s.ctx,
		ProjectID:               s.projectID,
		MonitoringClientOptions: s.withClientOptions("monitoring client"),
		TraceClientOptions:      s.withClientOptions("trace client"),
	}

	exporter, err := stackdriver.NewExporter(opts)
	if err != nil {
		return fmt.Errorf("failed to initialize Stackdriver trace: %v", err)
	}

	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	_ = exporter.StartMetricsExporter()
	defer exporter.StopMetricsExporter()

	return nil
}

// Logging gets Stackdriver logging client
func (s Stackdriver) Logging() (*logging.Client, error) {
	lc, err := logging.NewClient(s.ctx, s.projectID, s.withClientOptions("logging")...)
	if err != nil {
		return nil, err
	}

	return lc, nil
}

// ErrorReporting gets Stackdriver errorreporting client
func (s Stackdriver) ErrorReporting() (*errorreporting.Client, error) {
	config := errorreporting.Config{
		ServiceName: s.projectID,
		OnError: func(err error) {
			logrus.WithError(err).Errorln("Could not log error")
		},
	}
	ec, err := errorreporting.NewClient(s.ctx, s.projectID, config, s.withClientOptions("error reporting")...)
	if err != nil {
		return nil, err
	}

	return ec, nil
}

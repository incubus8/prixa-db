package db

import (
	"strings"

	"github.com/spf13/cast"
)

// Type is struct for modelling list of questions on specific symptoms
type Type struct {
	ID          string
	Name        string
	NameIndo    string
	Description string
	Question    string
	AskOnce     bool
	Order       int
	Properties  []string
}

// Property is struct for modelling definition of each Type
// for example if Type is Site then the Property are forehead facial jaw ear etc
type Property struct {
	ID          string
	Name        string
	NameIndo    string
	Description string
	Type        string
	Keywords    []string
	Triage      string
	UID         string
}

// PropertyPair is a struct holding Type
type PropertyPair struct {
	Type string `json:"type"`
	Prop string `json:"prop"`
}

// PropertyPairs is array of PropertPair
type PropertyPairs []PropertyPair

// Profile is
type Profile struct {
	ID                          string  `json:"id"`
	Name                        string  `json:"name"`
	NameIndo                    string  `json:"name_indo"`
	Description                 string  `json:"description"`
	PreconditionDescription     string  `json:"preconditionDescription"`
	PreconditionDescriptionCopy string  `json:"preconditionDescription_copy"`
	Type                        string  `json:"type"`
	Order                       int     `json:"order"`
	CustomValue1                float64 `json:"custom_value1"`
	CustomValue2                float64 `json:"custom_value2"`
	Scoring                     float32 `json:"scoring"`
}

// Characteristic is
type Characteristic struct {
	ID          string
	Name        string
	NameIndo    string
	Description string
	Question    string
	Properties  []string
}

// SymptomTriage holds information
type SymptomTriage struct {
	ID   string
	Name string
}

// MetaTable holds information about other stuffs
type MetaTable struct {
	Types         map[string]Type
	TypeIndex     map[string]string
	Props         map[string]Property
	PropIndex     map[string]string
	PropUID       map[string]string
	Chars         map[string]Characteristic
	Profiles      map[string]Profile
	SymptomTriage map[string]SymptomTriage
}

// MatchProfile match preconditions with profiles
func (t *MetaTable) MatchProfile(preconditions []string, profiles []Profile) bool {
	if len(preconditions) == 0 {
		return true
	}
	typeMatch := map[string]bool{}

	profileIDs := make([]string, 0, len(profiles))
	for _, profile := range profiles {
		profileIDs = append(profileIDs, profile.ID)
	}

	for _, id := range preconditions {
		p := t.Profiles[id]
		val, ok := typeMatch[p.Type]
		if !ok || !val {
			if stringIn(profileIDs, p.ID) {
				typeMatch[p.Type] = true
			} else {
				typeMatch[p.Type] = false
			}
		}
	}

	for _, t := range typeMatch {
		if !t {
			return false
		}
	}

	return true
}

// ProfileByName look for profile by name
func (t *MetaTable) ProfileByName(name string) Profile {
	for _, profile := range t.Profiles {
		if strings.EqualFold(name, profile.Name) {
			return profile
		}
	}

	return Profile{}
}

// ProfilesByType gets Profile by its type
func (t *MetaTable) ProfilesByType(name string) []Profile {
	var profiles []Profile
	for _, profile := range t.Profiles {
		if strings.EqualFold(name, profile.Type) {
			profiles = append(profiles, profile)
		}
	}
	return profiles
}

// GetProfiles gets profile from ids
func (t *MetaTable) GetProfiles(ids []string) []Profile {
	var res []Profile
	for _, id := range ids {
		profile, ok := t.Profiles[id]
		if ok {
			res = append(res, profile)
		}
	}

	return res
}

// ByIDs queries meta by array of IDs
func (propPairs PropertyPairs) ByIDs(typeIDs []string) PropertyPairs {
	results := PropertyPairs{}
	for _, typeID := range typeIDs {
		for _, propPair := range propPairs {
			if propPair.Type == typeID {
				results = append(results, propPair)
			}
		}
	}
	return results
}

// Init initialize MetaTable
func (t *MetaTable) Init(profiles, props, types, chars, symptomTriage []Record) {
	t.Profiles = initProfiles(profiles)
	t.Props = initProps(props)
	t.PropIndex = indexProps(t.Props)
	t.PropUID = indexPropsUID(t.Props)
	t.Types = initTypes(types)
	t.TypeIndex = indexTypes(t.Types)
	t.Chars = initChars(chars)
	t.SymptomTriage = initSymptomTriage(symptomTriage)
}

// TypeIDs returns type IDs form PropertPairs
func (propPairs PropertyPairs) TypeIDs() []string {
	types := make([]string, 0, len(propPairs))
	for _, propPair := range propPairs {
		types = append(types, propPair.Type)
	}
	return types
}

func initProfiles(records []Record) map[string]Profile {
	dict := map[string]Profile{}

	for _, record := range records {
		dict[record.ID] = mapProfile(record)
	}

	return dict
}

func initSymptomTriage(records []Record) map[string]SymptomTriage {
	dict := map[string]SymptomTriage{}

	for _, record := range records {
		dict[record.ID] = mapSymptomTriage(record)
	}

	return dict
}

func initProps(records []Record) map[string]Property {
	dict := map[string]Property{}

	for _, record := range records {
		dict[record.ID] = mapProperty(record)
	}

	return dict
}

func initTypes(records []Record) map[string]Type {
	dict := map[string]Type{}

	for _, record := range records {
		dict[record.ID] = mapType(record)
	}

	return dict
}

func indexTypes(types map[string]Type) map[string]string {
	index := map[string]string{}

	for _, t := range types {
		index[strings.ToLower(t.Name)] = t.ID
	}

	return index
}

func indexProps(props map[string]Property) map[string]string {
	index := map[string]string{}

	for _, p := range props {
		index[strings.ToLower(strings.Replace(p.Name, " ", "", -1))] = p.ID
	}

	return index
}

func indexPropsUID(props map[string]Property) map[string]string {
	index := map[string]string{}

	for _, p := range props {
		if p.UID != "" {
			index[p.UID] = p.ID
		}
	}

	return index
}

func initChars(records []Record) map[string]Characteristic {
	dict := map[string]Characteristic{}

	for _, record := range records {
		dict[record.ID] = mapCharacteristic(record)
	}

	return dict
}

func mapType(record Record) Type {
	return Type{
		ID:          record.ID,
		Name:        cast.ToString(record.Fields["Name"]),
		NameIndo:    cast.ToString(record.Fields["Name Indo"]),
		Description: cast.ToString(record.Fields["Description"]),
		Question:    cast.ToString(record.Fields["Question"]),
		AskOnce:     cast.ToString(record.Fields["Times Asked"]) == "Once only",
		Properties:  cast.ToStringSlice(record.Fields["Properties"]),
		Order:       cast.ToInt(record.Fields["Order"]),
	}
}

func mapProfile(record Record) Profile {
	return Profile{
		ID:                          record.ID,
		Name:                        cast.ToString(record.Fields["Name"]),
		NameIndo:                    cast.ToString(record.Fields["Name Indo"]),
		Description:                 cast.ToString(record.Fields["Description"]),
		PreconditionDescription:     cast.ToString(record.Fields["preconditionDescription"]),
		PreconditionDescriptionCopy: cast.ToString(record.Fields["preconditionDescriptionCopy"]),
		Type:                        cast.ToString(record.Fields["Type"]),
		Order:                       cast.ToInt(record.Fields["Order"]),
		CustomValue1:                cast.ToFloat64(record.Fields["customValue1"]),
		CustomValue2:                cast.ToFloat64(record.Fields["customValue2"]),
		Scoring:                     cast.ToFloat32(record.Fields["Scoring"]),
	}
}

func mapSymptomTriage(record Record) SymptomTriage {
	return SymptomTriage{
		ID:   record.ID,
		Name: cast.ToString(record.Fields["Name"]),
	}
}

func mapCharacteristic(record Record) Characteristic {
	return Characteristic{
		ID:          record.ID,
		Name:        cast.ToString(record.Fields["Name"]),
		NameIndo:    cast.ToString(record.Fields["Name Indo"]),
		Description: cast.ToString(record.Fields["Description"]),
		Question:    cast.ToString(record.Fields["Question"]),
		Properties:  cast.ToStringSlice(record.Fields["Properties"]),
	}
}

func mapTriage(record Record) Triage {
	return Triage{
		ID:          record.ID,
		Name:        cast.ToString(record.Fields["Name"]),
		NameIndo:    cast.ToString(record.Fields["Name Indo"]),
		Description: cast.ToString(record.Fields["Description"]),
		Symptoms:    cast.ToStringSlice(record.Fields["Symptoms"]),
		Properties:  cast.ToStringSlice(record.Fields["Properties"]),
		Diseases:    cast.ToStringSlice(record.Fields["Diseases"]),
	}
}

func mapProperty(record Record) Property {
	return Property{
		ID:          record.ID,
		Name:        cast.ToString(record.Fields["Name"]),
		NameIndo:    cast.ToString(record.Fields["Name Indo"]),
		Description: cast.ToString(record.Fields["Description"]),
		Type:        GetFirstStringIndex(record.Fields["Type"]),
		Keywords:    cast.ToStringSlice(record.Fields["Keywords"]),
		Triage:      GetFirstStringIndex(record.Fields["Triage"]),
		UID:         cast.ToString(record.Fields["Unique_ID"]),
	}
}

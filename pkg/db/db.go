package db

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cast"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/sirupsen/logrus"
)

// Record is data model from Airtable
type Record struct {
	ID          string                 `json:"id"`
	Fields      map[string]interface{} `json:"fields"`
	CreatedTime string                 `json:"createdTime"`
}

// DB holds database data
type DB struct {
	Diseases          DiseaseTable
	Symptoms          SymptomTable
	Differentials     DiffTable
	Triages           TriageTable
	Meta              MetaTable
	Testing           TestingTable
	Labs              LabTable
	Article           ArticleTable
	LocalTransmission LocalTransmissionTable
	AncillaryControls AncillaryControlTable
}

// FetchResult holds table information and result record
type FetchResult struct {
	Table  string
	Result []Record
	Done   bool
}

// RecordRoot encapsualtes Records
type RecordRoot struct {
	Records []Record `json:"records"`
	Offset  string   `json:"offset"`
}

func fetchDataFromAirTable(table string, offset string, channel chan FetchResult) error {
	baseURL := GetBaseURL()
	token := GetToken()

	if baseURL == "" || token == "" {
		logrus.WithField("baseUrl", baseURL).
			WithField("token", token).
			Panicln("No URL or Token specified in ENV")
	}

	tableURL := fmt.Sprintf("%v/%v", baseURL, table)
	if offset != "" {
		tableURL = fmt.Sprintf("%v/%v?offset=%v", baseURL, table, offset)
	}
	logrus.Infoln("Fetching", tableURL)

	client := &http.Client{}
	req, err := http.NewRequest("GET", tableURL, nil)
	if err != nil {
		logrus.WithError(err).Fatalln()
		return err
	}
	bearer := fmt.Sprintf("Bearer %v", token)
	req.Header.Set("Authorization", bearer)

	resp, err := client.Do(req)
	if err != nil {
		logrus.Errorln("Error fetching data", err)
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logrus.WithError(err).Fatalln("cannot read body")
		return err
	}

	var recordRoot RecordRoot

	if unmarshalError := json.Unmarshal(body, &recordRoot); unmarshalError != nil {
		logrus.WithError(unmarshalError).Fatalln()
		return unmarshalError
	}

	channel <- FetchResult{
		Table:  table,
		Result: recordRoot.Records,
	}

	if recordRoot.Offset != "" {
		_ = fetchDataFromAirTable(table, recordRoot.Offset, channel)
	} else {
		channel <- FetchResult{
			Table: table,
			Done:  true,
		}
	}

	return nil
}

// FetchAllTables is a method to populate all the
func (db *DB) FetchAllTables() {
	resultChan := make(chan FetchResult)

	tables := map[string][]Record{}
	names := []string{
		"Symptoms",
		"Differentials",
		"Diseases",
		"Types",
		"Triages",
		"Characteristics",
		"Profiles",
		"Properties",
		"Test-Cases",
		"Test-Items",
		"Symptoms-Triage",
		"Labs",
		"Articles",
		"Transmisi-Lokal",
		"contentCard",
		"Ancillary-Control",
	}

	for _, table := range names {
		go fetchTable(table, resultChan)
	}

	var counter int
	for res := range resultChan {

		if res.Done {
			logrus.Infof("Total Records Table %v : %v", res.Table, len(tables[res.Table]))
			counter++
		}

		if tables[res.Table] == nil {
			tables[res.Table] = []Record{}
		}

		if res.Result != nil {
			tables[res.Table] = append(tables[res.Table], res.Result...)
		}

		if counter == len(names) {
			break
		}

	}

	db.Diseases.Init(tables["Diseases"])
	db.Symptoms.Init(tables["Symptoms"])
	db.Labs.Init(tables["Labs"])
	db.Differentials.Init(tables["Differentials"])
	db.Triages.Init(tables["Triages"])
	db.Meta.Init(tables["Profiles"], tables["Properties"], tables["Types"], tables["Characteristics"], tables["Symptoms-Triage"])
	db.Testing.Init(tables["Test-Cases"], tables["Test-Items"])
	db.Article = NewArticleTable(tables["Articles"])
	db.LocalTransmission = NewLocalTransmissionTable(tables["Transmisi-Lokal"])
	db.AncillaryControls = NewAncillaryControlTable(tables["Ancillary-Control"])
}

func fetchTable(table string, channel chan FetchResult) {
	if isMeiliURLAlreadySet() {
		fetchDataFromMeilisearch(table, channel)
	} else {
		_ = fetchDataFromAirTable(table, "", channel)
	}
}

func fetchDataFromMeilisearch(table string, channel chan FetchResult) {
	url := GetMeilisearchURL()
	if url == "" {
		logrus.Panic("No elastic URL specified in ENV")
	}

	msClient := meili.NewClientWithCustomHTTPClient(meili.Config{
		Host:   url,
		APIKey: os.Getenv("MEILISEARCH_KEY"),
	}, http.Client{
		Timeout: 10 * time.Second,
	})

	var tableName string
	var fetchedDocs []map[string]interface{}
	if isForCovid19() {
		tableName = fmt.Sprintf("covid_%v", strings.ToLower(table))
	} else {
		tableName = fmt.Sprintf("nalar_%v", strings.ToLower(table))
	}
	if strings.ToLower(table) == "symptoms" {
		_, err := msClient.Settings(tableName).UpdateSearchableAttributes([]string{"Name", "Name Indo", "keyword"})
		if err != nil {
			logrus.Errorf("failed update searchableAttribte to meilisearch on index %v", strings.ToLower(table))
		}
	}
	err := msClient.Documents(strings.ToLower(tableName)).List(meili.ListDocumentsRequest{
		Limit:  5000,
		Offset: 0,
	}, &fetchedDocs)
	if err != nil {
		logrus.Errorf("Error fetching data from meilisearch on index %v. With details : %v", strings.ToLower(table), err)
	}

	records := []Record{}
	for _, doc := range fetchedDocs {
		records = append(records, MapToRecord(doc))
	}
	channel <- FetchResult{
		Table:  table,
		Result: records,
	}
	channel <- FetchResult{
		Table: table,
		Done:  true,
	}
	logrus.Infof("Fetched %v records from meilisearch index %v ", len(records), table)
}

func isForCovid19() bool {
	envCovidStatus, _ := strconv.Atoi(os.Getenv("ENABLE_COVID19_OPD"))
	return envCovidStatus == 1
}

// MapToRecord maps data to Record struct
func MapToRecord(data map[string]interface{}) Record {
	return Record{
		ID:          cast.ToString(data["id"]),
		Fields:      cast.ToStringMap(data["fields"]),
		CreatedTime: cast.ToString(data["createdTime"]),
	}
}

func isMeiliURLAlreadySet() bool {
	meilisearchURL := GetMeilisearchURL()
	return meilisearchURL != ""
}

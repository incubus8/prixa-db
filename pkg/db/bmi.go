package db

import "math"

// LowerBoundObesity is lower bound for BMI score to be considered not obese
const LowerBoundObesity = 30.0

// CalculateBMI calculate Body Mass Index based on weight (kg) and height (cm)
func CalculateBMI(weight, height float64) float64 {
	if height == 0 || weight == 0 {
		return 0.0
	}
	heightInMeter := height / 100.0
	return weight / math.Pow(heightInMeter, 2.0)
}

// IsObese if score is above lower bound
func IsObese(score float64) bool {
	return score > LowerBoundObesity
}

// IsObeseWithWeightHeight return is obese with weight (kg) and height (cm)
func IsObeseWithWeightHeight(weight, height float64) bool {
	return IsObese(CalculateBMI(weight, height))
}

package db

// Evidence holds user answer taken from symptoms question
type Evidence struct {
	SymptomID        string        `json:"symptomId"`
	Props            PropertyPairs `json:"props"`
	IsChiefComplaint bool          `json:"isChiefComplaint"`
	Answer           string        `json:"answer"`
}

// Evidences slice of Evidence
type Evidences []Evidence

// Merge combines evidences
func (evidence Evidence) Merge(pairs PropertyPairs) Evidence {
	propDict := map[string]string{}

	for _, pair := range evidence.Props {
		propDict[pair.Type] = pair.Prop
	}

	for _, pair := range pairs {
		propDict[pair.Type] = pair.Prop
	}

	props := PropertyPairs{}
	for t, p := range propDict {
		props = append(props, PropertyPair{
			Prop: p,
			Type: t,
		})
	}

	evidence.Props = props
	return evidence
}

// ChiefOnly filters only chiefcomplaints
func (evidences Evidences) ChiefOnly() Evidences {
	res := Evidences{}

	for _, evidence := range evidences {
		if evidence.IsChiefComplaint {
			res = append(res, evidence)
		}
	}

	return res
}

// WithoutChief gets any evidences which "not included" in chief complaint
func (evidences Evidences) WithoutChief() Evidences {
	res := Evidences{}

	for _, evidence := range evidences {
		if !evidence.IsChiefComplaint {
			res = append(res, evidence)
		}
	}

	return res
}

// SymptomIDs extracts evidences to symptom ids
func (evidences Evidences) SymptomIDs() []string {
	res := []string{}

	for _, evidence := range evidences {
		res = append(res, evidence.SymptomID)
	}

	return res
}

// FilterAnswer filters only evidences with certain answer
func (evidences Evidences) FilterAnswer(answer string) Evidences {
	res := Evidences{}

	for _, evidence := range evidences {
		if evidence.Answer == answer {
			res = append(res, evidence)
		}
	}

	return res
}

// Append performs evidence append (add new evidence from existing evidence)
func (evidences Evidences) Append(ev Evidence) Evidences {
	res := Evidences{}
	found := false

	for _, evidence := range evidences {
		if ev.SymptomID == evidence.SymptomID {
			found = true
			res = append(res, evidence.Merge(ev.Props))
		} else {
			res = append(res, evidence)
		}
	}

	if !found {
		res = append(res, ev)
	}

	return res
}

// FillEvidencesfromSymp build evidence from Symptom ID param
func FillEvidencesfromSymp(label string) Evidences {
	var props []PropertyPair
	var evidences []Evidence
	evidence := Evidence{}
	evidence.SymptomID = label
	evidence.Props = props
	evidence.IsChiefComplaint = true
	evidence.Answer = "yes"
	evidences = append(evidences, evidence)

	return evidences
}

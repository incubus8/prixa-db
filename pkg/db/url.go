package db

import (
	"os"
)

// GetBaseURL will fetch Airtable URL from env variable
func GetBaseURL() string {
	url := os.Getenv("DB_URL")
	return url
}

// GetToken fetch Airtable token fron env
func GetToken() string {
	token := os.Getenv("DB_TOKEN")
	return token
}

// GetElasticSearchURL will fetch ElasticSearch URL from env variable
func GetElasticSearchURL() string {
	url := os.Getenv("ELASTICSEARCH_URL")
	return url
}

// GetMeilisearchURL will fetch Meilisearch URL from env variable
func GetMeilisearchURL() string {
	url := os.Getenv("MEILISEARCH_URL")
	return url
}

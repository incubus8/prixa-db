package db

import "prixa.ai/prixa-db/pkg/util"

func stringIn(array []string, item string) bool {
	return util.StringIn(array, item)
}

// GetFirstStringIndex get first index (converted) string from interface array
func GetFirstStringIndex(i interface{}) string {
	if i == nil {
		return ""
	}

	return i.([]interface{})[0].(string)
}

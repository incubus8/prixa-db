package db

import (
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/spf13/cast"
)

// LocalTransmissionTable is
type LocalTransmissionTable struct {
	Dict map[string]*diagnosticGRPC.LocalTransmissionData
}

// GetAll gets all LocalTransmission data
func (t *LocalTransmissionTable) GetAll() (data []*diagnosticGRPC.LocalTransmissionData) {
	for _, localTransmissionData := range t.Dict {
		data = append(data, localTransmissionData)
	}
	return data
}

// NewLocalTransmissionTable initialize LocalTransmissionTable from records
func NewLocalTransmissionTable(records []Record) LocalTransmissionTable {
	dict := map[string]*diagnosticGRPC.LocalTransmissionData{}

	for _, record := range records {
		dict[record.ID] = mapLocalTransmission(record)
	}

	return LocalTransmissionTable{
		Dict: dict,
	}
}

// mapLocalTransmission converts record data to grpc LocalTransmission data
func mapLocalTransmission(record Record) *diagnosticGRPC.LocalTransmissionData {
	return &diagnosticGRPC.LocalTransmissionData{
		Id:       record.ID,
		Province: cast.ToString(record.Fields["Provinsi"]),
		City:     cast.ToString(record.Fields["Kota"]),
	}
}

package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsNotObese(t *testing.T) {
	user := User{
		Weight: 82,
		Height: 172,
	}
	isObese := user.IsObese()
	assert.Equal(t, isObese, false)
}

func TestIsObese(t *testing.T) {
	user := User{
		Weight: 102,
		Height: 172,
	}
	isObese := user.IsObese()
	assert.Equal(t, isObese, true)
}

func TestNoData(t *testing.T) {
	user := User{
		Weight: 0,
		Height: 0,
	}
	isObese := user.IsObese()
	assert.Equal(t, isObese, false)
}

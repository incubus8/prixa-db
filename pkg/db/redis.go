package db

import (
	"context"
	"os"

	"github.com/go-redis/redis/v7"

	"prixa.ai/prixa-db/pkg/services/tracing"
)

// NewRedisClient is
func NewRedisClient(ctx context.Context) *redis.Client {
	redisHost, _ := os.LookupEnv("REDIS_HOST")
	redisPassword, _ := os.LookupEnv("REDIS_PASSWORD")

	opts := &redis.Options{
		Addr:     redisHost,
		Password: redisPassword,
		DB:       0, // use default DB
	}
	client := redis.NewClient(opts).WithContext(ctx)
	client.AddHook(tracing.NewRedisHook([]string{opts.Addr}, opts.DB))

	return client
}

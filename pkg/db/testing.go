package db

import "github.com/spf13/cast"

// TestCase holds test case data
type TestCase struct {
	ID               string
	Name             string
	Description      string
	Profiles         []string
	ExpectedDiseases []string
	TestItems        []string
}

// TestItem holds test item data
type TestItem struct {
	ID         string
	Name       string
	TestCaseID string
	SymptomID  string
	Props      []string
}

// TestingTable holds test cases and test items
type TestingTable struct {
	TestCases map[string]TestCase
	TestItems map[string]TestItem
}

// Init init test cases
func (t *TestingTable) Init(testCases, testItems []Record) {
	t.TestCases = initTestCases(testCases)
	t.TestItems = initTestItems(testItems)
}

func initTestCases(records []Record) map[string]TestCase {
	dict := map[string]TestCase{}

	for _, record := range records {
		dict[record.ID] = mapTestCase(record)
	}

	return dict
}

func initTestItems(records []Record) map[string]TestItem {
	dict := map[string]TestItem{}

	for _, record := range records {
		dict[record.ID] = mapTestItem(record)
	}

	return dict
}

func mapTestCase(record Record) TestCase {
	return TestCase{
		ID:               record.ID,
		Name:             cast.ToString(record.Fields["Name"]),
		Description:      cast.ToString(record.Fields["Description"]),
		Profiles:         cast.ToStringSlice(record.Fields["Profiles"]),
		ExpectedDiseases: cast.ToStringSlice(record.Fields["Expected Diseases"]),
		TestItems:        cast.ToStringSlice(record.Fields["Items"]),
	}
}

func mapTestItem(record Record) TestItem {
	return TestItem{
		ID:         record.ID,
		Name:       cast.ToString(record.Fields["Name"]),
		TestCaseID: GetFirstStringIndex(record.Fields["Test Case"]),
		SymptomID:  GetFirstStringIndex(record.Fields["Symptom"]),
		Props:      cast.ToStringSlice(record.Fields["Props"]),
	}
}

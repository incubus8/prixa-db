package db

import (
	"github.com/spf13/cast"
)

// AncillaryControl is
type AncillaryControl struct {
	ID                 string
	PartnerID          string
	AppID              string
	AppName            string
	Telemedicine       bool
	Article            bool
	PharmacyDelivery   bool
	LabTest            bool
	AppointmentBooking bool
	Insurance          bool
	InsuranceBanner    bool
}

// AncillaryControlTable is
type AncillaryControlTable struct {
	Dict map[string]AncillaryControl
}

// NewAncillaryControlTable is
func NewAncillaryControlTable(records []Record) AncillaryControlTable {
	dict := map[string]AncillaryControl{}

	for _, record := range records {
		dict[record.ID] = mapAncillaryControl(record)
	}

	return AncillaryControlTable{
		Dict: dict,
	}
}

// ByPartnerIDAndAppID is
func (a *AncillaryControlTable) ByPartnerIDAndAppID(partnerID, appID string) AncillaryControl {
	for _, ancillaryControl := range a.Dict {
		if ancillaryControl.PartnerID == partnerID && ancillaryControl.AppID == appID {
			return ancillaryControl
		}
	}
	return AncillaryControl{}
}

func mapAncillaryControl(record Record) AncillaryControl {
	ancillaryControl := AncillaryControl{
		ID:                 record.ID,
		PartnerID:          cast.ToString(record.Fields["Partner ID"]),
		AppID:              cast.ToString(record.Fields["Partner Application ID"]),
		AppName:            cast.ToString(record.Fields["Application Name"]),
		Telemedicine:       cast.ToBool(record.Fields["telemedicine"]),
		Article:            cast.ToBool(record.Fields["article"]),
		PharmacyDelivery:   cast.ToBool(record.Fields["pharmacyDelivery"]),
		LabTest:            cast.ToBool(record.Fields["labTest"]),
		AppointmentBooking: cast.ToBool(record.Fields["appointmenBooking"]),
		Insurance:          cast.ToBool(record.Fields["insurance"]),
		InsuranceBanner:    cast.ToBool(record.Fields["insuranceBanner"]),
	}

	return ancillaryControl
}

package db

import "github.com/spf13/cast"

// Lab is struct
type Lab struct {
	ID       string
	Name     string
	SKU      string
	Diseases []string
}

// LabTable represents dict for Labs data
type LabTable struct {
	Dict map[string]Lab
}

func mapLab(record Record) Lab {
	return Lab{
		ID:       record.ID,
		Name:     cast.ToString(record.Fields["Name"]),
		SKU:      cast.ToString(record.Fields["SKU"]),
		Diseases: cast.ToStringSlice(record.Fields["Diseases"]),
	}
}

// Init creates LabsTable data
func (t *LabTable) Init(records []Record) {
	dict := map[string]Lab{}

	for _, record := range records {
		dict[record.ID] = mapLab(record)
	}

	t.Dict = dict
}

// ByID gets LabTable by ID
func (t *LabTable) ByID(id string) (Lab, bool) {
	lab, ok := t.Dict[id]
	return lab, ok
}

package db

import (
	"strings"
	"time"

	logrus "github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/util"
)

// User is data model for User's data
type User struct {
	ID        string    `json:"id"`
	Fullname  string    `json:"fullname"`
	Gender    string    `json:"gender"`    // male or female
	Birthdate string    `json:"birthdate"` // ISO 8601 YYYY-MM-DD
	Weight    float64   `json:"weight"`    // in kilograms
	Height    float64   `json:"height"`    // in centimeters
	Insurance Insurance `json:"insurance"`
	AgeYear   int       `json:"ageYear"`
	AgeMonth  int       `json:"ageMonth"`
}

// Insurance struct represent user information about insurance
type Insurance struct {
	HaveInsurance       bool `json:"haveInsurance"`
	PlanToHaveInsurance bool `json:"planToHaveInsurance"`
}

// Age will returns user's age
func (user *User) Age() int {
	age, err := util.DiffYearWithTime(user.Birthdate, time.Now())
	if err != nil {
		logrus.Errorln("Error on parsing date")
		logrus.Errorln(err)
		return 0
	}
	return age
}

// IsObese can tell user is obese from
func (user *User) IsObese() bool {
	return IsObese(CalculateBMI(user.Weight, user.Height))
}

// IsEmpty checks if all properties is filled of not
// return true if all properties of User is not zero value
// false otherwise
func (user User) IsEmpty() bool {
	return user == User{}
}

// FirstName returns firstname from a fullname
func (user User) FirstName() string {
	if user.Fullname == "" {
		return ""
	}
	names := strings.Split(user.Fullname, " ")
	return names[0]
}

// GetDetails get user details from User object
func (user *User) GetDetails() map[string]interface{} {
	userMap := make(map[string]interface{})
	userMap["ageMonth"] = user.AgeMonth
	userMap["ageYear"] = user.AgeYear
	userMap["isObese"] = user.IsObese()
	userMap["firstname"] = user.FirstName()
	return userMap
}

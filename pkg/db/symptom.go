package db

import (
	"sort"

	"github.com/spf13/cast"
)

// Symptom struct for modelling Symptoms
type Symptom struct {
	ID                 string
	Name               string
	NameIndo           string
	Description        string
	Explanation        string
	Question           string
	Characteristic     string
	MediaFilesLink     string
	SourceURL          string
	AssociatedDiseases []string
	Keywords           []string
	Differentials      []string
	Triage             string
	NiceToKnows        []string
	MustKnows          []string
	PreConditions      []string
	SymptomsTriage     []string
	SID                string
	ScoringType        string
	Scoring            float32
}

// SymptomTable is the data structure for storing symptoms and its index
type SymptomTable struct {
	Dict     map[string]Symptom
	IndexSID map[string]string
}

// Symptoms extends symptoms slice with additional methods
type Symptoms []Symptom

// Init initialize SymptomTable with data from records
func (t *SymptomTable) Init(records []Record) {
	dict := map[string]Symptom{}

	for _, record := range records {
		dict[record.ID] = mapSymptom(record)
	}

	t.Dict = dict
	t.IndexSID = indexSID(dict)
}

func indexSID(dict map[string]Symptom) map[string]string {
	index := map[string]string{}

	for _, symptom := range dict {
		if symptom.SID != "" {
			index[symptom.SID] = symptom.ID
		}
	}

	return index
}

// Len gets number of symptoms
func (t *SymptomTable) Len() int {
	return len(t.Dict)
}

// GetSymptomDataByID gets symptom data in dict by ID
func (t *SymptomTable) GetSymptomDataByID(id string) (Symptom, bool) {
	symptom, ok := t.Dict[id]
	return symptom, ok
}

// Sort sorts symptom by fn
func (s Symptoms) Sort(fn func(Symptom, Symptom) bool) Symptoms {
	sort.Slice(s, func(a, b int) bool {
		return fn(s[a], s[b])
	})

	return s
}

// HasMustKnow checks whether typeID is in the must know list
func (symptom Symptom) HasMustKnow(typeID string) bool {
	return stringIn(symptom.MustKnows, typeID)
}

// MatchProps checks whether disease symptom is matching
func (symptom Symptom) MatchProps(diff Differential, props []PropertyPair) bool {
	for _, p := range props {
		if symptom.HasMustKnow(p.Type) && !diff.MatchProp(p.Prop) {
			return false
		}
	}

	return true
}

// InEvidences checks whether symptom in evidences
func (symptom Symptom) InEvidences(evidences Evidences) bool {
	for _, evidence := range evidences {
		if symptom.ID == evidence.SymptomID {
			return true
		}
	}

	return false
}

func mapSymptom(record Record) Symptom {
	return Symptom{
		ID:                 record.ID,
		Name:               cast.ToString(record.Fields["Name"]),
		NameIndo:           cast.ToString(record.Fields["Name Indo"]),
		Description:        cast.ToString(record.Fields["Symptoms Description"]),
		Explanation:        cast.ToString(record.Fields["Symptoms Explanation"]),
		MediaFilesLink:     cast.ToString(record.Fields["MediaFilesLink"]),
		SourceURL:          cast.ToString(record.Fields["SourceURL"]),
		AssociatedDiseases: cast.ToStringSlice(record.Fields["Diseases"]),
		Differentials:      cast.ToStringSlice(record.Fields["Differential"]),
		Keywords:           cast.ToStringSlice(record.Fields["Keyword"]),
		Question:           cast.ToString(record.Fields["Question"]),
		Triage:             GetFirstStringIndex(record.Fields["Triage"]),
		NiceToKnows:        cast.ToStringSlice(record.Fields["Nice To Know"]),
		MustKnows:          cast.ToStringSlice(record.Fields["Must Know"]),
		Characteristic:     GetFirstStringIndex(record.Fields["Characteristic"]),
		PreConditions:      cast.ToStringSlice(record.Fields["Precondition"]),
		SymptomsTriage:     cast.ToStringSlice(record.Fields["Symptoms-Triage"]),
		SID:                cast.ToString(record.Fields["Symptom_ID"]),
		ScoringType:        cast.ToString(record.Fields["Scoring Triage"]),
		Scoring:            cast.ToFloat32(record.Fields["Scoring"]),
	}
}

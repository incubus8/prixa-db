package db

import "github.com/spf13/cast"

// Differential is Object Model for Differential
type Differential struct {
	ID      string
	Disease string
	Symptom string
	Props   map[string][]string
}

// DiffTable is
type DiffTable struct {
	Dict    map[string]Differential
	IndexDS map[string]string
	IndexD  map[string][]string
	IndexS  map[string][]string
}

// Differentials extends differential slice with methods
type Differentials []Differential

// Len gets number of differentials
func (t *DiffTable) Len() int {
	return len(t.Dict)
}

// Init initializes DiffTable from records
func (t *DiffTable) Init(records []Record) {
	dict := map[string]Differential{}

	for _, record := range records {
		dict[record.ID] = mapDifferential(record)
	}

	t.Dict = dict

	t.Index()
}

// Index starts indexing for disease symptom
func (t *DiffTable) Index() {
	ds := map[string]string{}
	d := map[string][]string{}
	s := map[string][]string{}

	for _, diff := range t.Dict {
		ds[diff.Disease+diff.Symptom] = diff.ID
		if d[diff.Disease] == nil {
			d[diff.Disease] = []string{}
		}
		d[diff.Disease] = append(d[diff.Disease], diff.ID)
		if s[diff.Symptom] == nil {
			s[diff.Symptom] = []string{}
		}
		s[diff.Symptom] = append(s[diff.Symptom], diff.ID)
	}

	t.IndexDS = ds
	t.IndexD = d
	t.IndexS = s
}

// BySymptom gets differential by symptom
func (t *DiffTable) BySymptom(symptom Symptom) Differentials {
	res := Differentials{}

	for _, id := range t.IndexS[symptom.ID] {
		res = append(res, t.Dict[id])
	}

	return res
}

// ByDisease gets differentials related to disease
func (t *DiffTable) ByDisease(disease Disease) Differentials {
	res := Differentials{}

	for _, id := range t.IndexD[disease.ID] {
		res = append(res, t.Dict[id])
	}

	return res
}

// HasSymptom checks whether disease's Differential has a chief complaint
// returns true if it has chief complaints in Differential for that disease, false otherwise
func (t DiffTable) HasSymptom(disease Disease, symptomID string) bool {
	for _, diff := range t.ByDisease(disease) {
		if diff.Symptom == symptomID {
			return true
		}
	}
	return false
}

// MatchProp checks whether differential has prop
func (d Differential) MatchProp(propID string) bool {
	for _, t := range d.Props {
		if stringIn(t, propID) {
			return true
		}
	}

	return false
}

func mapDifferential(record Record) Differential {
	return Differential{
		ID:      record.ID,
		Disease: GetFirstStringIndex(record.Fields["Disease"]),
		Symptom: GetFirstStringIndex(record.Fields["Symptom"]),
		Props: map[string][]string{
			"Site":              cast.ToStringSlice(record.Fields["Site"]),
			"Onset":             cast.ToStringSlice(record.Fields["Onset"]),
			"Characteristic":    cast.ToStringSlice(record.Fields["Characteristic"]),
			"Timing":            cast.ToStringSlice(record.Fields["Timing"]),
			"Progressivity":     cast.ToStringSlice(record.Fields["Progressivity"]),
			"Severity":          cast.ToStringSlice(record.Fields["Severity"]),
			"Worsening Factors": cast.ToStringSlice(record.Fields["Worsening Factors"]),
			"Radiation":         cast.ToStringSlice(record.Fields["Radiation"]),
			"Pregnancy State":   cast.ToStringSlice(record.Fields["Pregnancy State"]),
		},
	}
}

package db

import (
	"errors"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/spf13/cast"
)

// ArticleTable is
type ArticleTable struct {
	Dict map[string]*diagnosticGRPC.GetDiseaseArticleResponse
}

// Len gets number of articles
func (t *ArticleTable) Len() int {
	return len(t.Dict)
}

// GetArticleByDiseaseID is getting article based on input diseaseID
func (t *ArticleTable) GetArticleByDiseaseID(diseaseID string) (*diagnosticGRPC.GetDiseaseArticleResponse, error) {
	for _, article := range t.Dict {
		if article.DiseaseName == diseaseID {
			return article, nil
		}
	}
	return nil, errors.New("no article found by requested diseaseID. Make sure input diseaseID is correct")
}

// NewArticleTable initialize ArticleTable from records
func NewArticleTable(records []Record) ArticleTable {
	dict := map[string]*diagnosticGRPC.GetDiseaseArticleResponse{}

	for _, record := range records {
		dict[record.ID] = mapArticle(record)
	}

	return ArticleTable{
		Dict: dict,
	}
}

// mapArticle converts record data to grpc disease article data
func mapArticle(record Record) *diagnosticGRPC.GetDiseaseArticleResponse {
	return &diagnosticGRPC.GetDiseaseArticleResponse{
		Id:                  record.ID,
		DiseaseName:         GetFirstStringIndex(record.Fields["diseaseName"]),
		DiseaseNameIndo:     cast.ToString(record.Fields["diseaseNameIndo"]),
		Author:              cast.ToString(record.Fields["Ditulis oleh:"]),
		CheckedBy:           GetFirstStringIndex(record.Fields["Diperiksa oleh:"]),
		Overview:            cast.ToString(record.Fields["Sekilas"]),
		Advice:              cast.ToString(record.Fields["Apa yang bisa Anda lakukan?"]),
		NonMedicalTreatment: cast.ToString(record.Fields["Penanganan Tanpa Obat"]),
		MedicalTreatment:    cast.ToString(record.Fields["Penanganan Menggunakan Obat"]),
		SupportingLabs:      cast.ToString(record.Fields["Pemeriksaan Tambahan"]),
		SeeDoctor:           cast.ToString(record.Fields["Kapan Anda perlu ke dokter?"]),
		HowToPrevent:        cast.ToString(record.Fields["Bagaimana cara mencegah timbulnya kondisi ini?"]),
		Prevention:          cast.ToString(record.Fields["Pencegahan"]),
		Reference:           cast.ToString(record.Fields["Referensi"]),
	}
}

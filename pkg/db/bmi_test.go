package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBMIResultAbove30(t *testing.T) {
	score := CalculateBMI(100, 165)
	assert.Equal(t, score, 36.73094582185492)
}

func TestBMIResultBelow30(t *testing.T) {
	score := CalculateBMI(70, 165)
	assert.Equal(t, score, 25.71166207529844)
}

func TestBMIResultReturn0(t *testing.T) {
	score := CalculateBMI(0, 0)
	assert.Equal(t, score, 0.0)
}

func TestBMIResultHeightIz0(t *testing.T) {
	score := CalculateBMI(100, 0)
	assert.Equal(t, score, 0.0)
}

func TestBMIResultWeightIs0(t *testing.T) {
	score := CalculateBMI(0, 100)
	assert.Equal(t, score, 0.0)
}

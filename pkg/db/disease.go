package db

import (
	"sort"

	"github.com/spf13/cast"
)

// Disease is Object Model for Disease
type Disease struct {
	ID                 string
	Name               string
	NameIndo           string
	Description        string
	AssociatedSymptoms []string
	Differentials      []string
	RiskFactors        []string
	Likeliness         int
	Triage             string
	PreConditions      []string
	MustHave           []string
	URL                string
	Labs               []string
	Prognosis          string
}

// Diseases adds additional functions to slice
type Diseases []Disease

// DiseaseTable is
type DiseaseTable struct {
	Dict map[string]Disease
}

// Init initialize DiseaseTable from records
func (t *DiseaseTable) Init(records []Record) {
	dict := map[string]Disease{}

	for _, record := range records {
		dict[record.ID] = mapDisease(record)
	}

	t.Dict = dict
}

// GetDiseaseByName is getting disease by matching input targetName and returns disease
func (t *DiseaseTable) GetDiseaseByName(targetName string) Disease {
	for _, disease := range t.Dict {
		if disease.Name == targetName {
			return disease
		}
	}
	return Disease{}
}

// Len gets number of diseases
func (t *DiseaseTable) Len() int {
	return len(t.Dict)
}

// Sort sorts diseases by provided func
func (d Diseases) Sort(fn func(Disease, Disease) bool) Diseases {
	sort.Slice(d, func(a, b int) bool {
		return fn(d[a], d[b])
	})

	return d
}

// SortByLikeliness sort diseases by likeliness
func (d Diseases) SortByLikeliness() Diseases {
	return d.Sort(func(a, b Disease) bool {
		return a.Likeliness < b.Likeliness
	})
}

// IsMustHave checks whether symptom is in the list of mustHave symptom
func (d Diseases) IsMustHave(symptom Symptom) bool {
	for _, disease := range d {
		if stringIn(disease.MustHave, symptom.ID) {
			return true
		}
	}

	return false
}

// SatisfyMustHave checks whether evidence satisfy musthave criteria
func (disease Disease) SatisfyMustHave(evidences Evidences) bool {
	if len(disease.MustHave) == 0 {
		return true
	}

	for _, evidence := range evidences.FilterAnswer("no") {
		if stringIn(disease.MustHave, evidence.SymptomID) {
			return false
		}
	}

	return true
}

func mapDisease(record Record) Disease {
	return Disease{
		ID:                 record.ID,
		Name:               cast.ToString(record.Fields["Name"]),
		NameIndo:           cast.ToString(record.Fields["Name Indo"]),
		Description:        cast.ToString(record.Fields["Description"]),
		AssociatedSymptoms: cast.ToStringSlice(record.Fields["Symptoms"]),
		Differentials:      cast.ToStringSlice(record.Fields["Differential"]),
		RiskFactors:        cast.ToStringSlice(record.Fields["Risk Factor"]),
		Likeliness:         mapLikelinessScore(cast.ToString(record.Fields["Likeliness"])),
		Triage:             GetFirstStringIndex(record.Fields["Triage"]),
		PreConditions:      cast.ToStringSlice(record.Fields["Pre Condition"]),
		MustHave:           cast.ToStringSlice(record.Fields["Must Have"]),
		URL:                GetFirstStringIndex(record.Fields["Articles"]),
		Labs:               cast.ToStringSlice(record.Fields["Labs"]),
		Prognosis:          GetFirstStringIndex(record.Fields["Prognosis"]),
	}
}

var likelinessScore = map[string]int{
	"Very Rare": 10,
	"Rare":      5,
	"Common":    1,
}

func mapLikelinessScore(str string) int {
	score, ok := likelinessScore[str]
	if !ok {
		return 0
	}
	return score
}

package db

import "errors"

// Triage is struct modelling suggestion to alleviate symptoms
type Triage struct {
	ID          string
	Name        string
	NameIndo    string
	Description string
	Symptoms    []string
	Properties  []string
	Diseases    []string
}

// TriageTable holds dict of Triages
type TriageTable struct {
	Dict map[string]Triage
}

// Init create new mapped triage
func (t *TriageTable) Init(records []Record) {
	dict := map[string]Triage{}

	for _, record := range records {
		dict[record.ID] = mapTriage(record)
	}

	t.Dict = dict
}

// ByID gets TriageTable by ID
func (t *TriageTable) ByID(id string) (Triage, bool) {
	triage, ok := t.Dict[id]
	return triage, ok
}

// ByName gets TriageTable by name
func (t *TriageTable) ByName(name string) (Triage, error) {
	for _, triage := range t.Dict {
		if triage.Name == name {
			return triage, nil
		}
	}
	return Triage{}, errors.New("triage not found")
}

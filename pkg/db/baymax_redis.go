package db

import (
	"context"
	"os"

	"github.com/go-redis/redis/v7"

	"prixa.ai/prixa-db/pkg/services/tracing"
)

// NewBaymaxRedisClient is
func NewBaymaxRedisClient(ctx context.Context) *redis.Client {
	baymaxRedisHost, _ := os.LookupEnv("BAYMAX_REDIS_HOST")
	baymaxRedisPassword, _ := os.LookupEnv("BAYMAX_REDIS_PASSWORD")

	opts := &redis.Options{
		Addr:     baymaxRedisHost,
		Password: baymaxRedisPassword,
		DB:       0, // use default DB
	}
	client := redis.NewClient(opts).WithContext(ctx)
	client.AddHook(tracing.NewRedisHook([]string{opts.Addr}, opts.DB))

	return client
}

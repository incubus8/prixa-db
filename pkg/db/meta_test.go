package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMatchDisease(t *testing.T) {
	pre1 := []string{"a", "b"}
	pre2 := []string{"a", "c"}
	pre3 := []string{"a", "b", "c"}
	pre4 := []string{"b"}

	a := Profile{ID: "a", Type: "x"}
	b := Profile{ID: "b", Type: "y"}
	c := Profile{ID: "c", Type: "y"}
	meta := MetaTable{
		Profiles: map[string]Profile{
			"a": a,
			"b": b,
			"c": c,
		},
	}

	assert.True(t, meta.MatchProfile(pre1, []Profile{a, b}))
	assert.False(t, meta.MatchProfile(pre2, []Profile{a, b}))
	assert.True(t, meta.MatchProfile(pre3, []Profile{a, b}))
	assert.True(t, meta.MatchProfile(pre4, []Profile{b}))
	assert.False(t, meta.MatchProfile(pre4, []Profile{}))
}

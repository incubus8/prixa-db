package messaging

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var pub *RabbitMQPublisher

func setupPublisher(t *testing.T) func(t *testing.T) {
	_ = os.Setenv("RABBITMQ_URL", "amqp://localhost:5672")
	var err error
	pub, err := NewRabbitMQPublisher()
	assert.NoError(t, err)

	return func(t *testing.T) {
		err = pub.Close()
		assert.NoError(t, err)
		assert.Equal(t, true, pub.rmqConn.conn.IsClosed())
	}
}

func TestErrorIfRabbitMqEnvUrlNotFound(t *testing.T) {
	_, err := NewRabbitMQPublisher()
	assert.Error(t, err, "RabbitMQ URL not found")
}

func TestIfConnectAndDisconnectSuccess(t *testing.T) {
	teardown := setupPublisher(t)
	defer teardown(t)
}

func TestPublishSuccess(t *testing.T) {
	teardown := setupPublisher(t)
	defer teardown(t)
	pub.Publish("TestPublish", "publishKeyTest", "")
}

func TestRedialPublishSuccessAfterConnectionLost(t *testing.T) {
	teardown := setupPublisher(t)
	defer teardown(t)

	pub.Publish("TestPublish1", "publishKeyTest", "")
	pub.rmqConn.channel.Close()
	pub.Publish("TestPublish2", "publishKeyTest", "")
	pub.Publish("TestPublish3", "publishKeyTest", "")
}

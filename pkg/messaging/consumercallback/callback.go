package consumercallback

import "encoding/json"

// ConsumerCallback is
type ConsumerCallback interface {
	OnConsume(payload json.RawMessage) error
}

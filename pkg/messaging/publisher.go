package messaging

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/cenkalti/backoff"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"prixa.ai/prixa-db/pkg/util"
)

// RabbitMQPublisher struct holds RabbitMQ connection and Publisher interface to publish message
type RabbitMQPublisher struct {
	Publisher
	rmqConn *rabbitMQConn

	ingestChan chan Buffer
	jobsChan   chan Buffer
}

// NewRabbitMQPublisher connects and starts RabbitMQ
func NewRabbitMQPublisher() (*RabbitMQPublisher, error) {
	conn := newRabbitMQConn()

	pub := &RabbitMQPublisher{
		ingestChan: make(chan Buffer, ingestSize),
		jobsChan:   make(chan Buffer, workerPoolSize),
		rmqConn:    conn,
	}
	err := pub.rmqConn.setupConnection(pub.handlerReconnect)
	if err != nil {
		logrus.WithError(err).WithContext(conn.ctx).Errorln("cannot setup rabbitmq connection")
		return nil, err
	}

	go pub.startConsumer(conn.ctx)

	conn.wg.Add(workerPoolSize)
	for i := 0; i < workerPoolSize; i++ {
		go pub.retryAndPublish(i)
	}

	return pub, nil
}

// Close for close connection
func (pub *RabbitMQPublisher) Close() error {
	return pub.rmqConn.Close()
}

// startConsumer puts ingest channel value to jobs channel and handles context cancellation signal
func (pub *RabbitMQPublisher) startConsumer(ctx context.Context) {
	for {
		select {
		case job := <-pub.ingestChan:
			pub.jobsChan <- job
		case <-pub.rmqConn.ctx.Done():
			logrus.WithContext(ctx).Infoln("Consumer received cancellation signal, closing jobsChan!")
			close(pub.jobsChan)
			logrus.WithContext(ctx).Infoln("Consumer closed jobsChan")
			return
		}
	}
}

// redial executes publisher with exponential backoff retry algorithm upon failures
func (pub *RabbitMQPublisher) redial(buffer Buffer) {
	operation := func() error {
		err := pub.publishToRabbitMQ(buffer.messages, buffer.routingKey, buffer.exchangName)
		if err != nil {
			logrus.WithError(err).Warnln("Retry to reconnect")
		} else {
			logrus.Debugf("Published message to rabbit %v", buffer)
		}

		return err
	}

	if err := backoff.Retry(operation, backoff.NewExponentialBackOff()); err != nil {
		logrus.WithError(err).Errorln()
		pub.ingestChan <- buffer
	}
}

// Publish puts message to ingest chan before worker consuming it
func (pub *RabbitMQPublisher) Publish(payload interface{}, publishKey string, exchangeName string) {
	pub.ingestChan <- Buffer{payload, publishKey, exchangeName}
}

func (pub *RabbitMQPublisher) retryAndPublish(index int) {
	defer pub.rmqConn.wg.Done()

	logrus.Debugf("Publisher %d starting", index)

	for eventIndex := range pub.jobsChan {
		logrus.Debugf("Worker %d started job %v", index, eventIndex)

		pub.redial(eventIndex)

		logrus.Debugf("Worker %d finished processing job %v", index, eventIndex)
	}
}

func (pub *RabbitMQPublisher) publishToRabbitMQ(payload interface{}, routingKey string, exchangeName string) error {
	if pub == nil {
		err := fmt.Errorf("connection to rabbitmq currently not available")
		logrus.WithError(err).Errorln()
		return err
	}

	data, err := json.Marshal(payload)
	if err != nil {
		logrus.WithError(err).Errorln()
		// Failed to encode payload
		return err
	}

	cid := util.GetRandomID()
	publishing := amqp.Publishing{
		ContentType:     "application/json",
		ContentEncoding: "UTF-8",
		CorrelationId:   cid,
		DeliveryMode:    amqp.Persistent,
		Body:            data,
	}

	err = pub.rmqConn.channel.Publish(exchangeName, routingKey, false, false, publishing)

	return err
}

func (pub *RabbitMQPublisher) handlerReconnect() error {
	return nil
}

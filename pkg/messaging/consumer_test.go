package messaging

import (
	"encoding/json"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"prixa.ai/prixa-db/pkg/query"
)

var (
	consumer *RabbitMQConsumer
	payload  string
)

type TestConsumerQueryImpl struct {
	query.SQLCommonQueryImpl
}

func (i *TestConsumerQueryImpl) OnConsume(value json.RawMessage) error {
	_ = json.Unmarshal(value, &payload)
	return nil
}

func setupConsumerAndPublisher(t *testing.T) func(t *testing.T) {
	_ = os.Setenv("RABBITMQ_URL", "amqp://localhost:5672")
	var err error
	consumer, err = NewRabbitMQConsumer()
	pub, err = NewRabbitMQPublisher()
	assert.NoError(t, err)

	return func(t *testing.T) {
		err = consumer.Close()
		assert.NoError(t, err)
		assert.Equal(t, true, consumer.rmqConn.conn.IsClosed())
	}
}

func TestConsumeSuccess(t *testing.T) {
	teardown := setupConsumerAndPublisher(t)
	defer teardown(t)
	pub.Publish("TestConsume", "consumerKeyTest", "")
	consumer.SetConsumerRepository(map[string]ConsumerRepository{
		"Tes": {
			RoutingKey:       "consumerKeyTest",
			ConsumerCallback: &TestConsumerQueryImpl{},
		},
	})
	consumer.StartConsumingAllQueues()
	time.Sleep(2 * time.Second)
	assert.Equal(t, "TestConsume", payload)
}

func TestConsumeSuccessAfterConnectionLost(t *testing.T) {
	teardown := setupConsumerAndPublisher(t)
	defer teardown(t)
	consumer.SetConsumerRepository(map[string]ConsumerRepository{
		"Tes": {
			RoutingKey:       "consumerKeyTest",
			ConsumerCallback: &TestConsumerQueryImpl{},
		},
	})
	consumer.StartConsumingAllQueues()
	consumer.rmqConn.Close()
	time.Sleep(3 * time.Second)
	payload = ""
	pub.Publish("TestConsume1", "consumerKeyTest", "")
	pub.Publish("TestConsume2", "consumerKeyTest", "")
	pub.Publish("TestConsume3", "consumerKeyTest", "")

	time.Sleep(3 * time.Second)
	assert.Contains(t, "TestConsume", payload)
}

package messaging

import (
	"context"
	"fmt"
	"math/rand"
	"os"

	"github.com/lib/pq"
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/util"
)

// MessagePackage represent messages type and delivery from the rabbitMQ
type MessagePackage struct {
	delivery amqp.Delivery
	consumer ConsumerRepository
}

// ConsumerRepository represent routingKey queue, message Type and it's repository struct to store data persistently
type ConsumerRepository struct {
	Index            string
	RoutingKey       string
	BindingKey       string
	ExchangeName     string
	ConsumerCallback consumercallback.ConsumerCallback
	Args             map[string]interface{}
}

// RabbitMQConsumer holds connection and list of consumerQueue and it's repository interface
type RabbitMQConsumer struct {
	rmqConn      *rabbitMQConn
	ingestChan   chan MessagePackage
	jobsChan     chan MessagePackage
	consumerMaps map[string]ConsumerRepository
}

// NewRabbitMQConsumer to start connection and starts rabbitmq
func NewRabbitMQConsumer() (*RabbitMQConsumer, error) {
	conn := newRabbitMQConn()

	consumer := &RabbitMQConsumer{
		ingestChan: make(chan MessagePackage, ingestSize),
		jobsChan:   make(chan MessagePackage, workerPoolSize),
		rmqConn:    conn,
	}
	err := consumer.rmqConn.setupConnection(consumer.StartConsumingAllQueues)
	if err != nil {
		logrus.WithError(err).WithContext(conn.ctx).Errorln("cannot setup rabbitmq connection")
		return nil, err
	}
	go consumer.startConsumer(consumer.rmqConn.ctx)

	consumer.rmqConn.wg.Add(workerPoolSize)
	for i := 0; i < workerPoolSize; i++ {
		go consumer.retryAndConsume(i)
	}
	return consumer, nil
}

// SetConsumerRepository to set consumerRepository
func (rmq *RabbitMQConsumer) SetConsumerRepository(maps map[string]ConsumerRepository) {
	rmq.consumerMaps = maps
	for _, consumer := range rmq.consumerMaps {
		amqpTable := amqp.Table{}
		for key, value := range consumer.Args {
			amqpTable[key] = value
		}
		queue, err := rmq.rmqConn.channel.QueueDeclare(consumer.RoutingKey, true, false, false, false, amqpTable)
		if err != nil {
			logrus.WithContext(rmq.rmqConn.ctx).WithFields(logrus.Fields{
				"argsTable":  amqpTable,
				"routingKey": consumer.RoutingKey,
			}).WithError(err).Errorln()
			continue
		}

		if consumer.BindingKey != "" {
			err = rmq.rmqConn.channel.QueueBind(
				queue.Name, // queue name
				consumer.BindingKey,
				consumer.ExchangeName, // exchange
				false,
				nil,
			)
			if err != nil {
				logrus.WithContext(rmq.rmqConn.ctx).WithFields(logrus.Fields{
					"ExchangeName": consumer.ExchangeName,
					"routingKey":   consumer.RoutingKey,
					"QueueName":    queue.Name,
					"BindingKey":   consumer.BindingKey,
				}).WithError(err).Errorln()
				continue
			}
		}

	}
}

func (rmq *RabbitMQConsumer) startConsumer(ctx context.Context) {
	for {
		select {
		case job := <-rmq.ingestChan:
			rmq.jobsChan <- job
		case <-ctx.Done():
			logrus.Debugln("Consumer received cancellation signal, closing jobsChan!")
			close(rmq.jobsChan)
			logrus.Debugln("Consumer closed jobsChan")
			return
		}
	}
}

// StartConsume unimplemented method
func (rmq *RabbitMQConsumer) StartConsume() error {
	return nil
}

// ConsumeQueue is
func (rmq *RabbitMQConsumer) ConsumeQueue(queueName string, handler func(<-chan amqp.Delivery)) {
	q, err := rmq.rmqConn.channel.QueueDeclare(queueName, false, false, false, false, nil)
	if err != nil {
		logrus.WithContext(rmq.rmqConn.ctx).WithError(err).Errorln()
	}
	cid := getConsumerID(queueName)
	msgs, err := rmq.rmqConn.channel.Consume(q.Name, cid, false, false, false, false, nil)
	if err != nil {
		logrus.WithContext(rmq.rmqConn.ctx).WithError(err).Errorln()
	}

	handler(msgs)
}

// StartConsumingAllQueues start engine to consume all queue in maps ConsumerRepository
func (rmq *RabbitMQConsumer) StartConsumingAllQueues() error {
	for _, cons := range rmq.consumerMaps {
		err := rmq.startConsumingQueue(cons)
		if err != nil {
			logrus.WithError(err).Errorln()
		}
	}
	return nil
}

// Stop close RabbitMQ channel and RabbitMQ connections
func (rmq *RabbitMQConsumer) Stop() error {
	err := rmq.rmqConn.channel.Close()
	if err != nil {
		logrus.WithError(err).Warnf("Error when closing RabbitMQ channel: %v", err)
	}

	err = rmq.rmqConn.Close()
	if err != nil {
		logrus.WithError(err).Warnf("Error when closing RabbitMQ connection: %v", err)
	}

	return err
}

// startConsumingQueue is
func (rmq *RabbitMQConsumer) startConsumingQueue(consumer ConsumerRepository) error {
	cid := getConsumerID(consumer.RoutingKey)

	deliveries, err := rmq.rmqConn.channel.Consume(consumer.RoutingKey, cid, false, false, false, false, nil)
	if err != nil {
		return err
	}

	go func() {
		defer util.RecoverWithSentry()
		for dlv := range deliveries {
			rmq.ingestChan <- MessagePackage{dlv, consumer}
		}
	}()

	return nil
}

func (rmq *RabbitMQConsumer) retryAndConsume(index int) {
	defer rmq.rmqConn.wg.Done()

	logrus.Debugf("Consumer %d starting\n", index)

	for eventIndex := range rmq.jobsChan {
		logrus.Debugf("Worker %d started job \n", index)

		rmq.saveToRepository(eventIndex)

		logrus.Debugf("Worker %d finished processing job\n", index)
	}
}

func (rmq *RabbitMQConsumer) saveToRepository(msgPackage MessagePackage) {
	err := msgPackage.consumer.ConsumerCallback.OnConsume(msgPackage.delivery.Body)
	handleDelivery(msgPackage.delivery, err)
}

func getConsumerID(routingKey string) string {
	pid := os.Getpid()
	return fmt.Sprintf("%s#%d@/%d", routingKey, pid, rand.Uint32())
}

func handleDelivery(delivery amqp.Delivery, err error) {
	if err != nil {
		logrus.WithError(err).Errorln()
		requeue := isMessageShouldBeRequeued(err)
		notAcknowledgeDelivery(delivery, requeue)
	} else {
		acknowledgeDelivery(delivery)
	}
}

func notAcknowledgeDelivery(delivery amqp.Delivery, requeue bool) {
	_ = delivery.Nack(false, requeue)
}

func acknowledgeDelivery(delivery amqp.Delivery) {
	logrus.Debugln("The Message has been consumed successfully")
	_ = delivery.Ack(false)
}

// error returned by json marshal won't be requeued
// error from library pq won't be requeued except from Class 08 - Connection Exception
func isMessageShouldBeRequeued(err error) bool {
	if pqErr, ok := err.(*pq.Error); ok {
		isConnectionError := pqErr.Code[:2] == "08" || pqErr.Code[:2] == "53"
		if isConnectionError {
			return true
		}
	} else if elasticErr, ok := err.(*elastic.Error); ok {
		if elastic.IsConnErr(elasticErr) || elastic.IsContextErr(elasticErr) || elastic.IsConflict(elasticErr) {
			return true
		}
	}
	return false
}

// Close for close connection
func (rmq *RabbitMQConsumer) Close() error {
	return rmq.rmqConn.Close()
}

package messaging

import (
	"context"
	"errors"
	"os"
	"sync"

	"github.com/cenkalti/backoff"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type handlerAfterReconnect func() error

// ingestSize is a number of maximum messages before consumed to worker
var ingestSize = 8092

// workerPoolSize is a number of workers, each worker will publish message to RabbitMQ
var workerPoolSize = 1

var (
	// QueueUserEvent is
	QueueUserEvent = "user.event.prixa-db"

	// QueueUserAssessmentHistory is
	QueueUserAssessmentHistory = "user.assessment-history.prixa-db"

	// QueueTrackEvent is
	QueueTrackEvent = "app.track.prixa-db"

	// QueueUserPreconditions subject for publishing user preconditions
	QueueUserPreconditions = "user.precondition.prixa-db"

	// CovidPrefix defines the prefix for specifying covid environment origin
	CovidPrefix = "covid"

	// QueueCovidForm is
	QueueCovidForm = "covid-form.prixa-db"

	// QueueAppointmentBookingEvent is
	QueueAppointmentBookingEvent = "appointment-booking.prixa-db"

	// QueueNalarEvent is
	QueueNalarEvent = "nalar.event.prixa-db"

	// QueueGeoLocationEnrichment is
	QueueGeoLocationEnrichment = "nalar.geolocation-enrichment.prixa-db"
)

// Publisher interface
type Publisher interface {
	Publish(payload interface{}, publishKey string, exchangName string)
}

// Consumer interface
type Consumer interface {
	StartConsumingAllQueues() error
	StartConsume() error
	ConsumeQueue(queueName string, handler func(<-chan amqp.Delivery))
	Stop() error
}

// Buffer struct represents message and destination
type Buffer struct {
	messages    interface{}
	routingKey  string
	exchangName string
}

type rabbitMQConn struct {
	sync.Mutex
	ctx      context.Context
	cancelFn context.CancelFunc

	wg *sync.WaitGroup

	conn    *amqp.Connection
	channel *amqp.Channel

	errorsConn chan *amqp.Error
	errorsChan chan *amqp.Error

	handlerAfterReconnect handlerAfterReconnect
}

func newRabbitMQConn() *rabbitMQConn {
	ctx, cancelFunc := context.WithCancel(context.Background())

	return &rabbitMQConn{
		ctx:      ctx,
		cancelFn: cancelFunc,
		wg:       &sync.WaitGroup{},
	}
}

func (r *rabbitMQConn) setupConnection(h handlerAfterReconnect) error {
	err := r.connectRabbitMq()
	r.handlerAfterReconnect = h
	if err != nil {
		return err
	}
	go r.watch()
	return nil
}

// setupConnection dials RabbitMQ connection and creates new channel
func (r *rabbitMQConn) connectRabbitMq() error {
	logrus.Println("Setup RabbitMQ - AMQP connection")

	url, _ := os.LookupEnv("RABBITMQ_URL")
	if url == "" {
		err := errors.New("RabbitMQ URL not found")
		logrus.WithError(err).Errorln()
		return err
	}

	logrus.WithField("url", url).Debugln("Dialing to RabbitMQ...")

	conn, err := amqp.Dial(url)
	if err != nil {
		logrus.WithError(err).Errorln("cannot dial RabbitMQ...")
		return err
	}

	logrus.Infoln("Connected to RabbitMQ")

	r.Lock()
	r.conn = conn
	r.Unlock()

	r.channel, err = conn.Channel()
	if err != nil {
		logrus.WithError(err).Errorln("cannot connect to channel...")
		return err
	}

	errorsConn := make(chan *amqp.Error)
	r.conn.NotifyClose(errorsConn)
	r.errorsConn = errorsConn

	errorsChan := make(chan *amqp.Error)
	r.channel.NotifyClose(errorsChan)
	r.errorsChan = errorsChan

	return nil
}

// Close closes running RabbitMQ connection, emits cancellation signal and wait until worker finished before
// closing the connection
func (r *rabbitMQConn) Close() error {
	r.cancelFn()
	r.wg.Wait()

	if r.conn == nil {
		return nil
	}

	logrus.Infoln("Connection Rabbitmq Close()")

	return r.conn.Close()
}

func (r *rabbitMQConn) watch() {
	for {
		select {
		case err := <-r.errorsConn:
			if err != nil {
				logrus.WithError(err).Warnf("Connection lost: %v\n", err)
				r.reconnectRabbitMQ()
			}
		case err := <-r.errorsChan:
			if err != nil {
				logrus.WithError(err).Warnf("Connection lost: %v\n", err)
				r.reconnectRabbitMQ()
			}
		}
	}
}

func (r *rabbitMQConn) reconnectRabbitMQ() {
	operation := func() error {
		err := r.setupConnection(r.handlerAfterReconnect)
		if err != nil {
			logrus.WithError(err).Warnln("Retry to reconnect")
		} else {
			err := r.handlerAfterReconnect()
			if err != nil {
				logrus.WithError(err).Fatalln()
			}
		}

		return err
	}
	err := backoff.Retry(operation, backoff.NewExponentialBackOff())
	if err != nil {
		logrus.WithError(err).Errorln()
		r.reconnectRabbitMQ()
	}
}

package partner

import (
	"encoding/json"

	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
)

// App is a model for Prixa 3rd party application that connects from 3rd party Partner
type App struct {
	ID                 string                `json:"id"`
	Name               string                `json:"name"`
	Status             string                `json:"status"`
	SecretKey          string                `json:"secretKey"`
	CreatedAt          string                `json:"createdAt"`
	UpdatedAt          string                `json:"updatedAt"`
	DeletedAt          string                `json:"deletedAt,omitempty"`
	Theme              *partnerAppGRPC.Theme `json:"theme"`
	TelemedicineSDKURL string                `json:"telemedicineSDKURL"`
}

// MarshalBinary will perform json marshal when saving to Redis (Op SET)
//lint:ignore U1001 used for the Redis lib
func (p *App) MarshalBinary() ([]byte, error) {
	return json.Marshal(p)
}

package partner

import partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"

// AppData partner app data redis
type AppData struct {
	PartnerID string                      `json:"partnerID"`
	AppID     string                      `json:"appID"`
	Metadata  *partnerAppGRPC.AppMetadata `json:"metadata"`
}

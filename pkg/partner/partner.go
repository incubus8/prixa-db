package partner

import (
	"encoding/json"

	"prixa.ai/prixa-db/pkg/query"
)

// Partner is a model for Prixa 3rd party partner
type Partner struct {
	ID        string   `json:"id"`
	Name      string   `json:"name"`
	Status    string   `json:"status"`
	AppIds    []string `json:"appIds"`
	CreatedAt string   `json:"createdAt"`
	UpdatedAt string   `json:"updatedAt"`
	DeletedAt string   `json:"deletedAt,omitempty"`
}

// MarshalBinary will perform json marshal when saving to Redis (Op SET)
//lint:ignore U1001 used for the Redis lib
func (p *Partner) MarshalBinary() ([]byte, error) {
	return json.Marshal(p)
}

// PageablePartnerData represents partner data with pagination
type PageablePartnerData struct {
	Partners []*Partner
	Page     *query.PageData
}

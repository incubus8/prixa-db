package partner

import (
	"errors"
)

var (
	// ErrEmptyPartnerID error upon empty partner ID
	ErrEmptyPartnerID = errors.New("partner id cannot be emptied")

	// ErrEmptyPartnerAppID error upon empty partner application ID
	ErrEmptyPartnerAppID = errors.New("partner app id cannot be emptied")

	// ErrNoPartnerIDFound error upon no partner id found in query store
	ErrNoPartnerIDFound = errors.New("partner does not exist")

	// ErrNoAppIDFound error upon no partner application id found in query store
	ErrNoAppIDFound = errors.New("partner app does not exist")

	// ErrInactivePartner error upon inactive partner
	ErrInactivePartner = errors.New("partner is no longer active")

	// ErrInactivePartnerApp error upon inactive partner application
	ErrInactivePartnerApp = errors.New("partner app is no longer active")
)

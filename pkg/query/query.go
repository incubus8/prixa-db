package query

import (
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/go-redis/redis/v7"
)

// TableQuery defines abstraction for getting table info
type TableQuery interface {
	TableName() string
}

// CommonQuery is an abstract that defines Common Query
type CommonQuery interface {
	TableQuery

	Save(id string, value interface{}) error
	FindByID(id string) (interface{}, error)
	Delete(id string) error
	FindAll(from, to int64) ([]interface{}, error)
	Count() int64
}

// PageableQuery is an abstract that defines Pagination Query
type PageableQuery interface {
	CommonQuery

	GetDataAtPage(pageNo int64) ([]interface{}, error)
	GetDataAtPageFromSize(pageNo int64, size uint) ([]interface{}, error)
	GetTotalPages() int64
	GetTotalPagesFromSize(size uint) int64
	SetPageSize(pageSize uint)
}

// PageableQueryRedisImpl is an implementation for PageableQuery interface
type PageableQueryRedisImpl struct {
	PageableQuery

	redis    *redis.Client
	name     string
	pageSize uint
}

// SetRedis is a helper to set redis client
func (pq *PageableQueryRedisImpl) SetRedis(redis *redis.Client) {
	pq.redis = redis
}

// GetRedis gets Redis connection
func (pq *PageableQueryRedisImpl) GetRedis() *redis.Client {
	return pq.redis
}

// SetTableName sets table name
func (pq *PageableQueryRedisImpl) SetTableName(name string) {
	pq.name = name
}

// TableName gets table name
func (pq *PageableQueryRedisImpl) TableName() string {
	return pq.name
}

// Save redis query with pagination, performs SET Op and ZADD Op
func (pq *PageableQueryRedisImpl) Save(id string, value interface{}) error {
	tx := pq.redis.TxPipeline()

	if err := tx.Set(id, value, 0).Err(); err != nil {
		return err
	}

	if err := tx.ZAdd(pq.TableName(), &redis.Z{
		Score:  float64(time.Now().Unix()),
		Member: id,
	}).Err(); err != nil {
		return err
	}

	if _, err := tx.Exec(); err != nil {
		return err
	}

	return nil
}

// FindByID is to get partner data
func (pq *PageableQueryRedisImpl) FindByID(key string) (interface{}, error) {
	result, err := pq.redis.Get(key).Result()
	if err != nil {
		return nil, errors.New("key not found " + key + " " + err.Error())
	}

	return result, nil
}

// FindAll finds all records starting from until to
func (pq *PageableQueryRedisImpl) FindAll(from, to int64) ([]interface{}, error) {
	str, err := pq.redis.ZRange(pq.TableName(), from, to).Result()
	if err != nil {
		return nil, err
	}

	if len(str) == 0 {
		return nil, errors.New("no record found")
	}

	results, err2 := pq.redis.MGet(str...).Result()
	if err2 != nil {
		return nil, err2
	}

	return results, nil
}

// GetDataAtPage gets record data at page
func (pq *PageableQueryRedisImpl) GetDataAtPage(pageNo int64) ([]interface{}, error) {
	totalPages := pq.GetTotalPages()
	if totalPages < pageNo {
		return nil, fmt.Errorf("page not found: page %v of %v", pageNo, totalPages)
	}

	from := pageNo * int64(pq.pageSize)
	to := from + int64(pq.pageSize)

	// Need to decrement since the record at "To" boundary is actually included
	if to != 0 {
		to--
	}

	return pq.FindAll(from, to)
}

// GetTotalPagesFromSize gets total page size
func (pq *PageableQueryRedisImpl) GetTotalPagesFromSize(size uint) int64 {
	page := math.Floor(float64(pq.Count()) / float64(size))
	return int64(page)
}

// GetDataAtPageFromSize gets data with size
func (pq *PageableQueryRedisImpl) GetDataAtPageFromSize(pageNo int64, size uint) ([]interface{}, error) {
	totalPages := pq.GetTotalPagesFromSize(size)
	if totalPages < pageNo {
		return nil, fmt.Errorf("page not found: page %v of %v", pageNo, totalPages)
	}

	from := pageNo * totalPages
	to := from + totalPages

	return pq.FindAll(from, to)
}

// GetTotalPages gets query total page
func (pq *PageableQueryRedisImpl) GetTotalPages() int64 {
	page := math.Floor(float64(pq.Count()) / float64(pq.pageSize))
	return int64(page)
}

// SetPageSize sets query page size
func (pq *PageableQueryRedisImpl) SetPageSize(pageSize uint) {
	pq.pageSize = pageSize
}

// Count is to count partner data
func (pq *PageableQueryRedisImpl) Count() int64 {
	return pq.redis.ZCount(pq.TableName(), "-inf", "+inf").Val()
}

// PageData holds pagination data for record
type PageData struct {
	PageNo       string `json:"pageNo"`
	TotalPages   string `json:"totalPages"`
	TotalRecords string `json:"totalRecords"`
}

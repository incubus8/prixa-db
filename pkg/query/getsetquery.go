package query

import (
	"errors"
	"time"

	"github.com/go-redis/redis/v7"
)

// GetSetQueryWithTTL query representation with TTL, currently used for Redis as data store implementation
type GetSetQueryWithTTL interface {
	CommonQuery

	SetTTL(seconds time.Duration)
	GetTTL() time.Duration
	SetAutoRenewWhenSet(renew bool)
}

// GetSetQueryWithTTLRedisImpl implements basic Redis Op with TTL
type GetSetQueryWithTTLRedisImpl struct {
	GetSetQueryWithTTL

	redis *redis.Client
	name  string
	ttl   time.Duration
	renew bool
}

// SetRedis is a helper to set redis client
func (pq *GetSetQueryWithTTLRedisImpl) SetRedis(redis *redis.Client) {
	pq.redis = redis
}

// SetTableName sets table name
func (pq *GetSetQueryWithTTLRedisImpl) SetTableName(name string) {
	pq.name = name
}

// TableName sets redis main key
func (pq *GetSetQueryWithTTLRedisImpl) TableName() string {
	return pq.name
}

// SetTTL sets redis TTL
func (pq *GetSetQueryWithTTLRedisImpl) SetTTL(duration time.Duration) {
	pq.ttl = duration
}

// GetTTL gets redis TTL
func (pq *GetSetQueryWithTTLRedisImpl) GetTTL() time.Duration {
	return pq.ttl
}

// Save saves data
func (pq *GetSetQueryWithTTLRedisImpl) Save(id string, value interface{}) error {
	tx := pq.redis.TxPipeline()

	if err := tx.Set(id, value, pq.GetTTL()).Err(); err != nil {
		return err
	}
	if pq.renew {
		if err := tx.Expire(id, pq.GetTTL()).Err(); err != nil {
			return err
		}
	}

	if _, err := tx.Exec(); err != nil {
		return err
	}
	return nil
}

// Delete deletes data in Redis
func (pq *GetSetQueryWithTTLRedisImpl) Delete(id string) error {
	err := pq.redis.Del(id)
	if err.Val() == 0 {
		return errors.New("key not found " + id)
	}
	return nil
}

// FindByID finds record by ID
func (pq *GetSetQueryWithTTLRedisImpl) FindByID(key string) (interface{}, error) {
	result, err := pq.redis.Get(key).Result()
	if err != nil {
		return nil, errors.New("key not found " + key + " " + err.Error())
	}
	return result, nil
}

// FindAll find all records
func (pq *GetSetQueryWithTTLRedisImpl) FindAll(from, to int64) ([]interface{}, error) {
	return nil, errors.New("method FindAll() is not implemented in GetSetQueryWithTTL")
}

// Count is to count partner data
func (pq *GetSetQueryWithTTLRedisImpl) Count() int64 {
	return 0
}

// SetAutoRenewWhenSet is
func (pq *GetSetQueryWithTTLRedisImpl) SetAutoRenewWhenSet(renew bool) {
	pq.renew = renew
}

// LPush is
func (pq *GetSetQueryWithTTLRedisImpl) LPush(list string, value interface{}) error {
	tx := pq.redis.TxPipeline()

	tx.LPush(list, value)

	if _, err := tx.Exec(); err != nil {
		return err
	}
	return nil
}

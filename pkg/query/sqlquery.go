package query

import (
	"database/sql"
)

// SQLCommonQuery is an abstract that defines SQL Common Query
type SQLCommonQuery interface {
	Create(query string, args ...interface{}) error
}

// SQLCommonQueryImpl is an implementation for SQLCommonQuery interface
type SQLCommonQueryImpl struct {
	SQLCommonQuery
	client *sql.DB
}

// SetClient is a helper to set db client
func (sq *SQLCommonQueryImpl) SetClient(dbClient *sql.DB) {
	sq.client = dbClient
}

// Create is to insert data to database
func (sq *SQLCommonQueryImpl) Create(query string, args ...interface{}) error {
	row, err := sq.client.Query(query, args...)
	if row != nil {
		_ = row.Close()
	}
	if err != nil {
		return err
	}
	return nil
}

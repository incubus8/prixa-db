package model

// DoctorChat is
type DoctorChat struct {
	ID             string
	Name           string
	Partner        string
	SpecialityID   string
	SpecialityName string
	Rating         float64
	Status         string
	PatientHandled int
	Totalpatient   int
}

// ChatRequest is
type ChatRequest struct {
	SessionID  string `json:"sessionId"`
	Partner    string `json:"partner"`
	Speciality string `json:"speciality"`
	User       string `json:"user"`
	CreatedAt  string `json:"createdAt"`
}

// ChatResult is
type ChatResult struct {
	SessionID   string      `json:"sessionId"`
	Partner     string      `json:"partner"`
	Speciality  string      `json:"speciality"`
	User        string      `json:"user"`
	CreatedAt   string      `json:"createdAt"`
	Doctor      *DoctorChat `json:"doctor"`
	Message     string      `json:"message"`
	LastUpdated string      `json:"lastUpdated"`
	Status      string      `json:"status"`
}

// ResolveChatRequest ia
type ResolveChatRequest struct {
	SessionID   string `json:"sessionId"`
	Information string `json:"information"`
}

// SetDoctorStatusRequest ia
type SetDoctorStatusRequest struct {
	DoctorID string `json:"doctorId"`
	Status   string `json:"status"`
}

package model

import (
	"encoding/json"
	"time"

	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
)

// DoctorLeaves is
type DoctorLeaves struct {
	DoctorID     string                                      `json:"doctorId"`
	HospitalID   string                                      `json:"hospitalId"`
	DoctorLeaves []*telemedicineGRPC.DoctorLeaveHospitalData `json:"doctorLeaves"`
}

// MarshalBinary is
//lint:ignore U1001 used for the Redis lib
func (leaves *DoctorLeaves) MarshalBinary() ([]byte, error) {
	return json.Marshal(leaves)
}

// DoctorTimeSlots is
type DoctorTimeSlots struct {
	DoctorID        string                                 `json:"doctorId"`
	HospitalID      string                                 `json:"hospitalId"`
	AppointmentDate *time.Time                             `json:"appointmentDate"`
	DoctorTimeSlots []*telemedicineGRPC.DoctorTimeSlotData `json:"doctorTimeSlots"`
}

// MarshalBinary is
//lint:ignore U1001 used for the Redis lib
func (timeSlot *DoctorTimeSlots) MarshalBinary() ([]byte, error) {
	return json.Marshal(timeSlot)
}

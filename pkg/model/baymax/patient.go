package baymax

// Patient is
type Patient struct {
	ID             string
	FirstName      string
	LastName       string
	CardNumber     string
	Gender         *string
	DOB            *string
	ClientAppID    string
	Organization   string
	PhoneNumber    *string
	VipStatus      bool
	Class          *string
	Height         *float64
	Weight         *float64
	Age            float64
	ExternalUserID *string
	PreconditionID *string
	PatientAddress *string
}

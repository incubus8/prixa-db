package baymax

// Permission is
type Permission struct {
	ModelID        string
	PermissionID   string
	PermissionName string
	GuardName      string
}

// PermissionViewConversationMessage is
type PermissionViewConversationMessage struct {
	UserID         string `json:"userID"`
	ConversationID string `json:"conversationID"`
	HasPermission  bool   `json:"hasPermission"`
}

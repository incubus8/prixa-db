package baymax

import (
	"os"

	"github.com/dgrijalva/jwt-go"
	baymaxGRPC "github.com/prixa-ai/prixa-proto/proto/baymax/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

// Transaction is
type Transaction struct {
	ID        string `json:"id"`
	DoctorID  string `json:"doctorId"`
	PatientID string `json:"patientId"`
	State     string `json:"state"`
}

// ActiveTransaction is
type ActiveTransaction struct {
	TransactionID     string
	ChatRoomID        string
	PatientID         string
	PatientToken      *string
	PatientCardNumber string
	State             string
	DoctorID          *string
	ConversationID    *string
	InboxID           *string
	WebsiteToken      *string
	SourceID          *string
	InboxName         *string
	DoctorFirstName   *string
	DoctorLastName    *string
}

func (trx *ActiveTransaction) generateCWToken() (string, error) {
	if trx.ConversationID == nil {
		return "", nil
	}

	signingKey := []byte(os.Getenv("JWT_SIGNATURE_KEY"))
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"source_id":      cast.ToString(trx.SourceID),
		"inbox_id":       cast.ToString(trx.InboxID),
		"transaction_id": trx.TransactionID,
		"pubsub_token":   cast.ToString(trx.PatientToken),
	})

	tokenString, err := token.SignedString(signingKey)
	return tokenString, err
}

// MapToGRPCActiveTransactionData is
func (trx *ActiveTransaction) MapToGRPCActiveTransactionData() *baymaxGRPC.ActiveTransaction {
	cwToken, err := trx.generateCWToken()
	if err != nil {
		logrus.Errorln("error generate cwToken " + err.Error())
	}
	return &baymaxGRPC.ActiveTransaction{
		ChatRoomId:        trx.ChatRoomID,
		ConversationId:    cast.ToString(trx.ConversationID),
		DoctorId:          cast.ToString(trx.DoctorID),
		InboxId:           cast.ToString(trx.InboxID),
		PatientCardNumber: trx.PatientCardNumber,
		PatientId:         trx.PatientID,
		PubsubToken:       cast.ToString(trx.PatientToken),
		State:             trx.State,
		TransactionId:     trx.TransactionID,
		WebsiteToken:      cast.ToString(trx.WebsiteToken),
		CwToken:           cwToken,
		DoctorFirstName:   cast.ToString(trx.DoctorFirstName),
		DoctorLastName:    cast.ToString(trx.DoctorLastName),
		InboxName:         cast.ToString(trx.InboxName),
	}
}

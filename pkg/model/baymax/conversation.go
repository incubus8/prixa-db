package baymax

import (
	baymaxGRPC "github.com/prixa-ai/prixa-proto/proto/baymax/v1"
	"github.com/spf13/cast"
)

// Conversation is
type Conversation struct {
	ID            string  `json:"id"`
	ClientID      string  `json:"clientId"`
	InboxID       string  `json:"inboxId"`
	TransactionID *string `json:"transactionId"`
	PatientID     *string `json:"patientId"`
	DoctorID      *string `json:"doctorId"`
}

// MessageData is
type MessageData struct {
	ID             string
	Content        string
	ClientID       string
	InboxID        string
	ConversationID string
	MessageType    string
	MessageSubType *string
	Sender         string
	Private        int
	ContentType    string
	Status         string
	DoctorID       *string
	PatientID      *string
	CreatedAt      string
	UpdatedAt      string
}

// MapToGRPCMessageData is
func (m *MessageData) MapToGRPCMessageData() *baymaxGRPC.MessageData {
	return &baymaxGRPC.MessageData{
		ClientId:       m.ClientID,
		Content:        m.Content,
		ConversationId: m.ConversationID,
		DoctorId:       cast.ToString(m.DoctorID),
		Id:             m.ID,
		InboxId:        m.InboxID,
		MessageSubtype: cast.ToString(m.MessageSubType),
		MessageType:    m.MessageType,
		PatientId:      cast.ToString(m.PatientID),
		Private:        cast.ToBool(m.Private),
		Status:         m.Status,
		CreatedAt:      m.CreatedAt,
		UpdatedAt:      m.UpdatedAt,
	}
}

// SendMessageEvent is
type SendMessageEvent struct {
	UuID          string                `json:"uuid"`
	DisplayName   string                `json:"displayName"`
	Job           string                `json:"job"`
	MaxTries      interface{}           `json:"maxTries"`
	MaxExceptions interface{}           `json:"maxExceptions"`
	Delay         interface{}           `json:"delay"`
	Timeout       interface{}           `json:"timeout"`
	TimeoutAt     interface{}           `json:"timeoutAt"`
	Data          *SendMessageEventData `json:"data"`
	ID            string                `json:"id"`
	Attempts      int                   `json:"attempts"`
}

// SendMessageEventData is
type SendMessageEventData struct {
	CommandName string `json:"commandName"`
	Command     string `json:"command"`
}

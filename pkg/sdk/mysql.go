package sdk

import (
	"database/sql"
	"errors"
	"os"

	_ "github.com/go-sql-driver/mysql" // required for connection to the mysql driver
)

// SetupMysqlClient to setup mysql as db client
func SetupMysqlClient() (*sql.DB, error) {
	url, _ := os.LookupEnv("MYSQL_URL")
	if url == "" {
		return nil, errors.New("mysql URL not found in ENV")
	}

	db, err := sql.Open("mysql", url)
	if err != nil {
		return nil, errors.New("unable to connect to mysql database")
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(75)
	db.SetMaxIdleConns(10)

	return db, nil
}

package sdk

import (
	"os"

	"github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/mailconfig"
)

// NotificationAPI defines Sendgrid client
type NotificationAPI struct {
	Client *sendgrid.Client

	apiKey string
}

// NewNotificationAPI creates new notification API
func NewNotificationAPI() NotificationAPI {
	apiKey := os.Getenv(mailconfig.SendgridAPIKey)
	return NotificationAPI{
		apiKey: apiKey,
		Client: sendgrid.NewSendClient(apiKey),
	}
}

// EmailRequest defines diagnose email data to be sent to Sendgrid
type EmailRequest struct {
	To            string `json:"to"`
	Name          string `json:"name"`
	Gender        string `json:"gender"`
	AgeRange      string `json:"ageRange"`
	AgeYear       string `json:"ageYear"`
	AgeMonth      string `json:"ageMonth"`
	WeightInKg    string `json:"weightInKg"`
	HeightInCm    string `json:"heightInCm"`
	BmiIndex      string `json:"bmiIndex"`
	Subject       string `json:"subject"`
	Preconditions string `json:"preconditions"`
	MainSymptoms  string `json:"mainSymptoms"`
	Symptoms      string `json:"symptoms"`
	NotKnown      string `json:"notKnown"`
	Unknown       string `json:"unknown"`
	PrixaResults  string `json:"prixaResults"`
	Email         string `json:"email"`
	URL           string `json:"url"`
}

// SendMail is to send mail
func (api *NotificationAPI) SendMail(request EmailRequest) (*rest.Response, error) {
	to := mail.NewEmail("", request.To)

	personals := mail.NewPersonalization()
	personals.AddTos(to)
	personals.SetDynamicTemplateData("Sender_Name", request.Name)
	personals.SetDynamicTemplateData("gender", request.Gender)
	personals.SetDynamicTemplateData("ageRange", request.AgeRange)
	personals.SetDynamicTemplateData("ageYear", request.AgeYear)
	personals.SetDynamicTemplateData("ageMonth", request.AgeMonth)
	personals.SetDynamicTemplateData("preconditions", request.Preconditions)
	personals.SetDynamicTemplateData("heightInCm", request.HeightInCm)
	personals.SetDynamicTemplateData("weightInKg", request.WeightInKg)
	personals.SetDynamicTemplateData("mainSymptoms", request.MainSymptoms)
	personals.SetDynamicTemplateData("symptoms", request.Symptoms)
	personals.SetDynamicTemplateData("notKnown", request.NotKnown)
	personals.SetDynamicTemplateData("unknown", request.Unknown)
	personals.SetDynamicTemplateData("prixaResults", request.PrixaResults)
	personals.SetDynamicTemplateData("bmiIndex", request.BmiIndex)

	msg := mailconfig.NewV3MailWithTracking()
	msg.SetTemplateID(mailconfig.MailerConfig[mailconfig.MailPrixaDiagRes])
	msg.Subject = request.Subject
	msg.AddPersonalizations(personals)

	response, err := api.Client.Send(msg)
	log := logrus.WithField("response", response)
	if err != nil {
		log.WithError(err).Errorln()
		return response, err
	}

	log.Infoln()
	return response, nil
}

// SendRegisterMail sends user registration email
func (api *NotificationAPI) SendRegisterMail(request EmailRequest) (*rest.Response, error) {
	to := mail.NewEmail("", request.To)

	personals := mail.NewPersonalization()
	personals.AddTos(to)
	personals.SetDynamicTemplateData("Sender_Name", request.Name)
	personals.SetDynamicTemplateData("url", request.URL)

	msg := mailconfig.NewV3MailWithTracking()
	msg.SetTemplateID(mailconfig.MailerConfig[mailconfig.MailPrixaSignup])
	msg.Subject = request.Subject
	msg.AddPersonalizations(personals)

	response, err := api.Client.Send(msg)
	log := logrus.WithField("response", response)
	if err != nil {
		log.WithError(err).Errorln()
		return response, err
	}

	log.Infoln()
	return response, nil
}

// SendForgetPasswordMail is
func (api *NotificationAPI) SendForgetPasswordMail(request EmailRequest) (*rest.Response, error) {
	to := mail.NewEmail("", request.To)

	personals := mail.NewPersonalization()
	personals.AddTos(to)
	personals.SetDynamicTemplateData("Sender_Name", request.Name)
	personals.SetDynamicTemplateData("url", request.URL)

	msg := mailconfig.NewV3MailWithTracking()
	msg.SetTemplateID(mailconfig.MailerConfig[mailconfig.MailPrixaForgetpwd])
	msg.Subject = request.Subject
	msg.AddPersonalizations(personals)

	response, err := api.Client.Send(msg)
	log := logrus.WithField("response", response)
	if err != nil {
		log.WithError(err).Errorln()
		return response, err
	}

	log.Infoln()
	return response, nil
}

// SendForgetPasswordSuccessMail is
func (api *NotificationAPI) SendForgetPasswordSuccessMail(request EmailRequest) (*rest.Response, error) {
	to := mail.NewEmail("", request.To)

	personals := mail.NewPersonalization()
	personals.AddTos(to)
	personals.SetDynamicTemplateData("Sender_Name", request.Name)

	msg := mailconfig.NewV3MailWithTracking()
	msg.SetTemplateID(mailconfig.MailerConfig[mailconfig.MailPrixaForgetpwdSuccess])
	msg.Subject = request.Subject
	msg.AddPersonalizations(personals)

	response, err := api.Client.Send(msg)
	log := logrus.WithField("response", response)
	if err != nil {
		log.WithError(err).Errorln()
		return response, err
	}

	log.Infoln()
	return response, nil
}

// SendRegisterSuccessMail sends user's success register email
func (api *NotificationAPI) SendRegisterSuccessMail(request EmailRequest) (*rest.Response, error) {
	to := mail.NewEmail("", request.To)

	personals := mail.NewPersonalization()
	personals.AddTos(to)
	personals.SetDynamicTemplateData("Sender_Name", request.Name)
	personals.SetDynamicTemplateData("url", request.URL)

	msg := mailconfig.NewV3MailWithTracking()
	msg.SetTemplateID(mailconfig.MailerConfig[mailconfig.MailPrixaSignupSuccess])
	msg.Subject = request.Subject
	msg.AddPersonalizations(personals)

	response, err := api.Client.Send(msg)
	log := logrus.WithField("response", response)
	if err != nil {
		log.WithError(err).Errorln()
		return response, err
	}

	log.Infoln()
	return response, nil
}

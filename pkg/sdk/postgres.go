package sdk

import (
	"database/sql"
	"errors"
	"os"
)

// SetupPostgreClient to setup postgres as db client
func SetupPostgreClient() (*sql.DB, error) {
	url, _ := os.LookupEnv("POSTGRE_URL")
	if url == "" {
		return nil, errors.New("postgres URL not found in ENV")
	}

	db, err := sql.Open("postgres", url)
	if err != nil {
		return nil, errors.New("unable to connect to postgre database")
	}

	err = db.Ping()
	if err != nil {
		return nil, err
	}

	db.SetMaxOpenConns(75)
	db.SetMaxIdleConns(10)

	return db, nil
}

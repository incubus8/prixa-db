package sdk

import (
	"context"
	"encoding/json"
	"os"
	"strings"

	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
)

// NewElasticSearchClient creates new Elasticsearch client
func NewElasticSearchClient() (*elastic.Client, error) {
	elasticSearchURL := getElasticSearchURL()
	esClient, err := elastic.NewClient(elastic.SetURL(elasticSearchURL),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false))
	return esClient, err
}

func getElasticSearchURL() string {
	url := os.Getenv("ELASTICSEARCH_URL")
	return url
}

// GetDataByIDInElasticSearch is get data in index elasticsearch by documentId
func GetDataByIDInElasticSearch(ctx context.Context, esClient *elastic.Client, indexName string, documentID string) (json.RawMessage, error) {
	hit, err := esClient.Get().Index(strings.ToLower(indexName)).Id(documentID).Do(ctx)
	if err != nil {
		logrus.WithError(err).Errorln()
		return nil, err
	}

	if !hit.Found {
		logrus.WithError(err).Errorln("ES hit is not found")
		return nil, err
	}

	return hit.Source, err
}

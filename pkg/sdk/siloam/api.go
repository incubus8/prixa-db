package siloam

import (
	"bytes"
	"context"
	"encoding/json"
	"os"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
	siloamGRPC "github.com/prixa-ai/prixa-proto/proto/siloam/v1"
	"github.com/sirupsen/logrus"
)

// MySiloamAPI maps HTTP API to Siloam HTTP API handler
type MySiloamAPI struct {
	Client  *resty.Client
	baseURL string
	siloamGRPC.SiloamServiceServer
}

// NewMySiloamAPI creates new siloam API connection
func NewMySiloamAPI(timeout, retryCount, retryWaitTime uint) MySiloamAPI {
	mySiloam := MySiloamAPI{}
	mySiloam.baseURL = os.Getenv("SILOAM_API_URL")
	mySiloam.Client = resty.New()
	mySiloam.Client.SetTimeout(time.Duration(timeout) * time.Second)
	mySiloam.Client.SetRetryCount(int(retryCount))
	mySiloam.Client.SetRetryWaitTime(time.Duration(retryWaitTime) * time.Second)

	return mySiloam
}

// GetSpecialitiesData get speciality
func (api *MySiloamAPI) GetSpecialitiesData(ctx context.Context, req *siloamGRPC.GetSpecialitiesRequestData) (*siloamGRPC.GetSpecialitiesResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}
	query := ""
	if req.HospitalId != "" {
		query += "hospitalId=" + req.HospitalId
	}

	url := api.baseURL + "/v2//generals/specialities?" + query

	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("url", url).Errorln()
		return nil, err
	}

	ret := siloamGRPC.GetSpecialitiesResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("response", resp).Errorln("failed to unmarshal to speciality response data")
		return nil, err
	}
	return &ret, nil
}

// GetDoctorsData gets doctor
func (api *MySiloamAPI) GetDoctorsData(ctx context.Context, req *siloamGRPC.GetDoctorsRequestData) (*siloamGRPC.GetDoctorsResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	queries := []string{}
	if req.DoctorId != "" {
		queries = append(queries, "doctorId="+req.DoctorId)
	}

	if req.DoctorName != "" {
		queries = append(queries, "name="+req.DoctorName)
	}

	if req.HospitalId != "" {
		queries = append(queries, "hospitalId="+req.HospitalId)
	}

	if req.SpecialityId != "" {
		queries = append(queries, "specialityId="+req.SpecialityId)
	}
	url := api.baseURL + "/v2/mobile/doctors?" + strings.Join(queries, "&")

	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("url", url).Errorln()
		return nil, err
	}

	ret := siloamGRPC.GetDoctorsResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("response", resp).Errorln("failed to unmarshal to doctor response data")
		return nil, err
	}
	return &ret, nil
}

// GetHospitalsData gets hospital
func (api *MySiloamAPI) GetHospitalsData(ctx context.Context, req *siloamGRPC.GetHospitalsRequestData) (*siloamGRPC.GetHospitalsResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	queries := []string{}
	if req.GetOnImplementation() != nil {
		queries = append(queries, "onImplementation="+req.OnImplementation.String())
	}

	if req.AreaId != "" {
		queries = append(queries, "areaId="+req.AreaId)
	}
	url := api.baseURL + "/v2/hospitals?" + strings.Join(queries, "&")

	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("url", url).Errorln()
		return nil, err
	}

	ret := siloamGRPC.GetHospitalsResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("response", resp).Errorln("failed to unmarshal to hospital response data")
		return nil, err
	}
	return &ret, nil
}

// GetSchedulesPerWeeksData get schedule of doctor in hospital
func (api *MySiloamAPI) GetSchedulesPerWeeksData(ctx context.Context, req *siloamGRPC.GetSchedulesPerWeeksRequestData) (*siloamGRPC.GetSchedulesPerWeeksResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	url := api.baseURL + "/v2/schedules/hospital-id/" + req.HospitalId + "/doctor-id/" + req.DoctorId
	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("url", url).Errorln()
		return nil, err
	}
	ret := siloamGRPC.GetSchedulesPerWeeksResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("response", resp).Errorln("failed to unmarshal to schedules response data")
		return nil, err
	}
	return &ret, nil
}

// GetTimeSlotsData gets doctor time slot data
func (api *MySiloamAPI) GetTimeSlotsData(ctx context.Context, req *siloamGRPC.GetTimeSlotsRequestData) (*siloamGRPC.GetTimeSlotsResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	appointmentDate, err := time.Parse("2006-01-02", req.GetAppointmentDate())
	if err != nil {
		return nil, err
	}

	url := api.baseURL + "/v2/schedules/time-slot/hospital-id/" + req.HospitalId + "/doctor-id/" + req.DoctorId + "/appointment-date/" + appointmentDate.Format("2006-01-02")
	logrus.Debugln(url)

	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).WithField("response", resp).Errorln()
		return nil, err
	}

	ret := siloamGRPC.GetTimeSlotsResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).WithField("response", resp).Errorln()
		return nil, err
	}
	ret.StatusCode = int32(resp.StatusCode())
	return &ret, nil
}

// GetDoctorLeavesHospitalData gets doctor hospital leaves info
func (api *MySiloamAPI) GetDoctorLeavesHospitalData(ctx context.Context, req *siloamGRPC.GetDoctorsLeavesHospitalRequestData) (*siloamGRPC.GetDoctorsLeavesHospitalResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	var queries []string
	if req.GetDoctorId() != nil {
		queries = append(queries, "doctorId="+req.DoctorId.GetValue())
	}
	url := api.baseURL + "/v2/mobile/doctors/leave/hospital/" + req.HospitalId + "?" + strings.Join(queries, "&")
	logrus.Debugln(url)

	resp, err := api.Client.R().Get(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).Errorln()
		return nil, err
	}

	ret := siloamGRPC.GetDoctorsLeavesHospitalResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).WithField("response", resp).Errorln()
		return nil, err
	}
	ret.StatusCode = int32(resp.StatusCode())
	return &ret, nil
}

// PostAppointmentData handles new doctor appointment
func (api *MySiloamAPI) PostAppointmentData(ctx context.Context, req *siloamGRPC.PostAppointmentRequestData) (*siloamGRPC.PostAppointmentResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}
	url := api.baseURL + "/v2/appointments/"
	var reqBody []byte
	var err error
	reqBody, err = json.Marshal(req)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("request", req).Errorln()
		return nil, err
	}

	if !req.IsWaitingList {
		var mapStruct map[string]interface{}
		err = json.Unmarshal(reqBody, &mapStruct)
		if err != nil {
			logrus.WithError(err).WithContext(ctx).WithField("request", req).Errorln()
			return nil, err
		}
		mapStruct["isWaitingList"] = false
		reqBody, err = json.Marshal(mapStruct)
		if err != nil {
			logrus.WithError(err).WithContext(ctx).WithField("request", req).Errorln()
			return nil, err
		}
	}

	resp, err := api.Client.R().SetBody(bytes.NewReader(reqBody)).Post(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("request", reqBody).WithField("response", resp).Errorln()
		return nil, err
	}

	ret := siloamGRPC.PostAppointmentResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("response", resp).Errorln()
		return nil, err
	}
	ret.StatusCode = int32(resp.StatusCode())
	return &ret, nil
}

// CancelAppointmentData handles cancel appointment
func (api *MySiloamAPI) CancelAppointmentData(ctx context.Context, req *siloamGRPC.CancelAppointmentRequestData) (*siloamGRPC.CancelAppointmentResponseData, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}
	url := api.baseURL + "/v2/appointments/" + req.AppointmentId
	logrus.Debugln(url)

	reqBody, err := json.Marshal(req)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("request", req).Errorln()
		return nil, err
	}
	resp, err := api.Client.R().SetBody(bytes.NewReader(reqBody)).Delete(url)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).Errorln()
		return nil, err
	}

	ret := siloamGRPC.CancelAppointmentResponseData{}
	err = json.Unmarshal(resp.Body(), &ret)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("siloamUrl", url).WithField("response", resp).Errorln()
		return nil, err
	}
	ret.StatusCode = int32(resp.StatusCode())
	return &ret, nil
}

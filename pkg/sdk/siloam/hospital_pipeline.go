package siloam

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/apache/beam/sdks/go/pkg/beam"
	"github.com/apache/beam/sdks/go/pkg/beam/runners/direct"
	graceful "github.com/ashulepov/gocron-graceful"
	"github.com/go-resty/resty/v2"
	"github.com/jasonlvhit/gocron"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/repository"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/util"
)

type hospitalPipeline struct {
	ctx  context.Context
	repo consumercallback.ConsumerCallback
}

// StartIngestHospitalDataFromAirtable is
func StartIngestHospitalDataFromAirtable(wg *sync.WaitGroup) {
	ctx := context.Background()
	interval, err := getSchedulingInterval(ctx)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Fatalln("failed to connect get interval")
	}
	pipeline := setupHospitalPipeline(ctx)

	cron := gocron.NewScheduler()
	err = cron.Every(interval).Minutes().From(gocron.NextTick()).Do(graceful.TaskWrapper, wg, pipeline.startProcessPipeline)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Fatalln()
	}
	go func() {
		<-cron.Start()
	}()
}

func setupHospitalPipeline(ctx context.Context) *hospitalPipeline {
	msClient := sdk.NewMeilisearchClient()
	repo := repository.NewMeilisearchClient(ctx, "hospitals", msClient)

	return &hospitalPipeline{
		ctx:  ctx,
		repo: repo,
	}
}

func (param *hospitalPipeline) startProcessPipeline() {
	p, s := beam.NewPipelineWithRoot()
	lines := beam.CreateList(s, []airtableParam{
		{
			URL:   os.Getenv("AIRTABLE_URL"),
			Token: os.Getenv("AIRTABLE_TOKEN"),
		},
	})
	p1 := getSpecialityRecords(s, lines)
	p2 := getHospitalRecords(s, p1)
	beam.ParDo0(s, func(data *telemedicineGRPC.HospitalData) {
		marshalData, err := json.Marshal(data)
		if err != nil {
			logrus.WithError(err).Warningln("failed to marshal hospital data")
			return
		}
		err = param.repo.OnConsume(marshalData)
		if err != nil {
			logrus.WithError(err).Warningln()
			return
		}
	}, p2)

	_ = direct.Execute(context.Background(), p)
}

type airtableParam struct {
	URL   string
	Token string
}

type getHospitalParam struct {
	airtableParam     airtableParam
	specialityRecords []db.Record
}

func getSpecialityRecordFromAirtable(a airtableParam, emit func(getHospitalParam)) {
	var records []db.Record
	offset := ""
	for ok := true; ok; ok = offset != "" {
		offset = ""
		recordRoot, _ := a.fetchDataFromAirtable(tableSpecialities, offset)
		if recordRoot != nil {
			records = append(records, recordRoot.Records...)
			offset = recordRoot.Offset
		}

	}
	emit(getHospitalParam{
		airtableParam:     a,
		specialityRecords: records,
	})
}

func getHospitalRecordFromAirtable(param getHospitalParam, emit func([]db.Record, []db.Record)) {
	offset := ""
	for ok := true; ok; ok = offset != "" {
		offset = ""
		recordRoot, _ := param.airtableParam.fetchDataFromAirtable(tableHospitals, offset)
		if recordRoot != nil {
			offset = recordRoot.Offset
			emit(recordRoot.Records, param.specialityRecords)
		}
	}
}

func (a *airtableParam) fetchDataFromAirtable(table, offset string) (*db.RecordRoot, error) {
	tableURL := fmt.Sprintf("%v/%v", a.URL, table)
	if offset != "" {
		tableURL = fmt.Sprintf("%v/%v?%v", a.URL, table, offset)
	}

	client := resty.New()
	client.SetTimeout(10 * time.Second)
	client.SetRetryCount(3)
	client.SetRetryWaitTime(5 * time.Second)
	client.SetHeader("Content-type", "application/json")
	client.SetHeader("Authorization", "Bearer "+a.Token)
	resp, err := client.R().Get(tableURL)
	if err != nil {
		logrus.WithField("url", tableURL).WithError(err).Errorln()
		return nil, err
	}

	var recordRoot db.RecordRoot
	if unmarshalError := json.Unmarshal(resp.Body(), &recordRoot); unmarshalError != nil {
		logrus.WithError(unmarshalError).Errorln("failed to unmarshal records speciality data")
		return nil, err
	}
	return &recordRoot, nil
}

func getSpecialityRecords(s beam.Scope, lines beam.PCollection) beam.PCollection {
	s = s.Scope("getSpecialityRecords")
	return beam.ParDo(s, getSpecialityRecordFromAirtable, lines)
}

func getHospitalRecords(s beam.Scope, lines beam.PCollection) beam.PCollection {
	s = s.Scope("getHospitalRecords")
	p1 := beam.ParDo(s, getHospitalRecordFromAirtable, lines)
	p2 := beam.ParDo(s, mapHospital, p1)
	return p2
}

func mapHospital(hospitalRecords []db.Record, specialityRecords []db.Record, emit func(*telemedicineGRPC.HospitalData)) {
	for _, record := range hospitalRecords {
		specialityIDs := cast.ToStringSlice(record.Fields["Specialities"])
		hospital := mapToTelemedicineHospital(record)
		hospital.Specialities = mapTelemedicineSpecialiteiesHospital(specialityIDs, specialityRecords)
		hospital.LastUpdated = time.Now().Format(util.LayoutFormatDateTime)
		emit(hospital)
	}
}

func mapTelemedicineSpecialiteiesHospital(specialityIDs []string, specialityRecords []db.Record) []*telemedicineGRPC.SpecialityData {
	specialities := make([]*telemedicineGRPC.SpecialityData, 0, len(specialityIDs))
	for _, id := range specialityIDs {
		for _, record := range specialityRecords {
			if record.ID == id {
				specialityID, _ := record.Fields["siloam_id"].(string)
				name, _ := record.Fields["Name"].(string)
				nameIndo, _ := record.Fields["Name Indo"].(string)

				specialities = append(specialities, &telemedicineGRPC.SpecialityData{
					Id:       specialityID,
					Name:     name,
					NameIndo: nameIndo,
				})
			}
		}
	}
	return specialities
}

func mapToTelemedicineHospital(record db.Record) *telemedicineGRPC.HospitalData {
	hospital := &telemedicineGRPC.HospitalData{
		Id:   cast.ToString(record.Fields["siloam_id"]),
		Name: cast.ToString(record.Fields["Hospital Name"]),
		Address: &telemedicineGRPC.AddressData{
			District:    cast.ToString(record.Fields["City"]),
			Province:    cast.ToString(record.Fields["Province"]),
			SubDistrict: cast.ToString(record.Fields["Sub District"]),
			Street:      cast.ToString(record.Fields["Address"]),
			ZipCode:     cast.ToString(record.Fields["Zipcode"]),
		},
		Alias:  cast.ToString(record.Fields["Alias"]),
		AreaId: cast.ToString(record.Fields["siloam_areaId"]),
		Contact: &telemedicineGRPC.ContactData{
			Email:       cast.ToString(record.Fields["Email"]),
			PhoneNumber: cast.ToString(record.Fields["Phone"]),
		},
		ImageUrl: cast.ToString(record.Fields["Image URL"]),
		Source: &telemedicineGRPC.SourceData{
			OriginId:   cast.ToString(record.Fields["siloam_id"]),
			SourceId:   siloamSourceID,
			SourceName: siloamSourceName,
		},
	}
	lat := cast.ToFloat32(record.Fields["Latitude"])
	lon := cast.ToFloat32(record.Fields["Longitude"])
	if lat != 0 && lon != 0 && isValidCoordinate(lat, lon) {
		hospital.Coordinate = &telemedicineGRPC.CoordinateData{
			Lat: lat,
			Lon: lon,
		}
	}
	return hospital
}

func isValidCoordinate(lat, lon float32) bool {
	if lat < -90 || lat > 90 || lon < -180 || lon > 180 {
		return false
	}
	return true
}

package siloam

import (
	"context"
	"os"
	"strconv"
	"sync"
	"time"

	graceful "github.com/ashulepov/gocron-graceful"
	"github.com/jasonlvhit/gocron"
	meili "github.com/meilisearch/meilisearch-go"
	"github.com/mitchellh/mapstructure"
	siloamGRPC "github.com/prixa-ai/prixa-proto/proto/siloam/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/util"
)

const (
	tableDoctors      string = "doctors"
	tableHospitals    string = "hospitals"
	tableSpecialities string = "specialities"
	siloamSourceID    string = "siloam-api-v2"
	siloamSourceName  string = "Siloam-API-v2"
)

func getSchedulingInterval(ctx context.Context) (uint64, error) {
	intervalEnv := os.Getenv("INTERVAL_SCHEDULE_INGESTION_IN_MINUTES")
	interval, err := strconv.ParseUint(intervalEnv, 10, 64)
	if err != nil {
		logrus.WithContext(ctx).WithField("interval", intervalEnv).WithError(err).Fatalln()
		return 0, err
	}
	return interval, nil
}

type mySiloamPipelining struct {
	mySiloamAPI MySiloamAPI
	publisher   messaging.Publisher
	ctx         context.Context
	msClient    *meili.Client
}

func newSiloamPipelining(ctx context.Context, publisher messaging.Publisher, msClient *meili.Client) *mySiloamPipelining {
	return &mySiloamPipelining{
		mySiloamAPI: NewMySiloamAPI(10, 3, 4),
		ctx:         ctx,
		publisher:   publisher,
		msClient:    msClient,
	}
}

func setupSiloamPipelining(ctx context.Context) *mySiloamPipelining {
	publisher, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln("error when setup Publisher RabbitMQ")
	}

	msClient := sdk.NewMeilisearchClient()

	return newSiloamPipelining(ctx, publisher, msClient)
}

// TaskPipeliningSiloamAPIToRabbitMQ is
func TaskPipeliningSiloamAPIToRabbitMQ(wg *sync.WaitGroup) {
	ctx := context.Background()
	siloamPipelining := setupSiloamPipelining(ctx)
	interval, err := getSchedulingInterval(ctx)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Fatalln()
	}

	cron := gocron.NewScheduler()
	err = cron.Every(interval).Minutes().From(gocron.NextTick()).Do(graceful.TaskWrapper, wg, siloamPipelining.startProcessPipeliningSiloam)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Fatalln()
	}
	go func() {
		<-cron.Start()
	}()
}

func (siloam *mySiloamPipelining) startProcessPipeliningSiloam() {
	hospitalsResponse, err := siloam.mySiloamAPI.GetHospitalsData(siloam.ctx, &siloamGRPC.GetHospitalsRequestData{})
	if err != nil {
		logrus.WithContext(siloam.ctx).WithFields(logrus.Fields{
			"baseUrl": siloam.mySiloamAPI.baseURL,
			"api":     "Hospitals",
		}).WithError(err).Errorln()
		return
	}

	if hospitalsResponse.Data == nil {
		return
	}

	for _, hospital := range hospitalsResponse.Data {
		siloam.processPipeliningSiloamDoctorAPIToRabbitMQ(hospital)
	}
}

func (siloam *mySiloamPipelining) processPipeliningSiloamDoctorAPIToRabbitMQ(hospital *siloamGRPC.HospitalData) {
	doctorResponse, err := siloam.mySiloamAPI.GetDoctorsData(siloam.ctx, &siloamGRPC.GetDoctorsRequestData{
		HospitalId: hospital.HospitalId,
	})
	if err != nil {
		logrus.WithContext(siloam.ctx).WithFields(logrus.Fields{
			"url":        siloam.mySiloamAPI.baseURL,
			"api":        "Doctor",
			"hospitalID": hospital.HospitalId,
		}).WithError(err).Errorln()
		return
	}

	if doctorResponse.Data == nil {
		return
	}
	for _, doctor := range doctorResponse.Data {
		go siloam.processPipeliningSiloamDoctorSchedulesAPIToRabbitMQ(hospital, doctor)
		existDoctor := getExistDoctorInMeilisearch(siloam.ctx, doctor.DoctorId, siloam.msClient)
		doctorTransform := transformToDoctorTelemedicineData(doctor, hospital, existDoctor)
		siloam.publisher.Publish(doctorTransform, os.Getenv("RABBITMQ_SILOAM_DOCTOR_QUEUE"), "")
	}
}

func getExistDoctorInMeilisearch(ctx context.Context, doctorID string, msClient *meili.Client) *telemedicineGRPC.DoctorData {
	doc, err := sdk.GetDocByID(msClient, tableDoctors, doctorID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("index", tableDoctors).WithField("doctorID", doctorID).Warningln()
		return nil
	}

	var doctorData telemedicineGRPC.DoctorData
	if err = mapstructure.Decode(doc, &doctorData); err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("hit", doc).Errorln("error during unmarshal doctor data")
		return nil
	}
	return &doctorData
}

func transformToDoctorTelemedicineData(doctor *siloamGRPC.DoctorData, hospital *siloamGRPC.HospitalData, existDoctor *telemedicineGRPC.DoctorData) *telemedicineGRPC.DoctorData {
	var hospitals []*telemedicineGRPC.HospitalData
	hospitals = append(hospitals, &telemedicineGRPC.HospitalData{
		Alias:  hospital.Alias,
		AreaId: hospital.AreaId,
		Name:   hospital.Name,
		Id:     hospital.HospitalId,
	})
	if existDoctor != nil {
		for _, existHospital := range existDoctor.Hospitals {
			if existHospital.Id != hospital.HospitalId {
				hospitals = append(hospitals, existHospital)
			}
		}
	}

	return &telemedicineGRPC.DoctorData{
		Id:          doctor.DoctorId,
		Contact:     &telemedicineGRPC.ContactData{},
		ImageUrl:    doctor.ImageUrl,
		LastUpdated: time.Now().Format(util.LayoutFormatDateTime),
		Name:        doctor.Name,
		Speciality: &telemedicineGRPC.SpecialityData{
			Id:       doctor.SpecialityId,
			Name:     doctor.SpecializationNameEn,
			NameIndo: doctor.SpecializationName,
		},
		Source: &telemedicineGRPC.SourceData{
			OriginId:   doctor.DoctorId,
			SourceId:   siloamSourceID,
			SourceName: siloamSourceName,
		},
		Hospitals: hospitals,
	}
}

func (siloam *mySiloamPipelining) processPipeliningSiloamDoctorSchedulesAPIToRabbitMQ(hospital *siloamGRPC.HospitalData, doctor *siloamGRPC.DoctorData) {
	scheduleResponse, err := siloam.mySiloamAPI.GetSchedulesPerWeeksData(siloam.ctx, &siloamGRPC.GetSchedulesPerWeeksRequestData{
		HospitalId: hospital.HospitalId,
		DoctorId:   doctor.DoctorId,
	})
	if err != nil {
		logrus.WithContext(siloam.ctx).WithFields(logrus.Fields{
			"baseUrl":    siloam.mySiloamAPI.baseURL,
			"api":        "Doctor Schedules",
			"hospitalID": hospital.HospitalId,
			"doctorID":   doctor.DoctorId,
		}).WithError(err).Errorln()
		return
	}
	if scheduleResponse.Data == nil {
		return
	}

	for _, schedule := range scheduleResponse.Data {
		doctorSchedulesTransform := transformToDoctorSchedulesTelemedicineData(schedule, hospital, doctor)
		siloam.publisher.Publish(doctorSchedulesTransform, os.Getenv("RABBITMQ_SILOAM_DOCTOR_SCHEDULES_QUEUE"), "")
	}
}

func transformToDoctorSchedulesTelemedicineData(schedule *siloamGRPC.SchedulesPerWeeksData, hospital *siloamGRPC.HospitalData, doctor *siloamGRPC.DoctorData) *telemedicineGRPC.ScheduleData {
	return &telemedicineGRPC.ScheduleData{
		FromTime:    schedule.FromTime,
		Id:          schedule.ScheduleId,
		Day:         schedule.Day,
		LastUpdated: time.Now().Format(util.LayoutFormatDateTime),
		Source: &telemedicineGRPC.SourceData{
			OriginId:   schedule.ScheduleId,
			SourceId:   siloamSourceID,
			SourceName: siloamSourceName,
		},
		ToTime: schedule.ToTime,
		Hospital: &telemedicineGRPC.HospitalData{
			Alias:  hospital.Alias,
			AreaId: hospital.AreaId,
			Name:   hospital.Name,
			Id:     hospital.HospitalId,
		},
		Doctor: &telemedicineGRPC.DoctorData{
			Id:   doctor.DoctorId,
			Name: doctor.Name,
			Speciality: &telemedicineGRPC.SpecialityData{
				Id:       doctor.SpecialityId,
				NameIndo: doctor.SpecializationName,
				Name:     doctor.SpecializationNameEn,
			},
		},
	}
}

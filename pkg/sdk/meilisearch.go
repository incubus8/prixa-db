package sdk

import (
	"net/http"
	"os"
	"strings"
	"time"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

// DefaultRules defines the default ranking rules by meilisearch
var DefaultRules = []string{"typo", "words", "proximity", "attribute", "wordsPosition", "exactness"}

// NewMeilisearchClient is
func NewMeilisearchClient() *meili.Client {
	config := getMeilisearchConfig()
	logrus.Infoln("Connected to MeiliSearch at ", config.Host)

	return meili.NewClientWithCustomHTTPClient(config, http.Client{
		Timeout: 10 * time.Second,
	})
}

func getMeilisearchConfig() meili.Config {
	return meili.Config{
		Host:   os.Getenv("MEILISEARCH_URL"),
		APIKey: os.Getenv("MEILISEARCH_KEY"),
	}
}

// GetDocs with pagination of limit and offset
func GetDocs(msClient *meili.Client, index string, offset int) ([]map[string]interface{}, error) {
	var fetchedDocs []map[string]interface{}
	err := msClient.Documents(strings.ToLower(index)).List(meili.ListDocumentsRequest{
		Limit:  cast.ToInt64(25),
		Offset: cast.ToInt64(offset),
	}, &fetchedDocs)
	if err != nil {
		return nil, err
	}

	return fetchedDocs, nil
}

// GetDocByID gets doc by given ID
func GetDocByID(msClient *meili.Client, index, id string) (map[string]interface{}, error) {
	var doc map[string]interface{}
	err := msClient.Documents(index).Get(id, &doc)
	if err != nil {
		return nil, err
	}
	return doc, nil
}

// Search is
func Search(msClient *meili.Client, index string, searchRequest meili.SearchRequest) (*meili.SearchResponse, error) {
	resp, err := msClient.Search(index).Search(searchRequest)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// MeiliIndexSetting is
type MeiliIndexSetting struct {
	Default         bool     `json:"default"`
	Rule            []string `json:"rule"`
	FacetAttributes []string `json:"facetAttr"`
}

// MeiliIndexes is
type MeiliIndexes struct {
	Nalar map[string]MeiliIndexSetting `json:"nalar"`
	Covid map[string]MeiliIndexSetting `json:"covid"`
}

// CreateIndex is
func CreateIndex(client *meili.Client, index string, indexSetting MeiliIndexSetting) error {
	logrus.Infof("creating index %v with settings %v", index, indexSetting)
	_, err := client.Indexes().Create(meili.CreateIndexRequest{
		UID:        index,
		PrimaryKey: "id",
	})
	if err != nil {
		return err
	}
	logrus.Infof("index %v created", index)

	if !indexSetting.Default {
		logrus.Infof("index %v does not using default settings with given settings %v", index, indexSetting)
		_, err = client.Settings(index).UpdateRankingRules(append(indexSetting.Rule, DefaultRules...))
		if err != nil {
			return err
		}
		_, err = client.Settings(index).UpdateAttributesForFaceting(indexSetting.FacetAttributes)
		if err != nil {
			return err
		}
	}

	return nil
}

// DeleteIndex is
func DeleteIndex(client *meili.Client, index string, _ MeiliIndexSetting) error {
	logrus.Infof("starting index %v deletion process", index)
	_, err := client.Documents(index).DeleteAllDocuments()
	if err != nil {
		return err
	}
	logrus.Infof("all docs in index %v has been deleted", index)
	ok, _ := client.Indexes().Delete(index)
	if !ok {
		return err
	}
	logrus.Infof("index %v has been deleted", index)
	return nil
}

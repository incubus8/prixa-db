package gdbc

import (
	"context"
	"database/sql"
)

// SQLDBTx is the concrete implementation of sqlGdbc by using *sql.DB
type SQLDBTx struct {
	DB *sql.DB
}

// SQLConnTx is the concrete implementation of sqlGdbc by using *sql.Tx
type SQLConnTx struct {
	DB *sql.Tx
}

// Exec Query without returning any rows.
func (sdt *SQLDBTx) Exec(query string, args ...interface{}) (sql.Result, error) {
	return sdt.DB.Exec(query, args...)
}

// ExecContext Exec Query with context without returning any rows.
func (sdt *SQLDBTx) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return sdt.DB.ExecContext(ctx, query, args...)
}

// Prepare statement
func (sdt *SQLDBTx) Prepare(query string) (*sql.Stmt, error) {
	return sdt.DB.Prepare(query)
}

// PrepareContext prepare statement with context
func (sdt *SQLDBTx) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	return sdt.DB.PrepareContext(ctx, query)
}

// Query with returning rows.
func (sdt *SQLDBTx) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return sdt.DB.Query(query, args...)
}

// QueryContext with returning rows with context.
func (sdt *SQLDBTx) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return sdt.DB.QueryContext(ctx, query, args...)
}

// QueryRow executes a query that is expected to return at most one row.
func (sdt *SQLDBTx) QueryRow(query string, args ...interface{}) *sql.Row {
	return sdt.DB.QueryRow(query, args...)
}

// QueryRowContext executes a query that is expected to return at most one row with context.
func (sdt *SQLDBTx) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return sdt.DB.QueryRowContext(ctx, query, args...)
}

// Exec Query without returning any rows.
func (sdt *SQLConnTx) Exec(query string, args ...interface{}) (sql.Result, error) {
	return sdt.DB.Exec(query, args...)
}

// ExecContext Exec Query with context without returning any rows.
func (sdt *SQLConnTx) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return sdt.DB.ExecContext(ctx, query, args...)
}

// Prepare statement
func (sdt *SQLConnTx) Prepare(query string) (*sql.Stmt, error) {
	return sdt.DB.Prepare(query)
}

// PrepareContext prepare statement with context
func (sdt *SQLConnTx) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	return sdt.DB.PrepareContext(ctx, query)
}

// Query with returning rows.
func (sdt *SQLConnTx) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return sdt.DB.Query(query, args...)
}

// QueryContext with returning rows with context.
func (sdt *SQLConnTx) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return sdt.DB.QueryContext(ctx, query, args...)
}

// QueryRow executes a query that is expected to return at most one row.
func (sdt *SQLConnTx) QueryRow(query string, args ...interface{}) *sql.Row {
	return sdt.DB.QueryRow(query, args...)
}

// QueryRowContext executes a query that is expected to return at most one row with context.
func (sdt *SQLConnTx) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return sdt.DB.QueryRowContext(ctx, query, args...)
}

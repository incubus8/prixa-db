package gdbc

// Rollback DB doesn't rollback, do nothing here
func (cdt *SQLDBTx) Rollback() error {
	return nil
}

// Commit DB doesnt commit, do nothing here
func (cdt *SQLDBTx) Commit() error {
	return nil
}

// TxBegin TransactionBegin starts a transaction
func (cdt *SQLDBTx) TxBegin() (SQLGdbc, error) {
	tx, err := cdt.DB.Begin()
	sct := SQLConnTx{tx}
	return &sct, err
}

// TxEnd DB doesnt rollback, do nothing here
func (cdt *SQLDBTx) TxEnd(txFunc func() error) error {
	return nil
}

// TxEnd is
func (sct *SQLConnTx) TxEnd(txFunc func() error) error {
	var err error
	tx := sct.DB

	defer func() {
		if p := recover(); p != nil {
			err = tx.Rollback()
			panic(p)
		} else if err != nil {
			err = tx.Rollback() // err is non-nil; don't change it
		} else {
			err = tx.Commit() // if Commit returns error update err with commit err
		}
	}()
	err = txFunc()
	return err
}

// TxBegin *sql.Tx can't begin a transaction, transaction always begins with a *sql.DB
func (sct *SQLConnTx) TxBegin() (SQLGdbc, error) {
	return nil, nil
}

// Rollback is
func (sct *SQLConnTx) Rollback() error {
	return sct.DB.Rollback()
}

// Commit is
func (sct *SQLConnTx) Commit() error {
	return sct.DB.Commit()
}

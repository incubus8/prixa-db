package sdk

import (
	"context"
	"errors"
	"os"

	"github.com/crisp-im/go-crisp-api/crisp"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/config"
	"prixa.ai/prixa-db/pkg/store"
)

const (
	// DataPassword holds the data identifier as key for data map to store desired data
	DataPassword = "password"
	// DataSessionID represents user session id key in Crisp
	DataSessionID = "sessionID"
	// DataEmailStatus represents user email status in Crisp
	DataEmailStatus = "emailStatus"
	// DataToken represents user token in Crisp
	DataToken = "token"
	// DataAvatarURL represents avatar URL in Crisp
	DataAvatarURL = "avatarUrl"
	// DataBirthdate represents user birth date in Crisp
	DataBirthdate = "birthdate"
	// DataRegistrationSource represents user registration source
	DataRegistrationSource = "registrationSource"
)

// CrispData holds the values for crisp
type CrispData struct {
	Client     *crisp.Client
	Identifier string
	Key        string
	WebsiteID  string
	Query      store.CrispUserStore
}

// getCrispCredentials is getting the env variables for crisp identifier and key
func getCrispCredentials() (string, string, string) {
	identifier := os.Getenv("CRISP_IDENTIFIER")
	key := os.Getenv("CRISP_KEY")
	websiteID := os.Getenv("CRISP_WEBSITE_ID")

	return identifier, key, websiteID
}

// NewCrispClient is initiate new client
func NewCrispClient() CrispData {
	identifier, key, websiteID := getCrispCredentials()
	client := crisp.New()
	client.Authenticate(identifier, key)
	return CrispData{
		Identifier: identifier,
		Key:        key,
		WebsiteID:  websiteID,
		Client:     client,
	}
}

// GetWebsiteIDByPartner sets websiteID for desired pID and appID
func GetWebsiteIDByPartner(ctx context.Context) (string, error) {
	meta := metautils.ExtractIncoming(ctx)
	pID, appID := meta.Get("x-partner-id"), meta.Get("x-partner-app-id")
	websiteID := config.PartnerWebsiteID[pID+":"+appID]
	if websiteID == "" {
		err := errors.New("failed to get new websiteID")
		logrus.WithError(err).Errorln()
		return websiteID, err
	}
	return websiteID, nil
}

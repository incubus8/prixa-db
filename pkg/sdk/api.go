package sdk

import "github.com/go-resty/resty/v2"

// API base HTTP client
type API struct {
	Client *resty.Client
}

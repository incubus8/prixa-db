package mailconfig

import (
	"os"
	"strings"

	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

const (
	// SendgridAPIKey is send grid env variable key
	SendgridAPIKey = "SENDGRID_API_KEY"
	// MailPrixaDiagRes is a helper key for diagnostic mail config
	MailPrixaDiagRes = "MAIL_PRIXA_DIAG_RES"
	// MailPrixaSignup is a helper key for signup mail config
	MailPrixaSignup = "MAIL_PRIXA_SIGNUP"
	// MailPrixaForgetpwd is a helper key for forget password mail config
	MailPrixaForgetpwd = "MAIL_PRIXA_FORGETPWD" // #nosec
	// MailPrixaSignupSuccess is a helper key for success signup mail config
	MailPrixaSignupSuccess = "MAIL_PRIXA_SIGNUP_SUCCESS"
	// MailPrixaForgetpwdSuccess is a helper key for success send forget password mail config
	MailPrixaForgetpwdSuccess = "MAIL_PRIXA_FORGETPWD_SUCCESS" // #nosec
)

// MailerConfig sendgrid template IDs
var MailerConfig = map[string]string{
	MailPrixaDiagRes:          "d-4dd3cac20e2c4cb6a283c311535aad93",
	MailPrixaSignup:           "d-a750667b4a6a4a4eb3eefd88f7059f68",
	MailPrixaForgetpwd:        "d-26b8efa6ca9142a3a3984e5fee3eeb1f",
	MailPrixaSignupSuccess:    "d-b8e7f854a78d498ebf25188a5a9b2974",
	MailPrixaForgetpwdSuccess: "d-6b7a2b45891843a19674531e43f66559",
}

func trackingSettings() *mail.TrackingSettings {
	tracking := mail.NewTrackingSettings()

	clickTracking := mail.NewClickTrackingSetting()
	clickTracking.SetEnable(true)
	clickTracking.SetEnableText(true)
	tracking.SetClickTracking(clickTracking)

	openTracking := mail.NewOpenTrackingSetting()
	openTracking.SetEnable(true)
	tracking.SetOpenTracking(openTracking)

	subscriptionTracking := mail.NewSubscriptionTrackingSetting()
	subscriptionTracking.SetEnable(false)
	// subscriptionTracking.SetHTML("If you would like to unsubscribe and stop receiving these emails <% click_here %>.")
	// subscriptionTracking.SetSubstitutionTag("<% click_here %>")
	// subscriptionTracking.SetText("If you would like to unsubscribe and stop receiving these emails <% click_here %>.")
	tracking.SetSubscriptionTracking(subscriptionTracking)

	// For local testing
	if !strings.EqualFold(os.Getenv("GO_ENV"), "PRODUCTION") {
		spamCheckSettings := mail.NewSpamCheckSetting()
		spamCheckSettings.SetEnable(true)
		sandboxMode := mail.NewSandboxModeSetting(true, true, spamCheckSettings)
		tracking.SandboxMode = sandboxMode
	}

	return tracking
}

func getFromMail() *mail.Email {
	fromEmailName := os.Getenv("SENDGRID_FROM_EMAIL_NAME")
	if fromEmailName == "" {
		fromEmailName = "Prixa"
	}

	fromEmailAddress := os.Getenv("SENDGRID_FROM_EMAIL_ADDR")
	if fromEmailAddress == "" {
		fromEmailAddress = "tech@prixa.ai"
	}

	return mail.NewEmail(fromEmailName, fromEmailAddress)
}

// NewV3MailWithTracking initializes new sendgrid V3 mail API with tracking
func NewV3MailWithTracking() *mail.SGMailV3 {
	msg := mail.NewV3Mail()
	tracking := trackingSettings()
	msg.SetTrackingSettings(tracking)

	from := getFromMail()
	msg.SetFrom(from)

	return msg
}

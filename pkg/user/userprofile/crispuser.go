package userprofile

import (
	"github.com/crisp-im/go-crisp-api/crisp"
)

// CrispUserData is
type CrispUserData struct {
	PersonProfile *crisp.PeopleProfile `json:"personProfile"`
	PersonData    map[string]string    `json:"personData"`
}

// CrispUserRedisData is
type CrispUserRedisData struct {
	Key  string        `json:"key"`
	User CrispUserData `json:"CrispUserData"`
}

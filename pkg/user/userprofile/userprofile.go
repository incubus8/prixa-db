package userprofile

import (
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
)

// ProfileProperties is
type ProfileProperties struct {
	DiagnosisResult *diagnosticGRPC.DiagnosisResultData
	Email           string
	Name            string
}

package user

import (
	"errors"

	"github.com/crisp-im/go-crisp-api/crisp"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"golang.org/x/crypto/bcrypt"

	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"
)

// ErrEmailOrPasswordDidNotMatch is an error for un-match email and its password
var ErrEmailOrPasswordDidNotMatch = errors.New("incorrect email or password")

// ErrLoginSource is an error for un-match registration source
var ErrLoginSource = errors.New("cannot login using this procedure")

// ErrEmailAlreadyRegistered is error for already registered email
var ErrEmailAlreadyRegistered = errors.New("email already registered")

// ErrFailedToRegisterEmail is error for failed registration other than already registered email
var ErrFailedToRegisterEmail = errors.New("failed to register")

// CrispData extends sdk.CrispData
type CrispData sdk.CrispData

// IsEmailExist is checking if the input email is in available contacts
func (crispData CrispData) IsEmailExist(email string) (*crisp.PeopleProfile, error) {
	var personProfile *crisp.PeopleProfile
	var errGetPeopleProfile error
	websiteID, err := crispData.Query.GetUserWebsiteID(email)
	if err != nil {
		personProfile, _, errGetPeopleProfile = crispData.Client.Website.GetPeopleProfile(crispData.WebsiteID, email)
	} else {
		personProfile, _, errGetPeopleProfile = crispData.Client.Website.GetPeopleProfile(websiteID, email)
	}
	if errGetPeopleProfile != nil {
		logrus.WithError(errGetPeopleProfile).WithField("email", email).Errorln()
		return nil, errGetPeopleProfile
	}

	return personProfile, nil
}

// HashPassword hashes input password
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// MatchPassword matches given password with currently have password
func MatchPassword(password, hashedPwd string) error {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPwd), []byte(password))
	return err
}

// SavePersonData is saving person data based on key on data given
func (crispData CrispData) SavePersonData(email string, data map[string]string) error {
	websiteID, errGetWebsiteID := crispData.Query.GetUserWebsiteID(email)
	var resp *crisp.Response
	var err error
	if errGetWebsiteID != nil {
		resp, err = crispData.Client.Website.SavePeopleData(crispData.WebsiteID, email, map[string]map[string]string{
			"data": data,
		})
	} else {
		resp, err = crispData.Client.Website.SavePeopleData(websiteID, email, map[string]map[string]string{
			"data": data,
		})
	}
	if err != nil {
		logrus.WithError(err).WithField("response", resp).Errorln("Failed on saving person data")
		return err
	}
	return nil
}

// GetPersonData is getting person data based on given key
func (crispData CrispData) GetPersonData(email string) map[string]string {
	websiteID, errGetWebsiteID := crispData.Query.GetUserWebsiteID(email)
	var personAllData *crisp.PeopleData
	var resp *crisp.Response
	var err error
	if errGetWebsiteID != nil {
		personAllData, resp, err = crispData.Client.Website.GetPeopleData(crispData.WebsiteID, email)
	} else {
		personAllData, resp, err = crispData.Client.Website.GetPeopleData(websiteID, email)
	}
	if err != nil {
		logrus.WithError(err).WithField("response", resp).Errorln("Error login not found")
	}

	return cast.ToStringMapString(*personAllData.Data)
}

// RegisterNewContact is register new contact
func (crispData CrispData) RegisterNewContact(email, nickname string, password string, source string) (*crisp.PeopleProfile, error) {
	resp, err := crispData.Client.Website.AddNewPeopleProfile(crispData.WebsiteID, crisp.PeopleProfileUpdateCard{
		Email: email,
		Person: &crisp.PeopleProfileCardPerson{
			Nickname: &nickname,
		},
	})
	log := logrus.WithField("response", resp).
		WithField("email", email).
		WithField("nickname", nickname)
	if err != nil {
		if resp.StatusCode == 409 {
			err = ErrEmailAlreadyRegistered
		}
		// people_exists
		log.WithError(err).WithField("websiteID", crispData.WebsiteID).WithField("peopleProfile", crisp.PeopleProfileUpdateCard{
			Email: email,
			Person: &crisp.PeopleProfileCardPerson{
				Nickname: &nickname,
			},
		}).Errorln("Error Register Found")
		return nil, err
	}

	log.Infoln("Register Info")

	personProfile, err := crispData.IsEmailExist(email)
	if err != nil {
		log.WithError(err).WithField("email", email).Errorln("Email not exist")
		return nil, ErrFailedToRegisterEmail
	}

	hashedPwd, _ := HashPassword(password)
	err = crispData.SavePersonData(email, map[string]string{
		sdk.DataPassword:           hashedPwd,
		sdk.DataRegistrationSource: source,
	})
	if err != nil {
		log.WithError(err).WithField("PeopleID", *personProfile.PeopleID).WithField("data", map[string]string{
			sdk.DataPassword:           hashedPwd,
			sdk.DataRegistrationSource: source,
		}).Errorln()
		return nil, ErrFailedToRegisterEmail
	}

	log.Infoln("Register info success")
	return personProfile, nil
}

// UserLogin is matching email and password and available contacts
func (crispData CrispData) UserLogin(userEmail string, password string, query store.CrispUserStore) (*crisp.PeopleProfile, error) {
	log := logrus.WithField("email", userEmail).WithField("action", "UserLogin")

	var personData map[string]string
	var personProfile *crisp.PeopleProfile
	crispUserData, err := query.GetPersonProfile(userEmail)
	if err != nil {
		personProfile, err = crispData.IsEmailExist(userEmail)
		if err != nil {
			newErr := ErrEmailOrPasswordDidNotMatch
			log.WithError(err).Errorln("Error login not found")
			return nil, newErr
		}
		personData = crispData.GetPersonData(userEmail)
		errSavePersonProfile := query.SavePersonProfile(&userprofile.CrispUserRedisData{
			Key: userEmail,
			User: userprofile.CrispUserData{
				PersonProfile: personProfile,
				PersonData:    personData,
			},
		})
		if errSavePersonProfile != nil {
			log.WithError(err).Warnln(errSavePersonProfile)
		}
	} else {
		personProfile = crispUserData.User.PersonProfile
		personData = crispUserData.User.PersonData
	}

	if personData[sdk.DataRegistrationSource] != "prixa" {
		newErr := ErrLoginSource
		log.WithError(newErr).Errorln("can't login using prixa procedure")
		return nil, newErr
	}

	errMatchPassword := MatchPassword(password, personData[sdk.DataPassword])
	if errMatchPassword != nil {
		newErr := ErrEmailOrPasswordDidNotMatch
		log.WithError(newErr).Errorln("Error login not found")
		return nil, newErr
	}

	return personProfile, nil
}

// GoogleUserLogin is same with user login but without checking password (because using google oauth)
func (crispData CrispData) GoogleUserLogin(userEmail string, query store.CrispUserStore, personProfile *crisp.PeopleProfile, personData map[string]string) (*crisp.PeopleProfile, map[string]string, error) {
	log := logrus.WithField("email", userEmail).WithField("action", "UserLogin")

	crispUserData, err := query.GetPersonProfile(userEmail)
	if err != nil {
		if personProfile == nil {
			newErr := errors.New("missing person profile")
			log.WithError(newErr).Errorln()
			return nil, nil, newErr
		}

		if personData == nil {
			newErr := errors.New("missing person data")
			log.WithError(newErr).Errorln()
			return nil, nil, newErr
		}

		errSavePersonProfile := query.SavePersonProfile(&userprofile.CrispUserRedisData{
			Key: userEmail,
			User: userprofile.CrispUserData{
				PersonProfile: personProfile,
				PersonData:    personData,
			},
		})
		if errSavePersonProfile != nil {
			log.WithError(err).Warnln(errSavePersonProfile)
		}
	} else {
		personProfile = crispUserData.User.PersonProfile
		personData = crispUserData.User.PersonData
	}

	if personData[sdk.DataRegistrationSource] != "google" {
		newErr := ErrLoginSource
		log.WithError(newErr).Errorln("can't login using google procedure")
		return nil, nil, newErr
	}

	return personProfile, personData, nil
}

package user

import (
	"github.com/crisp-im/go-crisp-api/crisp"
	"github.com/sirupsen/logrus"
)

// Conversation is
func (crispData CrispData) Conversation(sessionID, name, email, content string) {
	_, err := crispData.Client.Website.InitiateConversationWithExistingSession(crispData.WebsiteID, sessionID)
	if err != nil {
		logrus.WithError(err).Errorln("cannot initiate Crisp conversation with existing session")
	}

	_, err = crispData.Client.Website.UpdateConversationMetas(crispData.WebsiteID, sessionID, crisp.ConversationMetaUpdate{
		Nickname: name,
		Email:    email,
	})
	if err != nil {
		logrus.WithError(err).Errorln("cannot update Crisp conversation metas")
	}

	_, _, err = crispData.Client.Website.SendTextMessageInConversation(crispData.WebsiteID, sessionID, crisp.ConversationTextMessageNew{
		Content: content,
		Type:    "text",
		From:    "user",
		Origin:  "chat",
	})
	if err != nil {
		logrus.WithError(err).Errorln("cannot send Crisp text message in conversation")
	}
}

package telemedicine

import (
	"context"
	"errors"
	"os"
	"strings"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/sirupsen/logrus"
	"github.com/slack-go/slack"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"

	"prixa.ai/prixa-db/pkg/store/publisher/queuelayer"
	"prixa.ai/prixa-db/pkg/store/redislayer"
	"prixa.ai/prixa-db/pkg/util"
)

var partners = []string{
	"SHDP", "SHMT", "MRCCC", "SHCN", "SH ASRI", "SHBG", "SHKJ", "SHLC", "SHLV", "SHTB", "SHCB", "SHSR", "SHSB",
	"SHYG", "SHBP", "SHPR", "SHMK", "SHMN", "SHPD", "SHBB", "SHJB", "SHMD", "SHPL", "SHLL", "SHJR",
}

var specialities = []string{
	"24bf6171-0aef-4579-864b-51bfa3328290", "b11e174b-1eea-43e8-a082-13c3ce2ed179", "55d93a52-7ffb-4cc3-b287-18ff593faf84", "42231f2b-d871-417b-8cad-5a10ea57167e", "493068b9-e822-426c-a95b-757b7fba6af4",
	"44d54965-b740-451c-9fec-a21b6e7544bf", "85af0200-d3e5-49c6-9906-4100643ee113", "a9053d8d-0f6b-43df-a801-ab4f6f3142d5", "8646beb4-3cc4-4c62-bdd7-51529311bf11", "5fd16e43-b6ff-429d-aa86-f9b089e89c71", "b23e147e-00d2-4da4-9f68-c9b7a6325267",
	"6837e280-146b-4c44-be49-2fe92be12d47", "f93e7a24-1c92-42f8-9a84-b9cc7446f489", "2c3ba315-555e-4fca-b4fe-ae9ec50bf7cf", "3416fac3-82f0-467d-9665-5ff9f65cbad9", "fb7c495e-c7b9-48c0-896d-ecfee8c588f2", "09cb5094-d3de-4d8e-b792-59a9410595cd",
	"8cb05634-2c48-4955-867b-e30f4af903e6", "39ff8dbc-0aa1-4643-9108-e8a523138b48", "646fc37d-08cd-4a35-af66-31ac0c3abf64", "67d08367-643b-4741-b1f7-23f77575ebcb", "c1f95028-8e0d-4b5b-9026-6e26f355ae0c", "205c7231-017c-498c-baf4-6f4f859abdcc",
	"9a5e44fc-49c2-421d-b004-b982938ca870", "42f9185e-7fba-4854-9d8b-3a0942b93e37", "51637b01-fa8c-4f5c-868f-38b484570431", "385198b6-0c6d-436e-adcd-3677df3255f9", "eb1a6af5-bb95-4bdb-bedb-3a32922f6a6e", "4591fae3-43e5-42e0-9505-dafdf0c78d57",
}

// DoctorAllocationEngine is
type DoctorAllocationEngine struct {
	publisher  messaging.Publisher
	slack      *slack.Client
	store      store.Store
	redis      *redis.Client
	ctx        context.Context
	subscriber *redis.PubSub
}

// NewDoctorAllocationEngine is
func NewDoctorAllocationEngine(ctx context.Context, redis *redis.Client, publisher messaging.Publisher) *DoctorAllocationEngine {
	app := &DoctorAllocationEngine{
		publisher: publisher,
		slack:     slack.New(os.Getenv("SLACK_ACCESS_TOKEN")),
		redis:     redis,
		ctx:       ctx,
	}
	app.initStore()
	return app
}

func (app *DoctorAllocationEngine) initStore() {
	rootStore := store.NewAppStore()
	redisStore := redislayer.NewRedisStore(rootStore, app.redis, nil)
	app.store = queuelayer.NewQueueStore(redisStore, app.publisher)
}

// StartDoctorAllocationEngine is
func (app *DoctorAllocationEngine) StartDoctorAllocationEngine() {
	rtm := app.slack.NewRTM()
	go rtm.ManageConnection()

	go func() {
		for msg := range rtm.IncomingEvents {
			logrus.Println(msg)
			switch ev := msg.Data.(type) {
			case *slack.MessageEvent:
				go app.handleMessage(ev)
			}
		}
	}()
}

func (app *DoctorAllocationEngine) handleMessage(msgEvent *slack.MessageEvent) {
	splitMessage := strings.Split(msgEvent.Text, ";")
	if len(splitMessage) < 1 {
		return
	}
	switch splitMessage[0] {
	case "search":
		app.searchAvailableDoctor(msgEvent)
	case "doctor-online":
		_ = app.updateDoctorStatus("ONLINE", msgEvent)
	case "doctor-offline":
		_ = app.updateDoctorStatus("OFFLINE", msgEvent)
	case "resolve":
		_ = app.resolveChatByUser(msgEvent)
	case "reset":
		_ = app.resetIdleTimeOut(msgEvent)
	}
}

func (app *DoctorAllocationEngine) searchAvailableDoctor(msgEvent *slack.MessageEvent) {
	user := msgEvent.User
	hospital, speciality, err := getValidHospitalAndSpecialityFromTextMessage(msgEvent.Text)
	if err != nil {
		_, _, err := app.slack.PostMessage(msgEvent.User, slack.MsgOptionAsUser(true), slack.MsgOptionText("getting error : "+err.Error(), false))
		if err != nil {
			logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to post message to slack")
		}
		return
	}
	sessionID := util.GetRandomID()

	chat := &model.ChatRequest{
		Partner:    hospital,
		CreatedAt:  time.Now().Format(util.LayoutFormatDateTime),
		SessionID:  sessionID,
		Speciality: speciality,
		User:       user,
	}

	app.store.ChatRequest().PublishChatRequest(chat)
	result, err := getSessionChatResult(app.store, chat.SessionID)
	if err != nil {
		_, _, err = app.slack.PostMessage(msgEvent.User, slack.MsgOptionAsUser(true), slack.MsgOptionText("getting error : "+err.Error(), false))
		if err != nil {
			logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to post message to slack")
		}
		return
	}

	message := result.Message
	if result.Doctor != nil {
		message = "SessionID : " + sessionID + "\nDoctor : " + result.Doctor.ID + " - " + result.Doctor.Name
	}

	_, _, err = app.slack.PostMessage(msgEvent.User, slack.MsgOptionAsUser(true), slack.MsgOptionText(message, false))
	if err != nil {
		logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to post message to slack")
	}
}

func getSessionChatResult(store store.Store, sessionID string) (queryResult *model.ChatResult, err error) {
	timeout := time.After(10 * time.Second)
	for {
		select {
		case <-timeout:
			return nil, errors.New("session result not yet created")
		case <-time.After(1 * time.Second):
			queryResult, err = store.ChatResult().FindChatResultByID(sessionID)
			if err != nil {
				logrus.Infoln("currently get error : " + err.Error() + " still search available doctor")
				continue
			}
			return queryResult, nil
		}
	}
}

func getValidHospitalAndSpecialityFromTextMessage(message string) (string, string, error) {
	splitMessage := strings.Split(message, ";")
	if len(splitMessage) < 2 {
		return "", "", errors.New("invalid format to get hospital and speciality")
	}
	hospital := splitMessage[1]
	speciality := splitMessage[2]
	if !isValidPartner(hospital) || !isValidSpeciality(speciality) {
		return "", "", errors.New("invalid hospital or speciality")
	}
	return hospital, speciality, nil
}

func isValidPartner(p string) bool {
	for _, partner := range partners {
		if partner == p {
			return true
		}
	}
	return false
}

func isValidSpeciality(s string) bool {
	for _, speciality := range specialities {
		if speciality == s {
			return true
		}
	}
	return false
}

// AutomaticResolveIdleChat is
func (app *DoctorAllocationEngine) AutomaticResolveIdleChat() {
	_, err := app.redis.ConfigSet("notify-keyspace-events", "KEx").Result()
	if err != nil {
		logrus.WithContext(app.ctx).WithError(err).Fatalln("failed to set config keyspace event in redis")
	}
	patternKey := "shadow:chatresult:*"
	subscriber := app.redis.PSubscribe("__keyspace@0__:" + patternKey)
	app.subscriber = subscriber
	go func() {
		for {
			msg, err := subscriber.ReceiveMessage()
			if err != nil {
				continue
			}
			if msg.Payload == "expired" {
				go app.publishResolveChat(msg.Channel)
			}

		}
	}()
}

func (app *DoctorAllocationEngine) resolveChatByUser(msgEvent *slack.MessageEvent) error {
	splitMessage := strings.Split(msgEvent.Text, ";")
	if len(splitMessage) < 1 {
		return errors.New("invalid format to resolve chat")
	}
	chatSessionID := splitMessage[1]
	resolveChatRequest := &model.ResolveChatRequest{
		SessionID:   chatSessionID,
		Information: "RESOLVED BY USER",
	}
	app.store.ChatRequest().PublishResolvedChatRequest(resolveChatRequest)
	_, _, err := app.slack.PostMessage(msgEvent.User, slack.MsgOptionAsUser(true), slack.MsgOptionText("Publish Resolved Chat "+chatSessionID+" By User", false))
	if err != nil {
		logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to post message to slack")
	}
	return nil
}

func (app *DoctorAllocationEngine) updateDoctorStatus(status string, msgEvent *slack.MessageEvent) error {
	splitMessage := strings.Split(msgEvent.Text, ";")
	if len(splitMessage) < 1 {
		return errors.New("invalid format to resolve chat")
	}
	doctorID := splitMessage[1]
	doctorStatus := &model.SetDoctorStatusRequest{
		DoctorID: doctorID,
		Status:   status,
	}
	app.store.ChatRequest().PublishSetDoctorStatusRequest(doctorStatus)
	return nil
}

func (app *DoctorAllocationEngine) publishResolveChat(payload string) {
	chatSessionID, err := getChatSessionID(payload)
	if err != nil {
		logrus.WithField("payload", payload).WithError(err).Errorln("payload is not chat id")
		return
	}

	resolveChatRequest := &model.ResolveChatRequest{
		SessionID:   chatSessionID,
		Information: "RESOLVED BY SYSTEM",
	}
	app.store.ChatRequest().PublishResolvedChatRequest(resolveChatRequest)
	chatResult, err := app.store.ChatResult().FindChatResultByID(chatSessionID)
	if err != nil {
		logrus.WithField("chatSessionID", chatSessionID).WithError(err).Errorln("failed to get chat result in redis")
	}
	msg := "Session Chat " + chatSessionID + " is resolved by system"
	_, _, err = app.slack.PostMessage(chatResult.User, slack.MsgOptionAsUser(true), slack.MsgOptionText(msg, false))
	if err != nil {
		logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to post message to slack")
	}
}

func getChatSessionID(payload string) (string, error) {
	prefix := "chatresult:"
	pos := strings.LastIndex(payload, prefix)
	if pos == -1 {
		return "", errors.New("can't find prefix " + prefix + " in payload")
	}
	adjustedPos := pos + len(prefix)
	if adjustedPos >= len(payload) {
		return "", errors.New("unexplained error")
	}
	chatSessionID := payload[adjustedPos:]
	return chatSessionID, nil
}

func (app *DoctorAllocationEngine) resetIdleTimeOut(msgEvent *slack.MessageEvent) error {
	splitMessage := strings.Split(msgEvent.Text, ";")
	if len(splitMessage) < 1 {
		return errors.New("invalid format to resolve chat")
	}
	chatSessionID := splitMessage[1]
	err := app.store.ChatResult().ResetIdleTimeoutToDefault(chatSessionID)
	if err != nil {
		logrus.WithField("slackClient", app.slack).WithError(err).Errorln("failed to reset idle timeout")
		return err
	}

	return nil
}

// Close currently to close pubsub redis
func (app *DoctorAllocationEngine) Close() error {
	if app.subscriber != nil {
		return app.subscriber.Close()
	}
	return nil
}

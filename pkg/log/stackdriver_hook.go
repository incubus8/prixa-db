package log

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"runtime"

	"cloud.google.com/go/errorreporting"
	"cloud.google.com/go/logging"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/sirupsen/logrus"
)

// StackDriverHook hook for stackdriver
type StackDriverHook struct {
	loggingClient *logging.Client
	errorClient   *errorreporting.Client
	logger        *logging.Logger
}

var logLevelMappings = map[logrus.Level]logging.Severity{
	logrus.TraceLevel: logging.Default,
	logrus.DebugLevel: logging.Debug,
	logrus.InfoLevel:  logging.Info,
	logrus.WarnLevel:  logging.Warning,
	logrus.ErrorLevel: logging.Error,
	logrus.FatalLevel: logging.Critical,
	logrus.PanicLevel: logging.Critical,
}

var loggingLevel = logrus.AllLevels

// NewStackDriverHook create new stackdriver hook
func NewStackDriverHook(lc *logging.Client, ec *errorreporting.Client) *StackDriverHook {
	logName := os.Getenv("GOOGLE_STACKDRIVER_LOG_NAME")
	return &StackDriverHook{
		loggingClient: lc,
		errorClient:   ec,
		logger:        lc.Logger(logName),
	}
}

// Close close stackdriver hook
func (sh *StackDriverHook) Close() {
	_ = sh.loggingClient.Close()
	_ = sh.errorClient.Close()
}

// SetLevels set custom logging level
func (sh *StackDriverHook) SetLevels(levels []logrus.Level) {
	loggingLevel = levels
}

// Levels logging level
func (sh *StackDriverHook) Levels() []logrus.Level {
	return loggingLevel
}

// Fire execute logging and errorreporting
func (sh *StackDriverHook) Fire(entry *logrus.Entry) error {
	if entry == nil || entry.Context == nil {
		return nil
	}

	incomingMds := metautils.ExtractIncoming(entry.Context)
	payload := map[string]interface{}{
		"Message":    entry.Message,
		"Data":       entry.Data,
		"api_method": incomingMds.Get("grpcMethod"),
	}
	level := logLevelMappings[entry.Level]
	sh.logger.Log(logging.Entry{Payload: payload, Severity: level})

	if int(level) >= int(logging.Error) {
		err := getError(entry)
		sh.errorClient.Report(errorreporting.Entry{
			User:  incomingMds.Get("email"),
			Error: err,
			Stack: sh.getStackTrace(),
		})
	}
	return nil
}

func (sh *StackDriverHook) getStackTrace() []byte {
	stackSlice := make([]byte, 2048)
	length := runtime.Stack(stackSlice, false)
	stack := string(stackSlice[0:length])
	re := regexp.MustCompile("[\r\n].*logrus.*")
	res := re.ReplaceAllString(stack, "")
	re = regexp.MustCompile("[\r\n].*log.*")
	res = re.ReplaceAllString(res, "")
	return []byte(res)
}

type stackDriverError struct {
	Err          interface{}
	Message      string
	Context      context.Context
	CustomFields map[string]interface{}
}

func (e stackDriverError) Error() string {
	return fmt.Sprintf(
		`Error: %v | Message: %v | Context: %v | CustomFields: %v`,
		e.Err,
		e.Message,
		e.Context,
		e.CustomFields,
	)
}

func getError(entry *logrus.Entry) error {
	errData := entry.Data["error"]
	msg := entry.Message
	ctx := entry.Context
	customFields := make(map[string]interface{}, len(entry.Data)-1)
	for k, v := range entry.Data {
		if k != "error" {
			customFields[k] = v
		}
	}

	err := stackDriverError{
		Err:          errData,
		Message:      msg,
		Context:      ctx,
		CustomFields: customFields,
	}

	return err
}

// Wait is
func (sh *StackDriverHook) Wait() {}

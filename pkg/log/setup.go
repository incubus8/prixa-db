package log

import (
	"os"
	"strings"

	"cloud.google.com/go/errorreporting"
	"cloud.google.com/go/logging"
	sentryhook "github.com/prixa-ai/logrus-sentry-hook"
	"github.com/sirupsen/logrus"
)

// Setup setup global log handler using Logrus
func Setup() {
	if strings.EqualFold(os.Getenv("GO_ENV"), "LOCALHOST") {
		logrus.SetFormatter(&logrus.TextFormatter{})
		logrus.SetReportCaller(false)
	} else {
		// Log as JSON instead of the default ASCII formatter.
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}

	if os.Getenv("DEBUG") == "1" {
		logrus.SetLevel(logrus.DebugLevel)
	} else {
		logrus.SetLevel(logrus.InfoLevel)
	}

	// Output to stdout instead of the default stderr
	// Can be any io.Writer
	logrus.SetOutput(os.Stdout)
}

// AddSentryHook enhances Logrus to send error to Sentry.io
func AddSentryHook() {
	if os.Getenv("SENTRY_DSN") != "" {
		logrus.AddHook(sentryhook.New([]logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel}))
	} else {
		logrus.Warnln("You have called AddSentryHook() function, " +
			"but your SENTRY_DSN environment variable is empty. Please specify your environment variable")
	}
}

// AddStackdriverLoggingHook enhances Logrus to send log and error to Google Stackdriver
func AddStackdriverLoggingHook(lc *logging.Client, ec *errorreporting.Client) {
	if lc != nil && ec != nil {
		hook := NewStackDriverHook(lc, ec)
		logrus.AddHook(hook)
	} else {
		logrus.Warnln("Stackdriver hook will not work")
	}
}

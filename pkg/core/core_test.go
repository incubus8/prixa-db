package core

//var c *Core
//
//func TestMain(m *testing.M) {
//	if err := godotenv.Load(); err != nil {
//		logrus.Print("No .env file found")
//	}
//	c = &Core{}
//	c.FetchAllTables()
//
//	retCode := m.Run()
//	os.Exit(retCode)
//}
//
//func TestGetSymptomProps(t *testing.T) {
//	symp := c.Symptoms.Search("pusing")[0]
//	typeId := c.Meta.TypeIDFromName("onset")
//	propType, _ := c.Meta.Types[typeId]
//	results := c.GetSymptomProps(symp, propType)
//	assert.GreaterOrEqual(t, len(results), 1)
//}
//
//func TestConsult(t *testing.T) {
//	symp := c.Symptoms.Search("pusing")[0]
//	propID := c.Meta.PropIDFromName("underoneday")
//	constantPropID := c.Meta.PropIDFromName("constant")
//	prop := c.Meta.Props[propID]
//	constantProp := c.Meta.Props[constantPropID]
//	evidence := db.Evidence{
//		SymptomID: symp.ID,
//		Props: []db.PropertyPair{
//			{
//				Type: prop.Type,
//				Prop: propID,
//			},
//			{
//				Type: constantProp.Type,
//				Prop: constantPropID,
//			},
//		},
//	}
//	adult, _ := c.Meta.ProfileByName("adult")
//	male, _ := c.Meta.ProfileByName("male")
//	setting := Setting{
//		Profile:   []string{adult.ID, male.ID},
//		MaxResult: 5,
//	}
//	result := c.Consult([]db.Evidence{evidence}, setting)
//	assert.GreaterOrEqual(t, len(result), 1)
//}
//
//func TestNLP(t *testing.T) {
//	adult, _ := c.Meta.ProfileByName("adult")
//	male, _ := c.Meta.ProfileByName("male")
//	setting := Setting{
//		Profile:   []string{adult.ID, male.ID},
//		MaxResult: 5,
//	}
//	result := c.ParseText("aku sakit kepala dari kemarin dan meler dari dua hari yang lalu", setting)
//	headache := c.Symptoms.Search("Sakit Kepala")[0]
//	runnyNose := c.Symptoms.Search("Pilek")[0]
//	assert.Equal(t, len(result), 2)
//	assert.Equal(t, result[0].SymptomID, headache.ID)
//	assert.Equal(t, result[1].SymptomID, runnyNose.ID)
//}
//func TestDiagnosa(t *testing.T) {
//	symp := c.Symptoms.Search("pusing")[0]
//	propID := c.Meta.PropIDFromName("underoneday")
//	constantPropID := c.Meta.PropIDFromName("constant")
//	prop := c.Meta.Props[propID]
//	constantProp := c.Meta.Props[constantPropID]
//	evidence := db.Evidence{
//		SymptomID: symp.ID,
//		Props: []db.PropertyPair{
//			{
//				Type: prop.Type,
//				Prop: propID,
//			},
//			{
//				Type: constantProp.Type,
//				Prop: constantPropID,
//			},
//		},
//	}
//	adult, _ := c.Meta.ProfileByName("adult")
//	male, _ := c.Meta.ProfileByName("male")
//
//	result := c.Diagnose([]db.Evidence{evidence}, Setting{
//		Profile:   []string{adult.ID, male.ID},
//		MaxResult: 5,
//	})
//	assert.GreaterOrEqual(t, len(result), 1)
//}
//
//func TestSearchSymp(t *testing.T) {
//	adult, _ := c.Meta.ProfileByName("adult")
//	male, _ := c.Meta.ProfileByName("male")
//	setting := Setting{
//		Profile:   []string{adult.ID, male.ID},
//		MaxResult: 5,
//	}
//
//	potentialSymptoms := db.Symptoms{}
//	profiles := c.Meta.GetProfiles(setting.Profile)
//	sympTest := c.Symptoms.Search("pusing")[0]
//	symp := c.Symptoms.SearchSymp(potentialSymptoms,"pusing", profiles, c.Meta)
//	assert.Equal(t, symp[0].ID, sympTest.ID)
//}
//
//func TestContextualSearchSymp(t *testing.T) {
//	adult, _ := c.Meta.ProfileByName("adult")
//	male, _ := c.Meta.ProfileByName("male")
//	setting := Setting{
//		Profile:   []string{adult.ID, male.ID},
//		MaxResult: 5,
//	}
//
//	potentialSymptoms := db.Symptoms{}
//	profiles := c.Meta.GetProfiles(setting.Profile)
//	sympTest := c.Symptoms.Search("sakit kepala")[0]
//
//	sites := []string{
//        "kepala",
//    }
//	context := make(map[string][]string)
//	context["sakit"] = sites
//	symp := c.Symptoms.ContextualSearchSymp(potentialSymptoms, context, profiles, c.Meta)
//	assert.Equal(t, symp[0].ID, sympTest.ID)
//}

package core

import (
	"sort"

	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/util"
)

// Consult is
func (core *Core) Consult(evidences []db.Evidence, setting Setting) []PotentialSymptom {
	if len(evidences) == 0 {
		return nil
	}

	profiles := core.Meta.GetProfiles(setting.Profile)

	diseases := core.GetDiseasesFromEvidences(evidences, profiles).SortByLikeliness()

	symptoms := core.GetSymptomsOfDiseases(diseases, evidences, profiles).Sort(func(a, b db.Symptom) bool {
		scoreA := len(a.Differentials)
		scoreB := len(b.Differentials)
		if diseases.IsMustHave(a) {
			scoreA = scoreA + 1000
		}
		if diseases.IsMustHave(b) {
			scoreB = scoreB + 1000
		}
		return scoreA > scoreB
	})

	res := core.EvidencesToPotentialSymptoms(evidences)
	for _, symptom := range symptoms {
		res = append(res, core.GetPotentialSymptom(symptom, evidences))
	}

	n := setting.MaxResult

	if n == 0 || len(res) < n {
		return res
	}

	return res[:n]
}

// GetSymptomsOfDiseases returns all symptoms associated with diseases
func (core *Core) GetSymptomsOfDiseases(diseases []db.Disease, evidences db.Evidences, profiles []db.Profile) db.Symptoms {
	symptoms := map[string]bool{}
	res := []db.Symptom{}

	for _, disease := range diseases {
		for _, symptom := range core.GetAssociatedSymptoms(disease, evidences, profiles) {
			if symptoms[symptom.ID] {
				continue
			}
			res = append(res, symptom)
			symptoms[symptom.ID] = true
		}
	}

	return res
}

// GetAssociatedSymptoms returns all symptoms associated with disease
func (core *Core) GetAssociatedSymptoms(disease db.Disease, evidences db.Evidences, profiles []db.Profile) db.Symptoms {
	symptoms := []db.Symptom{}

	for _, diff := range core.Differentials.ByDisease(disease) {
		symptom, ok := core.Symptoms.Dict[diff.Symptom]
		if !ok || symptom.InEvidences(evidences) || !core.Meta.MatchProfile(symptom.PreConditions, profiles) {
			continue
		}
		symptoms = append(symptoms, symptom)
	}

	return symptoms
}

// GetDiseasesFromEvidences retrieves all diseases associated with evidence
func (core *Core) GetDiseasesFromEvidences(evidences db.Evidences, profiles []db.Profile) (res db.Diseases) {
	dmap := map[string]bool{}

	for _, evidence := range evidences.ChiefOnly() {

		symptom, ok := core.Symptoms.Dict[evidence.SymptomID]
		if !ok || !core.Meta.MatchProfile(symptom.PreConditions, profiles) {
			continue
		}
		for _, diff := range core.Differentials.BySymptom(symptom) {
			if dmap[diff.Disease] || !symptom.MatchProps(diff, evidence.Props) {
				continue
			}
			disease := core.Diseases.Dict[diff.Disease]
			dmap[disease.ID] = true
			if !core.Meta.MatchProfile(disease.PreConditions, profiles) || !disease.SatisfyMustHave(evidences) {
				continue
			}
			res = append(res, disease)
		}
	}

	return
}

// EvidencesToPotentialSymptoms converts "yes" answered evidences to PotentialSymptom
func (core *Core) EvidencesToPotentialSymptoms(evidences db.Evidences) (res []PotentialSymptom) {
	for _, evidence := range evidences {
		if evidence.Answer != "yes" {
			continue
		}
		symptom, ok := core.Symptoms.Dict[evidence.SymptomID]
		if !ok {
			continue
		}
		ps := core.GetPotentialSymptom(symptom, evidences)
		if len(ps.PropTypes) > 0 {
			res = append(res, ps)
		}
	}

	return
}

// GetPotentialSymptom is
func (core *Core) GetPotentialSymptom(symptom db.Symptom, evidences db.Evidences) PotentialSymptom {
	return PotentialSymptom{
		ID:          symptom.ID,
		Name:        symptom.NameIndo,
		Description: symptom.Description,
		Explanation: symptom.Explanation,
		Media:       symptom.MediaFilesLink,
		Source:      symptom.SourceURL,
		Question:    symptom.Question,
		PropTypes:   core.GetSymptomPropTypes(symptom, evidences),
		SID:         symptom.SID,
	}
}

// GetSymptomPropTypes get PropTypes of a Symptom
func (core *Core) GetSymptomPropTypes(symptom db.Symptom, evidences db.Evidences) (res []PropType) {
	types := symptom.MustKnows

	if util.StringIn(evidences.ChiefOnly().SymptomIDs(), symptom.ID) {
		types = append(types, symptom.NiceToKnows...)
	}

	for _, id := range types {
		t := core.Meta.Types[id]
		if core.PropTypeAsked(evidences, symptom, t) {
			continue
		}
		options := core.GetSymptomProps(symptom, t)
		if len(options) > 0 {
			res = append(res, PropType{
				TypeID:      id,
				Name:        t.NameIndo,
				Description: t.Description,
				Question:    t.Question,
				Options:     options,
				Order:       t.Order,
				AskOnce:     t.AskOnce,
			})
		}
	}

	sort.Slice(res, func(a, b int) bool {
		return res[a].Order < res[b].Order
	})

	return
}

// GetSymptomProps gets property options for a Symptom, for patient to choose
func (core *Core) GetSymptomProps(symptom db.Symptom, t db.Type) (res []PropertyOption) {
	has := map[string]bool{}
	for _, diff := range core.Differentials.BySymptom(symptom) {
		for _, props := range diff.Props {
			for _, propID := range props {
				prop := core.Meta.Props[propID]
				if prop.Type == t.ID && !has[prop.ID] {
					res = append(res, PropertyOption{
						PropID:      prop.ID,
						Name:        prop.NameIndo,
						Description: prop.Description,
					})
					has[prop.ID] = true
				}
			}
		}
	}

	sort.Slice(res, func(a, b int) bool {
		return res[a].Name < res[b].Name
	})

	return
}

// PropTypeAsked checks whether prop with type t has been asked, and stored in Evidence
func (core *Core) PropTypeAsked(evidences db.Evidences, symptom db.Symptom, t db.Type) bool {
	if len(evidences) == 0 {
		return false
	}

	for _, evidence := range evidences {
		for _, p := range evidence.Props {
			prop, ok := core.Meta.Props[p.Prop]
			if ok && t.ID == prop.Type && (symptom.ID == evidence.SymptomID || t.AskOnce) {
				return true
			}
		}
	}

	return false
}

package core

import (
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/util"
)

// NormaliseEvidence will normalise types
// Several Types (Site, Onset,... ) if they has been asked in Chief Complaint or in other additional symptoms
// will be skipped if Times Asked is set to "Once only"
// NormaliseEvidence will set these skipped types and apply them in Evidences
func (core Core) NormaliseEvidence(evidences db.Evidences) db.Evidences {
	normalised := evidences.ChiefOnly()
	for _, chief := range evidences.ChiefOnly() {
		for _, withoutChief := range evidences.WithoutChief() {
			symptom, ok := core.DB.Symptoms.Dict[withoutChief.SymptomID]
			mustKnows := symptom.MustKnows
			if !ok {
				continue
			}
			if res := util.Intersect(chief.Props.TypeIDs(), mustKnows); len(res) > 0 {
				normalised = append(normalised, db.Evidence{
					SymptomID:        withoutChief.SymptomID,
					Props:            append(withoutChief.Props, chief.Props.ByIDs(res)...),
					IsChiefComplaint: withoutChief.IsChiefComplaint,
					Answer:           withoutChief.Answer,
				})
			} else {
				normalised = append(normalised, withoutChief)
			}
		}
	}
	return normalised
}

package core

import (
	"prixa.ai/prixa-db/pkg/db"
)

const (
	// StatusEnable is
	StatusEnable int = 1
)

// Core is data holder for all Airtable data
type Core struct {
	db.DB
	Weights map[string]map[string]int
}

// New create new Nalar core
func New() *Core {
	c := &Core{}
	c.FetchAllTables()
	c.ComputeWeights()

	return c
}

// PropertyOption is
type PropertyOption struct {
	PropID      string `json:"propId"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

// PropType is
type PropType struct {
	TypeID      string           `json:"typeId"`
	Name        string           `json:"name"`
	Description string           `json:"description"`
	Question    string           `json:"question"`
	Order       int              `json:"order"`
	AskOnce     bool             `json:"asked_once"`
	Options     []PropertyOption `json:"options"`
}

// PotentialSymptom is
// Media contains url of images, video, etc
// Source contains url/owner of the source of the media
type PotentialSymptom struct {
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Explanation string     `json:"explanation"`
	Question    string     `json:"question"`
	Media       string     `json:"media"`
	Source      string     `json:"source"`
	PropTypes   []PropType `json:"propTypes"`
	SID         string     `json:"sid"`
}

// TriageResult is brief Information of triage for potential disease  (exclude symptom, properties and diseases)
type TriageResult struct {
	ID          string
	Name        string
	NameIndo    string
	Description string
}

// PotentialDisease is
type PotentialDisease struct {
	ID          string       `json:"id"`
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Likeliness  string       `json:"likeliness"`
	Score       float64      `json:"score"`
	URL         string       `json:"url"`
	Triage      TriageResult `json:"triage"`
	Labs        []db.Lab     `json:"labs"`
	Prognosis   string       `json:"prognosis"`
}

// Setting is
type Setting struct {
	Profile   []string `json:"profile"`
	MaxResult int      `json:"maxResult"`
	Threshold float64  `json:"threshold"`
}

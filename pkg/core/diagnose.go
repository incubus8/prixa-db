package core

import (
	"math"
	"sort"

	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/db"
)

const defaultMaxResult int = 4

var likelinessScore = map[int]string{
	1:  "Rare",
	5:  "Rare",
	10: "Very Rare",
}

func remapLikelinessScore(num int) string {
	val, ok := likelinessScore[num]
	if !ok {
		return "Unknown"
	}
	return val
}

// DiseaseScore is struct for modelling a Disease and its score
type DiseaseScore struct {
	Disease db.Disease
	Score   float64
}

// DiffScore is differential score
type DiffScore map[string]map[string]float64

// Diagnose is
func (core *Core) Diagnose(evidences db.Evidences, setting Setting) []PotentialDisease {
	profiles := core.Meta.GetProfiles(setting.Profile)
	diffScore := core.CalculateScore(evidences, profiles)

	opdTriageID, isOPDExist := core.isOPDTriageExistInEvidences(evidences.FilterAnswer("yes"))

	ranks := core.Rank(diffScore, setting.MaxResult, setting.Threshold)
	diseases := make([]PotentialDisease, 0, len(ranks))
	for _, item := range ranks {
		if isOPDExist {
			item.Disease.Triage = opdTriageID
		}
		diseases = append(diseases, PotentialDisease{
			ID:          item.Disease.ID,
			Name:        item.Disease.NameIndo,
			Description: item.Disease.Description,
			Likeliness:  remapLikelinessScore(item.Disease.Likeliness),
			Score:       item.Score,
			URL:         item.Disease.URL,
			Triage:      getTriageResult(item.Disease.Triage, core),
			Labs:        getLabsInformation(item.Disease.Labs, core),
			Prognosis:   core.GetPrognosisName(item.Disease.Prognosis),
		})
	}

	return diseases
}

// GetPrognosisName is
func (core *Core) GetPrognosisName(id string) string {
	prognosis, ok := core.Labs.ByID(id)
	if !ok {
		logrus.Warnf("missing prognosis from ID: %v", id)
		return ""
	}

	return prognosis.Name
}

func (core *Core) isOPDTriageExistInEvidences(evidences db.Evidences) (string, bool) {
	if !IsForCovid19() {
		return "", false
	}

	opdTriage, errGetTriage := core.Triages.ByName("OPD")
	if errGetTriage != nil {
		logrus.WithError(errGetTriage).Errorln("Disabling OPD due to data error")
		return "", false
	}

	for _, evidence := range evidences {
		if core.Symptoms.Dict[evidence.SymptomID].Triage == opdTriage.ID {
			return opdTriage.ID, true
		}
	}

	return "", false
}

func getLabsInformation(labsID []string, core *Core) []db.Lab {
	var result []db.Lab
	for _, id := range labsID {
		lab, ok := core.Labs.ByID(id)
		if ok {
			result = append(result, lab)
		}
	}
	return result
}

func getTriageResult(id string, core *Core) TriageResult {
	triage, ok := core.Triages.ByID(id)
	if ok {
		return TriageResult{
			ID:          triage.ID,
			Name:        triage.Name,
			NameIndo:    triage.NameIndo,
			Description: triage.Description,
		}
	}
	return TriageResult{}
}

// ComputeWeights calculate initial weights of all diseases
func (core *Core) ComputeWeights() {
	weights := map[string]map[string]int{}
	for _, disease := range core.Diseases.Dict {
		weights[disease.ID] = map[string]int{}
		for _, diff := range core.Differentials.ByDisease(disease) {
			weights[disease.ID][diff.Symptom] = 1
			for _, propType := range diff.Props {
				for _, propID := range propType {
					prop := core.Meta.Props[propID]
					weights[disease.ID][propKey(diff.Symptom, prop.Type, prop.ID)] = 1
				}
			}
		}
	}

	core.Weights = weights
}

// CalculateScore is function to calculate score for each disease
// based on patients's choice
func (core *Core) CalculateScore(evidences db.Evidences, profiles []db.Profile) DiffScore {
	result := DiffScore{}
	chiefComplaintID := evidences.ChiefOnly()[0].SymptomID
	normalizedEvidences := core.NormaliseEvidence(evidences.FilterAnswer("yes"))
	for _, evidence := range normalizedEvidences {
		symptom, ok := core.Symptoms.Dict[evidence.SymptomID]
		if !ok {
			continue
		}
		diffs := core.Differentials.BySymptom(symptom)
		for _, diff := range diffs {
			disease, ok := core.Diseases.Dict[diff.Disease]
			if !ok ||
				!core.Meta.MatchProfile(disease.PreConditions, profiles) ||
				!symptom.MatchProps(diff, evidence.Props) ||
				!disease.SatisfyMustHave(evidences) ||
				// !diff.SatisfyChiefComplaintMustKnow(
				// 	core.Symptoms.Dict[chiefComplaintID].MustKnows,
				// 	core.Meta.Types,
				// 	normalizedEvidences.ChiefOnly()[0].Props,
				// ) ||
				!core.Differentials.HasSymptom(disease, chiefComplaintID) {
				continue
			}

			// diffByDisease := core.Differentials.ByDisease(disease)
			// diffByDiseaseWithSymptomMustKnow := core.Symptoms.GetDiffWithSympMustKnow(diffByDisease, core.Meta.TypeIDFromName("Site"), chiefComplaintID)
			// additionalSymptomsEvidences := getAdditionalSymptomsEvidences(normalizedEvidences)
			// if !core.Symptoms.AdditionalSymptomsSatisfyMustKnow(diffByDiseaseWithSymptomMustKnow, additionalSymptomsEvidences) {
			// 	continue
			// }

			num := float64(core.tf(symptom.ID, disease.ID)) * core.idf(symptom.ID)
			for _, choice := range evidence.Props {
				key := propKey(symptom.ID, choice.Type, choice.Prop)
				num += float64(core.tf(key, disease.ID)) * core.idf(key)
			}
			if result[disease.ID] == nil {
				result[disease.ID] = map[string]float64{}
			}
			result[diff.Disease][symptom.ID] = num
		}
	}
	return result
}

// Rank is function to calculate score for a Disease based on Differential
func (core *Core) Rank(diffScores DiffScore, top int, threshold float64) []DiseaseScore {
	if top == 0 {
		top = defaultMaxResult
	}

	ranking := make([]DiseaseScore, 0, len(diffScores))
	for diseaseID, symptoms := range diffScores {
		total := 0.0
		for _, score := range symptoms {
			total += score
		}
		ranking = append(ranking, DiseaseScore{
			Disease: core.Diseases.Dict[diseaseID],
			Score:   total,
		})
	}

	sort.Slice(ranking, func(i, j int) bool {
		return ranking[i].Score > ranking[j].Score
	})

	if len(ranking) > top {
		ranking = ranking[:top]
	}

	sum := 0.0
	for _, value := range ranking {
		sum += value.Score
	}

	var result []DiseaseScore
	for _, ds := range ranking {
		score := ds.Score / sum
		if score > threshold {
			result = append(result, DiseaseScore{
				Disease: ds.Disease,
				Score:   ds.Score / sum,
			})
		}
	}

	return result
}

func (core *Core) tf(prop string, diseaseID string) int {
	if core.Weights[diseaseID] == nil {
		return 0
	}
	return core.Weights[diseaseID][prop]
}

func (core *Core) df(prop string) int {
	count := 0
	for _, labels := range core.Weights {
		if labels[prop] > 0 {
			count++
		}
	}
	return count
}

func (core *Core) idf(prop string) float64 {
	N := float64(core.Diseases.Len())
	df := float64(core.df(prop))
	result := math.Log(N / (1 + df))

	return result
}

func propKey(symptom string, t string, prop string) string {
	return symptom + ":" + t + ":" + prop
}

package core

import (
	"os"
	"strconv"

	"prixa.ai/prixa-db/pkg/db"
)

const (
	typeEmergencyRespiratorySymptom    = "pernapasan emergency"
	typeNonEmergencyRespiratorySymptom = "pernapasan non emergency"
)

// Covid19Data is the struct that holds profileMap and Symptom
type Covid19Data struct {
	ProfileMap                           map[string]db.Profile
	Symptom                              db.Symptom
	Score                                float32
	IsEmergencyRespiratorySympPresent    bool
	IsNonEmergencyRespiratorySympPresent bool
}

// IsForCovid19 checks whether a certain functions works for covid19 environtment
func IsForCovid19() bool {
	envCovidStatus, _ := strconv.Atoi(os.Getenv("ENABLE_COVID19_OPD"))
	return envCovidStatus == StatusEnable
}

// GetCovid19Score gets the score for chosen
func (data *Covid19Data) GetCovid19Score(profileType string) *Covid19Data {
	if data.ProfileMap != nil {
		for _, profile := range data.ProfileMap {
			data.Score += profile.Scoring
			data.ProfileMap = nil
			return data
		}
	}

	if !data.IsEmergencyRespiratorySympPresent && profileType == typeEmergencyRespiratorySymptom {
		data.IsEmergencyRespiratorySympPresent = true
		data.Score += data.Symptom.Scoring
	} else if !data.IsNonEmergencyRespiratorySympPresent && profileType == typeNonEmergencyRespiratorySymptom {
		data.IsNonEmergencyRespiratorySympPresent = true
		data.Score += data.Symptom.Scoring
	}

	data.Symptom = db.Symptom{}
	return data
}

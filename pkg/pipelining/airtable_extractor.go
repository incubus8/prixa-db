package pipelining

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
)

type extractorAirtableImpl struct {
	client    *resty.Client
	baseURL   string
	authToken string
}

// newExtractorAirtableImpl creates new Extractor for airtable implementation
func newExtractorAirtableImpl() Extractor {
	airtable := &extractorAirtableImpl{}
	airtable.baseURL = os.Getenv("AIRTABLE_URL")
	airtable.authToken = os.Getenv("AIRTABLE_TOKEN")
	airtable.client = resty.New()
	airtable.client.SetTimeout(10 * time.Second)

	return airtable
}

func (e *extractorAirtableImpl) FetchDataFromSource(param extractorParam) (*extractorResultData, error) {
	if e.baseURL == "" || e.authToken == "" {
		logrus.WithField("baseUrl", e.baseURL).
			WithField("token", e.authToken).
			Errorln("No URL or Token specified in ENV")
		return nil, errors.New("no URL or token specified in ENV")
	}

	tableURL := fmt.Sprintf("%v/%v", e.baseURL, param.Table)
	if param.Query != "" {
		tableURL = fmt.Sprintf("%v/%v?%v", e.baseURL, param.Table, param.Query)
	}
	logrus.Infoln("Fetching", tableURL)

	e.client.SetHeader("Authorization", "Bearer "+e.authToken)
	e.client.SetHeader("Content-type", "application/json")
	e.client.SetRetryCount(3)
	e.client.SetRetryWaitTime(5 * time.Second)
	resp, err := e.client.R().Get(tableURL)
	if err != nil {
		logrus.WithError(err).WithField("airtableUrl", tableURL).WithField("airtableToken", e.authToken).Errorln()
		return nil, err
	}

	var airTableData airtableTransformData
	if unmarshalError := json.Unmarshal(resp.Body(), &airTableData); unmarshalError != nil {
		logrus.WithField("body", resp.Body()).WithError(unmarshalError).Errorln()
		return nil, unmarshalError
	}
	return &extractorResultData{
		Table:   param.Table,
		Offset:  airTableData.Offset,
		Content: &airTableData,
	}, nil
}

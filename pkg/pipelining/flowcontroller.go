package pipelining

type flowController interface {
	startPipeliningProcess()
	gracefulStop() error
}

package pipelining

const (
	// AirtableToRabbitMQJob defines source name in pipeling process
	airtableToRabbitMQJob             = "AirtableToRabbitMQ"
	rabbitMQToElasticSearch           = "RabbitMQToElasticSearch"
	rabbitMQToPostgres                = "RabbitMQToPostgres"
	removeOutdatedDataInElasticSearch = "RemoveOutdatedDataInElasticSearch"
	httpRequestToRabbitMQ             = "HTTPRequestToRabbitMQ"
	rabbitMQToMeilisearch             = "RabbitMQToMeilisearch"
)

// flowPipeline is defines everything needed in the pipelining process
type flowPipeline struct {
	Extractor            Extractor
	flowParams           []flowParam
	flowRequestAPIParams []flowRequestAPIParam
}

func initAirtableToRabbitMQFlowPipleline(flowParams []flowParam) *flowPipeline {
	return &flowPipeline{
		Extractor:  newExtractorAirtableImpl(),
		flowParams: flowParams,
	}
}

func initHTTPRequestToRabbitMQFlowPipleline(flowRequestParam []flowRequestAPIParam) *flowPipeline {
	return &flowPipeline{
		Extractor:            newExtractorHTTPRequestmpl(),
		flowRequestAPIParams: flowRequestParam,
	}
}

// flowConsumerPipeline struct
type flowConsumerPipeline struct {
	Params []flowParam
}

func initConsumerToRepositoryFlowPipeline(flowParams []flowParam) *flowConsumerPipeline {
	return &flowConsumerPipeline{
		Params: flowParams,
	}
}

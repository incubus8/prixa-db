package pipelining

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging"
)

// flowController is controller for pipelining process
type flowPublisherController struct {
	job          string
	publisher    messaging.Publisher
	flowPipeline *flowPipeline
}

// newFlowController to instantiate flowcontroller
func newFlowPublisherController(pub messaging.Publisher, flowPipeline *flowPipeline, job string) flowController {
	return &flowPublisherController{
		publisher:    pub,
		flowPipeline: flowPipeline,
		job:          job,
	}
}

type pipeliningPublishHandler func(flow *flowPipeline, publisher messaging.Publisher) error

func (c *flowPublisherController) mapSourcePipeliningPublisherHandler() map[string]pipeliningPublishHandler {
	return map[string]pipeliningPublishHandler{
		airtableToRabbitMQJob: c.processPipelineAirtableToRabbitMQ,
		httpRequestToRabbitMQ: c.processPipelineHTTPRequestToRabbitMQ,
	}
}

// startPipeliningProcess to start pipelining process
func (c *flowPublisherController) startPipeliningProcess() {
	pipelineHandler, ok := c.mapSourcePipeliningPublisherHandler()[c.job]
	if !ok {
		logrus.WithError(fmt.Errorf("job %v is invalid or not supported", c.job)).Errorln()
	}
	err := pipelineHandler(c.flowPipeline, c.publisher)
	if err != nil {
		logrus.WithError(err).Errorln()
	}
}

func (c *flowPublisherController) gracefulStop() error {
	return nil
}

func fetchDataFromAirTable(extractor Extractor, table string, query string) (*extractorResultData, error) {
	extractData, err := extractor.FetchDataFromSource(
		extractorParam{
			Table: table,
			Query: query,
		},
	)
	if err != nil {
		logrus.WithError(err).Errorln(err)
		return nil, err
	}
	return extractData, nil
}

func (c *flowPublisherController) processPipelineAirtableToRabbitMQ(flow *flowPipeline, publisher messaging.Publisher) error {
	for _, param := range flow.flowParams {
		offsetQuery := ""
		for ok := true; ok; ok = offsetQuery != "" {
			extractData, err := fetchDataFromAirTable(flow.Extractor, param.Table, offsetQuery)
			offsetQuery = ""
			if err != nil {
				logrus.WithError(err).Errorln()
			}
			if extractData == nil {
				continue
			}

			if extractData.Offset != "" {
				offsetQuery = "offset=" + extractData.Offset
			}
			go transformAndPublishExtractData(extractData, c.publisher, param)
		}
	}

	return nil
}

func transformAndPublishExtractData(extractData *extractorResultData, publisher messaging.Publisher, param flowParam) {
	payloads, err := extractData.Content.TransformData()
	if err != nil {
		logrus.WithError(err).Errorln()
	}
	for _, payload := range payloads {
		publisher.Publish(payload, param.RoutingKey, "")
	}
}

func (c *flowPublisherController) processPipelineHTTPRequestToRabbitMQ(flow *flowPipeline, publisher messaging.Publisher) error {
	for _, requestParam := range flow.flowRequestAPIParams {
		extractData, err := c.flowPipeline.Extractor.FetchDataFromSource(extractorParam{
			requestParam: requestParam,
		})
		if err != nil {
			logrus.WithError(err).WithField("requestParam", requestParam).Errorln()
			continue
		}

		payloads, err := extractData.Content.TransformData()
		if err != nil {
			logrus.WithError(err).Errorln()
			continue
		}
		for _, payload := range payloads {
			publisher.Publish(payload, requestParam.RoutingKey, "")
		}
	}
	return nil
}

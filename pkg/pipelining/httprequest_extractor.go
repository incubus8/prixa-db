package pipelining

import (
	"errors"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
)

type extractorHTTPRequestImpl struct {
	client *resty.Client
}

// newExtractorHTTPRequestmpl creates new extractor for HTPP Request API implementation
func newExtractorHTTPRequestmpl() Extractor {
	httpRequest := &extractorHTTPRequestImpl{}
	httpRequest.client = resty.New()
	httpRequest.client.SetTimeout(10 * time.Second)
	httpRequest.client.SetRetryCount(3)
	httpRequest.client.SetRetryWaitTime(5 * time.Second)
	return httpRequest
}

func (e *extractorHTTPRequestImpl) FetchDataFromSource(param extractorParam) (*extractorResultData, error) {
	if param.requestParam.Request.URL == "" {
		logrus.WithField("param.requestParam.URL", param.requestParam.Request.URL).
			Errorln("no URL specified in request parameter")
		return nil, errors.New("no URL specified in request parameter")
	}

	if param.requestParam.Request.Method == "" {
		logrus.WithField("param.requestParam.Method", param.requestParam.Request.URL).
			Errorln("no Request Method specified in request parameter")
		return nil, errors.New("no Request Method specified in request parameter")
	}
	logrus.Infoln("Fetching", param.requestParam.Request.URL)
	for hKey, hValue := range param.requestParam.Request.Headers {
		e.client.SetHeader(hKey, hValue)
	}
	if _, ok := param.requestParam.Request.Headers["Content-type"]; !ok {
		e.client.SetHeader("Content-type", "application/json")
	}

	resp, err := e.client.R().Execute(param.requestParam.Request.Method, param.requestParam.Request.URL)
	if err != nil {
		logrus.WithError(err).WithField("url", param.requestParam.Request.URL).WithField("method", param.requestParam.Request.Method).Errorln()
		return nil, err
	}

	if resp.StatusCode() != 200 {
		logrus.WithError(err).WithField("url", param.requestParam.Request.URL).WithField("method", param.requestParam.Request.Method).Errorln()
		return nil, err
	}

	payloadData := httpRequestAPITransformData{
		transformJSONPath: param.requestParam.Request.TransformJSONPath,
		payloadMap:        param.requestParam.PayloadMap,
		data:              resp.Body(),
	}
	return &extractorResultData{
		Content: &payloadData,
	}, nil
}

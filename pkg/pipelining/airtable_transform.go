package pipelining

import (
	"encoding/json"
	"time"

	"github.com/sirupsen/logrus"
)

type airtableTransformData struct {
	Records []airtableRecord `json:"records"`
	Offset  string           `json:"offset"`
}

type airtableRecord struct {
	ID          string                 `json:"id"`
	Fields      map[string]interface{} `json:"fields"`
	CreatedTime string                 `json:"createdTime"`
	LastUpdated string                 `json:"lastUpdated"`
}

// TransformData to transfor extractor result data to slice of json
func (airtable *airtableTransformData) TransformData() ([]json.RawMessage, error) {
	now := time.Now().Format("2006-01-02T15:04:05.000Z")
	payloads := []json.RawMessage{}
	for _, record := range airtable.Records {
		record.LastUpdated = now
		payload, err := json.Marshal(record)
		if err != nil {
			logrus.WithError(err).Errorln("Failed to encode payload airtable")
			continue
		}
		payloads = append(payloads, payload)
	}
	return payloads, nil
}

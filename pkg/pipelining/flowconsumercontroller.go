package pipelining

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging"
)

// flowConsumerController is controller for pipelining consumer process
type flowConsumerController struct {
	consumer     messaging.Consumer
	flowPipeline *flowConsumerPipeline
	job          string
}

// newFlowConsumerController to instantiate flowcontroller
func newFlowConsumerController(consumer messaging.Consumer, flowPipeline *flowConsumerPipeline, job string) flowController {
	return &flowConsumerController{
		consumer:     consumer,
		flowPipeline: flowPipeline,
		job:          job,
	}
}

type pipeliningConsumeHandler func(flow *flowConsumerPipeline, publisher messaging.Consumer) error

func (c *flowConsumerController) mapSourcePipeliningConsumerHandler() map[string]pipeliningConsumeHandler {
	return map[string]pipeliningConsumeHandler{
		rabbitMQToElasticSearch: c.processPipelineRabbitMQToRepository,
		rabbitMQToMeilisearch:   c.processPipelineRabbitMQToRepository,
		rabbitMQToPostgres:      c.processPipelineRabbitMQToRepository,
	}
}

func (c *flowConsumerController) startPipeliningProcess() {
	pipelineHandler, ok := c.mapSourcePipeliningConsumerHandler()[c.job]
	if !ok {
		logrus.WithError(fmt.Errorf("job %v is invalid or not supported", c.job)).Errorln()
	}
	err := pipelineHandler(c.flowPipeline, c.consumer)
	if err != nil {
		logrus.WithError(err).Errorln()
	}
}

func (c *flowConsumerController) gracefulStop() error {
	return c.consumer.Stop()
}

func (c *flowConsumerController) processPipelineRabbitMQToRepository(_ *flowConsumerPipeline, consumer messaging.Consumer) error {
	_ = c.consumer.StartConsumingAllQueues()
	return nil
}

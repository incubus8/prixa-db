package pipelining

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	"github.com/goccy/go-yaml"
	"github.com/sirupsen/logrus"
)

type prixaPipeliningHandler func(ctx context.Context, pipeline *prixaDataPipeline) error

var prixaPipelineHandlerMaps = map[string]prixaPipeliningHandler{
	airtableToRabbitMQJob:             runPipeliningAirtableToRabbitMQ,
	rabbitMQToElasticSearch:           runPipeliningRabbitMQToElasticSearch,
	rabbitMQToMeilisearch:             runPipeliningRabbitMQToMeilisearch,
	removeOutdatedDataInElasticSearch: runRemovedOutdatedDataInElasticSearch,
	rabbitMQToPostgres:                runPipeliningRabbitMQToPostgres,
	httpRequestToRabbitMQ:             runPipeliningHTTPRequestToRabbitMQ,
}

const (
	prixaDataPipelineVersion string = "v1"
	prixaDataPipelineKind    string = "PrixaDataPipeline"
	pipelineCronJobType      string = "CronJob"
)

type prixaDataPipelineEnv struct {
	Name  string `yaml:"name"`
	Value string `yaml:"value"`
}

type prixaDataPipelineMetadata struct {
	Type     string `yaml:"type"`
	Name     string `yaml:"name"`
	Interval string `yaml:"interval"`
}

type prixaDataPipeline struct {
	APIVersion       string                    `yaml:"apiVersion"`
	Kind             string                    `yaml:"kind"`
	Metadata         prixaDataPipelineMetadata `yaml:"metadata"`
	Env              []prixaDataPipelineEnv    `yaml:"env"`
	Job              string                    `yaml:"job"`
	FlowParams       []flowParam               `yaml:"param"`
	FlowRequestParam []flowRequestAPIParam     `yaml:"requestAPIParam"`
}

// RunPrixaPipelining run pipelining process based on yaml
func RunPrixaPipelining(ctx context.Context, yamlConfig json.RawMessage) error {
	prixaPipeline, err := initPrixaDataPipeline(yamlConfig)
	if err != nil {
		logrus.WithContext(ctx).WithError(err).Errorln()
		return err
	}
	err = prixaPipeline.runPrixaPipelineJob(ctx)
	return err
}

func initPrixaDataPipeline(yamlConfig json.RawMessage) (*prixaDataPipeline, error) {
	prixaPipeline, err := getPrixaDataPipeline(yamlConfig)
	if err != nil {
		return nil, err
	}
	if err = validatePrixaDataPipeline(prixaPipeline); err != nil {
		return nil, err
	}
	setEnvironmentVariableFromYaml(prixaPipeline.Env)
	return prixaPipeline, nil
}

func getPrixaDataPipeline(yamlConfig json.RawMessage) (*prixaDataPipeline, error) {
	var prixaPipeline prixaDataPipeline
	if err := yaml.Unmarshal(yamlConfig, &prixaPipeline); err != nil {
		logrus.WithError(err).Fatalln("error when unmarshal yaml configuration")
		return nil, err
	}
	return &prixaPipeline, nil
}

func validatePrixaDataPipeline(pipeline *prixaDataPipeline) error {
	if pipeline.APIVersion == "" {
		return fmt.Errorf("api version must exist")
	}

	if pipeline.APIVersion != prixaDataPipelineVersion {
		return fmt.Errorf("api version %v is invalid", pipeline.APIVersion)
	}

	if pipeline.Kind == "" {
		return fmt.Errorf("kind must exist")
	}

	if pipeline.Kind != prixaDataPipelineKind {
		return fmt.Errorf("kind %v is invalid", pipeline.Kind)
	}

	if pipeline.Metadata.Name == "" {
		return fmt.Errorf("name must be exist in metadata")
	}

	if pipeline.Metadata.Type == "" {
		return fmt.Errorf("type must be exist in metadata")
	}

	if pipeline.Metadata.Type == pipelineCronJobType && pipeline.Metadata.Interval == "" {
		return fmt.Errorf("interval must be exist in metadata for type CronJob")
	}

	if pipeline.Job == "" {
		return fmt.Errorf("job must be exist")
	}
	return nil
}

func setEnvironmentVariableFromYaml(envList []prixaDataPipelineEnv) {
	for _, env := range envList {
		_ = os.Setenv(env.Name, env.Value)
	}
}

func (pipeline *prixaDataPipeline) runPrixaPipelineJob(ctx context.Context) error {
	handler, ok := prixaPipelineHandlerMaps[pipeline.Job]
	if !ok {
		return fmt.Errorf("job %v is invalid or not already supported", pipeline.Job)
	}
	err := handler(ctx, pipeline)
	return err
}

package pipelining

import "encoding/json"

// extractorParam is a parameter that needs to be passed into the FetchDataFromSource Process
type extractorParam struct {
	Table        string
	Query        string
	Index        string
	requestParam flowRequestAPIParam
}

// extractorResultData is a result of the FetchDataFromSource Process
type extractorResultData struct {
	Table   string
	Content flowContent
	Offset  string
	Index   string
}

// Extractor is an abstract that defines in Extractor
type Extractor interface {
	FetchDataFromSource(param extractorParam) (*extractorResultData, error)
}

// flowContent is an abstract that define function to manage flowcontent
type flowContent interface {
	TransformData() ([]json.RawMessage, error)
}

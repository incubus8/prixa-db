package pipelining

import (
	"context"
	"database/sql"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/spf13/cast"

	graceful "github.com/ashulepov/gocron-graceful"
	"github.com/jasonlvhit/gocron"
	meili "github.com/meilisearch/meilisearch-go"
	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/repository"
	"prixa.ai/prixa-db/pkg/sdk"
)

func runPipeliningAirtableToRabbitMQ(ctx context.Context, pipeline *prixaDataPipeline) error {
	flowPipeline := initAirtableToRabbitMQFlowPipleline(pipeline.FlowParams)

	publisher, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln("error when setup Publisher RabbitMQ")
		return err
	}

	flowController := newFlowPublisherController(publisher, flowPipeline, pipeline.Job)
	cronDuration, err := strconv.Atoi(pipeline.Metadata.Interval)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln()
		return err
	}
	graceful.Worker("task For Ingest Airtable Nalar Data and Publish to RabbitMQ", runCronJobPipeliningProcess(ctx, cronDuration, flowController.startPipeliningProcess))
	return nil
}

func runCronJobPipeliningProcess(ctx context.Context, duration int, pipeliningFunc func()) func(wg *sync.WaitGroup) {
	return func(wg *sync.WaitGroup) {
		cron := gocron.NewScheduler()
		err := cron.Every(cast.ToUint64(duration)).Minutes().From(gocron.NextTick()).Do(graceful.TaskWrapper, wg, pipeliningFunc)
		if err != nil {
			logrus.WithContext(ctx).WithError(err).Errorln()
		}
		go func() {
			<-cron.Start()
		}()
	}
}

func runPipeliningRabbitMQToMeilisearch(ctx context.Context, pipeline *prixaDataPipeline) error {
	flowPipeline := initConsumerToRepositoryFlowPipeline(pipeline.FlowParams)

	consumer, err := setRabbitMQConsumerWithMeilisearchRepository(ctx, pipeline)
	if err != nil {
		return err
	}
	consumerController := newFlowConsumerController(consumer, flowPipeline, pipeline.Job)
	consumerController.startPipeliningProcess()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// blocks until receiving KILL and INTERRUPT
	<-c

	return nil
}

func setRabbitMQConsumerWithMeilisearchRepository(ctx context.Context, pipeline *prixaDataPipeline) (messaging.Consumer, error) {
	consumer, err := messaging.NewRabbitMQConsumer()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln("error when setup Consumer RabbitMQ")
		return nil, err
	}
	msClient := sdk.NewMeilisearchClient()
	consumer.SetConsumerRepository(initConsumerMeilisearchRepository(ctx, msClient, pipeline.FlowParams))
	return consumer, nil
}

func initConsumerMeilisearchRepository(ctx context.Context, msClient *meili.Client, flowParams []flowParam) map[string]messaging.ConsumerRepository {
	consumerRepositories := make(map[string]messaging.ConsumerRepository, len(flowParams))
	for _, param := range flowParams {
		consumerRepositories[param.Index] = messaging.ConsumerRepository{
			Index:            param.Index,
			RoutingKey:       param.RoutingKey,
			ConsumerCallback: repository.NewMeilisearchClient(ctx, param.Index, msClient),
		}
	}
	return consumerRepositories
}

func runPipeliningRabbitMQToElasticSearch(ctx context.Context, pipeline *prixaDataPipeline) error {
	flowPipeline := initConsumerToRepositoryFlowPipeline(pipeline.FlowParams)

	consumer, err := setRabbitMQConsumerWithElasticSearchRepository(ctx, pipeline)
	if err != nil {
		return err
	}
	consumerController := newFlowConsumerController(consumer, flowPipeline, pipeline.Job)
	consumerController.startPipeliningProcess()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// blocks until receiving KILL and INTERRUPT
	<-c

	return nil
}

func setRabbitMQConsumerWithElasticSearchRepository(ctx context.Context, pipeline *prixaDataPipeline) (messaging.Consumer, error) {
	consumer, err := messaging.NewRabbitMQConsumer()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln("error when setup Consumer RabbitMQ")
		return nil, err
	}
	esClient, err := sdk.NewElasticSearchClient()
	if err != nil {
		logrus.WithError(err).Errorln("failed to connect to elasticsearch")
		return nil, err
	}
	consumer.SetConsumerRepository(initConsumerElasticSearchRepository(ctx, esClient, pipeline.FlowParams))
	return consumer, nil
}

func initConsumerElasticSearchRepository(ctx context.Context, esClient *elastic.Client, flowParams []flowParam) map[string]messaging.ConsumerRepository {
	consumerRepositories := make(map[string]messaging.ConsumerRepository, len(flowParams))
	for _, param := range flowParams {
		consumerRepositories[param.Index] = messaging.ConsumerRepository{
			Index:            param.Index,
			RoutingKey:       param.RoutingKey,
			ConsumerCallback: repository.NewElasticSearchClient(ctx, param.Index, esClient),
		}
	}
	return consumerRepositories
}

func runRemovedOutdatedDataInElasticSearch(ctx context.Context, pipeline *prixaDataPipeline) error {
	handler, err := getHandlerToRemoveOutdatedDataInElasticSearch(ctx, pipeline)
	if err != nil {
		return err
	}
	cronDuration, err := strconv.Atoi(pipeline.Metadata.Interval)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln()
		return err
	}
	graceful.Worker("task For Remove outdated data in elasticsearch", runCronJobPipeliningProcess(ctx, cronDuration, handler))

	return nil
}

func getHandlerToRemoveOutdatedDataInElasticSearch(ctx context.Context, pipeline *prixaDataPipeline) (func(), error) {
	esClient, err := sdk.NewElasticSearchClient()
	if err != nil {
		logrus.WithError(err).Errorln("failed to connect to elasticsearch")
		return nil, err
	}

	return func() {
		thresholdEnv, err := strconv.Atoi(os.Getenv("THRESHOLD_TIME_IN_MINUTES"))
		if err != nil {
			logrus.WithContext(ctx).WithError(err).Errorf("threshold value are not valid int, threshold: %v\n", thresholdEnv)
		}
		thresholdDuration := time.Duration(-1*thresholdEnv) * time.Minute
		thresholdTime := time.Now().Add(thresholdDuration).Format("2006-01-02T15:04:05.000Z")

		for _, param := range pipeline.FlowParams {
			logrus.Printf("Start Remove outdated data in elasticsearch index %v that have lastUpdated less than %v", param.Index, thresholdTime)
			query := elastic.NewRangeQuery("lastUpdated").Lt(thresholdTime)
			resp, err := esClient.DeleteByQuery().Index(strings.ToLower(param.Index)).Query(query).Do(ctx)
			if err != nil {
				logrus.WithContext(ctx).WithError(err).Errorln("failed to delete data from elasticsearch")
				continue
			}
			logrus.Printf("Finished Remove %v outdated data in elasticsearch index %v", resp.Deleted, param.Index)
		}
	}, nil
}

func runPipeliningRabbitMQToPostgres(ctx context.Context, pipeline *prixaDataPipeline) error {
	flowPipeline := initConsumerToRepositoryFlowPipeline(pipeline.FlowParams)

	consumer, err := setRabbitMQConsumerWithPostgresRepository(ctx, pipeline)
	if err != nil {
		return err
	}
	consumerController := newFlowConsumerController(consumer, flowPipeline, pipeline.Job)
	consumerController.startPipeliningProcess()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// blocks until receiving KILL and INTERRUPT
	<-c

	return nil
}

func setRabbitMQConsumerWithPostgresRepository(ctx context.Context, pipeline *prixaDataPipeline) (messaging.Consumer, error) {
	consumer, err := messaging.NewRabbitMQConsumer()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln("error when setup Consumer RabbitMQ")
		return nil, err
	}
	pg, err := sdk.SetupPostgreClient()
	if err != nil {
		logrus.WithError(err).Errorln("failed to connect to elasticsearch")
		return nil, err
	}
	consumer.SetConsumerRepository(initConsumerPostgresRepository(pg, pipeline.FlowParams))
	return consumer, nil
}

func initConsumerPostgresRepository(pg *sql.DB, flowParams []flowParam) map[string]messaging.ConsumerRepository {
	consumerRepositories := make(map[string]messaging.ConsumerRepository, len(flowParams))
	for _, param := range flowParams {
		consumerRepositories[param.Table] = messaging.ConsumerRepository{
			Index:            param.Table,
			RoutingKey:       param.RoutingKey,
			ConsumerCallback: repository.NewPostgresRepositoryQuery(pg, param.Table, mapToPostgresRepositoryColumnData(param.ColumnsMap)),
		}
	}
	return consumerRepositories
}

func mapToPostgresRepositoryColumnData(columns []flowParamColumn) []repository.ColumnData {
	columnsRepo := make([]repository.ColumnData, 0, len(columns))
	for _, column := range columns {
		columnsRepo = append(columnsRepo, repository.ColumnData{
			Name:     column.Name,
			JSONPath: column.JSONPath,
			Type:     column.Type,
		})
	}
	return columnsRepo
}

func runPipeliningHTTPRequestToRabbitMQ(ctx context.Context, pipeline *prixaDataPipeline) error {
	flowPipeline := initHTTPRequestToRabbitMQFlowPipleline(pipeline.FlowRequestParam)

	publisher, err := messaging.NewRabbitMQPublisher()
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln("error when setup Publisher RabbitMQ")
		return err
	}

	flowController := newFlowPublisherController(publisher, flowPipeline, pipeline.Job)
	cronDuration, err := strconv.Atoi(pipeline.Metadata.Interval)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln()
		return err
	}
	graceful.Worker("task For Ingest HTTP Request API and Publish to RabbitMQ", runCronJobPipeliningProcess(ctx, cronDuration, flowController.startPipeliningProcess))
	return nil
}

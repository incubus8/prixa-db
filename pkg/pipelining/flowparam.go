package pipelining

// flowParam is parameter that needed in the pipelining process
type flowParam struct {
	RoutingKey string            `yaml:"routingKey"`
	Index      string            `yaml:"index"`
	Table      string            `yaml:"table"`
	ColumnsMap []flowParamColumn `yaml:"columnsMap"`
	Query      string            `yaml:"query"`
}

type flowParamColumn struct {
	Name     string `yaml:"name"`
	JSONPath string `yaml:"jsonpath"`
	Type     string `yaml:"type"`
}

type flowRequestAPIParam struct {
	RoutingKey string                 `yaml:"routingKey"`
	Request    httpRequestParam       `yaml:"request"`
	PayloadMap map[string]interface{} `yaml:"payloadMap"`
}

type httpRequestParam struct {
	URL               string            `yaml:"url"`
	Method            string            `yaml:"method"`
	Headers           map[string]string `yaml:"headers"`
	TransformJSONPath string            `yaml:"transformJSONPath"`
}

package pipelining

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/PaesslerAG/jsonpath"
	"github.com/mitchellh/mapstructure"
	"github.com/qntfy/kazaam"
	"github.com/sirupsen/logrus"
)

type validValueFunc func(defaultValue string, valueType string) (interface{}, error)

var validValueFuncMaps = map[string]validValueFunc{
	"array string":   getValidValueForArrayType,
	"array int":      getValidValueForArrayType,
	"array integer":  getValidValueForArrayType,
	"array float":    getValidValueForArrayType,
	"array double":   getValidValueForArrayType,
	"array bool":     getValidValueForArrayType,
	"array boolean":  getValidValueForArrayType,
	"array date":     getValidValueForArrayType,
	"array time":     getValidValueForArrayType,
	"array datetime": getValidValueForArrayType,
	"string":         getValidValue,
	"int":            getValidValue,
	"integer":        getValidValue,
	"float":          getValidValue,
	"double":         getValidValue,
	"bool":           getValidValue,
	"boolean":        getValidValue,
	"date":           getValidValue,
	"time":           getValidValue,
	"datetime":       getValidValue,
}

type httpRequestAPITransformData struct {
	data              json.RawMessage
	payloadMap        map[string]interface{}
	transformJSONPath string
}

// TransformData to transfor extractor result data to slice of json
func (r *httpRequestAPITransformData) TransformData() ([]json.RawMessage, error) {
	payloadSource, err := getPayloadSource(r.data, r.transformJSONPath)
	if err != nil {
		return nil, err
	}
	var payloads []json.RawMessage
	if r.payloadMap == nil {
		payload, err := json.Marshal(payloadSource)
		if err != nil {
			logrus.WithError(err).Errorln("failed to marshal payload data")
			return nil, err
		}
		payloads = append(payloads, payload)
	} else {
		rt := reflect.TypeOf(payloadSource)
		switch rt.Kind() {
		case reflect.Slice,
			reflect.Array:
			var records []map[string]interface{}
			err := mapstructure.Decode(payloadSource, &records)
			if err != nil {
				err = errors.New("failed to convert payload source to slice of map")
				logrus.WithError(err).Errorln()
				return nil, err
			}
			for _, record := range records {
				payload, err := getPayloadData(record, r.payloadMap)
				if err == nil {
					payloads = append(payloads, payload)
				}
			}
		case reflect.Map:
			record, ok := payloadSource.(map[string]interface{})
			if !ok {
				err = errors.New("failed to convert payload source to map")
				logrus.WithError(err).Errorln()
			}
			payload, err := getPayloadData(record, r.payloadMap)
			if err == nil {
				payloads = append(payloads, payload)
			}
		default:
			return nil, errors.New("can't form payload from payloadMap specification")
		}
	}

	return payloads, err
}

func getPayloadSource(data json.RawMessage, transformJSONPath string) (interface{}, error) {
	unmarshalData := interface{}(nil)
	err := json.Unmarshal(data, &unmarshalData)
	if err != nil {
		logrus.WithError(err).Errorln("failed to unmarshal payload response data")
		return nil, err
	}

	if transformJSONPath != "" {
		unmarshalData, err = jsonpath.Get(transformJSONPath, unmarshalData)
		if err != nil {
			logrus.WithError(err).Errorln("Cannot get value in payload source for JSON path " + transformJSONPath)
			return nil, err
		}
	}
	return unmarshalData, nil
}

func getPayloadData(record map[string]interface{}, payloadMap map[string]interface{}) (json.RawMessage, error) {
	marshalRec, err := json.Marshal(record)
	if err != nil {
		logrus.WithField("Record", record).WithError(errors.New("failed to marshal payload data")).Errorln()
		return nil, err
	}

	joltSpec, err := getJoltSpec(payloadMap)
	if err != nil {
		logrus.WithField("payloadMap", payloadMap).WithError(err).Errorln()
		return nil, err
	}

	payload, err := getTransformPayloadData(marshalRec, joltSpec)
	if err != nil {
		logrus.WithField("record", record).WithField("joltSpec", joltSpec).WithError(err).Errorln()
		return nil, err
	}
	return payload, nil
}

func getJoltSpec(payloadMap map[string]interface{}) (string, error) {
	shiftSpecMap := make(map[string]interface{})
	shiftSpecMap["operation"] = "shift"
	shiftSpec := make(map[string]interface{})

	defaultSpecMap := make(map[string]interface{})
	defaultSpecMap["operation"] = "default"
	defaultSpec := make(map[string]interface{})

	for pathKey, specValue := range payloadMap {
		pathParam, ok := specValue.(map[string]interface{})
		if !ok {
			logrus.Debugf("can't convert spec payload %v to map", specValue)
			continue
		}
		path, ok := pathParam["transformPath"]
		if ok {
			shiftSpec[pathKey] = path
			continue
		}

		defaultValue, haveDefaultValue := pathParam["defaultValue"].(string)
		if haveDefaultValue {
			valueType, haveType := pathParam["type"].(string)
			if !haveType || valueType == "" {
				logrus.WithFields(logrus.Fields{
					"haveType":         haveType,
					"valueType":        valueType,
					"defaultValue":     defaultValue,
					"haveDefaultValue": haveDefaultValue,
				}).Errorln("default value must have field type")
				continue
			}
			validValue, err := getValidDefaultValue(defaultValue, valueType)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"defaultValue":     defaultValue,
					"haveDefaultValue": haveDefaultValue,
				}).WithError(err).Errorln()
				continue
			}
			defaultSpec[pathKey] = validValue
		}
	}
	var joltSpec []map[string]interface{}
	if len(shiftSpec) > 0 {
		shiftSpecMap["spec"] = shiftSpec
		joltSpec = append(joltSpec, shiftSpecMap)
	}

	if len(defaultSpec) > 0 {
		defaultSpecMap["spec"] = defaultSpec
		joltSpec = append(joltSpec, defaultSpecMap)
	}

	if len(joltSpec) == 0 {
		return "", errors.New("no jolt specification to transform payload source data")
	}

	marshalSpec, err := json.Marshal(joltSpec)
	if err != nil {
		return "", fmt.Errorf("failed to marshal Jolt Spec: %v", err)
	}
	return string(marshalSpec), nil
}

func getTransformPayloadData(record []byte, joltSpec string) ([]byte, error) {
	config, err := kazaam.NewKazaam(joltSpec)
	if err != nil {
		return nil, err
	}
	payload, err := config.Transform(record)
	if err != nil {
		return nil, err
	}
	return payload, nil
}

func getDateTimeValue(defaultValue string) (string, error) {
	if defaultValue == "now()" {
		return time.Now().Format("2006-01-02T15:04:05.000Z"), nil
	}
	now, err := time.Parse("2006-01-02 15:04:00", defaultValue)
	if err == nil {
		return now.Format("2006-01-02T15:04:05.000Z"), nil
	}
	now, err = time.Parse("2006-01-02T15:04:05.000Z", defaultValue)
	if err == nil {
		return now.Format("2006-01-02T15:04:05.000Z"), nil
	}
	return "", fmt.Errorf("%v can't convert to datetime", defaultValue)
}

func getDateValue(defaultValue string) (string, error) {
	if defaultValue == "now()" {
		return time.Now().Format("2006-01-02"), nil
	}

	_, err := time.Parse("2006-01-02", defaultValue)
	if err == nil {
		return defaultValue, nil
	}
	return "", fmt.Errorf("%v can't convert to date", defaultValue)
}

func getTimeValue(defaultValue string) (string, error) {
	if defaultValue == "now()" {
		return time.Now().Format("15:04:00"), nil
	}
	_, err := time.Parse("15:04:00", defaultValue)
	if err == nil {
		return defaultValue, nil
	}
	return "", fmt.Errorf("%v can't convert to time", defaultValue)
}

func getValidValueForArrayType(defaultValues string, valueType string) (interface{}, error) {
	arr := strings.Split(defaultValues, ",")
	validValues := make([]interface{}, len(arr))
	for _, defaultValue := range arr {
		var validValue interface{}
		var err error
		switch strings.ToLower(valueType) {
		case "array string":
			validValue = defaultValue
		case "array int",
			"array integer":
			validValue, err = strconv.Atoi(defaultValue)
		case "array float",
			"array double":
			validValue, err = strconv.ParseFloat(defaultValue, 10)
		case "array bool",
			"array boolean":
			validValue, err = strconv.ParseBool(defaultValue)
		case "array datetime":
			validValue, err = getDateTimeValue(defaultValue)
		case "array date":
			validValue, err = getDateValue(defaultValue)
		case "array time":
			validValue, err = getTimeValue(defaultValue)
		default:
			err = errors.New("invalid type")
		}
		if err != nil {
			logrus.WithField("defaultValue", defaultValue).WithField("valueType", valueType).WithError(err).Warningln()
			continue
		}
		validValues = append(validValues, validValue)
	}
	return validValues, nil
}

func getValidValue(defaultValue string, valueType string) (interface{}, error) {
	switch strings.ToLower(valueType) {
	case "string":
		return defaultValue, nil
	case "int",
		"integer":
		return strconv.Atoi(defaultValue)
	case "float",
		"double":
		return strconv.ParseFloat(defaultValue, 10)
	case "bool",
		"boolean":
		return strconv.ParseBool(defaultValue)
	case "datetime":
		return getDateTimeValue(defaultValue)
	case "date":
		return getDateValue(defaultValue)
	case "time":
		return getTimeValue(defaultValue)
	default:
		return nil, fmt.Errorf("invalid value type %v", valueType)
	}
}

func getValidDefaultValue(defaultValue string, valueType string) (interface{}, error) {
	handler, ok := validValueFuncMaps[valueType]
	if !ok {
		return nil, fmt.Errorf("invalid value type %v", valueType)
	}

	return handler(defaultValue, valueType)
}

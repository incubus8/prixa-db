package grpc

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	labtestGRPC "github.com/prixa-ai/prixa-proto/proto/labtest/v1"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

type labtestAPIHandlerImpl struct {
	labtestGRPC.LabtestServiceServer

	store store.Store
}

func (s *labtestAPIHandlerImpl) GetLabtestCoupon(ctx context.Context, _ *empty.Empty) (*labtestGRPC.GetLabtestCouponResponse, error) {
	incomingMds := metautils.ExtractIncoming(ctx)
	email := incomingMds.Get("email")

	couponCode, err := s.getOrSetCouponCode(email)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, nil)
	}

	return &labtestGRPC.GetLabtestCouponResponse{
		Status:     "success",
		CouponCode: fmt.Sprintf("PRX-%v", couponCode),
	}, nil
}

func (s *labtestAPIHandlerImpl) getOrSetCouponCode(email string) (string, error) {
	couponCode, err := s.store.Labtest().GetCouponCode(email)
	if err != nil {
		couponData, errDB := s.store.Labtest().GetCouponFromDB(email)
		if errDB != nil {
			couponCode = util.RanStrWithLength(10)
			timeNow := time.Now()
			createdAt := timeNow
			expiredAt := timeNow.Add(30 * 24 * time.Hour)
			reqBody, _ := json.Marshal(map[string]string{
				"coupon_code":     fmt.Sprintf("PRX-%v", couponCode),
				"date_created":    createdAt.Format("2006-01-02 15:04:05"),
				"expiration_date": expiredAt.Format("2006-01-02 15:04:05"),
				"email_address":   email,
				"discount":        "20%",
				"api_key":         os.Getenv("TRIASSE_API_KEY"),
			})
			resp, _ := http.Post(os.Getenv("TRIASSE_BASE_URL")+"/api/v1/voucher/", "application/json", bytes.NewBuffer(reqBody))
			if resp.StatusCode != 200 {
				return "", errors.New("internal server error")
			}
			defer resp.Body.Close()

			err := s.store.Labtest().SaveCouponCode(email, couponCode)
			if err != nil {
				return "", err
			}
			s.store.Labtest().SaveCouponToDB(model.CouponData{
				Email:     email,
				Code:      couponCode,
				CreatedAt: createdAt,
				ExpiredAt: expiredAt,
			})
			return couponCode, nil
		}
		if couponData != nil {
			couponCode = couponData.Code
		}
	}
	return couponCode, nil
}

package grpc

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
	"unicode"

	"cloud.google.com/go/storage"
	"github.com/catinello/base62"
	"github.com/crisp-im/go-crisp-api/crisp"
	"github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	stringBase62 "github.com/lytics/base62"
	"github.com/prixa-ai/gosocial"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/gcloud"
	"prixa.ai/prixa-db/pkg/mailconfig"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/services/superadmin"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user"
	"prixa.ai/prixa-db/pkg/user/userprofile"
	"prixa.ai/prixa-db/pkg/util"
)

const (
	sendMailVerification = "email will be send"
)

var (
	gocial = gosocial.NewDispatcher()

	errDiagnosisSessionNotFound        = errors.New("diagnosisSessionID is not found")
	errPasswordConfirmationDidNotMatch = errors.New("password confirmation did not match the password entered")
	errOldPasswordDidNotMatch          = errors.New("entered old password did not match your current password")
	errUnauthorizedAccess              = errors.New("unauthorized access")
	errValidationFailed                = errors.New("validation failed. Make sure the data are in the correct format")
)

// passwordRegex represents password policy
type passwordRegex struct {
	EightOrMore bool
	Number      bool
	Upper       bool
}

// getRegexStatus returns password requirement info
func getRegexStatus(regex passwordRegex) string {
	eightOrMoreStatus := "password must be 8 characters or more"
	numberStatus := "password must be alphanumeric"
	upperStatus := "password must contain an uppercase letter"
	if !regex.EightOrMore {
		return eightOrMoreStatus
	} else if !regex.Number {
		return numberStatus
	} else if !regex.Upper {
		return upperStatus
	} else {
		return ""
	}
}

// pwdRegexApproved determines password correctness
func pwdRegexApproved(pass string) (bool, string) {
	regexStatus := getRegexStatus(verifyPassword(pass))
	if regexStatus != "" {
		return false, regexStatus
	}
	return true, regexStatus
}

// verifyPassword checks password validity
func verifyPassword(s string) passwordRegex {
	var number, upper bool
	for _, c := range s {
		if unicode.IsNumber(c) {
			number = true
		} else if unicode.IsUpper(c) {
			upper = true
		}
	}
	return passwordRegex{
		EightOrMore: len(s) >= 8,
		Number:      number,
		Upper:       upper,
	}
}

// login using method GoogleUserLogin that doesn't need password
func googleLogin(s *userAPIHandlerImpl, email string, personProfile *crisp.PeopleProfile, personData map[string]string) (*userGRPC.LoginResponse, error) {
	var nickname string
	personProfile, personData, err := s.crispData.GoogleUserLogin(email, s.store.CrispUser(), personProfile, personData)
	if err != nil {
		return nil, err
	}
	nickname = *personProfile.Person.Nickname

	logrus.WithFields(logrus.Fields{
		"status":   "Login Successful",
		"email":    email,
		"username": nickname,
	}).Info("Login Info")

	claimsToken := base62.Encode(int(time.Now().Unix()))
	claims := userClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(24*7) * time.Hour).Unix(),
		},
		Name:  nickname,
		Email: email,
		Token: claimsToken,
	}
	token := jwt.NewWithClaims(
		JwtSigningMethod,
		claims,
	)
	signedToken, err := token.SignedString(getJWTSignatureKey())
	if err != nil {
		return nil, err
	}
	saveTokenErr := s.store.User().SaveToken(&userprofile.RedisData{
		Key:   email + ":" + claimsToken,
		Token: claimsToken,
	})
	if saveTokenErr != nil {
		return nil, saveTokenErr
	}

	isEmailVerified, _ := strconv.ParseBool(personData[sdk.DataEmailStatus])

	return &userGRPC.LoginResponse{
		LoginToken: signedToken,
		PersonID:   *personProfile.PeopleID,
		SessionID:  personData[sdk.DataSessionID],
		IsVerified: isEmailVerified,
	}, nil
}

type userAPIHandlerImpl struct {
	userGRPC.UserServiceServer

	bot        *bot.Bot
	core       *core.Core
	crispData  user.CrispData
	store      store.Store
	superAdmin superadmin.Service
}

func (s *userAPIHandlerImpl) OauthGoogle(ctx context.Context, _ *empty.Empty) (*userGRPC.OauthGoogleURL, error) {
	hostname := os.Getenv("FRONTEND_BASE_URL")
	u, err := url.Parse(hostname)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"description": "invalid hostname settings"})
	}
	if strings.Contains(hostname, ".prixa.ai") {
		u.Scheme = "https"
	}
	if u.Scheme == "" {
		u.Scheme = "http"
	}

	googleClientID := os.Getenv("GOOGLE_OAUTH_CLIENT_ID")
	googleClientSecret := os.Getenv("GOOGLE_OAUTH_CLIENT_SECRET")
	if googleClientID == "" {
		return nil, grpcRespondError(ctx, codes.Internal, errors.New("google client id is empty"), logrus.Fields{})
	}
	if googleClientSecret == "" {
		return nil, grpcRespondError(ctx, codes.Internal, errors.New("google client secret is empty"), logrus.Fields{})
	}

	redirectURL := u.String() + "/oauth/google/callback"

	incomingMds := metautils.ExtractIncoming(ctx)
	xPartnerID := incomingMds.Get("x-partner-id")
	xPartnerAppID := incomingMds.Get("x-partner-app-id")
	additionalData := base64.StdEncoding.EncodeToString([]byte(xPartnerID + "|" + xPartnerAppID))

	logrus.WithContext(ctx).
		WithField("GOOGLE_OAUTH_CLIENT_ID", googleClientID).
		WithField("GOOGLE_OAUTH_CLIENT_SECRET", googleClientSecret).
		WithField("REDIRECT_URL", redirectURL).
		WithField("ADDITIONAL_DATA", additionalData).
		Infoln("google oauth")

	authURL, err := gocial.New().
		Driver("google").
		Scopes([]string{}).
		Redirect(
			googleClientID,
			googleClientSecret,
			redirectURL,
			additionalData,
		)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"description": "failed to create auth URL"})
	}

	return &userGRPC.OauthGoogleURL{
		AuthURL: authURL,
	}, nil
}

func (s *userAPIHandlerImpl) CallbackOauthGoogle(ctx context.Context, request *userGRPC.CallbackGoogleRequest) (*userGRPC.LoginResponse, error) {
	newWebsiteID, err := sdk.GetWebsiteIDByPartner(ctx)
	if err == nil {
		errSaveWebsiteID := s.store.CrispUser().SaveUserWebsiteID("email", newWebsiteID)
		if errSaveWebsiteID != nil {
			return nil, grpcRespondError(ctx, codes.Internal, errSaveWebsiteID, logrus.Fields{"request": request})
		}
	}

	state, code := request.GetState(), request.GetCode()
	oauthUser, _, encodedData, err := gocial.Handle(state, code)
	if err != nil {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       "",
			UserName:    "",
			Description: "User failed to google login",
			EventName:   "FailedGoogleLoginEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": err.Error(), "state": state, "code": code},
		})
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{
			"state": state,
			"code":  code,
		})
	}

	email := oauthUser.Email
	name := oauthUser.FullName

	decodedData, err := base64.StdEncoding.DecodeString(encodedData)
	if err != nil {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    name,
			Description: "User failed to google login",
			EventName:   "FailedGoogleLoginEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": err.Error(), "encodedData": encodedData},
		})
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"encodedData": encodedData})
	}
	dataSplitted := strings.Split(string(decodedData), "|")
	xPartnerID, xPartnerAppID := dataSplitted[0], dataSplitted[1]

	// validate partnerID and partnerAppID
	foundPartner, foundPartnerApp, err := s.store.PartnerApp().FindActivePartnerAndPartnerApp(xPartnerID, xPartnerAppID)
	if err != nil {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    name,
			Description: "User failed to google login",
			EventName:   "FailedGoogleLoginEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": err.Error()},
		})
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	// This is needed since we don't get from middleware
	trackCtx := metautils.ExtractIncoming(ctx).
		Add("x-partner-id", xPartnerID).
		Add("x-partner-app-id", xPartnerAppID).
		Add("partnerName", foundPartner.Name).
		Add("partnerAppName", foundPartnerApp.Name).
		ToIncoming(ctx)

	// check if email exist, if exist then login
	personProfile, err := s.crispData.IsEmailExist(email)
	if err == nil {
		personData := s.crispData.GetPersonData(email)
		data, err := googleLogin(s, email, personProfile, personData)
		if err != nil {
			s.store.User().TrackUserActivity(&store.Param{
				Ctx:         trackCtx,
				Email:       email,
				UserName:    name,
				Description: "User failed to google login",
				EventName:   "FailedGoogleLoginEvent",
				EventType:   store.ErrorEvent,
				Metadata:    map[string]string{"error": err.Error()},
			})
			return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
		}

		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         trackCtx,
			Email:       email,
			UserName:    name,
			Description: "Successful Google Login",
			EventName:   "SuccessGoogleLoginEvent",
			EventType:   store.SuccessEvent,
			Metadata:    nil,
		})
		data.WebsiteID = newWebsiteID
		return data, nil
	}

	// register then login
	personProfile, err = s.crispData.RegisterNewContact(email, name, "", "google")
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	logrus.WithFields(logrus.Fields{
		"status":   "Google Register Successful",
		"email":    *personProfile.Email,
		"username": *personProfile.Person.Nickname,
	}).Info("Login Info")

	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataEmailStatus] = strconv.FormatBool(true)
	if err := s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	queryParam := buildQueryParam(ctx)
	responseSendMail := sendToSendGrid(mailconfig.MailPrixaSignupSuccess, sdk.EmailRequest{
		To:   email,
		Name: name,
		URL:  os.Getenv("FRONTEND_BASE_URL") + "/continue" + queryParam,
	})

	if responseSendMail == responseFailed {
		logrus.Warnln("Failed to send email to ", email)
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         trackCtx,
		Email:       email,
		UserName:    name,
		Description: "Successful Google Signup",
		EventName:   "SuccessGoogleSignupEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	data, err := googleLogin(s, email, personProfile, personData)
	if err != nil {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         trackCtx,
			Email:       email,
			UserName:    name,
			Description: "User failed to google login",
			EventName:   "FailedGoogleLoginEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": err.Error()},
		})
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         trackCtx,
		Email:       email,
		UserName:    name,
		Description: "Successful Google Login",
		EventName:   "SuccessGoogleLoginEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})
	data.WebsiteID = newWebsiteID
	return data, nil
}

// Login is
func (s *userAPIHandlerImpl) Login(ctx context.Context, request *userGRPC.LoginRequest) (*userGRPC.LoginResponse, error) {
	email, password := request.AuthData.GetEmail(), request.AuthData.GetPassword()
	newWebsiteID, err := sdk.GetWebsiteIDByPartner(ctx)
	if err == nil {
		errSaveWebsiteID := s.store.CrispUser().SaveUserWebsiteID(email, newWebsiteID)
		if errSaveWebsiteID != nil {
			return nil, grpcRespondError(ctx, codes.Internal, errSaveWebsiteID, logrus.Fields{"request": request})
		}
	}

	var personProfile *crisp.PeopleProfile
	var nickname string
	if s.superAdmin.IsMatchEmail(email) {
		if !s.superAdmin.IsMatchPassword(password) {
			return nil, grpcRespondError(ctx, codes.Unauthenticated, errUnauthorizedAccess, logrus.Fields{"request": request})
		}
		nickname = email
	} else {
		personProfile, err = s.crispData.UserLogin(email, password, s.store.CrispUser())
		if err != nil {
			s.store.User().TrackUserActivity(&store.Param{
				Ctx:         ctx,
				Email:       email,
				UserName:    "",
				Description: "User failed to login",
				EventName:   "FailedGoogleLoginEvent",
				EventType:   store.ErrorEvent,
				Metadata:    map[string]string{"error": err.Error()},
			})
			return nil, grpcRespondError(ctx, codes.Unauthenticated, err, logrus.Fields{"request": request})
		}
		nickname = *personProfile.Person.Nickname
	}

	logrus.WithFields(logrus.Fields{
		"status":   "Login Successful",
		"email":    email,
		"username": nickname,
	}).Info("Login Info")

	claimsToken := base62.Encode(int(time.Now().Unix()))
	claims := userClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(24*7) * time.Hour).Unix(),
		},
		Name:  nickname,
		Email: email,
		Token: claimsToken,
	}
	if s.superAdmin.IsMatchEmail(email) {
		claims.Role = "admin"
	}
	token := jwt.NewWithClaims(
		JwtSigningMethod,
		claims,
	)
	signedToken, err := token.SignedString(getJWTSignatureKey())
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSignTokenFailed, logrus.Fields{"request": request})
	}
	saveTokenErr := s.store.User().SaveToken(&userprofile.RedisData{
		Key:   email + ":" + claimsToken,
		Token: claimsToken,
	})
	if saveTokenErr != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSaveTokenFailed, logrus.Fields{"request": request})
	}
	if s.superAdmin.IsMatchEmail(email) {
		return &userGRPC.LoginResponse{
			LoginToken: signedToken,
			PersonID:   email,
		}, nil
	}

	personData := s.crispData.GetPersonData(email)
	isEmailVerified, _ := strconv.ParseBool(personData[sdk.DataEmailStatus])

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    nickname,
		Description: "Successful Login",
		EventName:   "SuccessLoginEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	return &userGRPC.LoginResponse{
		LoginToken: signedToken,
		PersonID:   *personProfile.PeopleID,
		SessionID:  personData[sdk.DataSessionID],
		IsVerified: isEmailVerified,
		WebsiteID:  newWebsiteID,
	}, nil
}

// Consent is
func (s *userAPIHandlerImpl) Consent(_ context.Context, _ *empty.Empty) (*userGRPC.ConsentResponse, error) {
	return &userGRPC.ConsentResponse{
		Data: s.bot.Messages["userRegisterConsent"],
	}, nil
}

// ResendEmailVerification sends confirmation email upon user login incase the user still has not receive the confirmation email after register process
func (s *userAPIHandlerImpl) ResendEmailVerification(ctx context.Context, _ *empty.Empty) (*userGRPC.ResendEmailVerificationResponse, error) {
	meta := metautils.ExtractIncoming(ctx)

	name, email := meta.Get("name"), meta.Get("email")
	resendEmailToken := stringBase62.StdEncoding.EncodeToString([]byte(email))
	queryParam := buildQueryParam(ctx)
	responseSendMail := sendToSendGrid(mailconfig.MailPrixaSignup, sdk.EmailRequest{
		To:   email,
		Name: name,
		URL:  os.Getenv("FRONTEND_BASE_URL") + "/verification-check/" + resendEmailToken + queryParam,
	})

	if responseSendMail == responseFailed {
		logrus.Warnln("Failed to send 'resend email verification' to ", email)
	}

	return &userGRPC.ResendEmailVerificationResponse{
		Message: "success",
	}, nil
}

func buildQueryParam(ctx context.Context) string {
	incomingMds := metautils.ExtractIncoming(ctx)
	xPartnerID := incomingMds.Get("x-partner-id")
	xPartnerAppID := incomingMds.Get("x-partner-app-id")

	return "?pId=" + xPartnerID + "&appId=" + xPartnerAppID
}

// Register is
func (s *userAPIHandlerImpl) Register(ctx context.Context, request *userGRPC.RegisterRequest) (*userGRPC.RegisterResponse, error) {
	email, password, confirmPassword := request.AuthData.GetEmail(), request.AuthData.GetPassword(), request.AuthData.GetPasswordConfirmation()
	newWebsiteID, err := sdk.GetWebsiteIDByPartner(ctx)
	if err == nil {
		errSaveWebsiteID := s.store.CrispUser().SaveUserWebsiteID(email, newWebsiteID)
		if errSaveWebsiteID != nil {
			return nil, grpcRespondError(ctx, codes.Internal, errSaveWebsiteID, logrus.Fields{"request": request})
		}
	}

	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}
	if password != confirmPassword {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errPasswordConfirmationDidNotMatch, logrus.Fields{"request": request})
	}
	regexStatus, regexMsg := pwdRegexApproved(password)
	if !regexStatus {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New(regexMsg), logrus.Fields{"request": request})
	}

	profileData := request.GetProfileData()
	personProfile, err := s.crispData.RegisterNewContact(email, profileData.GetName(), password, "prixa")
	if err != nil {
		return nil, grpcRespondError(ctx, codes.AlreadyExists, err, logrus.Fields{"request": request})
	}

	logrus.WithFields(logrus.Fields{
		"status":   "Login Successful",
		"email":    *personProfile.Email,
		"username": *personProfile.Person.Nickname,
	}).Info("Login Info")

	token := stringBase62.StdEncoding.EncodeToString([]byte(email))
	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataToken], personData[sdk.DataEmailStatus] = token, strconv.FormatBool(profileData.GetEmailVerified())
	if err = s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	queryParam := buildQueryParam(ctx)
	responseSendMail := sendToSendGrid(mailconfig.MailPrixaSignup, sdk.EmailRequest{
		To:   email,
		Name: profileData.GetName(),
		URL:  os.Getenv("FRONTEND_BASE_URL") + "/verification-check/" + token + queryParam,
	})

	if responseSendMail == responseFailed {
		logrus.Warnln("Failed to send registration email to ", email)
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    profileData.GetName(),
		Description: "Successful signup",
		EventName:   "SuccessSignupEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	logrus.WithFields(logrus.Fields{
		"message":       sendMailVerification,
		"tokenRegister": token,
	}).Info("Register Response")
	return &userGRPC.RegisterResponse{
		Message: sendMailVerification,
	}, nil
}

// VerifyRegister is
func (s *userAPIHandlerImpl) VerifyRegister(ctx context.Context, request *userGRPC.VerifyRegisterRequest) (*userGRPC.VerifyRegisterResponse, error) {
	reqToken := request.GetRegisterToken()
	decodedToken, _ := stringBase62.StdEncoding.DecodeString(reqToken)
	email := string(decodedToken)
	personData := s.crispData.GetPersonData(email)
	if personData[sdk.DataToken] != reqToken {
		return nil, grpcRespondError(ctx, codes.Unauthenticated, errUnauthorizedAccess, logrus.Fields{"request": request})
	}
	personData[sdk.DataEmailStatus] = strconv.FormatBool(true)
	if err := s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	queryParam := buildQueryParam(ctx)
	responseSendMail := sendToSendGrid(mailconfig.MailPrixaSignupSuccess, sdk.EmailRequest{
		To:   email,
		Name: email,
		URL:  os.Getenv("FRONTEND_BASE_URL") + "/continue" + queryParam,
	})

	if responseSendMail == responseFailed {
		logrus.Warnln("Failed to send email to ", email)
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    "",
		Description: "Successful verification email",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	return &userGRPC.VerifyRegisterResponse{
		LoginToken: personData[sdk.DataToken],
		SessionID:  personData[sdk.DataSessionID],
	}, nil
}

// ForgetPassword is
func (s *userAPIHandlerImpl) ForgetPassword(ctx context.Context, request *userGRPC.ForgetPasswordRequest) (*userGRPC.ForgetPasswordResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	email := request.GetEmail()
	token := stringBase62.StdEncoding.EncodeToString([]byte(email))
	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataToken] = token
	if err := s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	queryParam := buildQueryParam(ctx)
	responseSendMail := sendToSendGrid(mailconfig.MailPrixaForgetpwd, sdk.EmailRequest{
		To:  email,
		URL: os.Getenv("FRONTEND_BASE_URL") + "/reset-password/" + token + queryParam,
	})

	if responseSendMail == responseFailed {
		logrus.WithField("email", email).Warnln("Failed to send forget password email to ", email)
	}

	logrus.WithFields(logrus.Fields{
		"message":        sendMailVerification,
		"tokenForgetPwd": token,
	}).Info("Forget Password Response")
	return &userGRPC.ForgetPasswordResponse{
		Message: sendMailVerification,
	}, nil
}

func (s *userAPIHandlerImpl) ForgetPasswordVerif(ctx context.Context, request *userGRPC.ForgetPasswordVerifRequest) (*userGRPC.ForgetPasswordVerifResponse, error) {
	reqToken := request.GetForgetPwdToken()
	decodedToken, _ := stringBase62.StdEncoding.DecodeString(reqToken)
	email := string(decodedToken)
	personData := s.crispData.GetPersonData(email)
	if personData[sdk.DataToken] != reqToken {
		return nil, grpcRespondError(ctx, codes.Unauthenticated, errUnauthorizedAccess, logrus.Fields{"request": request})
	}
	if err := s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	return &userGRPC.ForgetPasswordVerifResponse{
		Message: "success",
	}, nil
}

// UpdatePassword updates password from forget password, requires forget password token
func (s *userAPIHandlerImpl) UpdatePassword(ctx context.Context, request *userGRPC.UpdatePasswordRequest) (*userGRPC.UpdatePasswordResponse, error) {
	forgetPwdToken, password, confirmPassword := request.GetForgetPasswordToken(), request.GetPassword(), request.GetConfirmPassword()
	decodedToken, errDecode := stringBase62.StdEncoding.DecodeString(forgetPwdToken)
	if errDecode != nil {
		return nil, grpcRespondError(ctx, codes.NotFound, errors.New("invalid token"), logrus.Fields{"request": request})
	}
	email := string(decodedToken)
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	personProfile, _, errGetPersonProfile := s.crispData.Client.Website.GetPeopleProfile(s.crispData.WebsiteID, email)
	if errGetPersonProfile != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errGetPersonProfile, logrus.Fields{"request": request})
	}

	if password != confirmPassword {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    *personProfile.Person.Nickname,
			Description: "Failed to reset user password due to mismatch password and confirm password",
			EventName:   "FailedResetPasswordEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": errPasswordConfirmationDidNotMatch.Error()},
		})
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errPasswordConfirmationDidNotMatch, logrus.Fields{"request": request})
	}
	regexStatus, regexMsg := pwdRegexApproved(password)
	if !regexStatus {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    *personProfile.Person.Nickname,
			Description: "Failed to reset user password",
			EventName:   "FailedResetPasswordEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": regexMsg},
		})
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New(regexMsg), logrus.Fields{"request": request})
	}

	claimsToken := base62.Encode(int(time.Now().Unix()))
	claims := userClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(24*7) * time.Hour).Unix(),
		},
		Name:  *personProfile.Person.Nickname,
		Email: email,
		Token: claimsToken,
	}
	token := jwt.NewWithClaims(
		JwtSigningMethod,
		claims,
	)
	signedToken, err := token.SignedString(getJWTSignatureKey())
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSignTokenFailed, logrus.Fields{"request": request})
	}
	saveTokenErr := s.store.User().SaveToken(&userprofile.RedisData{
		Key:   email + ":" + claimsToken,
		Token: claimsToken,
	})
	if saveTokenErr != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSaveTokenFailed, logrus.Fields{"request": request})
	}

	hashedPwd, _ := user.HashPassword(password)
	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataPassword] = hashedPwd
	if err = s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    *personProfile.Person.Nickname,
		Description: "Password reset successfully",
		EventName:   "SuccessResetPasswordEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	// delete crisp user data from crisp if any
	if err := s.store.CrispUser().DeletePersonProfile(email); err != nil {
		logrus.WithError(err).Errorln()
	}

	return &userGRPC.UpdatePasswordResponse{
		Message:    "Successfully change the password",
		LoginToken: signedToken,
		SessionID:  personData[sdk.DataSessionID],
	}, nil
}

// UserInfo gets user information, requires Bearer token
func (s *userAPIHandlerImpl) UserInfo(ctx context.Context, _ *empty.Empty) (*userGRPC.UserInfoResponse, error) {
	meta := metautils.ExtractIncoming(ctx)
	email := meta.Get("email")
	personProfile, _, err := s.crispData.Client.Website.GetPeopleProfile(s.crispData.WebsiteID, email)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"error": err})
	}
	emptyStr := ""
	if personProfile.Person.Address == nil {
		personProfile.Person.Address = &emptyStr
	}
	if personProfile.Person.Phone == nil {
		personProfile.Person.Phone = &emptyStr
	}
	personData := s.crispData.GetPersonData(email)
	isEmailVerified, _ := strconv.ParseBool(personData[sdk.DataEmailStatus])
	return &userGRPC.UserInfoResponse{
		ProfileData: &userGRPC.ProfileData{
			Email:     email,
			Name:      *personProfile.Person.Nickname,
			AvatarURL: personData[sdk.DataAvatarURL],
			Birthdate: personData[sdk.DataBirthdate],
			Address:   *personProfile.Person.Address,
			Phone:     *personProfile.Person.Phone,
		},
		IsVerified: isEmailVerified,
	}, nil
}

// ChangePassword changes user password upon login, requires Bearer token
func (s *userAPIHandlerImpl) ChangePassword(ctx context.Context, req *userGRPC.ChangePasswordRequest) (*userGRPC.ChangePasswordResponse, error) {
	oldPassword, password, confirmPassword := req.GetOldPassword(), req.GetPassword(), req.GetConfirmPassword()
	meta := metautils.ExtractIncoming(ctx)
	email := meta.Get("email")
	personData := s.crispData.GetPersonData(email)
	crispUserData, errGetPersonProfile := s.store.CrispUser().GetPersonProfile(email)
	if errGetPersonProfile != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errGetPersonProfile, logrus.Fields{"request": req})
	}

	errMatchOldPwd := user.MatchPassword(oldPassword, personData[sdk.DataPassword])
	if errMatchOldPwd != nil {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    *crispUserData.User.PersonProfile.Person.Nickname,
			Description: "Change password request failed",
			EventName:   "FailedChangePasswordEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": errMatchOldPwd.Error()},
		})
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errOldPasswordDidNotMatch, logrus.Fields{"request": req})
	}
	if password != confirmPassword {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    *crispUserData.User.PersonProfile.Person.Nickname,
			Description: "Change password request failed",
			EventName:   "FailedChangePasswordEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": errPasswordConfirmationDidNotMatch.Error()},
		})
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errPasswordConfirmationDidNotMatch, logrus.Fields{"request": req})
	}
	if err := req.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": req})
	}
	regexStatus, regexMsg := pwdRegexApproved(password)
	if !regexStatus {
		s.store.User().TrackUserActivity(&store.Param{
			Ctx:         ctx,
			Email:       email,
			UserName:    *crispUserData.User.PersonProfile.Person.Nickname,
			Description: "Change password request failed",
			EventName:   "FailedChangePasswordEvent",
			EventType:   store.ErrorEvent,
			Metadata:    map[string]string{"error": regexMsg},
		})
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New(regexMsg), logrus.Fields{"request": req})
	}

	hashedPwd, _ := user.HashPassword(password)
	personData[sdk.DataPassword] = hashedPwd
	if errSavePersonData := s.crispData.SavePersonData(email, personData); errSavePersonData != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSavePersonData, logrus.Fields{"request": req})
	}

	crispUserData.User.PersonData = personData
	errSavePersonProfile := s.store.CrispUser().SavePersonProfile(crispUserData)
	if errSavePersonProfile != nil {
		logrus.WithError(errSavePersonProfile).Errorln("cannot save profile to Crisp")
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    *crispUserData.User.PersonProfile.Person.Nickname,
		Description: "Password changed successfully",
		EventName:   "SuccessChangePasswordEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	return &userGRPC.ChangePasswordResponse{
		Message: "successfully change the password",
	}, nil
}

// Logout logs user out from current session. Also remove tokens present on cache
func (s *userAPIHandlerImpl) Logout(ctx context.Context, _ *empty.Empty) (*userGRPC.LogoutResponse, error) {
	meta := metautils.ExtractIncoming(ctx)
	email := meta.Get("email")
	if errLogout := s.store.User().DeleteToken(email + ":" + meta.Get("userToken")); errLogout != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errUnauthorizedAccess, logrus.Fields{"error": errLogout})
	}
	if s.superAdmin.IsMatchEmail(email) {
		return &userGRPC.LogoutResponse{
			Message: "successfully logout",
		}, nil
	}

	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataSessionID] = ""
	if errSavePersonData := s.crispData.SavePersonData(email, personData); errSavePersonData != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errSavePersonData, logrus.Fields{"error": errSavePersonData})
	}

	s.store.User().TrackUserActivity(&store.Param{
		Ctx:         ctx,
		Email:       email,
		UserName:    "",
		Description: "User successfully logged out",
		EventName:   "SuccessLoggedOutEvent",
		EventType:   store.SuccessEvent,
		Metadata:    nil,
	})

	return &userGRPC.LogoutResponse{
		Message: "successfully logout",
	}, nil
}

// UpdateUserInfo updates user profile data
func (s *userAPIHandlerImpl) UpdateUserInfo(ctx context.Context, request *userGRPC.UpdateUserInfoRequest) (*userGRPC.UpdateUserInfoResponse, error) {
	meta := metautils.ExtractIncoming(ctx)
	email := meta.Get("email")
	personProfile, _, errGetPersonProfile := s.crispData.Client.Website.GetPeopleProfile(s.crispData.WebsiteID, email)
	if errGetPersonProfile != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errGetPersonProfile, logrus.Fields{"request": request})
	}
	profileData := request.GetProfileData()
	if profileData.Name == "" {
		profileData.Name = *personProfile.Person.Nickname
	}
	emptyStr := ""
	if profileData.Address == "" {
		if personProfile.Person.Address == nil {
			personProfile.Person.Address = &emptyStr
		}
		profileData.Address = *personProfile.Person.Address
	}
	if profileData.Phone == "" {
		if personProfile.Person.Phone == nil {
			personProfile.Person.Phone = &emptyStr
		}
		profileData.Phone = *personProfile.Person.Phone
	}
	_, err := s.crispData.Client.Website.UpdatePeopleProfile(s.crispData.WebsiteID, email, crisp.PeopleProfileUpdateCard{
		Person: &crisp.PeopleProfileCardPerson{
			Nickname: &profileData.Name,
			Address:  &profileData.Address,
			Phone:    &profileData.Phone,
		},
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	personData := s.crispData.GetPersonData(email)
	if profileData.AvatarURL != "" {
		personData[sdk.DataAvatarURL] = profileData.AvatarURL
	}
	if profileData.Birthdate != "" {
		personData[sdk.DataBirthdate] = profileData.Birthdate
	}
	if err = s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	return &userGRPC.UpdateUserInfoResponse{
		Message: "successfully updated user profile",
	}, nil
}

func (s *userAPIHandlerImpl) GetSignedURL(ctx context.Context, _ *empty.Empty) (*userGRPC.GetSignedURLResponse, error) {
	meta := metautils.ExtractIncoming(ctx)
	contentType := meta.Get("grpcgateway-content-type")

	var ext string
	if !strings.Contains(contentType, "image") {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, gcloud.ErrInvalidContentType, nil)
	}
	ext = strings.Split(contentType, "/")[1]
	key := util.GetRandomID() + "." + ext

	url, err := storage.SignedURL(gcloud.UploadableBucket, key, &storage.SignedURLOptions{
		GoogleAccessID: gcloud.SAConfig.Email,
		Method:         http.MethodPut,
		Expires:        gcloud.SignedURLExpirationTime,
		ContentType:    contentType,
		PrivateKey:     gcloud.SAConfig.PrivateKey,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, nil)
	}
	return &userGRPC.GetSignedURLResponse{
		Url:  url,
		Name: "https://storage.cloud.google.com/" + gcloud.UploadableBucket + "/" + key,
	}, nil
}

func (s *userAPIHandlerImpl) SetPrecondition(ctx context.Context, request *userGRPC.PreconditionsList) (*empty.Empty, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errValidationFailed, logrus.Fields{"request": request})
	}

	strPreconds, err := json.Marshal(request.GetPrecondList())
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	s.store.User().SaveUserPreconditions(&store.Param{
		Ctx:       ctx,
		EventName: "SavePreconditionEvent",
		Metadata: map[string]string{
			"preconditions": string(strPreconds),
		},
	})

	return &empty.Empty{}, nil
}

func (s *userAPIHandlerImpl) GetPrecondition(ctx context.Context, _ *empty.Empty) (*userGRPC.PreconditionsList, error) {
	incomingMds := metautils.ExtractIncoming(ctx)
	email := incomingMds.Get("email")
	precondList := s.store.User().GetPreconditionsList(email)

	allPreconditionsDataMap := make(map[string][]db.Profile)
	for _, profileType := range bot.ProfileTypesWithoutUser {
		profiles := s.core.Meta.ProfilesByType(profileType)
		allPreconditionsDataMap[profileType] = profiles
	}
	allPreconds, err := mapPreconditionsData(bot.Action{
		Type:  bot.PreconditionType,
		Value: allPreconditionsDataMap,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, nil)
	}

	return &userGRPC.PreconditionsList{
		PrecondList:      precondList,
		AllPreconditions: allPreconds,
	}, nil
}

// GetAssessmentHistory gets user assessment history
func (s *userAPIHandlerImpl) GetAssessmentHistory(_ context.Context, _ *empty.Empty) (*userGRPC.GetAssessmentHistoryResponse, error) {
	assessmentResult := s.store.User().GetAssessmentResult()
	return assessmentResult, nil
}

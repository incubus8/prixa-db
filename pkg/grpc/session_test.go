package grpc

import (
	"testing"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	"github.com/stretchr/testify/assert"
)

func TestGRPCCreatePrixaSession(t *testing.T) {
	teardown := setup(t, true)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()
	client := diagnosticGRPC.NewDiagnosticServiceClient(conn)
	clpartnerApp := partnerAppGRPC.NewPartnerApplicationServiceClient(conn)
	clpartner := partnerGRPC.NewPartnerServiceClient(conn)
	t.Run("Create Partner", func(t *testing.T) {
		resp, err := clpartner.CreatePartner(ctx, &partnerGRPC.CreatePartnerRequest{Name: "Partner F"})
		if err != nil {
			t.Fatalf("Failed to create partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, "Partner F")
		assert.Equal(t, resp.Data.Status, "ACTIVE")
		assert.Equal(t, resp.Data.AppIds, []string(nil))

		testPartnerID = resp.Data.Id
	})
	t.Run("Test Create PartnerApp", func(t *testing.T) {
		resp, err := clpartnerApp.CreatePartnerApplication(ctx, &partnerAppGRPC.CreatePartnerApplicationRequest{Name: "Partner F.app", PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to create partner app %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.SecretKey)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, "Partner F.app")
		assert.Equal(t, resp.Data.Status, "ACTIVE")

		testPartnerAppNewName = "Partner FT.app"
		testPartnerAppID = resp.Data.Id
	})
	t.Run("Create New Session with Request Data", func(t *testing.T) {
		resp, err := client.CreatePrixaSession(ctx, &diagnosticGRPC.CreatePrixaSessionRequest{
			PartnerId:    testPartnerID,
			PartnerAppId: testPartnerAppID,
			UserData: &diagnosticGRPC.UserData{
				Id:        "userID001",
				Fullname:  "userTest001",
				Gender:    "gender",
				Birthdate: "xx-xx-xxxx",
				Height:    170,
				Weight:    70,
				Insurance: &diagnosticGRPC.InsuranceStatus{
					HaveInsurance:       true,
					PlanToHaveInsurance: true,
				},
			},
		})
		if err != nil {
			t.Fatalf("Failed create new session %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.NotEmpty(t, resp.SessionId)
		assert.Equal(t, resp.UserData.Id, "userID001")
		assert.Equal(t, resp.UserData.Fullname, "userTest001")
		assert.Equal(t, resp.UserData.Gender, "gender")
		assert.Equal(t, resp.UserData.Height, float32(170))
		assert.Equal(t, resp.UserData.Weight, float32(70))
		assert.Equal(t, resp.UserData.Birthdate, "xx-xx-xxxx")
		assert.Equal(t, resp.UserData.Insurance.HaveInsurance, true)
		assert.Equal(t, resp.UserData.Insurance.PlanToHaveInsurance, true)
	})

	t.Run("Create New Session with No Request Data", func(t *testing.T) {
		resp, err := client.CreatePrixaSession(ctx, &diagnosticGRPC.CreatePrixaSessionRequest{})
		if err != nil {
			t.Fatalf("Failed create new session %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.NotEmpty(t, resp.SessionId)
		assert.NotEqual(t, resp.UserData.Id, "userID001")
		assert.NotEqual(t, resp.UserData.Fullname, "userID001")
		assert.NotEqual(t, resp.UserData.Gender, "gender")
		assert.NotEqual(t, resp.UserData.Height, float32(170))
		assert.NotEqual(t, resp.UserData.Weight, float32(70))
		assert.NotEqual(t, resp.UserData.Birthdate, "xx-xx-xxxx")
		assert.Equal(t, resp.UserData.Insurance.HaveInsurance, false)
		assert.Equal(t, resp.UserData.Insurance.PlanToHaveInsurance, false)
	})
}

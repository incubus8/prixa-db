package grpc

import (
	"context"
	"errors"
	"os"
	"strings"
	"time"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"go.opencensus.io/plugin/ocgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"prixa.ai/prixa-db/pkg/store"
)

func getLogrusOpts() (*logrus.Entry, []grpc_logrus.Option) {
	std := logrus.StandardLogger()
	logrusEntry := logrus.NewEntry(std)
	opts := []grpc_logrus.Option{
		grpc_logrus.WithDurationField(func(duration time.Duration) (key string, value interface{}) {
			return "grpc.time_ns", duration.Nanoseconds()
		}),
	}
	grpc_logrus.ReplaceGrpcLogger(logrusEntry)
	return logrusEntry, opts
}

func getRecoveryOpts() []grpc_recovery.Option {
	customFunc := func(p interface{}) (err error) {
		return status.Errorf(codes.Unknown, "panic triggered: %v", p)
	}
	return []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(customFunc),
	}
}

// getDialOpts represents GRPC client options
func getDialOpts(additional ...grpc.DialOption) []grpc.DialOption {
	retryOpts := []grpc_retry.CallOption{
		grpc_retry.WithMax(3),
		grpc_retry.WithBackoff(grpc_retry.BackoffExponential(50 * time.Millisecond)),
		grpc_retry.WithCodes(codes.NotFound, codes.Aborted, codes.Unavailable, codes.ResourceExhausted),
	}

	logrusEntry, logrusOpts := getLogrusOpts()

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithStatsHandler(
			NewStatsWrapper(&ocgrpc.ClientHandler{}),
		),
		grpc.WithChainUnaryInterceptor(
			grpc_retry.UnaryClientInterceptor(retryOpts...),
			grpc_logrus.UnaryClientInterceptor(logrusEntry, logrusOpts...),
		),
		grpc.WithChainStreamInterceptor(
			grpc_retry.StreamClientInterceptor(retryOpts...),
			grpc_logrus.StreamClientInterceptor(logrusEntry, logrusOpts...),
		),
	}
	opts = append(opts, additional...)
	return opts
}

// newGRPCServer gets new server with middleware
func newGRPCServer(store store.Store) *grpc.Server {
	logrusEntry, logrusOpts := getLogrusOpts()
	recoveryOpts := getRecoveryOpts()

	return grpc.NewServer(
		grpc.StatsHandler(
			NewStatsWrapper(&ocgrpc.ServerHandler{}),
		),
		grpc_middleware.WithUnaryServerChain(
			grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.UnaryServerInterceptor(logrusEntry, logrusOpts...),
			grpc_recovery.UnaryServerInterceptor(recoveryOpts...),
			unaryStoreContextInterceptor(store),
			unaryServerAPIKeyInterceptor,
			unaryServerAuthInterceptor(store.User()),
			unaryServerPartnerInterceptor(store),
			unaryServerAncillaryControlInterceptor(store),
		),
		grpc_middleware.WithStreamServerChain(
			grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.StreamServerInterceptor(logrusEntry, logrusOpts...),
			grpc_recovery.StreamServerInterceptor(recoveryOpts...),
		),
	)
}

// customHeaderMatcherMap matches custom header, case sensitive!
var customHeaderMatcherMap = map[string]bool{
	"x-prixa-api-key":  true,
	"x-partner-id":     true,
	"x-partner-app-id": true,

	// Cloudflare Proxied Request Headers
	// https://support.cloudflare.com/hc/en-us/articles/200170986-How-does-Cloudflare-handle-HTTP-Request-headers-
	"cf-ipcountry":      true,
	"cf-connecting-ip":  true,
	"cf-ray":            true,
	"cf-visitor":        true,
	"true-client-ip":    true,
	"cdn-loop":          true,
	"x-forwarded-proto": true,
}

// customHeaderMatcher handles custom header request
func customHeaderMatcher(key string) (string, bool) {
	if ok := customHeaderMatcherMap[strings.ToLower(key)]; ok {
		return key, ok
	}
	return runtime.DefaultHeaderMatcher(key)
}

// customHeaderMatcherOption is a mux option custom handler for custom header
func customHeaderMatcherOption() runtime.ServeMuxOption {
	return runtime.WithIncomingHeaderMatcher(customHeaderMatcher)
}

// getRuntimeServeMuxOptions returns opts
func getRuntimeServeMuxOptions() []runtime.ServeMuxOption {
	return []runtime.ServeMuxOption{customHeaderMatcherOption()}
}

// unaryStoreContextInterceptor injects request context to store
func unaryStoreContextInterceptor(store store.Store) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		incomingMds := metautils.ExtractIncoming(ctx)
		newCtx := incomingMds.
			Add("grpcMethod", info.FullMethod).
			ToIncoming(ctx)

		store.SetContext(newCtx)

		return handler(newCtx, req)
	}
}

// Indicates that this method should only be accessed via GRPC / internal
var grpcOnly = map[string]bool{
	"/grpc.health.v1.Health/Check":                                   true,
	"/prixa.diagnostic.v1.DiagnosticService/GetDiagnosticStatistics": true,
	"/prixa.user.v1.UserService/CallbackOauthGoogle":                 true,
}

var baymaxAPI = map[string]bool{
	"/prixa.baymax.v1.BaymaxService/CreateWidgetMessage":           true,
	"/prixa.baymax.v1.BaymaxService/CreateConversationMessage":     true,
	"/prixa.baymax.v1.BaymaxService/GetActiveTransactionByPatient": true,
	"/prixa.baymax.v1.BaymaxService/GetConversationMessage":        true,
	"/prixa.baymax.v1.BaymaxService/GetWidgetMessages":             true,
}

var (
	errNoPrixaAPIKeyPresent = errors.New("unauthorized access. no prixa api key present")
	errInvalidPrixaAPIKey   = errors.New("invalid prixa api key")
	errMethodNotAllowed     = errors.New("method is not allowed for this partner and application")
)

func unaryServerAPIKeyInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	if grpcOnly[info.FullMethod] {
		return handler(ctx, req)
	}

	if baymaxAPI[info.FullMethod] {
		return handler(ctx, req)
	}

	incomingMds := metautils.ExtractIncoming(ctx)

	xPrixaAPIKey := incomingMds.Get("x-prixa-api-key")
	if xPrixaAPIKey == "" {
		return nil, grpcRespondError(ctx, codes.Unauthenticated, errNoPrixaAPIKeyPresent, nil)
	}

	// Do not forget, it matches with "case sensitive"
	if xPrixaAPIKey != os.Getenv("X_PRIXA_API_KEY") {
		return nil, grpcRespondError(ctx, codes.Unauthenticated, errInvalidPrixaAPIKey, nil)
	}

	return handler(ctx, req)
}

func unaryServerAncillaryControlInterceptor(store store.Store) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if !store.PartnerApp().IsValidAncillary() {
			incomingMds := metautils.ExtractIncoming(ctx)
			return nil, grpcRespondError(ctx, codes.NotFound, errMethodNotAllowed, logrus.Fields{
				"partnerID":    incomingMds.Get("x-partner-id"),
				"partnerAppID": incomingMds.Get("x-partner-app-id"),
			})
		}

		return handler(ctx, req)
	}
}

// needAuthorization defines GRPC method that needs "Authorization" Header
var needAuthorization = map[string]bool{
	"/prixa.user.v1.UserService/UserInfo":                                             true,
	"/prixa.user.v1.UserService/ChangePassword":                                       true,
	"/prixa.user.v1.UserService/ResendEmailVerification":                              true,
	"/prixa.user.v1.UserService/UpdateUserInfo":                                       true,
	"/prixa.user.v1.UserService/Logout":                                               true,
	"/prixa.user.v1.UserService/GetSignedURL":                                         true,
	"/prixa.user.v1.UserService/GetAssessmentHistory":                                 true,
	"/prixa.user.v1.UserService/GetPrecondition":                                      true,
	"/prixa.user.v1.UserService/SetPrecondition":                                      true,
	"/prixa.user.v1.LabtestService/GetLabtestCoupon":                                  true,
	"/prixa.partner.v1.PartnerService/CreatePartner":                                  true,
	"/prixa.partner.v1.PartnerService/UpdatePartner":                                  true,
	"/prixa.partner.v1.PartnerService/DeletePartner":                                  true,
	"/prixa.partner.v1.PartnerService/ListPartners":                                   true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/CreatePartnerApplication": true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/DeletePartnerApplication": true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/ListPartnerApplications":  true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/UpdateAppMetadata":        true,
	"/prixa.telemedicine.v1.TelemedicineService/InitConversation":                     true,
	"/prixa.telemedicine.v1.TelemedicineService/PostAppointmentBookingData":           true,
	"/prixa.telemedicine.v1.TelemedicineService/CancelAppointmentBookingData":         true,
	"/prixa.baymax.v1.BaymaxService/CreateWidgetMessage":                              true,
	"/prixa.baymax.v1.BaymaxService/CreateConversationMessage":                        true,
	"/prixa.baymax.v1.BaymaxService/GetActiveTransactionByPatient":                    true,
	"/prixa.baymax.v1.BaymaxService/GetConversationMessage":                           true,
	"/prixa.baymax.v1.BaymaxService/GetWidgetMessages":                                true,
}

// needAdminRole defines GRPC method that needs "admin" role in JWT token
// and checks on header x-partner-id and x-partner-app-id will be skipped
var needAdminRole = map[string]bool{
	"/prixa.partner.v1.PartnerService/CreatePartner":                                  true,
	"/prixa.partner.v1.PartnerService/UpdatePartner":                                  true,
	"/prixa.partner.v1.PartnerService/DeletePartner":                                  true,
	"/prixa.partner.v1.PartnerService/ListPartners":                                   true,
	"/prixa.user.v1.UserService/Login":                                                true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/CreatePartnerApplication": true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/DeletePartnerApplication": true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/ListPartnerApplications":  true,
	"/prixa.partnerapplication.v1.PartnerApplicationService/UpdateAppMetadata":        true,
}

func unaryServerAuthInterceptor(userStore store.UserStore) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if grpcOnly[info.FullMethod] {
			return handler(ctx, req)
		}

		if !needAuthorization[info.FullMethod] {
			return handler(ctx, req)
		}

		incomingMds := metautils.ExtractIncoming(ctx)

		bearerToken := incomingMds.Get("authorization")
		if bearerToken == "" {
			return nil, grpcRespondError(ctx, codes.Unauthenticated, errors.New("invalid authorization header"), nil)
		}

		reqToken := strings.Replace(bearerToken, "Bearer ", "", 1) // case sensitive!
		if reqToken == "" {
			return nil, grpcRespondError(ctx, codes.Unauthenticated, errors.New("invalid bearer token"), nil)
		}

		claims, authErr := verifyJwtBearer(info.FullMethod, reqToken)
		if authErr != nil {
			return nil, grpcRespondError(ctx, codes.PermissionDenied, errors.New("unauthorized access"), nil)
		}

		if strings.EqualFold(cast.ToString(claims["origin"]), "backoffice") {
			newCtx := incomingMds.
				Add("email", cast.ToString(claims["email"])).
				Add("name", cast.ToString(claims["name"])).
				Add("id", cast.ToString(claims["sub"])).
				ToIncoming(ctx)

			return handler(newCtx, req)
		}
		email := cast.ToString(claims["email"])
		tokenFromClaims := cast.ToString(claims["token"])
		userSavedToken, errGetToken := userStore.GetToken(email + ":" + tokenFromClaims)
		if errGetToken != nil {
			return nil, grpcRespondError(ctx, codes.Internal, errGetToken, nil)
		}

		if tokenFromClaims != userSavedToken {
			return nil, grpcRespondError(ctx, codes.PermissionDenied, errUnauthorizedAccess, nil)
		}

		newCtx := incomingMds.
			Add("email", email).
			Add("userToken", tokenFromClaims).
			Add("name", cast.ToString(claims["name"])).
			ToIncoming(ctx)

		return handler(newCtx, req)
	}
}

var skipPartnerHeaderValidation = map[string]bool{
	"/prixa.partner.v1.PartnerService/GetPartner":                           true,
	"/prixa.partner.v1.PartnerApplicationService/GetPartnerApplication":     true,
	"/prixa.partner.v1.PartnerApplicationService/GetAppMetadata":            true,
	"/prixa.user.v1.UserService/GetAssessmentHistory":                       true,
	"/prixa.user.v1.UserService/GetPrecondition":                            true,
	"/prixa.user.v1.UserService/SetPrecondition":                            true,
	"/prixa.user.v1.LabtestService/GetLabtestCoupon":                        true,
	"/prixa.diagnostic.v1.DiagnosticService/GetDiagnosisResult":             true,
	"/prixa.baymax.v1.BaymaxService/GetActiveTransactionByPatient":          true,
	"/prixa.baymax.v1.BaymaxService/CreateWidgetMessage":                    true,
	"/prixa.baymax.v1.BaymaxService/CreateConversationMessage":              true,
	"/prixa.telemedicine.v1.TelemedicineService/GetAppointmentBookingsData": true,
	"/prixa.baymax.v1.BaymaxService/GetConversationMessage":                 true,
	"/prixa.baymax.v1.BaymaxService/GetWidgetMessages":                      true,
}

func unaryServerPartnerInterceptor(store store.Store) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if grpcOnly[info.FullMethod] {
			return handler(ctx, req)
		}

		if skipPartnerHeaderValidation[info.FullMethod] {
			return handler(ctx, req)
		}

		// Only administrative tasks that won't get partner and partner app validated
		if needAdminRole[info.FullMethod] {
			return handler(ctx, req)
		}

		incomingMds := metautils.ExtractIncoming(ctx)
		xPartnerID := incomingMds.Get("x-partner-id")
		xPartnerAppID := incomingMds.Get("x-partner-app-id")

		foundPartner, foundPartnerApp, err := store.PartnerApp().FindActivePartnerAndPartnerApp(xPartnerID, xPartnerAppID)
		if err != nil {
			return nil, grpcRespondError(ctx, codes.Unauthenticated, err, nil)
		}

		newCtx := incomingMds.
			Add("partnerName", foundPartner.Name).
			Add("partnerAppName", foundPartnerApp.Name).
			ToIncoming(ctx)

		return handler(newCtx, req)
	}
}

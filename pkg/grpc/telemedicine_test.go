package grpc

import (
	"os"
	"testing"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"

	"prixa.ai/prixa-db/pkg/bot"
)

func TestTelemedicineAPI(t *testing.T) {
	teardown := setup(t, true)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	meta, _ := metadata.FromIncomingContext(ctx)
	md := metadata.Join(meta, metadata.MD{
		"x-prixa-api-key": []string{
			os.Getenv("X_PRIXA_API_KEY"),
		},
	})
	ctx = metadata.NewIncomingContext(ctx, md)

	defer conn.Close()

	testEmail := "usertest_account@tf.tf"
	testPassword := "Prixa12345ok"
	testDiagnosticSessionID := ""

	client := telemedicineGRPC.NewTelemedicineServiceClient(conn)
	diagClient := diagnosticGRPC.NewDiagnosticServiceClient(conn)
	userClient := userGRPC.NewUserServiceClient(conn)
	t.Run("Init Conversation", func(t *testing.T) {
		respCreateSession, errCreateSession := diagClient.CreatePrixaSession(ctx, &diagnosticGRPC.CreatePrixaSessionRequest{})
		if errCreateSession != nil {
			t.Fatalf("Failed to create session:  %v", errCreateSession)
		}
		testDiagnosticSessionID = respCreateSession.SessionId
		errSaveToRedis := diagnosticStore.SaveDiagnosisResult(&bot.DiagnosisResData{
			Key:  testDiagnosticSessionID,
			Data: &diagnosticGRPC.DiagnosisResultData{},
		})
		if errSaveToRedis != nil {
			t.Fatalf("Failed to save to redis:  %v", errSaveToRedis)
		}

		respLogin, errLogin := userClient.Login(ctx, &userGRPC.LoginRequest{
			AuthData: &userGRPC.AuthData{
				Email:               testEmail,
				Password:            testPassword,
				DiagnosticSessionID: testDiagnosticSessionID,
			},
		})
		if errLogin != nil {
			t.Fatalf("failed to login: %v", errLogin)
		}

		meta, _ := metadata.FromIncomingContext(ctx)
		md := metadata.Join(meta, metadata.MD{
			"authorization": []string{
				"Bearer " + respLogin.LoginToken,
			},
		})
		newCtx := metadata.NewIncomingContext(ctx, md)

		resp, err := client.InitConversation(newCtx, &telemedicineGRPC.InitConversationRequest{
			DiagnosticSessionID: testDiagnosticSessionID,
		})
		if err != nil {
			t.Fatalf("err found on initiating the conversation: %v", err)
		}

		assert.NotEmpty(t, resp)
		// assert.NotEmpty(t, resp.SessionID)
		assert.Equal(t, resp.WebsiteID, os.Getenv("CRISP_WEBSITE_ID"))
	})
}

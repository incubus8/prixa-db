package grpc

import (
	"errors"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/spf13/cast"
)

var (
	// JwtSigningMethod user jwt signing method
	JwtSigningMethod = jwt.SigningMethodHS256

	errSignTokenFailed   = errors.New("failed to make token")
	errSaveTokenFailed   = errors.New("failed to save token")
	errAdminRoleRequired = errors.New("admin role required")
	errInvalidClaims     = errors.New("invalid claims conversion")
)

const (
	roleAdmin = "admin"
)

// UserClaims holds the data for user claims
type userClaims struct {
	jwt.StandardClaims

	Name  string `json:"name"`
	Email string `json:"email"`
	Token string `json:"token"`
	Role  string `json:"role"`
}

// get JWT Signature Key
func getJWTSignatureKey() []byte {
	return []byte(os.Getenv("JWT_SIGNATURE_KEY"))
}

// verifyJwtBearer is authenticate bearer and get the user data from the jwt
func verifyJwtBearer(rpcName string, signedToken string) (jwt.MapClaims, error) {
	userToken, err := jwt.Parse(signedToken, func(token *jwt.Token) (interface{}, error) {
		if method, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		} else if method != JwtSigningMethod {
			return nil, errors.New("invalid signing method")
		}

		return getJWTSignatureKey(), nil
	})
	if err != nil {
		return nil, err
	}

	userClaims, ok := userToken.Claims.(jwt.MapClaims)
	if !ok || !userToken.Valid {
		return nil, errInvalidClaims
	}

	role := cast.ToString(userClaims["role"])
	if needAdminRole[rpcName] && role != roleAdmin {
		return nil, errAdminRoleRequired
	}

	return userClaims, nil
}

package grpc

import (
	"context"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/partner"
	"prixa.ai/prixa-db/pkg/store"
)

type partnerAppAPIHandlerImpl struct {
	partnerAppGRPC.PartnerApplicationServiceServer

	store store.Store
}

// mapPartnerAppResponseData maps partner apps property to GRPC response data (PartnerAppResponseData)
func mapPartnerAppResponseData(partnerApp *partner.App) *partnerAppGRPC.PartnerAppResponseData {
	return &partnerAppGRPC.PartnerAppResponseData{
		Id:                 partnerApp.ID,
		Name:               partnerApp.Name,
		SecretKey:          partnerApp.SecretKey,
		Status:             partnerApp.Status,
		CreatedAt:          &timestamp.Timestamp{Seconds: cast.ToTime(partnerApp.CreatedAt).Unix()},
		UpdatedAt:          &timestamp.Timestamp{Seconds: cast.ToTime(partnerApp.UpdatedAt).Unix()},
		Theme:              partnerApp.Theme,
		TelemedicineSDKURL: partnerApp.TelemedicineSDKURL,
	}
}

// CreatePartnerApplication saves partner app data
func (p *partnerAppAPIHandlerImpl) CreatePartnerApplication(ctx context.Context, request *partnerAppGRPC.CreatePartnerApplicationRequest) (*partnerAppGRPC.CreatePartnerApplicationResponse, error) {
	requestPartnerID := request.GetPartnerId()

	foundPartner, err := p.store.Partner().FindActivePartnerID(requestPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	req := &partner.App{
		Name:               request.GetName(),
		Theme:              request.GetTheme(),
		TelemedicineSDKURL: request.GetTelemedicineSDKURL(),
	}
	foundPartnerApps, err := p.store.PartnerApp().SavePartnerApp(foundPartner.ID, req)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerAppProperty := mapPartnerAppResponseData(foundPartnerApps)

	return &partnerAppGRPC.CreatePartnerApplicationResponse{Data: partnerAppProperty}, nil
}

// GetPartnerApplication gets partner app detail by matching the given PartnerID
func (p *partnerAppAPIHandlerImpl) GetPartnerApplication(ctx context.Context, request *partnerAppGRPC.GetPartnerApplicationRequest) (*partnerAppGRPC.GetPartnerApplicationResponse, error) {
	requestPartnerID := request.GetPartnerId()
	requestPartnerAppID := request.GetApplicationId()

	foundPartner, foundPartnerApps, err := p.store.PartnerApp().FindActivePartnerAndPartnerApp(requestPartnerID, requestPartnerAppID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request": request,
			"partner": foundPartner,
			"message": "Cannot retrieve partner app",
		})
	}

	partnerAppProperty := mapPartnerAppResponseData(foundPartnerApps)

	ancillary := p.store.PartnerApp().GetAncillaryControl(requestPartnerID, requestPartnerAppID)
	partnerAppProperty.Ancillary = &partnerAppGRPC.AncillaryControl{
		AppointmenBooking: ancillary.AppointmentBooking,
		Article:           ancillary.Article,
		Insurance:         ancillary.Insurance,
		LabTest:           ancillary.LabTest,
		PharmacyDelivery:  ancillary.PharmacyDelivery,
		Telemedicine:      ancillary.Telemedicine,
		InsuranceBanner:   ancillary.InsuranceBanner,
	}

	return &partnerAppGRPC.GetPartnerApplicationResponse{Data: partnerAppProperty}, nil
}

// ListPartnerApplications gets all available partner applications data
func (p *partnerAppAPIHandlerImpl) ListPartnerApplications(ctx context.Context, request *partnerAppGRPC.ListPartnerApplicationsRequest) (*partnerAppGRPC.ListPartnerApplicationsResponse, error) {
	requestPartnerID := request.GetPartnerId()

	foundPartner, err := p.store.Partner().FindActivePartnerID(requestPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerApps := make([]*partner.App, 0, len(foundPartner.AppIds))
	for _, appID := range foundPartner.AppIds {
		// Ignore partner application status
		keyID := p.store.PartnerApp().MakeKeyWithPartnerID(foundPartner.ID, appID)
		pa, err := p.store.PartnerApp().FindPartnerAppByID(keyID)
		if err != nil {
			logrus.WithError(err).WithFields(logrus.Fields{
				"request":   request,
				"partnerId": foundPartner.ID,
				"appID":     appID,
				"keyID":     keyID,
				"message":   "Cannot retrieve partner app",
			}).Warnln()
			continue
		}

		partnerApps = append(partnerApps, pa)
	}

	partnerAppProperties := make([]*partnerAppGRPC.PartnerAppResponseData, 0, len(partnerApps))
	for _, partnerApp := range partnerApps {
		partnerAppProperty := mapPartnerAppResponseData(partnerApp)
		partnerAppProperties = append(partnerAppProperties, partnerAppProperty)
	}

	return &partnerAppGRPC.ListPartnerApplicationsResponse{Data: partnerAppProperties}, nil
}

// DeletePartnerApplication changes selected partner application status from ACTIVE to DELETED
func (p *partnerAppAPIHandlerImpl) DeletePartnerApplication(ctx context.Context, request *partnerAppGRPC.DeletePartnerApplicationRequest) (*partnerAppGRPC.DeletePartnerApplicationResponse, error) {
	requestPartnerID := request.GetPartnerId()
	requestPartnerAppID := request.GetApplicationId()

	foundPartner, err := p.store.Partner().FindActivePartnerID(requestPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerApp, err := p.store.PartnerApp().FindActivePartnerAppID(foundPartner.ID, requestPartnerAppID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerApp.Status = "DELETED"
	partnerApp.DeletedAt = time.Now().String()

	updatedPartnerApp, err := p.store.PartnerApp().UpdatePartnerApp(foundPartner.ID, partnerApp)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request":   request,
			"partnerId": foundPartner.ID,
			"appId":     requestPartnerAppID,
			"message":   "Cannot retrieve partner app",
		})
	}

	partnerAppProperty := mapPartnerAppResponseData(updatedPartnerApp)

	return &partnerAppGRPC.DeletePartnerApplicationResponse{Data: partnerAppProperty}, nil
}

// UpdatePartnerApplication updates field's name from partner application to given new name on the request
func (p *partnerAppAPIHandlerImpl) UpdatePartnerApplication(ctx context.Context, request *partnerAppGRPC.UpdatePartnerApplicationRequest) (*partnerAppGRPC.UpdatePartnerApplicationResponse, error) {
	requestPartnerID := request.GetPartnerId()
	requestPartnerAppID := request.GetApplicationId()
	requestPartnerAppName := request.GetName()
	requestTheme := request.GetTheme()

	foundPartner, partnerApp, err := p.store.PartnerApp().FindActivePartnerAndPartnerApp(requestPartnerID, requestPartnerAppID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	// Update name
	partnerApp.Name = requestPartnerAppName
	partnerApp.Theme = requestTheme
	partnerApp.TelemedicineSDKURL = request.GetTelemedicineSDKURL()

	updatedPartnerApp, err := p.store.PartnerApp().UpdatePartnerApp(foundPartner.ID, partnerApp)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request":   request,
			"partnerId": foundPartner.ID,
			"appId":     requestPartnerAppID,
			"message":   "Cannot retrieve partner app",
		})
	}

	partnerAppProperty := mapPartnerAppResponseData(updatedPartnerApp)

	return &partnerAppGRPC.UpdatePartnerApplicationResponse{Data: partnerAppProperty}, nil
}

// UpdateAppMetadata updates application metadata
func (p *partnerAppAPIHandlerImpl) UpdateAppMetadata(ctx context.Context, request *partnerAppGRPC.UpdateAppMetadataRequest) (*partnerAppGRPC.UpdateAppMetadataResponse, error) {
	err := p.store.PartnerApp().SaveAppMetadata(partner.AppData{
		PartnerID: request.GetPartnerId(),
		AppID:     request.GetApplicationId(),
		Metadata:  request.GetMetadata(),
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &partnerAppGRPC.UpdateAppMetadataResponse{
		Message: "success",
	}, nil
}

// GetAppMetadata gets application metadata
func (p *partnerAppAPIHandlerImpl) GetAppMetadata(ctx context.Context, request *partnerAppGRPC.GetAppMetadataRequest) (*partnerAppGRPC.GetAppMetadataResponse, error) {
	key := p.store.PartnerApp().MakeKeyWithPartnerID(request.GetPartnerId(), request.GetApplicationId()) + ":metadata"
	appData, err := p.store.PartnerApp().GetAppMetadata(key)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &partnerAppGRPC.GetAppMetadataResponse{
		Metadata: appData.Metadata,
	}, nil
}

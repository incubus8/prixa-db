package grpc

import (
	"testing"

	"github.com/golang/protobuf/ptypes/empty"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/proto"
)

func TestDiagnosticAPI(t *testing.T) {
	teardown := setup(t, false)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()

	client := diagnosticGRPC.NewDiagnosticServiceClient(conn)
	t.Run("Get Feedback Content", func(t *testing.T) {
		emptyReq := new(empty.Empty)
		resp, err := client.GetFeedbackContent(ctx, emptyReq)
		if err != nil {
			t.Fatalf("Failed to get feedback contents %v", err)
		}

		assert.NotEmpty(t, resp.Question)
		assert.NotEmpty(t, resp.Instruction)
		assert.NotEmpty(t, resp.Choices)
	})

	t.Run("Get Diagnosis Result", func(t *testing.T) {
		testData := &diagnosticGRPC.DiagnosisResultData{
			User: &diagnosticGRPC.UserData{
				Weight: 60,
			},
			UserDetails: &diagnosticGRPC.UserDetailsData{
				AgeYear: 23,
			},
			Profiles: []*diagnosticGRPC.ProfileData{
				{
					Type: "bmi",
					Name: "normal",
				},
			},
			Symptoms: []*diagnosticGRPC.SymptomDesc{
				{
					SymptomName: "sariawan",
					PropNames:   []string{},
					Answer:      "yes",
					Chief:       true,
				},
			},
			Diseases: []*diagnosticGRPC.PotentialDisease{
				{
					Name: "disease x",
				},
			},
		}
		data, err := proto.Marshal(testData)
		if err != nil {
			t.Fatalf("Failed to marshal test data %v", err)
		}
		err = redisMock.Set("diagnostic_diagnosticResult:bda7c86c-1a8f-4d9d-b955-dfd756987742", data, 0).Err()
		if err != nil {
			t.Fatalf("Failed to save test data %v", err)
		}
		resp, err := client.GetDiagnosisResult(ctx, &diagnosticGRPC.GetDiagnosisResultRequest{
			DiagnosticSessionID: "bda7c86c-1a8f-4d9d-b955-dfd756987742",
		})
		if err != nil {
			t.Fatalf("Failed to get diagnosis result data %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.Empty(t, err)
		assert.Equal(t, "disease x", resp.GetDiseases()[0])
		assert.Equal(t, "sariawan", resp.GetSymptom().GetName())
		assert.Equal(t, "normal", resp.GetPatient().GetBmi())
		assert.Equal(t, float32(60), resp.GetPatient().GetWeight())
		assert.Equal(t, int32(23), resp.GetPatient().GetAge())
	})

	t.Run("Get Diagnosis Result Fails Upon Diagnosis Not Found", func(t *testing.T) {
		resp, err := client.GetDiagnosisResult(ctx, &diagnosticGRPC.GetDiagnosisResultRequest{
			DiagnosticSessionID: "test_id",
		})
		if err != nil {
			assert.Empty(t, resp)
			assert.NotEmpty(t, err)
		}
	})
}

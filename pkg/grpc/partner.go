package grpc

import (
	"context"

	"github.com/golang/protobuf/ptypes/timestamp"
	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/partner"
	"prixa.ai/prixa-db/pkg/query"
	"prixa.ai/prixa-db/pkg/store"
)

type partnerAPIHandlerImpl struct {
	partnerGRPC.PartnerServiceServer

	partnerStore store.PartnerStore
}

// mapPartnerResponseData maps partnerStore result to GRPC response data (PartnerResponseData)
func mapPartnerResponseData(partnerData *partner.Partner) *partnerGRPC.PartnerResponseData {
	return &partnerGRPC.PartnerResponseData{
		Id:        partnerData.ID,
		Name:      partnerData.Name,
		AppIds:    partnerData.AppIds,
		Status:    partnerData.Status,
		CreatedAt: &timestamp.Timestamp{Seconds: cast.ToTime(partnerData.CreatedAt).Unix()},
		UpdatedAt: &timestamp.Timestamp{Seconds: cast.ToTime(partnerData.UpdatedAt).Unix()},
	}
}

// mapPageProperty maps page results from listPartners to GRPC response data (PageProperty)
func mapPageProperty(pages *query.PageData) *partnerGRPC.PageProperty {
	pageNo, err := cast.ToInt32E(pages.PageNo)
	if err != nil {
		logrus.Warnln("Conversion from string to int failed", err)
	}
	totalPages, err := cast.ToInt32E(pages.TotalPages)
	if err != nil {
		logrus.Warnln("Conversion from string to int failed", err)
	}
	totalRecords, err := cast.ToInt32E(pages.TotalRecords)
	if err != nil {
		logrus.Warnln("Conversion from string to int failed", err)
	}

	return &partnerGRPC.PageProperty{
		PageNo:       pageNo,
		TotalPages:   totalPages,
		TotalRecords: totalRecords,
	}
}

// CreatePartner is saving partner data
func (p *partnerAPIHandlerImpl) CreatePartner(ctx context.Context, request *partnerGRPC.CreatePartnerRequest) (*partnerGRPC.CreatePartnerResponse, error) {
	req := new(partner.Partner)
	req.Name = request.GetName()

	foundPartners, err := p.partnerStore.SavePartner(req)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerProperty := mapPartnerResponseData(foundPartners)

	return &partnerGRPC.CreatePartnerResponse{Data: partnerProperty}, nil
}

// GetPartner is implementing the GetPartner detail by matching the given PartnerID
func (p *partnerAPIHandlerImpl) GetPartner(ctx context.Context, request *partnerGRPC.GetPartnerRequest) (*partnerGRPC.GetPartnerResponse, error) {
	reqPartnerID := request.GetPartnerId()

	foundPartner, err := p.partnerStore.FindActivePartnerID(reqPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerProperty := mapPartnerResponseData(foundPartner)

	return &partnerGRPC.GetPartnerResponse{Data: partnerProperty}, nil
}

// GetPageNoFromQueryString is a helper utility for deriving page no
func getPageNoFromQueryString(pageNoStr string) int64 {
	if pageNoStr == "" {
		pageNoStr = "0"
	}

	pageNo, err := cast.ToIntE(pageNoStr)
	if err != nil {
		return 0
	}

	if pageNo < 0 {
		pageNo = 0
	}

	if pageNo > 0 {
		pageNo--
	}

	return int64(pageNo)
}

// ListPartners is getting the value of all available partners
func (p *partnerAPIHandlerImpl) ListPartners(ctx context.Context, request *partnerGRPC.ListPartnersRequest) (*partnerGRPC.ListPartnersResponse, error) {
	pageNo := getPageNoFromQueryString(cast.ToString(request.GetPage()))
	foundData, err := p.partnerStore.FindAllPartnersAtPage(pageNo)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerProperties := make([]*partnerGRPC.PartnerResponseData, 0, len(foundData.Partners))
	for _, foundPartner := range foundData.Partners {
		partnerProperty := mapPartnerResponseData(foundPartner)
		partnerProperties = append(partnerProperties, partnerProperty)
	}
	responsePages := mapPageProperty(foundData.Page)

	return &partnerGRPC.ListPartnersResponse{Data: partnerProperties, Page: responsePages}, nil
}

// DeletePartner change the status of selected partner from ACTIVE to DELETED
func (p *partnerAPIHandlerImpl) DeletePartner(ctx context.Context, request *partnerGRPC.DeletePartnerRequest) (*partnerGRPC.DeletePartnerResponse, error) {
	reqPartnerID := request.GetPartnerId()

	foundPartner, err := p.partnerStore.FindActivePartnerID(reqPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	foundPartner.Status = "DELETED"

	updatedPartner, err := p.partnerStore.UpdatePartner(foundPartner)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}
	partnerProperty := mapPartnerResponseData(updatedPartner)

	return &partnerGRPC.DeletePartnerResponse{Data: partnerProperty}, nil
}

// UpdatePartner is updating the name field from partner to given new name on the request
func (p *partnerAPIHandlerImpl) UpdatePartner(ctx context.Context, request *partnerGRPC.UpdatePartnerRequest) (*partnerGRPC.UpdatePartnerResponse, error) {
	reqPartnerID := request.GetPartnerId()
	reqPartnerName := request.GetName()

	foundPartner, err := p.partnerStore.FindActivePartnerID(reqPartnerID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	foundPartner.Name = reqPartnerName

	updatedPartner, err := p.partnerStore.UpdatePartner(foundPartner)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	partnerProperty := mapPartnerResponseData(updatedPartner)

	return &partnerGRPC.UpdatePartnerResponse{Data: partnerProperty}, nil
}

package grpc

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"prixa.ai/prixa-db/pkg/store"
)

type analyticAPIHandlerImpl struct {
	analyticGRPC.AnalyticServiceServer

	store store.Store
}

func (a *analyticAPIHandlerImpl) Track(ctx context.Context, request *analyticGRPC.TrackEvent) (*empty.Empty, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, errValidationFailed, logrus.Fields{"request": request})
	}

	username := request.GetUserId()
	if username == "" {
		username = request.GetAnonymousId()
	}
	a.store.Analytic().TrackEvent(&store.Param{
		Ctx:       ctx,
		UserName:  username,
		EventType: store.SuccessEvent,
		EventName: request.GetEvent(),
		SentAt:    request.GetSentAt(),
		Context:   request.GetContext(),
		Metadata:  request.GetProperties(),
	})

	return &empty.Empty{}, nil
}

package grpc

import (
	"prixa.ai/prixa-db/pkg/user"
)

// ErrID translates error to Bahasa Indonesia
var errID = map[error]string{
	user.ErrEmailOrPasswordDidNotMatch: "Email atau password salah",
	user.ErrLoginSource:                "Tidak dapat login menggunakan metode ini",
	user.ErrEmailAlreadyRegistered:     "Email tersebut sudah pernah digunakan untuk mendaftar",
	user.ErrFailedToRegisterEmail:      "Gagal mendaftar",

	errDiagnosisSessionNotFound:        "Tidak dapat menemukan diagnosis",
	errPasswordConfirmationDidNotMatch: "Konfirmasi Password tidak sama dengan Password yang dimasukkan",
	errOldPasswordDidNotMatch:          "Password lama tidak sama dengan Password sekarang",
	errUnauthorizedAccess:              "Akses terlarang",
	errValidationFailed:                "Gagal validasi. Pastikan data memiliki format yang benar",
}

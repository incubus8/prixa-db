package grpc

import (
	"context"
	"database/sql"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"contrib.go.opencensus.io/exporter/stackdriver/propagation"
	"github.com/getsentry/sentry-go"
	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-redis/redis/v7"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	meili "github.com/meilisearch/meilisearch-go"
	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	baymaxGRPC "github.com/prixa-ai/prixa-proto/proto/baymax/v1"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	labtestGRPC "github.com/prixa-ai/prixa-proto/proto/labtest/v1"
	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/plugin/ocgrpc"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/stats/view"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/messaging"
	siloamSDK "prixa.ai/prixa-db/pkg/sdk/siloam"
	"prixa.ai/prixa-db/pkg/services/superadmin"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/store/ancillarylayer"
	"prixa.ai/prixa-db/pkg/store/meilisearchlayer"

	"prixa.ai/prixa-db/pkg/store/mysqllayer"
	"prixa.ai/prixa-db/pkg/store/postgreslayer"
	"prixa.ai/prixa-db/pkg/store/publisher/queuelayer"
	"prixa.ai/prixa-db/pkg/store/redislayer"
	"prixa.ai/prixa-db/pkg/user"
)

const (
	defaultGrpcPort        = "8000"
	defaultGrpcGatewayPort = "8001"
)

func (app *App) getGRPCPort() string {
	portStr := os.Getenv("GRPC_PORT")
	if portStr == "" {
		return defaultGrpcPort
	}
	return portStr
}

func (app *App) getGRPCGatewayPort() string {
	portStr := os.Getenv("GRPC_GATEWAY_PORT")
	if portStr == "" {
		return defaultGrpcGatewayPort
	}
	return portStr
}

// App defines application data and third party connections
type App struct {
	// Ctx describes main context
	Ctx           context.Context
	Bot           *bot.Bot
	Redis         *redis.Client
	Core          *core.Core
	Publisher     messaging.Publisher
	MsClient      *meili.Client
	PostgreClient *sql.DB
	SiloamAPI     siloamSDK.MySiloamAPI
	CrispData     user.CrispData
	MysqlClient   *sql.DB
	BaymaxRedis   *redis.Client

	store      store.Store
	superAdmin superadmin.Service

	grpcSrv      *grpc.Server
	grpcListener net.Listener
}

// OnStart starts GRPC server
func (app *App) OnStart() *App {
	app.initStore()
	app.initSuperAdmin()
	app.registerOC()
	app.startGRPCService()
	app.startGRPCGateway()

	return app
}

// OnShutdown shutdown listener
func (app *App) OnShutdown(callback func()) {
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	<-osSignals

	logrus.Infoln("osSignal Interrupt triggered")

	if callback != nil {
		callback()
	}

	app.grpcSrv.GracefulStop()
	_ = app.grpcListener.Close()

	sentry.Flush(1 * time.Minute)
}

// New Data Store API
func (app *App) initStore() {
	rootStore := store.NewAppStore()
	redisStore := redislayer.NewRedisStore(rootStore, app.Redis, app.BaymaxRedis)
	postgresStore := postgreslayer.NewPostgresStore(redisStore, app.PostgreClient)
	meiliStore := meilisearchlayer.NewMeiliStore(postgresStore, app.MsClient)
	queueStore := queuelayer.NewQueueStore(meiliStore, app.Publisher)
	mysqlStore := mysqllayer.NewMysqlStore(queueStore, app.MysqlClient)
	ancillaryStore := ancillarylayer.NewAncillaryStore(mysqlStore, app.Core.AncillaryControls)
	openTracingStore := store.NewOpenTracingLayer(ancillaryStore)
	app.store = openTracingStore
}

func (app *App) initSuperAdmin() {
	app.superAdmin = superadmin.New(app.store)
	app.superAdmin.Create()
}

func (app App) registerOC() {
	// GRPC OC
	if err := view.Register(ocgrpc.DefaultServerViews...); err != nil {
		logrus.WithError(err).Errorln("Failed to register open census grpc view")
	}

	// HTTP OC
	err := view.Register(
		ochttp.ServerRequestCountView,
		ochttp.ServerRequestBytesView,
		ochttp.ServerResponseBytesView,
		ochttp.ServerLatencyView,
		ochttp.ServerRequestCountByMethod,
		ochttp.ServerResponseCountByStatusCode,
	)
	if err != nil {
		logrus.WithError(err).Errorln("Failed to register open census http view")
	}
}

// startGRPCService initializes GRPC service to run on specified GRPC PORT
func (app *App) startGRPCService() {
	listener, listenErr := net.Listen("tcp", ":"+app.getGRPCPort())
	if listenErr != nil {
		logrus.WithError(listenErr).WithContext(app.Ctx).Fatalln()
	}
	logrus.WithContext(app.Ctx).Infoln("Starting GRPC Server at :", app.getGRPCPort())

	srv := newGRPCServer(app.store)
	app.CrispData.Query = app.store.CrispUser()
	partnerGRPC.RegisterPartnerServiceServer(srv, &partnerAPIHandlerImpl{
		partnerStore: app.store.Partner(),
	})
	partnerAppGRPC.RegisterPartnerApplicationServiceServer(srv, &partnerAppAPIHandlerImpl{
		store: app.store,
	})
	diagnosticGRPC.RegisterDiagnosticServiceServer(srv, &conversationPageHandlerImpl{
		bot:      app.Bot,
		core:     app.Core,
		store:    app.store,
		msClient: app.MsClient,
	})
	userGRPC.RegisterUserServiceServer(srv, &userAPIHandlerImpl{
		bot:        app.Bot,
		core:       app.Core,
		store:      app.store,
		crispData:  app.CrispData,
		superAdmin: app.superAdmin,
	})
	telemedicineGRPC.RegisterTelemedicineServiceServer(srv, &telemedicineAPIHandlerImpl{
		crispData: app.CrispData,
		siloamAPI: app.SiloamAPI,
		store:     app.store,
	})
	analyticGRPC.RegisterAnalyticServiceServer(srv, &analyticAPIHandlerImpl{
		store: app.store,
	})
	labtestGRPC.RegisterLabtestServiceServer(srv, &labtestAPIHandlerImpl{
		store: app.store,
	})
	baymaxGRPC.RegisterBaymaxServiceServer(srv, &baymaxAPIHandlerImpl{
		store: app.store,
	})

	server := health.NewServer()
	grpc_health_v1.RegisterHealthServer(srv, server)
	server.SetServingStatus("prixa-db", grpc_health_v1.HealthCheckResponse_SERVING)

	reflection.Register(srv)

	go func() {
		if err := srv.Serve(listener); err != nil {
			logrus.WithError(err).WithContext(app.Ctx).Fatalln()
		}
	}()

	app.grpcSrv = srv
	app.grpcListener = listener
}

// startGRPCGateway initializes GRPC Gateway
func (app *App) startGRPCGateway() {
	ctx, cancel := context.WithCancel(app.Ctx)
	defer cancel()

	conn, err := dialer["tcp"](ctx, ":"+app.getGRPCPort())
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln()
	}
	go func() {
		<-ctx.Done()
		if err := conn.Close(); err != nil {
			logrus.WithError(err).WithContext(ctx).Errorf("Failed to close a client connection to the gRPC server: %v", err)
		}
	}()

	opts := getRuntimeServeMuxOptions()
	gwmux, err := newGateway(ctx, conn, opts)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Fatalln()
	}

	sentryHandler := sentryhttp.New(sentryhttp.Options{Repanic: true, WaitForDelivery: true})

	mux := http.NewServeMux()
	mux.Handle("/", sentryHandler.Handle(gwmux))
	mux.HandleFunc("/_health", healthzServer(conn))

	// SkipedSampleAPIs skip tracing sample data for these apis
	skipedSampleAPIs := map[string]bool{
		"/_health": true,
	}

	ochttpHandler := &ochttp.Handler{
		Handler:          mux,
		IsPublicEndpoint: true,
		Propagation:      &propagation.HTTPFormat{},
		IsHealthEndpoint: func(r *http.Request) bool {
			return skipedSampleAPIs[r.URL.Path]
		},
	}

	s := &http.Server{
		Addr:         ":" + app.getGRPCGatewayPort(),
		Handler:      allowCORS(ochttpHandler),
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
	go func() {
		<-ctx.Done()
		logrus.WithContext(ctx).Infof("Shutting down the http server")
		if err := s.Shutdown(context.Background()); err != nil {
			logrus.WithError(err).WithContext(ctx).Errorf("Failed to shutdown http server: %v", err)
		}
	}()

	logrus.WithContext(ctx).Infoln("Starting GRPC Gateway server at :", app.getGRPCGatewayPort())
	logrus.WithContext(ctx).Infoln("Using OPD Setting: ", os.Getenv("ENABLE_COVID19_OPD"))
	logrus.WithContext(ctx).Infoln("Is OPD Setting: ", core.IsForCovid19())

	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		logrus.WithError(err).WithContext(ctx).Errorf("Failed to listen and serve: %v", err)
	}
}

// newGateway registers routes to gateway
func newGateway(ctx context.Context, conn *grpc.ClientConn, opts []runtime.ServeMuxOption) (http.Handler, error) {
	handlers := []func(context.Context, *runtime.ServeMux, *grpc.ClientConn) error{
		partnerGRPC.RegisterPartnerServiceHandler,
		partnerAppGRPC.RegisterPartnerApplicationServiceHandler,
		diagnosticGRPC.RegisterDiagnosticServiceHandler,
		userGRPC.RegisterUserServiceHandler,
		analyticGRPC.RegisterAnalyticServiceHandler,
		telemedicineGRPC.RegisterTelemedicineServiceHandler,
		labtestGRPC.RegisterLabtestServiceHandler,
		baymaxGRPC.RegisterBaymaxServiceHandler,
	}

	mux := runtime.NewServeMux(opts...)
	for _, f := range handlers {
		if err := f(ctx, mux, conn); err != nil {
			logrus.WithError(err).WithContext(ctx).Errorln("serve grpc as grpc gateway")
			return nil, err
		}
	}

	return mux, nil
}

var dialer = map[string]func(ctx context.Context, addr string) (*grpc.ClientConn, error){
	"tcp":  dialTCP,
	"unix": dialUnix,
}

// dialTCP creates a client connection via TCP.
// "addr" must be a valid TCP address with a port number.
func dialTCP(ctx context.Context, addr string) (*grpc.ClientConn, error) {
	return grpc.DialContext(ctx, addr, getDialOpts()...)
}

// dialUnix creates a client connection via a unix domain socket.
// "addr" must be a valid path to the socket.
func dialUnix(ctx context.Context, addr string) (*grpc.ClientConn, error) {
	d := func(ctx context.Context, s string) (net.Conn, error) {
		return net.DialTimeout("unix", addr, 30*time.Second)
	}
	return grpc.DialContext(ctx, addr, getDialOpts(grpc.WithContextDialer(d))...)
}

package grpc

import (
	"context"
	"errors"
	"fmt"

	emailGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"

	"github.com/sendgrid/rest"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/mailconfig"
	"prixa.ai/prixa-db/pkg/sdk"
)

// status defines email response
type emailResponseStatus int64

const (
	// success response if request to prixa-notification success
	responseSuccess emailResponseStatus = 1

	// failed response if request to prixa-notification failed
	responseFailed emailResponseStatus = 0
)

var emailResponseMessages = map[emailResponseStatus]string{
	responseSuccess: "Email will be send.",
	responseFailed:  "Failed to send the email. Please try again.",
}

type mailHandler func(sdk.EmailRequest) (*rest.Response, error)

func emailHandlerMapper() map[string]mailHandler {
	notif := sdk.NewNotificationAPI()
	return map[string]mailHandler{
		mailconfig.MailPrixaDiagRes:          notif.SendMail,
		mailconfig.MailPrixaForgetpwd:        notif.SendForgetPasswordMail,
		mailconfig.MailPrixaSignup:           notif.SendRegisterMail,
		mailconfig.MailPrixaSignupSuccess:    notif.SendRegisterSuccessMail,
		mailconfig.MailPrixaForgetpwdSuccess: notif.SendForgetPasswordSuccessMail,
	}
}

// requestGRPCToEmailNotif sends a gRPC request to prixa-notification
func sendToSendGrid(mailID string, emailReq sdk.EmailRequest) emailResponseStatus {
	emailHandler, ok := emailHandlerMapper()[mailID]
	if !ok {
		return responseFailed
	}
	resp, errStatus := emailHandler(emailReq)
	if errStatus != nil {
		logrus.WithError(errStatus).WithField("request", emailReq).Errorln()
		return responseFailed
	}

	logrus.WithFields(logrus.Fields{
		"request":         emailReq,
		"response":        resp.Body,
		"response_status": resp.StatusCode,
	}).Info("Send Email")

	return responseSuccess
}

// SendEmail is requesting to send email to corresponding email to prixa-notification service
func (s *conversationPageHandlerImpl) SendEmail(ctx context.Context, request *emailGRPC.SendEmailRequest) (*emailGRPC.SendEmailResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	requestEmail := request.GetEmail()
	requestSessionID := request.GetSessionId()
	requestUsername := request.GetUsername()

	sessionResult, err := s.store.Diagnostic().GetSessionResult(requestSessionID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("request", request).Errorln()
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request": request,
			"message": "Invalid session ID",
		})
	}

	session := sessionResult.Session
	if session.State != bot.Fin {
		err := errors.New("needs to answer all questions first")
		logrus.WithError(err).WithContext(ctx).WithField("request", request).Errorln("session state is not finished but trying to send email")
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request": request,
			"message": "You need to answer all questions to send email",
		})
	}

	diagnosisResult, err := s.store.DiagnosticResult().GetDiagnosisResult(requestSessionID)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).WithField("request", request).Errorln()
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"request": request,
			"message": "Invalid session ID",
		})
	}

	emailReq := sdk.EmailRequest{
		Name:    requestUsername,
		To:      requestEmail,
		Subject: requestUsername + " hasil Prixa kamu nih!",

		Gender:     session.GetGender(),
		AgeRange:   session.PrintAge(),
		AgeYear:    fmt.Sprintf("%d", diagnosisResult.Data.UserDetails.AgeYear),
		AgeMonth:   fmt.Sprintf("%d", diagnosisResult.Data.UserDetails.AgeMonth),
		HeightInCm: fmt.Sprintf("%.2f", session.GetHeight()),
		WeightInKg: fmt.Sprintf("%.2f", session.GetWeight()),
		BmiIndex:   fmt.Sprintf("%.2f", db.CalculateBMI(session.GetWeight(), session.GetHeight())),

		Preconditions: s.bot.PrintPreconditionNames(s.bot.GetPreconditionsName(session.ProfileTypeMap)),
		MainSymptoms:  s.bot.PrintMainSymptoms(session.Evidences),
		Symptoms:      s.bot.PrintKnownSymptoms(session.Evidences),
		NotKnown:      s.bot.PrintNotKnownSymptoms(session.Evidences),
		Unknown:       s.bot.PrintUnknownSymptoms(session.Evidences),
		PrixaResults:  s.bot.PrintDiseases(diagnosisResult.Data.Diseases),
	}

	responseStatus := sendToSendGrid(mailconfig.MailPrixaDiagRes, emailReq)

	return &emailGRPC.SendEmailResponse{Message: emailResponseMessages[responseStatus]}, nil
}

package grpc

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	empty "github.com/golang/protobuf/ptypes/empty"
	meili "github.com/meilisearch/meilisearch-go"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

// SurveyData holds survey log information
type SurveyData struct {
	UserID        string    `json:"user_id"`
	PartnerID     string    `json:"partner_id"`
	ApplicationID string    `json:"application_id"`
	SessionID     string    `json:"session_id"`
	Rating        int       `json:"rating"`
	Feedback      string    `json:"feedback"`
	Timestamp     time.Time `json:"timestamp"`
}

func mapSurveyDataFromRequest(request *diagnosticGRPC.SendSurveyRequest) SurveyData {
	return SurveyData{
		ApplicationID: request.GetApplicationId(),
		PartnerID:     request.GetPartnerId(),
		SessionID:     request.GetSessionId(),
		UserID:        request.GetUserId(),
		Rating:        int(request.GetRating()),
		Feedback:      util.SanitizeText(request.GetFeedback()),
		Timestamp:     time.Now(),
	}
}

// SendSurvey is
func (s *conversationPageHandlerImpl) SendSurvey(ctx context.Context, request *diagnosticGRPC.SendSurveyRequest) (*empty.Empty, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	survey := mapSurveyDataFromRequest(request)
	s.store.Diagnostic().SaveSurvey(survey)

	return &empty.Empty{}, nil
}

// GetFeedbackContent is getting the content for the feedback page for users
func (s *conversationPageHandlerImpl) GetFeedbackContent(ctx context.Context, emptyReq *empty.Empty) (*diagnosticGRPC.FeedbackContentResponse, error) {
	question := "Apa feedback Anda mengenai pertanyaan ini?"
	instruction := "Anda dapat memilih lebih dari satu feedback. Jika perlu, silahkan tulis feedback anda dengan lebih terperinci pada isian di bawah. Feedback Anda akan membantu Prixa memberikan layanan yang lebih baik lagi."
	choices := FeedbackChoices

	return &diagnosticGRPC.FeedbackContentResponse{
		Question:    question,
		Instruction: instruction,
		Choices:     choices,
	}, nil
}

// FeedbackQuestion struct represent user feedback for question about symptom
type FeedbackQuestion struct {
	UserID        string    `json:"user_id"`
	PartnerID     string    `json:"partner_id"`
	ApplicationID string    `json:"application_id"`
	SessionID     string    `json:"session_id"`
	SymptomID     string    `json:"symptom_id"`
	Question      string    `json:"question"`
	Feedbacks     []string  `json:"feedbacks"`
	TimeCreated   time.Time `json:"timestamp"`
	Detail        string    `json:"detail"`
}

// mapFeedbackData map the feedback data from users to feedback struct
func mapFeedbackData(request *diagnosticGRPC.SendFeedbackRequest) FeedbackQuestion {
	return FeedbackQuestion{
		ApplicationID: request.GetApplicationId(),
		PartnerID:     request.GetPartnerId(),
		SessionID:     request.GetSessionId(),
		SymptomID:     request.GetSymptomId(),
		UserID:        request.GetUserId(),
		Detail:        util.SanitizeText(request.GetDetail()),
		Feedbacks:     request.GetFeedbacks(),
		Question:      request.GetQuestion(),
		TimeCreated:   time.Now(),
	}
}

// SendFeedback is storing the feedback inputs from user
func (s *conversationPageHandlerImpl) SendFeedback(ctx context.Context, request *diagnosticGRPC.SendFeedbackRequest) (*diagnosticGRPC.SendFeedbackResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	feedback := mapFeedbackData(request)

	if len(feedback.Detail) > 500 {
		err := errors.New("uraian Feedback tidak boleh melebihi 500 karakter")
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	if err := validateFeedbackMustExistInChoices(feedback.Feedbacks); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	s.store.Diagnostic().SaveFeedbackQuestion(feedback)

	return &diagnosticGRPC.SendFeedbackResponse{Success: true}, nil
}

// FeedbackChoices represents list of feedback choices for user
var FeedbackChoices = []string{
	"Saya tidak mengerti maksud pertanyaan ini",
	"Tidak ada jawaban yang sesuai dengan kondisi yang saya alami",
	"Saya ingin memilih lebih dari satu jawaban untuk pertanyaan ini",
}

func validateFeedbackMustExistInChoices(feedbacks []string) error {
	if len(feedbacks) == 0 {
		return fmt.Errorf("salah satu dari pilihan feedback harus dipilih")
	}

	for _, feedback := range feedbacks {
		exist := false
		for _, choice := range FeedbackChoices {
			if strings.EqualFold(feedback, choice) {
				exist = true
				break
			}
		}
		if !exist {
			return fmt.Errorf("feedback %v tidak ada dalam pilihan", feedback)
		}
	}
	return nil
}

// GetDiagnosticStatistics
func (s *conversationPageHandlerImpl) GetDiagnosticStatistics(_ context.Context, _ *empty.Empty) (*diagnosticGRPC.GetDiagnosticStatisticsResponse, error) {
	return &diagnosticGRPC.GetDiagnosticStatisticsResponse{
		SymptomsCount:        fmt.Sprintf("%v", s.core.Symptoms.Len()),
		DiseaseCount:         fmt.Sprintf("%v", s.core.Diseases.Len()),
		DifferentialsCount:   fmt.Sprintf("%v", s.core.Differentials.Len()),
		ArticlesCount:        fmt.Sprintf("%v", s.core.Article.Len()),
		ProfilesCount:        fmt.Sprintf("%v", len(s.core.Meta.Profiles)),
		TypesCount:           fmt.Sprintf("%v", len(s.core.Meta.Types)),
		PropertiesCount:      fmt.Sprintf("%v", len(s.core.Meta.Props)),
		CharacteristicsCount: fmt.Sprintf("%v", len(s.core.Meta.Chars)),
	}, nil
}

// SendCovidForm publish covid form requested by user to the nats stream server
func (s *conversationPageHandlerImpl) SendCovidForm(ctx context.Context, request *diagnosticGRPC.SendCovidFormRequest) (*diagnosticGRPC.SendCovidFormResponse, error) {
	err := request.Validate()
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	s.store.Diagnostic().SaveCovid19Form(&store.Param{
		Ctx:       ctx,
		Email:     request.CovidForm.GetEmail(),
		EventName: "Covid19FormSubmissionEvent",
		Metadata: map[string]string{
			"diagnosticSessionID": request.CovidForm.GetDiagnosticSessionID(),
			"name":                request.CovidForm.GetName(),
			"phone":               request.CovidForm.GetPhone(),
			"email":               request.CovidForm.GetEmail(),
			"city":                request.CovidForm.GetCity(),
			"province":            request.CovidForm.GetProvince(),
			"timestamp":           time.Now().Format(time.RFC3339Nano),
		},
	})

	return &diagnosticGRPC.SendCovidFormResponse{
		Message: "success",
	}, nil
}

// GetLocalTransmissionData get all LocalTransmission data available
func (s *conversationPageHandlerImpl) GetLocalTransmissionData(ctx context.Context, _ *empty.Empty) (*diagnosticGRPC.GetLocalTransmissionDataResponse, error) {
	return &diagnosticGRPC.GetLocalTransmissionDataResponse{
		LocalTransmissionData: s.core.LocalTransmission.GetAll(),
	}, nil
}

// GetContentCard gets contentcard by given type from request
func (s *conversationPageHandlerImpl) GetContentCard(ctx context.Context, request *diagnosticGRPC.GetContentCardRequest) (*diagnosticGRPC.GetContentCardResponse, error) {
	return &diagnosticGRPC.GetContentCardResponse{
		ContentCard: s.getContentCardByType(ctx, s.msClient, request.GetType()),
	}, nil
}

// GetAllContentCard gets all contentcard
func (s *conversationPageHandlerImpl) GetAllContentCard(ctx context.Context, _ *empty.Empty) (*diagnosticGRPC.GetContentCardResponse, error) {
	return &diagnosticGRPC.GetContentCardResponse{
		ContentCard: s.getContentCardByType(ctx, s.msClient, "all"),
	}, nil
}

func (s *conversationPageHandlerImpl) getContentCardByType(ctx context.Context, msClient *meili.Client, contentCardType string) (contentCards []*diagnosticGRPC.ContentCard) {
	var searchRequest meili.SearchRequest
	table := "nalar_contentcard"
	if core.IsForCovid19() {
		table = "covid_contentcard"
	}
	if contentCardType == "all" {
		fetchedDocs, err := sdk.GetDocs(msClient, table, 0)
		if err != nil {
			logrus.WithError(err).WithContext(ctx).Errorln()
		}
		for _, doc := range fetchedDocs {
			contentCards = append(contentCards, mapContentCard(cast.ToStringMap(doc)))
		}
		return contentCards
	}
	searchRequest.Query = contentCardType
	searchRequest.Filters = fmt.Sprintf("type = '%v'", contentCardType)
	resp, err := sdk.Search(msClient, table, searchRequest)
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Errorln()
	}

	for _, hit := range resp.Hits {
		contentCards = append(contentCards, mapContentCard(cast.ToStringMap(hit)))
	}
	return contentCards
}

func mapContentCard(data map[string]interface{}) *diagnosticGRPC.ContentCard {
	fields := cast.ToStringMap(data["fields"])
	return &diagnosticGRPC.ContentCard{
		Id:           cast.ToString(data["id"]),
		Title:        cast.ToString(fields["title"]),
		Sort:         cast.ToInt32(fields["sort"]),
		ImageURL:     cast.ToString(fields["imageURL"]),
		Snippet:      cast.ToString(fields["snippet"]),
		ExternalLink: cast.ToString(fields["externalLink"]),
		Type:         cast.ToString(fields["type"]),
	}
}

func (s *conversationPageHandlerImpl) GetDiagnosisResult(ctx context.Context, request *diagnosticGRPC.GetDiagnosisResultRequest) (*diagnosticGRPC.GetDiagnosisResultResponse, error) {
	result, errGetDiagnosisResultData := s.store.DiagnosticResult().GetDiagnosisResult(request.GetDiagnosticSessionID())
	if errGetDiagnosisResultData != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errGetDiagnosisResultData, logrus.Fields{
			"request": request,
			"error":   "invalid diagnostic sessionID",
		})
	}

	diagnosticData := result.Data

	var (
		bmi    string
		gender string
	)
	diseases := []string{}
	preconditionsName := []string{}
	profiles := diagnosticData.GetProfiles()
	for _, profile := range profiles {
		if strings.EqualFold(profile.Type, "bmi") {
			bmi = profile.NameIndo
		} else if strings.EqualFold(profile.Type, "gender") {
			gender = profile.NameIndo
		}
		preconditionsName = append(preconditionsName, profile.NameIndo)
	}
	for _, disease := range diagnosticData.GetDiseases() {
		diseases = append(diseases, disease.GetName())
	}
	chiefComplaint := &diagnosticGRPC.SymptomsData{}
	associatedSymptoms := []*diagnosticGRPC.SymptomsData{}
	for _, symptom := range diagnosticData.GetSymptoms() {
		if strings.EqualFold(symptom.Answer, "yes") {
			if symptom.Chief {
				chiefComplaint.Name = symptom.SymptomName
				chiefComplaint.Properties = symptom.PropNames
			} else {
				associatedSymptom := &diagnosticGRPC.SymptomsData{
					Name:       symptom.SymptomName,
					Properties: symptom.PropNames,
				}
				associatedSymptoms = append(associatedSymptoms, associatedSymptom)
			}
		}
	}
	return &diagnosticGRPC.GetDiagnosisResultResponse{
		Message: "valid diagnostic sessionID",
		Patient: &diagnosticGRPC.PatientData{
			Gender:        gender,
			Age:           diagnosticData.GetUserDetails().GetAgeYear(),
			Weight:        diagnosticData.GetUser().GetWeight(),
			Height:        diagnosticData.GetUser().GetHeight(),
			Bmi:           bmi,
			Preconditions: preconditionsName,
		},
		Diseases:           diseases,
		Symptom:            chiefComplaint,
		AssociatedSymptoms: associatedSymptoms,
	}, nil
}

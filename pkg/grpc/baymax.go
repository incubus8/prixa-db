package grpc

import (
	"context"
	"errors"

	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	baymaxGRPC "github.com/prixa-ai/prixa-proto/proto/baymax/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"
	"prixa.ai/prixa-db/pkg/model/baymax"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/util"
)

type baymaxAPIHandlerImpl struct {
	baymaxGRPC.BaymaxServiceServer

	store store.Store
}

const (
	doctorsGuard       = "doctors-api"
	externalUsersGuard = "externalUsers-api"
)

func (b *baymaxAPIHandlerImpl) CreateConversationMessage(ctx context.Context, request *baymaxGRPC.ConversationMessageRequest) (*baymaxGRPC.ConversationMessageResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	conversationID := request.GetId()

	conversation, transaction, doctor, err := b.store.Baymax().ValidateConversationTransactionDoctor(ctx, conversationID)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	messageID := util.GetRandomID()

	err = b.store.Baymax().SendMessage(ctx, &baymax.MessageData{
		ID:             messageID,
		Content:        request.GetContent(),
		ConversationID: conversation.ID,
		MessageType:    "message",
		Sender:         doctor.ID,
		Private:        0,
		DoctorID:       &doctor.ID,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	return &baymaxGRPC.ConversationMessageResponse{
		Data: &baymaxGRPC.MessageData{
			Id:             messageID,
			ClientId:       conversation.ClientID,
			PatientId:      transaction.PatientID,
			DoctorId:       doctor.ID,
			Content:        request.GetContent(),
			InboxId:        conversation.InboxID,
			Private:        request.GetPrivate(),
			MessageType:    "message",
			ConversationId: conversationID,
		},
	}, nil
}

func (b *baymaxAPIHandlerImpl) CreateWidgetMessage(ctx context.Context, request *baymaxGRPC.WidgetMessageRequest) (*baymaxGRPC.WidgetMessageResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	websiteToken := request.GetWebsiteToken()
	cwConversation := request.GetCwConversation()
	conversation, transaction, err := b.store.Baymax().ParseWebsiteTokenCwConversation(ctx, websiteToken, cwConversation)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	messageID := util.GetRandomID()

	err = b.store.Baymax().SendMessage(ctx, &baymax.MessageData{
		ID:             messageID,
		Content:        request.GetMessage().GetContent(),
		ConversationID: conversation.ID,
		MessageType:    "message",
		Sender:         transaction.PatientID,
		Private:        0,
		PatientID:      &transaction.PatientID,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	return &baymaxGRPC.WidgetMessageResponse{
		Data: &baymaxGRPC.MessageData{
			Id:             messageID,
			ClientId:       conversation.ClientID,
			PatientId:      transaction.PatientID,
			DoctorId:       transaction.DoctorID,
			Content:        request.GetMessage().GetContent(),
			InboxId:        conversation.InboxID,
			MessageType:    "message",
			Private:        false,
			ConversationId: conversation.ID,
		},
	}, nil
}

func (b *baymaxAPIHandlerImpl) GetActiveTransactionByPatient(ctx context.Context, req *baymaxGRPC.GetActiveTransactionByPatientRequest) (*baymaxGRPC.GetActiveTransactionByPatientResponse, error) {
	if err := req.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": req})
	}

	incomingMds := metautils.ExtractIncoming(ctx)
	userID := incomingMds.Get("id")

	patientData, err := b.store.Baymax().GetPatientData(req.PatientId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.NotFound, err, logrus.Fields{"request": req})
	}

	if cast.ToString(patientData.ExternalUserID) != userID {
		return nil, grpcRespondError(ctx, codes.PermissionDenied, errors.New("user is not allowed to access this patient"), logrus.Fields{"request": req})
	}

	activeTransactions, err := b.store.Baymax().GetActiveTransactionByPatient(req.PatientId, req.TrxId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": req})
	}

	activeTransactionsGRPC := make([]*baymaxGRPC.ActiveTransaction, 0)
	for _, trx := range activeTransactions {
		activeTransactionsGRPC = append(activeTransactionsGRPC, trx.MapToGRPCActiveTransactionData())
	}

	return &baymaxGRPC.GetActiveTransactionByPatientResponse{
		Status: "success",
		Data:   activeTransactionsGRPC,
	}, nil
}

func (b *baymaxAPIHandlerImpl) GetConversationMessage(ctx context.Context, req *baymaxGRPC.GetConversationMessagesRequest) (*baymaxGRPC.GetConversationMessagesResponse, error) {
	if err := req.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": req})
	}

	conversation, err := b.store.Baymax().GetConversationData(req.Id)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.NotFound, err, logrus.Fields{"request": req})
	}

	incomingMds := metautils.ExtractIncoming(ctx)
	userID := incomingMds.Get("id")

	hasPermission, err := b.userHasPermissionToViewConversation(userID, conversation)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": req})
	}
	err = b.store.Baymax().SavePermissionViewConversationMessageInCache(&baymax.PermissionViewConversationMessage{
		ConversationID: conversation.ID,
		HasPermission:  hasPermission,
		UserID:         userID,
	})
	if err != nil {
		logrus.Errorln("failed to save permission view conversation message in cache")
	}
	if !hasPermission {
		return nil, grpcRespondError(ctx, codes.PermissionDenied, errors.New("user not have permission"), logrus.Fields{"request": req})
	}

	messages, err := b.store.Baymax().GetConversationMessages(req.Id, req.Before)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": req})
	}

	messagesGRPC := make([]*baymaxGRPC.MessageData, 0)
	for _, msg := range messages {
		messagesGRPC = append(messagesGRPC, msg.MapToGRPCMessageData())
	}

	return &baymaxGRPC.GetConversationMessagesResponse{
		Status: "success",
		Data:   messagesGRPC,
	}, nil
}

func (b *baymaxAPIHandlerImpl) GetWidgetMessages(ctx context.Context, request *baymaxGRPC.GetWidgetMessagesRequest) (*baymaxGRPC.GetWidgetMessagesResponse, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request})
	}

	websiteToken := request.GetWebsiteToken()
	cwConversation := request.GetCwConversation()
	conversation, _, err := b.store.Baymax().ParseWebsiteTokenCwConversation(ctx, websiteToken, cwConversation)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": request})
	}

	messages, err := b.store.Baymax().GetConversationMessages(conversation.ID, request.Before)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	messagesGRPC := make([]*baymaxGRPC.MessageData, 0)
	for _, msg := range messages {
		messagesGRPC = append(messagesGRPC, msg.MapToGRPCMessageData())
	}

	return &baymaxGRPC.GetWidgetMessagesResponse{
		Status: "success",
		Data:   messagesGRPC,
	}, nil
}

func (b *baymaxAPIHandlerImpl) userHasPermissionToViewConversation(userID string, conversation *baymax.Conversation) (bool, error) {
	permission, err := b.store.Baymax().HasPermissionViewConversationMessageInCache(userID, conversation.ID)
	if err == nil {
		return permission.HasPermission, nil
	}

	permissions, err := b.store.Baymax().GetListOfPermission(userID)
	if err != nil {
		return false, err
	}

	p := b.getPermissionByName("view.conversation_message_by_doctor", permissions)
	if p != nil {
		if userID != cast.ToString(conversation.DoctorID) {
			return false, nil
		}
		return true, nil
	}

	p = b.getPermissionByName("view.conversation_message_by_supervisor", permissions)
	if p != nil {
		if p.GuardName != doctorsGuard {
			return false, nil
		}
		exist, err := b.store.Baymax().IsDoctorExistInClient(userID, conversation.ClientID)
		if err != nil {
			return false, err
		}
		if !exist {
			return false, nil
		}

		return true, nil
	}

	p = b.getPermissionByName("view.conversation_message_by_external_user", permissions)
	if p != nil {
		if p.GuardName != externalUsersGuard {
			return false, nil
		}
		exist, err := b.store.Baymax().IsExternalUserExistInClient(userID, conversation.ClientID)
		if err != nil {
			return false, err
		}
		if !exist {
			return false, nil
		}

		return true, nil
	}

	return false, nil
}

func (b *baymaxAPIHandlerImpl) getPermissionByName(permissionName string, permissions []*baymax.Permission) *baymax.Permission {
	for _, p := range permissions {
		if p.PermissionName == permissionName {
			return p
		}
	}
	return nil
}

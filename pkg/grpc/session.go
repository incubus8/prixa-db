package grpc

import (
	"context"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/db"
)

// mapUserData returns db.user data struct from given grpc user data
func mapUserData(userData *diagnosticGRPC.UserData) db.User {
	if userData == nil {
		return db.User{}
	}
	return db.User{
		ID:        userData.Id,
		Fullname:  userData.Fullname,
		Gender:    userData.Gender,
		Birthdate: userData.Birthdate,
		Weight:    float64(userData.Weight),
		Height:    float64(userData.Height),
		Insurance: db.Insurance{
			HaveInsurance:       userData.Insurance.HaveInsurance,
			PlanToHaveInsurance: userData.Insurance.PlanToHaveInsurance,
		},
	}
}

// sessionCreated holds user info and its session ID information
type sessionCreated struct {
	SessionID string `json:"sessionID"`
	db.User   `json:"user"`
}

// map User to gRPC userData
func mapUser(user db.User) *diagnosticGRPC.UserData {
	return &diagnosticGRPC.UserData{
		Id:        user.ID,
		Fullname:  user.Fullname,
		Gender:    user.Gender,
		Birthdate: user.Birthdate,
		Weight:    float32(user.Weight),
		Height:    float32(user.Height),
		Insurance: &diagnosticGRPC.InsuranceStatus{
			HaveInsurance:       user.Insurance.HaveInsurance,
			PlanToHaveInsurance: user.Insurance.PlanToHaveInsurance,
		},
	}
}

// BotConversation handles bot conversation logic that starts when user uses diagnostic engine
func (s *conversationPageHandlerImpl) CreatePrixaSession(ctx context.Context, request *diagnosticGRPC.CreatePrixaSessionRequest) (*diagnosticGRPC.CreatePrixaSessionResponse, error) {
	user := mapUserData(request.UserData)

	var session bot.Session
	emptyRequest := request == &diagnosticGRPC.CreatePrixaSessionRequest{}
	if emptyRequest {
		session = bot.NewSession(ctx)
	} else {
		session = bot.NewSessionWithUser(ctx, user) // new session with user data
	}

	saveErr := s.store.Diagnostic().SaveSessionResult(session.ID, &bot.SessionResult{
		Session: session,
	})
	if saveErr != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, saveErr, logrus.Fields{
			"message": "Save Session has failed",
			"request": request,
			"session": session,
			"sessionCreated": sessionCreated{
				SessionID: session.ID,
				User:      user,
			},
		})
	}

	return &diagnosticGRPC.CreatePrixaSessionResponse{
		SessionId: session.ID,
		UserData:  mapUser(user),
	}, nil
}

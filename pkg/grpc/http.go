package grpc

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	epb "google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/status"

	"prixa.ai/prixa-db/pkg/util"
)

var allowedHeaders = []string{
	// Content-Type is added to fix issue when passing non json value
	"Content-Type",

	// Application Headers
	"Authorization",
	"X-Prixa-API-Key",
	"X-Partner-Id",
	"X-Partner-App-Id",

	// Cloudflare Headers
	"Cf-Ipcountry",
	"CF-Connecting-IP",
	"X-Forwarded-For",
	"X-Forwarded-Proto",
	"Cf-Ray",
	"Cf-Visitor",
	"True-Client-IP",
	"CDN-Loop",
}

var corsOptions = cors.Options{
	AllowedOrigins: []string{"https://*.prixa.ai"},
	AllowedMethods: []string{
		http.MethodGet,
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
	},
	AllowedHeaders:   allowedHeaders,
	AllowCredentials: false,
	Debug:            true,
}

// allowCORS allows Cross Origin Resource Sharing from any sub domain ".prixa.ai" in production
// Set DISABLE_CORS="1" -- To disable CORS (incl. localhost)
func allowCORS(h http.Handler) http.Handler {
	if strings.EqualFold(os.Getenv("DISABLE_CORS"), "1") {
		return cors.AllowAll().Handler(h)
	}

	return cors.New(corsOptions).Handler(h)
}

// healthzServer returns a simple health handler which returns ok.
func healthzServer(conn *grpc.ClientConn) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		if s := conn.GetState(); s != connectivity.Ready {
			http.Error(w, fmt.Sprintf("grpc server is %s", s), http.StatusBadGateway)
			return
		}
		_, _ = fmt.Fprintln(w, "ok")
	}
}

// grpcRespondError is a helper function to return error info
func grpcRespondError(ctx context.Context, code codes.Code, err error, details logrus.Fields) error {
	errInfo := &epb.ErrorInfo{
		Domain:   strings.ToLower(os.Getenv("HOSTNAME")),
		Metadata: util.LogrusFieldsToMapString(details),
	}

	errInfo.Metadata["errInd"] = errID[err]
	if errInfo.Metadata["errInd"] == "" {
		errInfo.Metadata["errInd"] = "Gangguan sistem, mohon coba kembali"
	}

	logrus.WithError(err).WithContext(ctx).WithFields(details).Errorln(errInfo)

	st := status.New(code, err.Error())
	ds, err2 := st.WithDetails(errInfo)
	if err2 != nil {
		return st.Err()
	}
	return ds.Err()
}

package grpc

import (
	"context"
	"net"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v7"
	"github.com/joho/godotenv"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/test/bufconn"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/store/redislayer"
	"prixa.ai/prixa-db/pkg/user"
)

var (
	redisMock *redis.Client
	c         *core.Core
)

const (
	bufSize = 1024 * 1024
)

func setup(t *testing.T, needFetched bool) func(t *testing.T) {
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	redisMock = redis.NewClient(&redis.Options{
		Addr: s.Addr(),
	})
	runGRPCServer(redisMock, needFetched)

	return func(t *testing.T) {
		defer redisMock.Close()
		defer s.Close()
	}
}

func getMessagesTestData() map[string]string {
	return map[string]string{
		"test": "test",
	}
}

func initEnv() {
	if err := godotenv.Load("../../.env"); err != nil {
		logrus.Println("No .env file found")
	}
}

func runGRPCServer(redis *redis.Client, needFetched bool) {
	initEnv()
	c = &core.Core{}
	if needFetched {
		c.FetchAllTables()
	}
	b := bot.NewBot(c, getMessagesTestData(), nil)

	lis = bufconn.Listen(bufSize)
	srv := grpc.NewServer()

	crispData := user.CrispData(sdk.NewCrispClient())

	partnerGRPC.RegisterPartnerServiceServer(srv, &partnerAPIHandlerImpl{})
	partnerAppGRPC.RegisterPartnerApplicationServiceServer(srv, &partnerAppAPIHandlerImpl{})

	store := redislayer.NewRedisStore(store.NewAppStore(), redis, nil)

	msClient := sdk.NewMeilisearchClient()
	diagnosticGRPC.RegisterDiagnosticServiceServer(srv, &conversationPageHandlerImpl{
		bot:      b,
		core:     c,
		store:    store,
		msClient: msClient,
	})
	userGRPC.RegisterUserServiceServer(srv, &userAPIHandlerImpl{
		bot:       b,
		core:      c,
		crispData: crispData,
	})
	telemedicineGRPC.RegisterTelemedicineServiceServer(srv, &telemedicineAPIHandlerImpl{
		crispData: crispData,
	})

	reflection.Register(srv)
	go func() {
		if err := srv.Serve(lis); err != nil {
			logrus.Fatalf("Server exited with error: %v", err)
		}
	}()
}

var (
	lis                *bufconn.Listener
	testPartnerID      = "d7744ff2-f88d-45de-97d1-208e5f223bcf"
	testPartnerNewName = ""
)

func runClient() (*grpc.ClientConn, context.Context, error) {
	dialFn := func(ctx context.Context, s string) (net.Conn, error) {
		return lis.Dial()
	}

	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "bufnet", grpc.WithContextDialer(dialFn), grpc.WithInsecure(), grpc.WithBlock())

	return conn, ctx, err
}

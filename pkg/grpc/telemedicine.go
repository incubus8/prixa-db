package grpc

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/wrappers"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	siloamGRPC "github.com/prixa-ai/prixa-proto/proto/siloam/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"github.com/umahmood/haversine"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/sdk"
	siloamSDK "prixa.ai/prixa-db/pkg/sdk/siloam"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user"
)

type telemedicineAPIHandlerImpl struct {
	telemedicineGRPC.TelemedicineServiceServer

	crispData user.CrispData

	siloamAPI siloamSDK.MySiloamAPI
	store     store.Store
}

type initChatRawContent struct {
	Name               string
	Gender             string
	Weight             string
	Height             string
	BMI                string
	Preconditions      string
	ChiefComplaint     string
	AdditionalSymptoms string
	PossibleDiseases   string
}

func (content *initChatRawContent) ToString() string {
	return "Nama : " + content.Name + "\n" +
		"Jenis Kelamin : " + content.Gender + "\n" +
		"Tinggi Badan : " + content.Height + "cm" + "\n" +
		"Berat Badan : " + content.Weight + "kg" + "\n" +
		"Indeks BMI : " + content.BMI + "\n" +
		"Prekondisi : \n" + content.Preconditions + "\n" +
		"Keluhan Utama : " + content.ChiefComplaint + "\n" +
		"Keluhan Tambahan : \n" + content.AdditionalSymptoms + "\n" +
		"Dugaan Penyakit : \n" + content.PossibleDiseases
}

func getPreconditionsName(profileData []*diagnosticGRPC.ProfileData) string {
	profileNames := make([]string, 0, len(profileData))
	ind := 1
	for _, profile := range profileData {
		profileNames = append(profileNames, "  "+strconv.Itoa(ind)+". "+profile.NameIndo)
		ind++
	}
	return strings.Join(profileNames, "\n")
}

func getChiefComplaint(symptoms []*diagnosticGRPC.SymptomDesc) string {
	for _, symptom := range symptoms {
		if symptom.Chief {
			return symptom.SymptomName
		}
	}
	return ""
}

func getSymptomsName(symptoms []*diagnosticGRPC.SymptomDesc) string {
	var symptomsName []string
	ind := 1
	for _, symptom := range symptoms {
		if !symptom.Chief && symptom.Answer == "yes" {
			symptomsName = append(symptomsName, "  "+strconv.Itoa(ind)+". "+symptom.SymptomName)
			ind++
		}
	}
	return strings.Join(symptomsName, "\n")
}

func getDiseasesName(diseases []*diagnosticGRPC.PotentialDisease) string {
	diseasesName := make([]string, 0, len(diseases))
	ind := 1
	for _, disease := range diseases {
		diseasesName = append(diseasesName, "  "+strconv.Itoa(ind)+". "+disease.Name)
		ind++
	}

	return strings.Join(diseasesName, "\n")
}

// InitConversation is
func (s *telemedicineAPIHandlerImpl) InitConversation(ctx context.Context, req *telemedicineGRPC.InitConversationRequest) (*telemedicineGRPC.InitConversationResponse, error) {
	if err := req.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": req})
	}

	diagnosisResultData, errGetDiagnosisResultData := s.store.DiagnosticResult().GetDiagnosisResult(req.GetDiagnosticSessionID())
	if errGetDiagnosisResultData != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errGetDiagnosisResultData, logrus.Fields{
			"request": req,
		})
	}

	meta := metautils.ExtractIncoming(ctx)
	name, email := meta.Get("name"), meta.Get("email")
	rawContent := &initChatRawContent{
		Name:               name,
		Gender:             diagnosisResultData.Data.User.Gender,
		Weight:             fmt.Sprintf("%.2f", diagnosisResultData.Data.User.Weight),
		Height:             fmt.Sprintf("%.2f", diagnosisResultData.Data.User.Height),
		BMI:                fmt.Sprintf("%.2f", db.CalculateBMI(float64(diagnosisResultData.Data.User.Weight), float64(diagnosisResultData.Data.User.Height))),
		Preconditions:      getPreconditionsName(diagnosisResultData.Data.Profiles),
		ChiefComplaint:     getChiefComplaint(diagnosisResultData.Data.Symptoms),
		AdditionalSymptoms: getSymptomsName(diagnosisResultData.Data.Symptoms),
		PossibleDiseases:   getDiseasesName(diagnosisResultData.Data.Diseases),
	}

	currentPersonProfile, _, errGetPersonData := s.crispData.Client.Website.GetPeopleProfile(s.crispData.WebsiteID, email)
	if errGetPersonData != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errGetPersonData, logrus.Fields{"request": req})
	}

	sessionID := req.GetSessionID()
	personData := s.crispData.GetPersonData(email)
	personData[sdk.DataSessionID] = sessionID
	if err := s.crispData.SavePersonData(email, personData); err != nil {
		return nil, grpcRespondError(ctx, codes.Internal, err, logrus.Fields{"request": req})
	}

	s.crispData.Conversation(sessionID, *currentPersonProfile.Person.Nickname, *currentPersonProfile.Email, rawContent.ToString())
	return &telemedicineGRPC.InitConversationResponse{
		WebsiteID: s.crispData.WebsiteID,
	}, nil
}

// Get Area Data By ID
func (s *telemedicineAPIHandlerImpl) GetAreaData(ctx context.Context, request *telemedicineGRPC.GetAreaRequestData) (*telemedicineGRPC.AreaResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}
	areaData, err := s.store.Hospital().GetAreaData(request.AreaId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.AreaResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    areaData,
	}, nil
}

// Get All Area Data
func (s *telemedicineAPIHandlerImpl) GetAreasData(ctx context.Context, request *telemedicineGRPC.GetAreasRequestData) (*telemedicineGRPC.AreasResponseData, error) {
	areaList, err := s.store.Hospital().GetAreasData(cast.ToInt(request.Page))
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.AreasResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    areaList,
	}, nil
}

func (s *telemedicineAPIHandlerImpl) GetSpecialityData(ctx context.Context, request *telemedicineGRPC.GetSpecialityRequestData) (*telemedicineGRPC.SpecialityResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	specialityData, err := s.store.Hospital().GetSpecialityData(request.SpecialityId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.SpecialityResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    specialityData,
	}, nil
}

func (s *telemedicineAPIHandlerImpl) GetSpecialitiesData(ctx context.Context, request *telemedicineGRPC.GetSpecialitiesRequestData) (*telemedicineGRPC.SpecialitiesResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	var specialities []*telemedicineGRPC.SpecialityData
	var err error
	if request.AreaId == "" && request.HospitalId == "" {
		specialities, err = s.store.Hospital().GetSpecialitiesData(cast.ToInt(request.Page), request.GetName())
	} else {
		specialities, err = s.store.Hospital().GetSpecialitiesDataByParam(cast.ToInt(request.Page), request.GetAreaId(), request.GetHospitalId(), request.GetName())
	}
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.SpecialitiesResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    specialities,
	}, nil
}

// Get HospitalStore Data By ID
func (s *telemedicineAPIHandlerImpl) GetHospitalData(ctx context.Context, request *telemedicineGRPC.GetHospitalRequestData) (*telemedicineGRPC.HospitalResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}
	hospitalData, err := s.store.Hospital().GetHospitalData(request.HospitalId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.HospitalResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    hospitalData,
	}, nil
}

// Get Hospitals Data by param
func (s *telemedicineAPIHandlerImpl) GetHospitalsData(ctx context.Context, request *telemedicineGRPC.GetHospitalsRequestData) (*telemedicineGRPC.HospitalsResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	if request.Param != nil {
		if request.Param.NameKeyword != "" && len(request.Param.NameKeyword) < 3 {
			return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New("parameter name keyword cannot be less than 3 characters"), logrus.Fields{"request": request})
		}
	}

	hospitalsData, err := s.store.Hospital().GetHospitalsData(cast.ToInt(request.Page), request.Param)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.HospitalsResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    hospitalsData,
	}, nil
}

// GetHospitalsDataByCoordinate by coordinate and filter km
func (s *telemedicineAPIHandlerImpl) GetHospitalsDataByCoordinate(ctx context.Context, request *telemedicineGRPC.GetHospitalsByCoordinateRequestData) (*telemedicineGRPC.HospitalsByCoordinateResponseData, error) {
	if request.Latitude == 0 || request.Longitude == 0 {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New("invalid parameter latitude or longitude"), logrus.Fields{"request": request})
	}

	if request.FilterInKm == 0 {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New("invalid parameter filter km"), logrus.Fields{"request": request})
	}

	hospitalsData, err := s.store.Hospital().GetHospitalsDataFilterByDistance(cast.ToInt(request.Page), request.Latitude, request.Longitude, request.FilterInKm)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	hospitalsResult := make([]*telemedicineGRPC.HospitalByCoordinateData, 0, len(hospitalsData))
	for _, hospital := range hospitalsData {
		hospitalsResult = append(hospitalsResult, &telemedicineGRPC.HospitalByCoordinateData{
			Hospital:     hospital,
			DistanceInKm: float32(getDistanceFromLatLonInKm(float64(request.Latitude), float64(request.Longitude), float64(hospital.Coordinate.Lat), float64(hospital.Coordinate.Lon))),
		})
	}

	hospitalsSort := sortHospitalByDistance(hospitalsResult, func(a, b *telemedicineGRPC.HospitalByCoordinateData) bool {
		return a.DistanceInKm < b.DistanceInKm
	})

	return &telemedicineGRPC.HospitalsByCoordinateResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    hospitalsSort,
	}, nil
}

func sortHospitalByDistance(s []*telemedicineGRPC.HospitalByCoordinateData, fn func(*telemedicineGRPC.HospitalByCoordinateData, *telemedicineGRPC.HospitalByCoordinateData) bool) []*telemedicineGRPC.HospitalByCoordinateData {
	sort.Slice(s, func(a, b int) bool {
		return fn(s[a], s[b])
	})

	return s
}

func getDistanceFromLatLonInKm(lat1, long1, lat2, long2 float64) float64 {
	from := haversine.Coord{Lat: lat1, Lon: long1}
	to := haversine.Coord{Lat: lat2, Lon: long2}
	_, km := haversine.Distance(from, to)
	return km
}

// Get DoctorStore Data By ID
func (s *telemedicineAPIHandlerImpl) GetDoctorData(ctx context.Context, request *telemedicineGRPC.GetDoctorRequestData) (*telemedicineGRPC.DoctorResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	doctorData, err := s.store.Hospital().GetDoctorData(request.DoctorId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}
	return &telemedicineGRPC.DoctorResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    doctorData,
	}, nil
}

// Get DoctorStore Data by param
func (s *telemedicineAPIHandlerImpl) GetDoctorsData(ctx context.Context, request *telemedicineGRPC.GetDoctorsRequestData) (*telemedicineGRPC.DoctorsResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	if request.Param != nil {
		if request.Param.NameKeyword != "" && len(request.Param.NameKeyword) < 3 {
			return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New("parameter name keyword cannot be less than 3 characters"), logrus.Fields{"request": request})
		}
	}

	doctorsData, err := s.store.Hospital().GetDoctorsData(cast.ToInt(request.Page), request.Param)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.DoctorsResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    doctorsData,
	}, nil
}

func (s *telemedicineAPIHandlerImpl) GetDoctorSchedulesData(ctx context.Context, request *telemedicineGRPC.GetDoctorSchedulesRequestData) (*telemedicineGRPC.DoctorSchedulesResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	page, err := cast.ToIntE(request.Page)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request, "desc": "page must be int"})
	}

	doctorSchedules, err := s.store.Hospital().GetDoctorSchedulesData(page, request.DoctorId, request.HospitalId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	doctorLeaves := getDoctorLeavesInCacheOrSiloamAPI(ctx, request.HospitalId, request.DoctorId, s.store.Doctor(), s.siloamAPI)

	return &telemedicineGRPC.DoctorSchedulesResponseData{
		Status:  "OK",
		Message: "Success",
		Data: &telemedicineGRPC.DoctorScheduleData{
			DoctorId:   request.DoctorId,
			HospitalId: request.HospitalId,
			Schedules:  doctorSchedules,
			Leaves:     doctorLeaves,
		},
	}, nil
}

func getDoctorLeavesInCacheOrSiloamAPI(ctx context.Context, hospitalID string, doctorID string, doctorStore store.DoctorStore, siloamAPI siloamSDK.MySiloamAPI) []*telemedicineGRPC.DoctorLeaveHospitalData {
	leaves, err := getDoctorLeavesInCache(hospitalID, doctorID, doctorStore)
	if err == nil {
		return leaves
	}
	response, err := siloamAPI.GetDoctorLeavesHospitalData(ctx, &siloamGRPC.GetDoctorsLeavesHospitalRequestData{
		HospitalId: hospitalID,
		DoctorId: &wrappers.StringValue{
			Value: doctorID,
		},
	})
	if err != nil {
		logrus.WithError(err).WithContext(ctx).Warnln()
		return []*telemedicineGRPC.DoctorLeaveHospitalData{}
	}

	if response.StatusCode != http.StatusOK && strings.ToLower(response.Status) != "ok" {
		logrus.WithError(errors.New(response.Message)).WithContext(ctx).Warnln()
		return []*telemedicineGRPC.DoctorLeaveHospitalData{}
	}

	leaves = mapAndFilterDoctorLeavesData(response.Data)
	_, err = saveDoctorLeavesInCache(&model.DoctorLeaves{
		DoctorID:     doctorID,
		HospitalID:   hospitalID,
		DoctorLeaves: leaves,
	}, doctorStore)

	if err != nil {
		logrus.WithError(err).WithContext(ctx).
			WithFields(logrus.Fields{
				"doctorStore":  doctorStore,
				"doctorId":     doctorID,
				"hospitalId":   hospitalID,
				"doctorLeaves": leaves,
			}).Errorln()
	}

	return leaves
}

func mapAndFilterDoctorLeavesData(leavesSiloam []*siloamGRPC.DoctorLeaveHospitalData) (filterLeaves []*telemedicineGRPC.DoctorLeaveHospitalData) {
	doctorLeaves := mapToDoctorLeavesData(leavesSiloam)
	for _, leave := range doctorLeaves {
		toDate, err := time.Parse("2006-01-02", leave.ToDate)
		if err != nil {
			continue
		}
		if time.Now().Add(-24 * time.Hour).Before(toDate) {
			filterLeaves = append(filterLeaves, leave)
		}
	}
	return filterLeaves
}

func mapToDoctorLeavesData(leavesSiloam []*siloamGRPC.DoctorLeaveHospitalData) (leaves []*telemedicineGRPC.DoctorLeaveHospitalData) {
	for _, leave := range leavesSiloam {
		timeSlot := &telemedicineGRPC.DoctorLeaveHospitalData{
			DoctorId:   leave.DoctorId,
			HospitalId: leave.HospitalId,
			FromDate:   leave.FromDate,
			ToDate:     leave.ToDate,
		}
		leaves = append(leaves, timeSlot)

	}
	return leaves
}

func (s *telemedicineAPIHandlerImpl) GetDoctorTimeSlotsData(ctx context.Context, request *telemedicineGRPC.GetDoctorTimeSlotRequestData) (*telemedicineGRPC.DoctorTimeSlotsResponseData, error) {
	if err := request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	appointmentDate, err := time.Parse("2006-01-02", request.GetAppointmentDate())
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	var doctorTimeSlots []*telemedicineGRPC.DoctorTimeSlotData
	doctorTimeSlots, err = getDoctorTimeSlotInCache(request.HospitalId, request.DoctorId, &appointmentDate, s.store.Doctor())
	if err == nil {
		return &telemedicineGRPC.DoctorTimeSlotsResponseData{
			Status:  "OK",
			Message: "Success",
			Data:    doctorTimeSlots,
		}, nil
	}

	response, err := s.siloamAPI.GetTimeSlotsData(ctx, &siloamGRPC.GetTimeSlotsRequestData{
		HospitalId:      request.HospitalId,
		DoctorId:        request.DoctorId,
		AppointmentDate: request.AppointmentDate,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	if response.StatusCode != http.StatusOK && strings.ToLower(response.Status) != "ok" {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, errors.New(response.Message), logrus.Fields{"request": request})
	}

	timeSlotsData := mapToDoctorTimeSlotsData(response.Data)
	_, err = saveDoctorTimeSlotInCache(
		&model.DoctorTimeSlots{
			DoctorID:        request.DoctorId,
			HospitalID:      request.HospitalId,
			AppointmentDate: &appointmentDate,
			DoctorTimeSlots: timeSlotsData,
		}, s.store.Doctor())

	if err != nil {
		logrus.WithError(err).WithContext(ctx).
			WithFields(logrus.Fields{
				"doctorId":        request.DoctorId,
				"hospitalId":      request.HospitalId,
				"appointmentDate": appointmentDate,
				"doctorTimeSlots": timeSlotsData,
			}).Errorln()
	}

	return &telemedicineGRPC.DoctorTimeSlotsResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    timeSlotsData,
	}, nil
}

func mapToDoctorTimeSlotsData(timeSlotsSiloam []*siloamGRPC.TimeSlotData) (timeSlots []*telemedicineGRPC.DoctorTimeSlotData) {
	for _, timeSlotSiloam := range timeSlotsSiloam {
		timeSlot := &telemedicineGRPC.DoctorTimeSlotData{
			ScheduleId:       timeSlotSiloam.ScheduleId,
			FromTime:         timeSlotSiloam.FromTime,
			HospitalTimeZone: timeSlotSiloam.HospitalTimeZone,
			IsFull:           timeSlotSiloam.IsFull,
			ToTime:           timeSlotSiloam.ToTime,
		}
		timeSlots = append(timeSlots, timeSlot)

	}
	return timeSlots
}

func getDoctorTimeSlotInCache(hospitalID string, doctorID string, appointmentDate *time.Time, doctorStore store.DoctorStore) ([]*telemedicineGRPC.DoctorTimeSlotData, error) {
	result, err := doctorStore.FindDoctorTimeSlotsData(hospitalID, doctorID, appointmentDate)
	if err != nil {
		return []*telemedicineGRPC.DoctorTimeSlotData{}, err
	}
	return result.DoctorTimeSlots, nil
}

func saveDoctorTimeSlotInCache(timeSlotData *model.DoctorTimeSlots, doctorStore store.DoctorStore) ([]*telemedicineGRPC.DoctorTimeSlotData, error) {
	result, err := doctorStore.SaveDoctorTimeSlotsData(timeSlotData)
	if err != nil {
		return []*telemedicineGRPC.DoctorTimeSlotData{}, err
	}
	return result.DoctorTimeSlots, nil
}

func getDoctorLeavesInCache(hospitalID string, doctorID string, doctorStore store.DoctorStore) ([]*telemedicineGRPC.DoctorLeaveHospitalData, error) {
	result, err := doctorStore.FindDoctorLeavesData(hospitalID, doctorID)
	if err != nil {
		return []*telemedicineGRPC.DoctorLeaveHospitalData{}, err
	}
	return result.DoctorLeaves, nil
}

func saveDoctorLeavesInCache(leavesData *model.DoctorLeaves, doctorStore store.DoctorStore) ([]*telemedicineGRPC.DoctorLeaveHospitalData, error) {
	result, err := doctorStore.SaveDoctorLeavesData(leavesData)
	if err != nil {
		return []*telemedicineGRPC.DoctorLeaveHospitalData{}, err
	}
	return result.DoctorLeaves, nil
}

func (s *telemedicineAPIHandlerImpl) GetAppointmentBookingsData(ctx context.Context, request *telemedicineGRPC.GetAppointmentBookingsRequestData) (*telemedicineGRPC.AppointmentBookingsResponseData, error) {
	email := request.GetEmail()
	startDate := request.GetFromDate()
	endDate := request.GetToDate()

	res, err := s.store.User().GetUserAppointmentBookingData(email, startDate, endDate)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	return &telemedicineGRPC.AppointmentBookingsResponseData{
		Status: "success",
		Data:   res,
	}, nil
}

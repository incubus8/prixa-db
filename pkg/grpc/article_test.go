package grpc

import (
	"testing"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/stretchr/testify/assert"
)

func TestArticleAPI(t *testing.T) {
	teardown := setup(t, true)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()

	client := diagnosticGRPC.NewDiagnosticServiceClient(conn)
	t.Run("Get Article with correct DiseaseID", func(t *testing.T) {
		disease := c.Diseases.GetDiseaseByName("Pharyngitis")
		resp, err := client.GetDiseaseArticle(ctx, &diagnosticGRPC.GetDiseaseArticleRequest{
			DiseaseID: disease.ID,
		})
		if err != nil {
			t.Fatalf("Failed to get disease article contents %v", err)
		}

		sympsNameIndo := []string{}
		for _, diff := range c.Differentials.ByDisease(disease) {
			sympsNameIndo = append(sympsNameIndo, c.Symptoms.Dict[diff.Symptom].NameIndo)
		}

		assert.NotEmpty(t, resp)
		assert.Equal(t, resp.DiseaseName, "Pharyngitis")
		assert.Equal(t, resp.DiseaseNameIndo, "Faringitis")
		assert.Equal(t, resp.Author, "Arini Ayatika Sadariskar, S.Ked.")
		assert.Equal(t, resp.CheckedBy, "dr. Frisca A. Halim")
		assert.Equal(t, resp.Overview, "Faringitis, atau yang umum dikenal sebagai radang tenggorokan, merupakan radang yang terjadi pada saluran yang menghubungkan rongga hidung dan mulut. Gejala dapat berupa nyeri menelan, demam, pilek, batuk, nyeri kepala, mual, muntah, lemas, dan penurunan nafsu makan. Faringitis biasanya disebabkan oleh infeksi virus atau bakteri.")
		assert.Equal(t, resp.Advice, "Faringtis yang disebabkan oleh virus tidak memerlukan antibiotik dan umumnya sembuh sendiri setelah 5-7 hari. Beberapa hal yang dapat Anda lakukan:\n1.\tIstirahat cukup\n2.\tBanyak minum air putih. Hindari kafein dan alkohol\n3.\tBerkumur dengan air garam\n4.\tMenghindari bahan iritan seperti asap rokok dan asap kendaraan\n\nObat-obatan yang dapat membantu meredakan gejala:\n1.\tParasetamol tablet 500 mg 1-2 tablet 3-4 kali sehari, untuk meredakan nyeri dan demam. Dosis untuk anak sesuai dengan instruksi pada kemasan obat.\n\nCek lab:\nKultur apusan tenggorokan (atas anjuran dokter).\n")
		assert.Equal(t, resp.SeeDoctor, "Anak-anak sebaiknya dibawa ke dokter apabila terdapat kesulitan bernapas, kesulitan menelan, produksi air liur berlebih, atau sakit tenggorokan tidak hilang setelah minum air putih. Orang dewasa perlu ke dokter jika terdapat kesulitan menelan, kesulitan bernapas, kesulitan membuka mulut, nyeri sendi, nyeri telinga, kemerahan pada kulit, demam di atas 38,3°C, darah pada liur atau dahak, nyeri tenggorokan berulang, benjolan pada leher, atau bengkak pada leher maupun wajah.")
		assert.Equal(t, resp.Prevention, "Rajin mencuci tangan, hindari berbagi alat makan, batuk atau bersin di atas tisu dan buang ke tempat sampah, dan hindari kontak dengan orang lain yang mengalami gejala yang sama.")
		assert.Equal(t, resp.Reference, "1.\tSore throats. American Academy of Otolaryngology — Head and Neck Surgery. http://www.entnet.org/content/sore-throats. Diakses pada Dec. 18, 2019.\n\n")
		assert.Equal(t, len(resp.RelatedSymptom), len(sympsNameIndo))
		assert.Equal(t, resp.RelatedSymptom, sympsNameIndo)
	})
	t.Run("Get Article with incorrect DiseaseID", func(t *testing.T) {
		resp, err := client.GetDiseaseArticle(ctx, &diagnosticGRPC.GetDiseaseArticleRequest{
			DiseaseID: "d_xxx",
		})
		if err != nil {
			assert.Empty(t, resp)
			assert.NotEmpty(t, err)
		}
	})
}

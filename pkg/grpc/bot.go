package grpc

import (
	"context"
	"errors"
	"strings"

	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	meili "github.com/meilisearch/meilisearch-go"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user/userprofile"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/db"
)

type conversationPageHandlerImpl struct {
	diagnosticGRPC.DiagnosticServiceServer

	bot      *bot.Bot
	core     *core.Core
	msClient *meili.Client
	store    store.Store
}

// BotConversation (DEPRECATED) handles bot conversation logic that starts when user uses diagnostic engine
func (s *conversationPageHandlerImpl) BotConversation(ctx context.Context, _ *diagnosticGRPC.BotConversationRequest) (*diagnosticGRPC.BotConversationResponse, error) {
	return nil, grpcRespondError(ctx, codes.Unimplemented, errors.New("deprecated and no longer maintained"), nil)
}

// BotConversation handles bot conversation that starts when user use diagnostic engine
func (s *conversationPageHandlerImpl) BotConversationV2(ctx context.Context, request *diagnosticGRPC.BotConversationRequest) (*diagnosticGRPC.BotConversationResponse, error) {
	return s.HandleConversation(ctx, request, bot.Version2)
}

type authData struct {
	isAuthorized bool
	claims       *userClaims
}

// getPreconditionsActualID gets actual id for every precondition data in userPreconditions
func (s *conversationPageHandlerImpl) getPreconditionsActualID(userPreconditions []*diagnosticGRPC.PreconditionsData) []*diagnosticGRPC.PreconditionsData {
	for _, userPrecondition := range userPreconditions {
		foundProfile := s.core.Meta.ProfileByName(userPrecondition.Name)
		if foundProfile != (db.Profile{}) {
			userPrecondition.Id = foundProfile.ID
		}
	}
	return userPreconditions
}

func (s *conversationPageHandlerImpl) updateReplyValueAndState(reply bot.Reply, state bot.State, authData authData) (interface{}, bot.State) {
	if state == bot.AskPreCondition && authData.isAuthorized && !strings.EqualFold(reply.Value.(string), bot.AnswerNotSelfAssessment) {
		userPreconditions := s.store.User().GetPreconditionsList(authData.claims.Email)
		if len(userPreconditions) != 0 {
			userPreconditions = s.getPreconditionsActualID(userPreconditions)
			return getReplyValueData(&diagnosticGRPC.ReplyData{
				Type:          bot.PreconditionType,
				Preconditions: userPreconditions,
			}), bot.ConfirmPreCondition
		}
	}
	return reply.Value, state
}

// HandleConversation handles conversation
func (s *conversationPageHandlerImpl) HandleConversation(ctx context.Context, request *diagnosticGRPC.BotConversationRequest, version bot.Version) (*diagnosticGRPC.BotConversationResponse, error) {
	requestReply := request.GetReply()
	reply := mapRequestReply(requestReply)

	savedSessionResult, err := s.store.Diagnostic().GetSessionResult(request.GetSessionId())
	if err != nil {
		sess := bot.NewSession(ctx) // recreate new session
		sess.Latitude = request.GeoLocation.GetLatitude()
		sess.Longitude = request.GeoLocation.GetLongitude()
		savedSessionResult.Session = sess
		savedSessionResult.Session.UID = request.GetUid()
	}

	bearerToken := metautils.ExtractIncoming(ctx).Get("authorization")
	claims, isAuthorized := authAndGetClaims(s.store, bearerToken)
	reply.Value, savedSessionResult.Session.State = s.updateReplyValueAndState(reply, savedSessionResult.Session.State, authData{
		isAuthorized: isAuthorized,
		claims:       claims,
	})

	session, result, currentState, err := s.bot.Process(savedSessionResult, reply, version, s.msClient)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"message":            "internal bot process error",
			"request":            request,
			"session":            session,
			"reply":              reply,
			"savedSessionResult": savedSessionResult,
		})
	}

	err = s.store.Diagnostic().SaveSessionResult(session.ID, &bot.SessionResult{
		Session: session,
		Result:  result,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"message":            "couldn't save session",
			"request":            request,
			"reply":              reply,
			"session":            session,
			"savedSessionResult": savedSessionResult,
		})
	}

	logrus.WithFields(logrus.Fields{
		"session_id":     session.ID,
		"previous_state": savedSessionResult.Session.State,
		"next_state":     session.State,
		"reply":          reply,
		"result":         result,
	}).Infoln("Sending message")

	mappedAction, err := mapActionData(result.Action)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"message":            "invalid casting in action data",
			"request":            request,
			"reply":              reply,
			"session":            session,
			"savedSessionResult": savedSessionResult,
		})
	}
	if mappedAction != nil && mappedAction.Type == bot.ActionTypeData {
		err = s.store.DiagnosticResult().SaveDiagnosisResult(&bot.DiagnosisResData{
			Key:  session.ID,
			Data: mappedAction.DiagnosisResult,
		})
		if err != nil {
			return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
				"message": "couldn't save session",
				"session": session,
			})
		}

		if bearerToken != "" && isAuthorized {
			userProfile := userprofile.ProfileProperties{
				DiagnosisResult: mappedAction.DiagnosisResult,
				Email:           claims.Email,
				Name:            claims.Name,
			}
			s.store.User().SaveAssessmentResult(userProfile)
		}
	}

	preconditions, err := mapPreconditionsData(result.Action)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{
			"message":            "invalid casting in action data",
			"request":            request,
			"reply":              reply,
			"session":            session,
			"savedSessionResult": savedSessionResult,
		})
	}

	return &diagnosticGRPC.BotConversationResponse{
		SessionId:             session.ID,
		ProgressBarPercentage: cast.ToFloat32(session.ProgressBar.CurrentPercentage),

		Result: &diagnosticGRPC.ResultData{
			Messages:      mapMessageData(result.Messages[0]),
			Actions:       mappedAction,
			CurrentState:  currentState,
			Preconditions: preconditions,
			SymptomID:     result.SymptomID,
			IsValidForRDT: (session.Covid19Data.Score >= 6.0) && (session.Covid19Data.IsEmergencyRespiratorySympPresent || session.Covid19Data.IsNonEmergencyRespiratorySympPresent),
		},
	}, nil
}

func authAndGetClaims(store store.Store, bearerToken string) (*userClaims, bool) {
	reqToken := strings.Replace(bearerToken, "Bearer ", "", 1) // case sensitive!
	if reqToken == "" {
		return nil, false
	}

	claims, authErr := verifyJwtBearer("", reqToken)
	if authErr != nil {
		return nil, false
	}

	email := cast.ToString(claims["email"])
	tokenFromClaims := cast.ToString(claims["token"])
	userSavedToken, errGetToken := store.User().GetToken(email + ":" + tokenFromClaims)
	if errGetToken != nil {
		return nil, false
	}
	if tokenFromClaims != userSavedToken {
		return nil, false
	}
	return &userClaims{
		Email: email,
		Name:  cast.ToString(claims["name"]),
	}, true
}

func getReplyValueData(reqReply *diagnosticGRPC.ReplyData) interface{} {
	if reqReply == nil {
		return nil
	}

	if !strings.EqualFold(reqReply.Type, bot.PreconditionType) {
		return reqReply.GetValue()
	}

	var preconditionData bot.PreconditionData
	for _, reply := range reqReply.Preconditions {
		preconditionHandler, ok := preconditionReplyHandler[reply.Type]
		if ok {
			preconditionData = preconditionHandler(preconditionData, reply)
		}
	}
	return preconditionData
}

func mapRequestReply(reqReply *diagnosticGRPC.ReplyData) bot.Reply {
	replyValue := getReplyValueData(reqReply)
	return bot.Reply{
		Type:  reqReply.GetType(),
		Tag:   reqReply.GetTag(),
		Value: replyValue,
	}
}

func mapMessageData(data bot.Message) *diagnosticGRPC.MessagesData {
	return &diagnosticGRPC.MessagesData{
		Value:       cast.ToString(data.Value),
		Explanation: data.Explanation,
		SourceURL:   data.Source,
		Media:       data.Media,
	}
}

func mapActionData(data interface{}) (*diagnosticGRPC.ActionData, error) {
	convertedData, ok := data.(bot.Action)
	if !ok {
		err := errors.New("casting failed. Data is not bot.Action")
		logrus.WithError(err).WithField("data", data).Errorln("casting failed")
		return nil, err
	}

	var valueData []*diagnosticGRPC.ValueData
	var val *diagnosticGRPC.ValueData
	var diagnosisResultData *diagnosticGRPC.DiagnosisResultData
	if convertedData.Type == bot.ActionTypeButtons {
		for _, convertedValue := range convertedData.Value.([]bot.ButtonValue) {
			val = &diagnosticGRPC.ValueData{
				Value:       cast.ToString(convertedValue.Value),
				Tag:         cast.ToString(convertedValue.Tag),
				Label:       convertedValue.Label,
				Description: convertedValue.Description,
			}
			valueData = append(valueData, val)
		}
	} else if convertedData.Type == bot.ActionTypeText {
		val = &diagnosticGRPC.ValueData{
			Value: cast.ToString(convertedData.Value),
		}
		valueData = append(valueData, val)
	} else if convertedData.Type == bot.ActionTypeData {
		diagnosisResult, ok := convertedData.Value.(bot.DiagnosisResult)
		if !ok {
			err := errors.New("internal engine error")
			logrus.WithError(err).WithFields(logrus.Fields{
				"message": "couldn't cast data",
				"data":    diagnosisResult,
			}).Errorln()
			return nil, err
		}
		diagnosisResultData = &diagnosticGRPC.DiagnosisResultData{
			User:        mapUser(diagnosisResult.User),
			UserDetails: mapUserDetails(diagnosisResult.UserDetails),
			Profiles:    mapProfiles(diagnosisResult.Profiles),
			Symptoms:    mapSymptoms(diagnosisResult.Symptoms),
			Diseases:    mapDiseases(diagnosisResult.Diseases),
		}
	}

	return &diagnosticGRPC.ActionData{
		Value:           valueData,
		Type:            convertedData.Type,
		DiagnosisResult: diagnosisResultData,
	}, nil
}

func mapUserDetails(userDetails map[string]interface{}) *diagnosticGRPC.UserDetailsData {
	return &diagnosticGRPC.UserDetailsData{
		AgeYear:   cast.ToInt32(userDetails["ageYear"]),
		AgeMonth:  cast.ToInt32(userDetails["ageMonth"]),
		IsObese:   cast.ToBool(userDetails["isObese"]),
		Firstname: cast.ToString(userDetails["firstname"]),
	}
}

func mapProfiles(profiles []db.Profile) (profilesData []*diagnosticGRPC.ProfileData) {
	for _, profile := range profiles {
		profileData := &diagnosticGRPC.ProfileData{
			Id:          profile.ID,
			Name:        profile.Name,
			NameIndo:    profile.NameIndo,
			Description: profile.Description,
			Type:        profile.Type,
			Order:       cast.ToInt32(profile.Order),
		}
		profilesData = append(profilesData, profileData)
	}
	return profilesData
}

func mapSymptoms(symptoms []bot.SymptomDesc) (symptomsDesc []*diagnosticGRPC.SymptomDesc) {
	for _, symptom := range symptoms {
		symptomDesc := &diagnosticGRPC.SymptomDesc{
			SymptomID:      symptom.SymptomID,
			SymptomName:    symptom.SymptomName,
			PropNames:      symptom.PropNames,
			Chief:          symptom.Chief,
			SymptomsTriage: symptom.SymptomsTriage,
			Answer:         symptom.Answer,
		}
		symptomsDesc = append(symptomsDesc, symptomDesc)
	}
	return symptomsDesc
}

func mapDiseases(diseases []core.PotentialDisease) (potentialDiseases []*diagnosticGRPC.PotentialDisease) {
	for _, disease := range diseases {
		var labsInfo []*diagnosticGRPC.LabInfo
		for _, lab := range disease.Labs {
			labInfo := &diagnosticGRPC.LabInfo{
				Id:       lab.ID,
				Name:     lab.Name,
				Sku:      lab.SKU,
				Diseases: lab.Diseases,
			}
			labsInfo = append(labsInfo, labInfo)
		}

		potentialDisease := &diagnosticGRPC.PotentialDisease{
			Id:          disease.ID,
			Name:        disease.Name,
			Description: disease.Description,
			Likeliness:  disease.Likeliness,
			Score:       cast.ToFloat32(disease.Score),
			Url:         disease.URL,
			Triage: &diagnosticGRPC.TriageResult{
				Id:          disease.Triage.ID,
				Name:        disease.Triage.Name,
				NameIndo:    disease.Triage.NameIndo,
				Description: disease.Triage.Description,
			},
			Labs:      labsInfo,
			Prognosis: disease.Prognosis,
		}
		potentialDiseases = append(potentialDiseases, potentialDisease)
	}
	return potentialDiseases
}

func mapPreconditionsData(data interface{}) ([]*diagnosticGRPC.PreconditionsData, error) {
	actionData, ok := data.(bot.Action)
	if !ok {
		err := errors.New("casting failed. Data is not bot.Action")
		logrus.WithError(err).WithField("data", data).Errorln("casting failed")
		return nil, err
	}

	if !strings.EqualFold(actionData.Type, bot.PreconditionType) {
		return nil, nil
	}

	mapProfiles, ok := actionData.Value.(map[string][]db.Profile)
	if !ok {
		err := errors.New("casting failed. result action value is not PreconditionData")
		logrus.WithError(err).WithField("actionData.Value", actionData.Value).Errorln("casting failed")
		return nil, err
	}

	var preconditionsGRPC []*diagnosticGRPC.PreconditionsData
	for profileType, profiles := range mapProfiles {
		for _, profile := range profiles {
			preconditionsGRPC = append(preconditionsGRPC, &diagnosticGRPC.PreconditionsData{
				Id:                           profile.ID,
				NameIndo:                     profile.NameIndo,
				Name:                         profile.Name,
				Type:                         profileType,
				PreconditionsDescription:     profile.PreconditionDescription,
				PreconditionsDescriptionCopy: profile.PreconditionDescriptionCopy,
			})
		}
	}
	return preconditionsGRPC, nil
}

type preconditionReplyTypeFunc func(bot.PreconditionData, *diagnosticGRPC.PreconditionsData) bot.PreconditionData

var preconditionReplyHandler = map[string]preconditionReplyTypeFunc{
	bot.ProfileTypeGender:                setPreconditionGender,
	bot.ProfileTypeAge:                   setPreconditionAge,
	bot.ProfileTypeBmi:                   setPreconditionBMI,
	bot.ProfileTypePregnancy:             setPreconditionPregnancy,
	bot.ProfileTypeHistory:               setPreconditionHistory,
	bot.ProfileTypeChronicDiseases:       setCommonPrecondition,
	bot.ProfileTypeContactHistoryCovid19: setCommonPrecondition,
	bot.ProfileTypeLocalTransmission:     setCommonPrecondition,
	bot.ProfileTypeHabitRecord:           setCommonPrecondition,
}

func setPreconditionAge(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	preconditionData.Age.Year = int(rpcPreconditionData.GetAgeYear())
	preconditionData.Age.Month = int(rpcPreconditionData.GetAgeMonth())
	return preconditionData
}

func setPreconditionBMI(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	preconditionData.BMI.Height = float64(rpcPreconditionData.GetHeight())
	preconditionData.BMI.Weight = float64(rpcPreconditionData.GetWeight())
	return preconditionData
}

func setPreconditionGender(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	preconditionData.Gender.ID = rpcPreconditionData.Id
	preconditionData.Gender.Name = rpcPreconditionData.Name
	return preconditionData
}

func setPreconditionHistory(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	preconditionData.History = append(preconditionData.History, bot.PreconditionDetail{
		ID:   rpcPreconditionData.Id,
		Name: rpcPreconditionData.Name,
	})
	return preconditionData
}

func setCommonPrecondition(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	commonPreconditionDetails := bot.GetPreconditionDetails(rpcPreconditionData.Type, preconditionData)
	commonPreconditionDetails = append(commonPreconditionDetails, bot.PreconditionDetail{
		ID:   rpcPreconditionData.Id,
		Name: rpcPreconditionData.Name,
	})
	if rpcPreconditionData.Type == bot.ProfileTypeChronicDiseases {
		preconditionData.ChronicDisease = commonPreconditionDetails
	} else if rpcPreconditionData.Type == bot.ProfileTypeLocalTransmission {
		preconditionData.LocalTransmission = commonPreconditionDetails
	} else if rpcPreconditionData.Type == bot.ProfileTypeContactHistoryCovid19 {
		preconditionData.ContactHistoryCovid = commonPreconditionDetails
	} else if rpcPreconditionData.Type == bot.ProfileTypeHabitRecord {
		preconditionData.HabitRecord = commonPreconditionDetails
	}
	return preconditionData
}

func setPreconditionPregnancy(preconditionData bot.PreconditionData, rpcPreconditionData *diagnosticGRPC.PreconditionsData) bot.PreconditionData {
	preconditionData.Pregnancy.ID = rpcPreconditionData.Id
	preconditionData.Pregnancy.Name = rpcPreconditionData.Name
	return preconditionData
}

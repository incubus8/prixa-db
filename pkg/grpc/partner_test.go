package grpc

import (
	"testing"

	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestGRPCApiPartner(t *testing.T) {
	teardown := setup(t, false)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()
	client := partnerGRPC.NewPartnerServiceClient(conn)
	t.Run("Create Partner", func(t *testing.T) {
		resp, err := client.CreatePartner(ctx, &partnerGRPC.CreatePartnerRequest{Name: "Partner F"})
		if err != nil {
			t.Fatalf("Failed to create partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, "Partner F")
		assert.Equal(t, resp.Data.Status, "ACTIVE")
		assert.Equal(t, resp.Data.AppIds, []string(nil))

		testPartnerID = resp.Data.Id
	})

	t.Run("Update Partner", func(t *testing.T) {
		testPartnerNewName = "Partner TF"
		resp, err := client.UpdatePartner(ctx, &partnerGRPC.UpdatePartnerRequest{Name: testPartnerNewName, PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to update partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Id, testPartnerID)
		assert.Equal(t, resp.Data.Name, testPartnerNewName)
		assert.Equal(t, resp.Data.Status, "ACTIVE")
		assert.Equal(t, resp.Data.AppIds, []string(nil))
	})

	t.Run("Test Update Partner Wrong ID", func(t *testing.T) {
		testPartnerNewName = "Partner TF"
		var expectedResponse *partnerGRPC.UpdatePartnerResponse
		wrongPartnerID := testPartnerID + "a"
		resp, err := client.UpdatePartner(ctx, &partnerGRPC.UpdatePartnerRequest{Name: testPartnerNewName, PartnerId: wrongPartnerID})
		if err != nil {
			logrus.Warnln("Wrong Partner ID")
			assert.NotEqual(t, err, nil)
			assert.Equal(t, resp, expectedResponse)
		}
	})

	t.Run("Test Get Partner", func(t *testing.T) {
		resp, err := client.GetPartner(ctx, &partnerGRPC.GetPartnerRequest{PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to get all partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Id, testPartnerID)
		assert.Equal(t, resp.Data.Name, testPartnerNewName)
		assert.Equal(t, resp.Data.Status, "ACTIVE")
		assert.Equal(t, resp.Data.AppIds, []string(nil))
	})

	t.Run("Test Delete Partner", func(t *testing.T) {
		resp, err := client.DeletePartner(ctx, &partnerGRPC.DeletePartnerRequest{PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to delete partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Id, testPartnerID)
		assert.Equal(t, resp.Data.Name, testPartnerNewName)
		assert.Equal(t, resp.Data.Status, "DELETED")
		assert.Equal(t, resp.Data.AppIds, []string(nil))
	})

	t.Run("Test Update Deleted Partner", func(t *testing.T) {
		testPartnerNewName = "Partner TF"
		var expectedResponse *partnerGRPC.UpdatePartnerResponse
		resp, err := client.UpdatePartner(ctx, &partnerGRPC.UpdatePartnerRequest{Name: testPartnerNewName, PartnerId: testPartnerID})
		if err != nil {
			logrus.Warnln("Partner has been deleted")
			assert.NotEqual(t, err, nil)
			assert.Equal(t, resp, expectedResponse)
		}
	})

	t.Run("Test List Partner", func(t *testing.T) {
		page := int32(1)
		resp, err := client.ListPartners(ctx, &partnerGRPC.ListPartnersRequest{Page: page})
		if err != nil {
			t.Fatalf("Failed to list all partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Page)
		assert.Equal(t, resp.Data[0].AppIds, []string(nil))
		assert.NotEmpty(t, resp.Data[0].CreatedAt)
		assert.NotEmpty(t, resp.Data[0].UpdatedAt)
		assert.Equal(t, resp.Page.PageNo, page)
		assert.Equal(t, resp.Page.TotalPages, page)
		assert.Equal(t, resp.Page.TotalRecords, page)
	})
}

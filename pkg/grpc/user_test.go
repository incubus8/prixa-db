package grpc

import (
	"testing"

	"github.com/golang/protobuf/ptypes/empty"
	stringBase62 "github.com/lytics/base62"
	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	userGRPC "github.com/prixa-ai/prixa-proto/proto/user/v1"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/bot"
	"prixa.ai/prixa-db/pkg/sdk"
	"prixa.ai/prixa-db/pkg/store"
	"prixa.ai/prixa-db/pkg/user"
)

var diagnosticStore store.DiagnosticStore

func TestUserAPI(t *testing.T) {
	teardown := setup(t, true)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()

	testName := "UserTest1"
	testEmail := "user1@test.tf"
	testPassword := "Prixa12345ok"
	testConfirmationPassword := "Prixa12345ok"
	testDiagnosticSessionID := ""
	testRegisterToken := stringBase62.StdEncoding.EncodeToString([]byte(testEmail))

	client := userGRPC.NewUserServiceClient(conn)
	diagClient := diagnosticGRPC.NewDiagnosticServiceClient(conn)
	t.Run("Register Contact", func(t *testing.T) {
		respCreateSession, errCreateSession := diagClient.CreatePrixaSession(ctx, &diagnosticGRPC.CreatePrixaSessionRequest{})
		if errCreateSession != nil {
			t.Fatalf("Failed to create session:  %v", errCreateSession)
		}
		testDiagnosticSessionID = respCreateSession.SessionId
		errSaveToRedis := diagnosticStore.SaveDiagnosisResult(&bot.DiagnosisResData{
			Key:  testDiagnosticSessionID,
			Data: &diagnosticGRPC.DiagnosisResultData{},
		})
		if errSaveToRedis != nil {
			t.Fatalf("Failed to save to redis:  %v", errSaveToRedis)
		}
		resp, err := client.Register(ctx, &userGRPC.RegisterRequest{
			AuthData: &userGRPC.AuthData{
				Email:                testEmail,
				Password:             testPassword,
				PasswordConfirmation: testConfirmationPassword,
				DiagnosticSessionID:  testDiagnosticSessionID,
			},
			ProfileData: &userGRPC.ProfileData{
				Email:         testEmail,
				EmailVerified: false,
				Name:          testName,
				Password:      testPassword,
				Phone:         "000000000000",
				Gender:        "gender",
				Height:        1.1,
				Weight:        1.1,
				SmokingStatus: false,
				AlcoholStatus: false,
				PhoneVerified: false,
			},
		})
		if err != nil {
			t.Fatalf("Failed on registering new contact: %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.Equal(t, resp.Message, sendMailVerification)
	})
	t.Run("Register Contact with existing data", func(t *testing.T) {
		request := &userGRPC.RegisterRequest{
			AuthData: &userGRPC.AuthData{
				Email:                testEmail,
				Password:             testPassword,
				PasswordConfirmation: testConfirmationPassword,
				DiagnosticSessionID:  testDiagnosticSessionID,
			},
			ProfileData: &userGRPC.ProfileData{
				Email:         testEmail,
				EmailVerified: false,
				Name:          testName,
				Password:      testPassword,
				Phone:         "000000000000",
				Gender:        "gender",
				Height:        1.1,
				Weight:        1.1,
				SmokingStatus: false,
				AlcoholStatus: false,
				PhoneVerified: false,
			},
		}
		resp, err := client.Register(ctx, request)

		assert.Empty(t, resp)
		assert.NotEmpty(t, err)
	})
	t.Run("Register with wrong email format", func(t *testing.T) {
		request := &userGRPC.RegisterRequest{
			AuthData: &userGRPC.AuthData{
				Email:                "emailtester.com",
				Password:             testPassword,
				PasswordConfirmation: testConfirmationPassword,
				DiagnosticSessionID:  testDiagnosticSessionID,
			},
			ProfileData: &userGRPC.ProfileData{
				Email:         "emailtester.com",
				EmailVerified: false,
				Name:          testName,
				Password:      testPassword,
				Phone:         "000000000000",
				Gender:        "gender",
				Height:        1.1,
				Weight:        1.1,
				SmokingStatus: false,
				AlcoholStatus: false,
				PhoneVerified: false,
			},
		}
		resp, err := client.Register(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request}))
	})
	t.Run("Register without matching password and its confirmation", func(t *testing.T) {
		request := &userGRPC.RegisterRequest{
			AuthData: &userGRPC.AuthData{
				Email:                testEmail,
				Password:             testPassword,
				PasswordConfirmation: testConfirmationPassword + "a",
				DiagnosticSessionID:  testDiagnosticSessionID,
			},
			ProfileData: &userGRPC.ProfileData{
				Email:         testEmail,
				EmailVerified: false,
				Name:          testName,
				Password:      testPassword,
				Phone:         "000000000000",
				Gender:        "gender",
				Height:        1.1,
				Weight:        1.1,
				SmokingStatus: false,
				AlcoholStatus: false,
				PhoneVerified: false,
			},
		}
		resp, err := client.Register(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.InvalidArgument, errPasswordConfirmationDidNotMatch, logrus.Fields{"request": request}))
	})
	t.Run("Register with wrong diagnosticSessionID", func(t *testing.T) {
		request := &userGRPC.RegisterRequest{
			AuthData: &userGRPC.AuthData{
				Email:                testEmail,
				Password:             testPassword,
				PasswordConfirmation: testConfirmationPassword,
				DiagnosticSessionID:  testDiagnosticSessionID + "a",
			},
			ProfileData: &userGRPC.ProfileData{
				Email:         testEmail,
				EmailVerified: false,
				Name:          testName,
				Password:      testPassword,
				Phone:         "000000000000",
				Gender:        "gender",
				Height:        1.1,
				Weight:        1.1,
				SmokingStatus: false,
				AlcoholStatus: false,
				PhoneVerified: false,
			},
		}
		resp, err := client.Register(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.NotFound, errDiagnosisSessionNotFound, logrus.Fields{"request": request}))
	})
	t.Run("Register By Google", func(t *testing.T) {
		resp, err := client.OauthGoogle(ctx, &empty.Empty{})
		if err != nil {
			t.Fatalf("Failed to generate google oauth URL")
		}

		assert.NotEmpty(t, resp)
		assert.NotEmpty(t, resp.AuthURL)
	})
	t.Run("Verify Register", func(t *testing.T) {
		resp, err := client.VerifyRegister(ctx, &userGRPC.VerifyRegisterRequest{
			RegisterToken:       testRegisterToken,
			DiagnosticSessionID: testDiagnosticSessionID,
		})
		if err != nil {
			t.Fatalf("Failed on verify registering new contact %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.NotEmpty(t, resp.LoginToken)
	})
	t.Run("Verify Register with wrong diagnosticSessionID", func(t *testing.T) {
		request := &userGRPC.VerifyRegisterRequest{
			RegisterToken:       testRegisterToken,
			DiagnosticSessionID: testDiagnosticSessionID + "a",
		}
		resp, err := client.VerifyRegister(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.NotFound, errDiagnosisSessionNotFound, logrus.Fields{"request": request}))
	})
	t.Run("Verify Register with wrong tokenRegister", func(t *testing.T) {
		request := &userGRPC.VerifyRegisterRequest{
			RegisterToken:       testRegisterToken + "a",
			DiagnosticSessionID: testDiagnosticSessionID,
		}
		resp, err := client.VerifyRegister(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.Unauthenticated, errUnauthorizedAccess, logrus.Fields{"request": request}))
	})
	t.Run("Login", func(t *testing.T) {
		resp, err := client.Login(ctx, &userGRPC.LoginRequest{
			AuthData: &userGRPC.AuthData{
				Email:               testEmail,
				Password:            testPassword,
				DiagnosticSessionID: testDiagnosticSessionID,
			},
		})
		if err != nil {
			t.Fatalf("Failed to login %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.NotEmpty(t, resp.LoginToken)
		assert.NotEmpty(t, resp.PersonID)
	})
	t.Run("Login with wrong credentials", func(t *testing.T) {
		request := &userGRPC.LoginRequest{
			AuthData: &userGRPC.AuthData{
				Email:               testEmail + "a",
				Password:            testPassword,
				DiagnosticSessionID: testDiagnosticSessionID,
			},
		}
		resp, err := client.Login(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.Unauthenticated, user.ErrEmailOrPasswordDidNotMatch, logrus.Fields{"request": request}))
	})
	t.Run("Forget Password", func(t *testing.T) {
		resp, err := client.ForgetPassword(ctx, &userGRPC.ForgetPasswordRequest{
			Email:               testEmail,
			DiagnosticSessionID: testDiagnosticSessionID,
		})
		if err != nil {
			t.Fatalf("Failed to init forget password %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.Equal(t, resp.Message, sendMailVerification)
	})
	t.Run("Forget Password with wrong email format", func(t *testing.T) {
		request := &userGRPC.ForgetPasswordRequest{
			Email:               "testEmail.com",
			DiagnosticSessionID: testDiagnosticSessionID,
		}
		resp, err := client.ForgetPassword(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.InvalidArgument, errValidationFailed, logrus.Fields{"request": request}))
	})
	t.Run("Forget Password with wrong diagnosticSessionID", func(t *testing.T) {
		request := &userGRPC.ForgetPasswordRequest{
			Email:               testEmail,
			DiagnosticSessionID: testDiagnosticSessionID + "a",
		}
		resp, err := client.ForgetPassword(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.NotFound, errDiagnosisSessionNotFound, logrus.Fields{"request": request}))
	})
	t.Run("Forget Password Verification", func(t *testing.T) {
		resp, err := client.ForgetPasswordVerif(ctx, &userGRPC.ForgetPasswordVerifRequest{
			ForgetPwdToken: testRegisterToken,
		})
		if err != nil {
			t.Fatalf("Failed to verify forget password %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.Equal(t, resp.Message, "success")
	})
	t.Run("Forget Password Verification with wrong token", func(t *testing.T) {
		request := &userGRPC.ForgetPasswordVerifRequest{
			ForgetPwdToken: testRegisterToken + "a",
		}
		resp, err := client.ForgetPasswordVerif(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.Unauthenticated, errUnauthorizedAccess, logrus.Fields{"request": request}))
	})
	t.Run("Update Password", func(t *testing.T) {
		resp, err := client.UpdatePassword(ctx, &userGRPC.UpdatePasswordRequest{
			ForgetPasswordToken: testRegisterToken,
			Password:            testPassword,
			ConfirmPassword:     testConfirmationPassword,
			DiagnosticSessionID: testDiagnosticSessionID,
		})
		if err != nil {
			t.Fatalf("Failed to update password %v", err)
		}

		assert.NotEmpty(t, resp)
		assert.Equal(t, resp.Message, "Successfully change the password")
		assert.NotEmpty(t, resp.LoginToken)
	})
	t.Run("Update Password with wrong DiagnosticSessionID", func(t *testing.T) {
		request := &userGRPC.UpdatePasswordRequest{
			ForgetPasswordToken: testRegisterToken,
			Password:            testPassword,
			ConfirmPassword:     testConfirmationPassword,
			DiagnosticSessionID: testDiagnosticSessionID + "a",
		}
		resp, err := client.UpdatePassword(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.NotFound, errDiagnosisSessionNotFound, logrus.Fields{"request": request}))
	})
	t.Run("Update Password does not match with ConfirmPassword", func(t *testing.T) {
		request := &userGRPC.UpdatePasswordRequest{
			ForgetPasswordToken: testRegisterToken,
			Password:            testPassword,
			ConfirmPassword:     testConfirmationPassword + "a",
			DiagnosticSessionID: testDiagnosticSessionID,
		}
		resp, err := client.UpdatePassword(ctx, request)

		assert.Empty(t, resp)
		assert.Equal(t, err, grpcRespondError(ctx, codes.InvalidArgument, errPasswordConfirmationDidNotMatch, logrus.Fields{"request": request}))
	})
	crispData := user.CrispData(sdk.NewCrispClient())
	_, _ = crispData.Client.Website.RemovePeopleProfile(crispData.WebsiteID, testEmail)
}

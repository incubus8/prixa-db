package grpc

import (
	"context"
	"fmt"
	"net/http"
	"time"

	empty "github.com/golang/protobuf/ptypes/empty"
	siloam "github.com/prixa-ai/prixa-proto/proto/siloam/v1"
	telemedicineGRPC "github.com/prixa-ai/prixa-proto/proto/telemedicine/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"

	"prixa.ai/prixa-db/pkg/store"
)

const (
	bookingStatusActive = "Active"
)

var appointmentErrorMessageMaps = map[int32]string{
	400: "Bad Request",
	480: "external appointment not allowed to send appointment number on request body",
	482: "schedule not available.",
	483: "cannot create duplicate appointment on same doctor & hospital.",
	484: "on appointment external, the default parameter should be name, DOB, phone number or contact ID.",
	485: "appointment same day should more than 4 hours from now.",
	486: "appointment time is invalid.",
	487: "time slot is full or already taken.",
	500: "Siloam: Internal Server Error",
}

const (
	statusCodeTimeSlotFull int32 = 487
)

const cancelEventDescription = "Cancel Appointment Booking"

const (
	prixaChannelID = "17"
	prixaUserID    = "Prixa"
	prixaSourceID  = "Prixa"
)

var paymentMethods = []string{
	"Diri Sendiri",
	"Asuransi",
}

func (s *telemedicineAPIHandlerImpl) GetValidPaymentMethodsData(ctx context.Context, emptyReq *empty.Empty) (*telemedicineGRPC.PaymentMethodResponseData, error) {
	paymentMethodsData := make([]*telemedicineGRPC.PaymentMethodData, 0, len(paymentMethods))
	for _, method := range paymentMethods {
		paymentMethodsData = append(paymentMethodsData, &telemedicineGRPC.PaymentMethodData{
			Name: method,
		})
	}

	return &telemedicineGRPC.PaymentMethodResponseData{
		Status:  "OK",
		Message: "Success",
		Data:    paymentMethodsData,
	}, nil
}

func (s *telemedicineAPIHandlerImpl) PostAppointmentBookingData(ctx context.Context, request *telemedicineGRPC.PostAppointmentBookingRequestData) (*telemedicineGRPC.AppointmentBookingResponseData, error) {
	var err error
	if err = request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	appointmentDate, err := time.Parse("2006-01-02", request.AppointmentDate)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	_, err = time.Parse("15:04:00", request.AppointmentTime)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	if err = validatePaymentMethod(request.PaymentMethod); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	doctorData, err := s.store.Hospital().GetDoctorData(request.DoctorId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	hospitalData, err := s.store.Hospital().GetHospitalData(request.HospitalId)
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	response, err := s.siloamAPI.PostAppointmentData(ctx, &siloam.PostAppointmentRequestData{
		ChannelId:           prixaChannelID,
		AppointmentDate:     request.AppointmentDate,
		AppointmentFromTime: request.AppointmentTime,
		ScheduleId:          request.ScheduleId,
		HospitalId:          request.HospitalId,
		DoctorId:            request.DoctorId,
		IsWaitingList:       false,
		Name:                request.Name,
		PhoneNumber1:        request.PhoneNumber,
		EmailAddress:        request.Email,
		BirthDate:           request.BirthDate,
		UserId:              prixaUserID,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	var appointmentData *telemedicineGRPC.AppointmentBookingData
	if response.StatusCode == http.StatusOK {
		appointmentData = &telemedicineGRPC.AppointmentBookingData{
			Id:            response.Data.AppointmentId,
			Name:          request.Name,
			Nik:           request.Nik,
			Email:         request.Email,
			PhoneNumber:   request.PhoneNumber,
			PaymentMethod: request.PaymentMethod,

			AppointmentDate: request.AppointmentDate,
			AppointmentTime: request.AppointmentTime,

			Doctor: &telemedicineGRPC.DoctorData{
				Id:         doctorData.Id,
				Name:       doctorData.Name,
				Speciality: doctorData.Speciality,
			},
			Hospital: &telemedicineGRPC.HospitalData{
				Id:      hospitalData.Id,
				Name:    hospitalData.Name,
				Address: hospitalData.Address,
				Alias:   hospitalData.Alias,
			},
			BookingStatus: bookingStatusActive,
			BirthDate:     request.BirthDate,
			CreatedTime:   time.Now().Format(time.RFC3339),
			ContactId:     response.Data.ContactId,
			ContactName:   response.Data.ContactName,
			ScheduleId:    request.ScheduleId,
		}

		s.store.AppointmentBooking().Save(appointmentData)
		_ = s.store.Doctor().RemoveDoctorTimeSlotsData(request.HospitalId, request.DoctorId, &appointmentDate)
	} else if response.StatusCode == statusCodeTimeSlotFull {
		_ = s.store.Doctor().RemoveDoctorTimeSlotsData(request.HospitalId, request.DoctorId, &appointmentDate)
	}

	message := response.Message
	if response.StatusCode != http.StatusOK {
		errMessage, ok := appointmentErrorMessageMaps[response.StatusCode]
		if ok {
			message = errMessage
		}
	}

	return &telemedicineGRPC.AppointmentBookingResponseData{
		Status:  response.Status,
		Message: message,
		Data:    appointmentData,
	}, nil
}

func (s *telemedicineAPIHandlerImpl) CancelAppointmentBookingData(ctx context.Context, request *telemedicineGRPC.CancelAppointmentBookingRequestData) (*telemedicineGRPC.CancelAppointmentBookingResponseData, error) {
	var err error
	if err = request.Validate(); err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}
	response, err := s.siloamAPI.CancelAppointmentData(ctx, &siloam.CancelAppointmentRequestData{
		AppointmentId: request.BookingId,
		UserId:        prixaUserID,
		Source:        prixaSourceID,
	})
	if err != nil {
		return nil, grpcRespondError(ctx, codes.InvalidArgument, err, logrus.Fields{"request": request})
	}

	cancelAppointmentData := &telemedicineGRPC.CancelAppointmentBookingData{
		BookingId:     response.Data.AppointmentId,
		BookingStatus: response.Data.AppointmentStatus,
		Reason:        request.Reason,
	}

	s.store.AppointmentBooking().CancelAppointmentBooking(&store.Param{
		Ctx:         ctx,
		Description: cancelEventDescription,
		EventName:   "AppointmentBookingCancelled",
		EventType:   store.SuccessEvent,
		Metadata: map[string]string{
			"bookingId":     cancelAppointmentData.BookingId,
			"bookingStatus": cancelAppointmentData.BookingStatus,
			"reason":        cancelAppointmentData.Reason,
		},
	})

	return &telemedicineGRPC.CancelAppointmentBookingResponseData{
		Status:  response.Status,
		Message: response.Message,
		Data:    cancelAppointmentData,
	}, nil
}

func validatePaymentMethod(paymentMethod string) error {
	exist := false
	for _, method := range paymentMethods {
		if method == paymentMethod {
			exist = true
			break
		}
	}

	if !exist {
		return fmt.Errorf("payment Method %v is invalid", paymentMethod)
	}

	return nil
}

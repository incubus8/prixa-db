package grpc

import (
	"testing"

	partnerGRPC "github.com/prixa-ai/prixa-proto/proto/partner/v1"
	partnerAppGRPC "github.com/prixa-ai/prixa-proto/proto/partnerapp/v1"
	"github.com/stretchr/testify/assert"
)

var (
	testPartnerAppNewName = ""
	testPartnerAppID      = ""
)

func TestCreatePartnerApp(t *testing.T) {
	teardown := setup(t, false)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()
	client := partnerAppGRPC.NewPartnerApplicationServiceClient(conn)
	clpartner := partnerGRPC.NewPartnerServiceClient(conn)
	t.Run("Create Partner", func(t *testing.T) {
		resp, err := clpartner.CreatePartner(ctx, &partnerGRPC.CreatePartnerRequest{Name: "Partner F"})
		if err != nil {
			t.Fatalf("Failed to create partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, "Partner F")
		assert.Equal(t, resp.Data.Status, "ACTIVE")
		assert.Equal(t, resp.Data.AppIds, []string(nil))

		testPartnerID = resp.Data.Id
	})
	t.Run("Test Create PartnerApp", func(t *testing.T) {
		resp, err := client.CreatePartnerApplication(ctx, &partnerAppGRPC.CreatePartnerApplicationRequest{Name: "Partner F.app", PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to create partner app %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.SecretKey)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, "Partner F.app")
		assert.Equal(t, resp.Data.Status, "ACTIVE")

		testPartnerAppNewName = "Partner FT.app"
		testPartnerAppID = resp.Data.Id
	})

	t.Run("Test Update PartnerApp", func(t *testing.T) {
		resp, err := client.UpdatePartnerApplication(ctx, &partnerAppGRPC.UpdatePartnerApplicationRequest{Name: testPartnerAppNewName, PartnerId: testPartnerID, ApplicationId: testPartnerAppID})
		if err != nil {
			t.Fatalf("Failed to update partner app %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.SecretKey)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, testPartnerAppNewName)
		assert.Equal(t, resp.Data.Status, "ACTIVE")
	})

	t.Run("Test Get PartnerApp Detail", func(t *testing.T) {
		resp, err := client.GetPartnerApplication(ctx, &partnerAppGRPC.GetPartnerApplicationRequest{PartnerId: testPartnerID, ApplicationId: testPartnerAppID})
		if err != nil {
			t.Fatalf("Failed to get all partner app based on partner %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.SecretKey)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, testPartnerAppNewName)
		assert.Equal(t, resp.Data.Status, "ACTIVE")
	})

	t.Run("Test Delete PartnerApp", func(t *testing.T) {
		resp, err := client.DeletePartnerApplication(ctx, &partnerAppGRPC.DeletePartnerApplicationRequest{PartnerId: testPartnerID, ApplicationId: testPartnerAppID})
		if err != nil {
			t.Fatalf("Failed to delete partner app %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.NotEmpty(t, resp.Data.Id)
		assert.NotEmpty(t, resp.Data.SecretKey)
		assert.NotEmpty(t, resp.Data.CreatedAt)
		assert.NotEmpty(t, resp.Data.UpdatedAt)
		assert.Equal(t, resp.Data.Name, testPartnerAppNewName)
		assert.Equal(t, resp.Data.Status, "DELETED")
	})

	t.Run("Test List PartnerApp", func(t *testing.T) {
		resp, err := client.ListPartnerApplications(ctx, &partnerAppGRPC.ListPartnerApplicationsRequest{PartnerId: testPartnerID})
		if err != nil {
			t.Fatalf("Failed to list partner app %v", err)
		}

		assert.NotEmpty(t, resp.Data)
		assert.Equal(t, len(resp.Data), 1)
		assert.Equal(t, resp.Data[0].Id, testPartnerAppID)
		assert.Equal(t, resp.Data[0].Name, testPartnerAppNewName)
	})
}

package grpc

import (
	"testing"

	analyticGRPC "github.com/prixa-ai/prixa-proto/proto/analytic/v1"
	"github.com/stretchr/testify/assert"
	"prixa.ai/prixa-db/pkg/store"
)

func TestAnalyticAPI(t *testing.T) {
	teardown := setup(t, true)
	defer teardown(t)

	conn, ctx, err := runClient()
	if err != nil {
		t.Fatalf("Failed to dial bufnet %v", err)
	}

	defer conn.Close()

	testName := "UserTest1"

	client := analyticGRPC.NewAnalyticServiceClient(conn)
	t.Run("Track Click Event", func(t *testing.T) {
		request := &analyticGRPC.TrackEvent{
			UserId: testName,
			Event:  store.SuccessEvent,
			SentAt: "2020-04-15 12:45:23",
			Properties: map[string]string{
				"test": "testing",
			},
			Context: &analyticGRPC.ContextFields{
				App: &analyticGRPC.AppFields{
					Name: "Testing",
				},
			},
		}
		resp, err := client.Track(ctx, request)

		assert.Empty(t, resp)
		assert.Empty(t, err)
	})
}

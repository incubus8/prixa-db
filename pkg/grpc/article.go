package grpc

import (
	"context"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
)

// GetDiseaseArticle gets article data from given disease id
func (s *conversationPageHandlerImpl) GetDiseaseArticle(ctx context.Context, request *diagnosticGRPC.GetDiseaseArticleRequest) (*diagnosticGRPC.GetDiseaseArticleResponse, error) {
	article, err := s.core.Article.GetArticleByDiseaseID(request.GetDiseaseID())
	if err != nil {
		return nil, grpcRespondError(ctx, codes.NotFound, err, logrus.Fields{
			"message": err,
			"request": request,
		})
	}

	disease := s.core.Diseases.Dict[article.DiseaseName]
	differentials := s.core.Differentials.ByDisease(disease)

	symptomsNameIndo := make([]string, 0, len(differentials))
	for _, diff := range differentials {
		symptomsNameIndo = append(symptomsNameIndo, s.core.Symptoms.Dict[diff.Symptom].NameIndo)
	}

	return &diagnosticGRPC.GetDiseaseArticleResponse{
		Id:                  article.Id,
		DiseaseName:         disease.Name,
		DiseaseNameIndo:     article.DiseaseNameIndo,
		Author:              article.Author,
		CheckedBy:           article.CheckedBy,
		Overview:            article.Overview,
		Advice:              article.Advice,
		NonMedicalTreatment: article.NonMedicalTreatment,
		MedicalTreatment:    article.MedicalTreatment,
		SupportingLabs:      article.SupportingLabs,
		SeeDoctor:           article.SeeDoctor,
		HowToPrevent:        article.HowToPrevent,
		Prevention:          article.Prevention,
		Reference:           article.Reference,
		RelatedSymptom:      symptomsNameIndo,
	}, nil
}

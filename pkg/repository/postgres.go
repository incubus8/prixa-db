package repository

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/PaesslerAG/jsonpath"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/lib/pq"
	"github.com/shuoli84/sqlm"
	"github.com/sirupsen/logrus"

	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/query"
)

// ColumnData defines column map between column name and json path
type ColumnData struct {
	Name     string
	JSONPath string
	Type     string
}

type postgresRepositoryQueryImpl struct {
	query.SQLCommonQueryImpl

	table      string
	columnsMap []ColumnData
}

// NewPostgresRepositoryQuery creates new postgres connection for given column data and table
func NewPostgresRepositoryQuery(db *sql.DB, tableName string, columns []ColumnData) consumercallback.ConsumerCallback {
	pqRepo := &postgresRepositoryQueryImpl{
		table:      tableName,
		columnsMap: columns,
	}
	pqRepo.SetClient(db)
	return pqRepo
}

// HandlePayloadDelivery performs save data for postgres repository
func (p *postgresRepositoryQueryImpl) OnConsume(value json.RawMessage) error {
	unmarshalData := interface{}(nil)
	err := json.Unmarshal(value, &unmarshalData)
	if err != nil {
		return err
	}

	columnsName := make([]string, 0, len(p.columnsMap))
	columnsValue := make([]interface{}, 0, len(p.columnsMap))
	for _, column := range p.columnsMap {
		v, err := jsonpath.Get(column.JSONPath, unmarshalData)
		if err != nil {
			logrus.WithError(err).Warnf("Cannot get value from JSON path: %v", column.JSONPath)
			continue
		}

		validValue, err := getValidValueForPostgres(v, column.Type)
		if err != nil {
			logrus.WithError(err).Warnf("Cannot get valid value: %v", v)
			continue
		}

		columnsValue = append(columnsValue, validValue)
		columnsName = append(columnsName, column.Name)
	}

	counters := make([]string, 0, len(columnsName))
	for counter := range columnsName {
		counters = append(counters, fmt.Sprintf("$%v", counter+1))
	}

	q, _ := sqlm.Build(
		"INSERT INTO", "\""+p.table+"\"", sqlm.F("(1, 2)", columnsName),
		"VALUES", sqlm.F("(1, 2)", counters),
	)

	err = p.Create(q, columnsValue...)
	if err != nil {
		logrus.WithField("q", q).WithField("args", columnsValue).WithError(err).Errorln()
		return err
	}

	return nil
}

func getValidValueForPostgres(value interface{}, valueType string) (interface{}, error) {
	switch valueType {
	case "google.protobuf.timestamp":
		seconds := &timestamp.Timestamp{Seconds: int64(value.(float64))}
		return ptypes.Timestamp(seconds)
	}

	switch value.(type) {

	// Map cases
	case map[string]string,
		map[string]int,
		map[string]int8,
		map[string]int16,
		map[string]int32,
		map[string]int64,
		map[string]float32,
		map[string]float64,
		map[string]interface{}:
		return json.Marshal(value)

	// Array cases
	case []string,
		[]int,
		[]int8,
		[]int16,
		[]int32,
		[]int64,
		[]float32,
		[]float64,
		[]interface{}:
		return pq.Array(value), nil

	default:
		return value, nil
	}
}

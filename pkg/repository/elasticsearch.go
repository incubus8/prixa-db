package repository

import (
	"context"
	"encoding/json"

	"github.com/olivere/elastic/v7"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
)

type elasticSearchRepositoryQueryImpl struct {
	index  string
	client *elastic.Client
	ctx    context.Context
}

func (es *elasticSearchRepositoryQueryImpl) OnConsume(value json.RawMessage) error {
	var indexID string
	var mapsData map[string]interface{}
	if err := json.Unmarshal(value, &mapsData); err != nil {
		logrus.WithError(err).Errorln("cannot unmarshal payload data for elasticsearch in index " + es.index)
		return err
	}
	id, ok := mapsData["id"]
	if ok {
		indexID, _ = id.(string)
	}

	_, err := es.client.Index().Index(es.index).Id(indexID).BodyJson(value).Do(es.ctx)
	return err
}

// NewElasticSearchClient creates new Elasticsearch client
func NewElasticSearchClient(ctx context.Context, index string, esClient *elastic.Client) consumercallback.ConsumerCallback {
	return &elasticSearchRepositoryQueryImpl{
		index:  index,
		client: esClient,
		ctx:    ctx,
	}
}

package repository

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/PaesslerAG/jsonpath"
	"github.com/lib/pq"
	"github.com/shuoli84/sqlm"
	"github.com/sirupsen/logrus"
	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/query"
)

type clickhouseRepositoryQueryImpl struct {
	query.SQLCommonQueryImpl

	table      string
	columnsMap []ColumnData
}

// NewClickhouseRepositoryQuery creates new clickhouse connection for given column data and table
func NewClickhouseRepositoryQuery(db *sql.DB, tableName string, columns []ColumnData) consumercallback.ConsumerCallback {
	clickhouse := &clickhouseRepositoryQueryImpl{
		table:      tableName,
		columnsMap: columns,
	}
	clickhouse.SetClient(db)
	return clickhouse
}

// SaveData performs save data for clickhouse repository
func (p *clickhouseRepositoryQueryImpl) OnConsume(value json.RawMessage) error {
	unmarshalData := interface{}(nil)
	err := json.Unmarshal(value, &unmarshalData)
	if err != nil {
		return err
	}

	columnsName := make([]string, 0, len(p.columnsMap))
	columnsValue := make([]interface{}, 0, len(p.columnsMap))
	for _, column := range p.columnsMap {
		v, err := jsonpath.Get(column.JSONPath, unmarshalData)
		if err != nil {
			logrus.WithError(err).Warnf("Cannot get value from JSON path: %v", column.JSONPath)
			continue
		}

		validValue, err := getValidValueForClickhouse(v)
		if err != nil {
			logrus.WithError(err).Warnf("Cannot get valid value: %v", v)
			continue
		}

		columnsValue = append(columnsValue, validValue)
		columnsName = append(columnsName, column.Name)
	}

	counters := make([]string, 0, len(columnsName))
	for counter := range columnsName {
		counters = append(counters, fmt.Sprintf("$%v", (counter+1)))
	}
	q, _ := sqlm.Build(
		"INSERT INTO", "\""+p.table+"\"", sqlm.F("(1, 2)", columnsName),
		"VALUES", sqlm.F("(1, 2)", counters),
	)
	err = p.Create(q, columnsValue...)
	if err != nil {
		logrus.WithField("q", q).WithField("args", columnsValue).WithError(err).Errorln()
		return err
	}

	return nil
}

func getValidValueForClickhouse(value interface{}) (interface{}, error) {
	switch value.(type) {

	// Map cases
	case map[string]string,
		map[string]int,
		map[string]int8,
		map[string]int16,
		map[string]int32,
		map[string]int64,
		map[string]float32,
		map[string]float64,
		map[string]interface{}:
		return json.Marshal(value)

	// Array cases
	case []string,
		[]int,
		[]int8,
		[]int16,
		[]int32,
		[]int64,
		[]float32,
		[]float64,
		[]interface{}:
		return pq.Array(value), nil
	default:
		return value, nil
	}
}

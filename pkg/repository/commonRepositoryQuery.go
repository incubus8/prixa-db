package repository

import (
	"encoding/json"
)

// CommonRepositoryQuery is
type CommonRepositoryQuery interface {
	HandlePayloadDelivery(payload json.RawMessage) error
}

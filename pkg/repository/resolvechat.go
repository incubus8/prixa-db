package repository

import (
	"encoding/json"

	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

type resolveChatConsumerService struct {
	store store.Store
}

// NewResolveChatConsumerService is
func NewResolveChatConsumerService(store store.Store) consumercallback.ConsumerCallback {
	aacs := &resolveChatConsumerService{
		store: store,
	}
	return aacs
}

// OnConsume is
func (p *resolveChatConsumerService) OnConsume(value json.RawMessage) error {
	var request *model.ResolveChatRequest
	err := json.Unmarshal(value, &request)
	if err != nil {
		return err
	}

	err = p.store.ChatResult().ProcessingResolveChatRequest(request)
	return err
}

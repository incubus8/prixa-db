package repository

import (
	"encoding/json"

	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

type setDoctorStatusConsumerService struct {
	store store.Store
}

// NewSetDoctorStatusConsumerService is
func NewSetDoctorStatusConsumerService(store store.Store) consumercallback.ConsumerCallback {
	aacs := &setDoctorStatusConsumerService{
		store: store,
	}
	return aacs
}

// OnConsume is
func (p *setDoctorStatusConsumerService) OnConsume(value json.RawMessage) error {
	var request *model.SetDoctorStatusRequest
	err := json.Unmarshal(value, &request)
	if err != nil {
		return err
	}
	err = p.store.DoctorAllocation().SetDoctorStatus(request)
	return err
}

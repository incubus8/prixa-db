package repository

import (
	"encoding/json"

	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/model"
	"prixa.ai/prixa-db/pkg/store"
)

type agentAllocationConsumerService struct {
	store store.Store
}

// NewAgentAllocationConsumerService creates new postgres connection for given column data and table
func NewAgentAllocationConsumerService(store store.Store) consumercallback.ConsumerCallback {
	aacs := &agentAllocationConsumerService{
		store: store,
	}
	return aacs
}

func (p *agentAllocationConsumerService) OnConsume(value json.RawMessage) error {
	var chatRequest *model.ChatRequest
	err := json.Unmarshal(value, &chatRequest)
	if err != nil {
		return err
	}

	if err := validatePartner(chatRequest); err != nil {
		return err
	}

	err = p.store.ChatResult().ProcessingChatRequest(chatRequest)
	return err
}

func validatePartner(chat *model.ChatRequest) error {
	return nil
}

package repository

import (
	"context"
	"encoding/json"
	"strings"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/messaging/consumercallback"
	"prixa.ai/prixa-db/pkg/util"
)

type meilisearchRepositoryQueryImpl struct {
	index  string
	client *meili.Client
	ctx    context.Context
}

type tableHandler func(map[string]interface{}) map[string]interface{}

func mapTableHandle() map[string]tableHandler {
	return map[string]tableHandler{
		"symptoms":                meiliSymptomHandle,
		"contentcard":             meiliContentCardHandle,
		"user-preconditions":      meiliUserPreconditionsHandle,
		"user-assessment-history": meiliUserAssessmentHistoriesHandle,
		"hospitals":               meiliHospitalsHandle,
		"doctors":                 meiliDoctorsHandle,
		"doctorschedules":         meiliDoctorSchedulesHandle,
		"triages":                 meiliTriagesHandle,
	}
}

func meiliSymptomHandle(data map[string]interface{}) map[string]interface{} {
	fields := cast.ToStringMap(data["fields"])
	if fields["Keyword"] != nil {
		data["keyword"] = fields["Keyword"]
	}
	if fields["Name"] != nil {
		data["Name"] = fields["Name"]
	}
	if fields["Name Indo"] != nil {
		data["Name Indo"] = fields["Name Indo"]
	}

	return data
}

func meiliTriagesHandle(data map[string]interface{}) map[string]interface{} {
	fields := cast.ToStringMap(data["fields"])
	if fields["Name"] != nil {
		data["Name"] = fields["Name"]
	}
	if fields["Name Indo"] != nil {
		data["Name Indo"] = fields["Name Indo"]
	}
	return data
}

func meiliContentCardHandle(data map[string]interface{}) map[string]interface{} {
	fields := cast.ToStringMap(data["fields"])
	ccType := cast.ToString(fields["type"])
	data["type"] = ccType
	return data
}

func meiliUserPreconditionsHandle(data map[string]interface{}) map[string]interface{} {
	datetime := cast.ToStringMap(data["date"])
	data["createdAt"] = cast.ToInt(datetime["seconds"])
	return data
}

func meiliUserAssessmentHistoriesHandle(data map[string]interface{}) map[string]interface{} {
	datetime := cast.ToStringMap(data["datetime"])
	data["createdAt"] = cast.ToInt(datetime["seconds"])
	return data
}

func meiliHospitalsHandle(data map[string]interface{}) map[string]interface{} {
	specialities := cast.ToSlice(data["specialities"])
	specialitiesID := []string{}
	for _, speciality := range specialities {
		castedSpeciality := cast.ToStringMap(speciality)
		specialitiesID = append(specialitiesID, cast.ToString(castedSpeciality["id"]))
	}
	data["specialitiesID"] = specialitiesID
	return data
}

func meiliDoctorsHandle(data map[string]interface{}) map[string]interface{} {
	speciality := cast.ToStringMap(data["speciality"])
	data["specialityID"] = cast.ToString(speciality["id"])

	hospitalsAreaID := []string{}
	hospitalsID := []string{}
	hospitals := cast.ToSlice(data["hospitals"])
	for _, hospital := range hospitals {
		hospitalData := cast.ToStringMap(hospital)
		hospitalsAreaID = append(hospitalsAreaID, cast.ToString(hospitalData["areaId"]))
		hospitalsID = append(hospitalsID, cast.ToString(hospitalData["id"]))
	}

	data["hospitalsAreaID"] = hospitalsAreaID
	data["hospitalsID"] = hospitalsID
	return data
}

func meiliDoctorSchedulesHandle(data map[string]interface{}) map[string]interface{} {
	doctor := cast.ToStringMap(data["doctor"])
	hospital := cast.ToStringMap(data["hospital"])

	data["doctorID"] = cast.ToString(doctor["id"])
	data["hospitalID"] = cast.ToString(hospital["id"])
	return data
}

var difByEnv = map[string]bool{
	"nalar": true,
	"covid": true,
}

func (ms *meilisearchRepositoryQueryImpl) OnConsume(value json.RawMessage) error {
	var mapsData map[string]interface{}
	if err := json.Unmarshal(value, &mapsData); err != nil {
		logrus.WithError(err).Errorln("cannot unmarshal payload data for meilisearch in index " + ms.index)
		return err
	}
	_, ok := mapsData["id"]
	if !ok {
		mapsData["id"] = util.GetRandomID()
	}

	index := ms.index
	envDetails := strings.Split(ms.index, "_")
	if difByEnv[envDetails[0]] && len(envDetails) == 2 {
		index = envDetails[1]
	}
	handler, ok := mapTableHandle()[index]
	if ok {
		mapsData = handler(mapsData)
	}

	payload := []map[string]interface{}{
		mapsData,
	}
	_, errAdd := ms.client.Documents(ms.index).AddOrUpdate(payload)
	return errAdd
}

// NewMeilisearchClient creates new Meilisearch client
func NewMeilisearchClient(ctx context.Context, index string, msClient *meili.Client) consumercallback.ConsumerCallback {
	return &meilisearchRepositoryQueryImpl{
		index:  index,
		client: msClient,
		ctx:    ctx,
	}
}

package util

import (
	"strings"
	"testing"
)

func TestGenUIShouldProduceUID(t *testing.T) {
	uid := GenUID()
	if !strings.HasPrefix(uid, "uid-") {
		t.Error("Should have uid- as prefix")
	}
}

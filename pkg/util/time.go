package util

import (
	"errors"
	"math"
	"strconv"
	"strings"
	"time"
)

// LayoutFormatDateTime layout format for datetime
const LayoutFormatDateTime string = "2006-01-02T15:04:05.000Z"

// DiffYearWithTime returns difference between a date with current date,
// will return parsing error if the date format is wrong
// date should be in YYYY-MM-DD format
func DiffYearWithTime(date string, currentTime time.Time) (int, error) {
	year, month, day, err := dateComponents(date)
	if err != nil {
		return 0, err
	}
	location, _ := time.LoadLocation("Asia/Jakarta")
	birthTime := time.Date(year, time.Month(month), day, 0, 0, 0, 0, location)
	age := DiffYear(birthTime, currentTime)
	return age, nil
}

// DiffYear returns difference of year between 2 times
// it's also calculates leap year
func DiffYear(startTime, endTime time.Time) int {
	leapHours := float64(NumOfLeapDays(startTime, endTime)) * 24.0
	numOfHoursInAYear := 365.0 * 24.0
	duration := endTime.Sub(startTime).Hours()
	years := (duration - leapHours) / numOfHoursInAYear

	return int(math.Floor(math.Abs(years)))
}

// IsLeapYear return whether a year is leap year or not
func IsLeapYear(year int) bool {
	if year < 0 {
		return false
	}
	location, _ := time.LoadLocation("Asia/Jakarta")
	theDate := time.Date(year, time.December, 31, 0, 0, 0, 0, location)

	numOfDays := theDate.YearDay()

	return numOfDays > 365
}

// NumOfLeapDays returns
func NumOfLeapDays(fromTime, toTime time.Time) int {
	results, err := NumOfLeapYears(fromTime.Year(), toTime.Year())
	if err != nil {
		return 0
	}
	numOfLeapDays := len(results)
	if toTime.Month() > 2 && IsLeapYear(toTime.Year()) {
		numOfLeapDays++
	}
	return numOfLeapDays
}

// NumOfLeapYears returns how many leap year in the range of fromYear < toYear (not inclusive)
// returns error if fromYear is greater than toYear
func NumOfLeapYears(fromYear, toYear int) ([]int, error) {
	if fromYear >= toYear {
		return nil, errors.New("fromYear should be greater than toYear")
	}
	leapYears := []int{}
	for year := fromYear; year < toYear; year++ {
		if IsLeapYear(year) {
			leapYears = append(leapYears, year)
		}
	}
	return leapYears, nil
}

// dateComponents splits date in YYYY-MM-DD format to its
// components: year, month, date
func dateComponents(date string) (int, int, int, error) {
	components := strings.Split(date, "-")
	if len(components) == 3 {
		var year int
		var month int
		var day int
		var err error
		year, err = strconv.Atoi(components[0])
		if err != nil {
			return 0, 0, 0, err
		}
		month, err = strconv.Atoi(components[1])
		if err != nil {
			return 0, 0, 0, err
		}
		day, err = strconv.Atoi(components[2])
		if err != nil {
			return 0, 0, 0, err
		}
		return year, month, day, nil
	}
	return 0, 0, 0, nil
}

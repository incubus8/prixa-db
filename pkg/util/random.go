package util

import (
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

// RandString produces random string contains variations from letter bytes with length of N
func RandString(n int) string {
	sb := strings.Builder{}
	sb.Grow(n)

	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return sb.String()
}

// GetRandomID produces random ID based on UUID
func GetRandomID() string {
	_uuid, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}

	return _uuid.String()
}

// GenUID generates UID
func GenUID() string {
	return "uid-" + GetRandomID()
}

// RanStrWithLength generates random string with specified length
func RanStrWithLength(n int) string {
	letterBytesCapital := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterBytesCapital[rand.Intn(len(letterBytesCapital))]
	}
	return string(b)
}

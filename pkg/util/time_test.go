package util

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// DiffYear
func TestDiffYearNotBirthdayYet(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	ti := time.Date(2019, time.Month(10), 7, 0, 0, 0, 0, location)
	diff, err := DiffYearWithTime("1987-11-13", ti) // should be 31, not 32 since it's not birthday yet
	assert.Equal(t, err, nil)
	assert.Equal(t, 31, diff)
}

func TestDiffYearPassBirthDay(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	ti := time.Date(2019, time.Month(11), 18, 0, 0, 0, 0, location) // pass birthday
	diff, err := DiffYearWithTime("1987-11-13", ti)
	assert.Equal(t, err, nil)
	assert.Equal(t, 32, diff)
}

func TestDiffYearOnBirthday(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	ti := time.Date(2019, time.Month(11), 13, 0, 0, 0, 0, location) // pass birthday
	diff, err := DiffYearWithTime("1987-11-13", ti)
	assert.Equal(t, err, nil)
	assert.Equal(t, 32, diff)
}

func TestDiffYearDuration(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	ti := time.Date(1987, time.Month(11), 13, 0, 0, 0, 0, location)
	diff, err := DiffYearWithTime("2019-10-07", ti)
	assert.Equal(t, err, nil)
	assert.Equal(t, 31, diff)
}

func TestDiffYearHandlingWrongFormat(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	ti := time.Date(1987, time.Month(11), 13, 0, 0, 0, 0, location)
	diff, err := DiffYearWithTime("2019-10-1OO", ti)
	assert.NotEqual(t, nil, err)
	assert.Equal(t, 0, diff)
}

// LeapYear

func TestIsLeapYear(t *testing.T) {
	assert.Equal(t, true, IsLeapYear(0))
	assert.Equal(t, true, IsLeapYear(1988))
	assert.Equal(t, true, IsLeapYear(1992))
	assert.Equal(t, true, IsLeapYear(2000))
	assert.Equal(t, true, IsLeapYear(2008))
}

func TestIsNotLeapYear(t *testing.T) {
	assert.Equal(t, false, IsLeapYear(-2000))
	assert.Equal(t, false, IsLeapYear(1))
	assert.Equal(t, false, IsLeapYear(2019))
	assert.Equal(t, false, IsLeapYear(2001))
	assert.Equal(t, false, IsLeapYear(2178))
}

func TestNumOfLeapYear(t *testing.T) {
	num, _ := NumOfLeapYears(2000, 2004)
	assert.Equal(t, len(num), 1)
}

func TestNumOfLeapYearNoResult(t *testing.T) {
	num, _ := NumOfLeapYears(2001, 2004)
	assert.Equal(t, len(num), 0)
}

func TestNumOfLeapDays(t *testing.T) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	from := time.Date(1987, time.Month(10), 7, 0, 0, 0, 0, location)

	location2, _ := time.LoadLocation("Asia/Jakarta")
	to := time.Date(2019, time.Month(11), 18, 0, 0, 0, 0, location2) // pass birthday
	leapDays := NumOfLeapDays(from, to)
	assert.Equal(t, 8, leapDays)
}

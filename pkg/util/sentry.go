package util

import (
	"os"
	"strconv"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/sirupsen/logrus"
)

func getDsn() string {
	dsnStr, _ := os.LookupEnv("SENTRY_DSN")

	return dsnStr
}

func getDebugVal() bool {
	debugStr, _ := os.LookupEnv("SENTRY_DEBUG")
	debug, _ := strconv.ParseBool(debugStr)

	return debug
}

// InitSentry initializes Sentry DSN
func InitSentry() {
	dsn := getDsn()
	if dsn == "" {
		logrus.WithField("sentryDSN", dsn).Warnln("Sentry DSN value is empty. Sentry will not be initialized")
		return
	}

	debug := getDebugVal()

	opts := sentry.ClientOptions{
		Dsn:              dsn,
		Debug:            debug,
		AttachStacktrace: true,
	}
	err := sentry.Init(opts)
	if err != nil {
		logrus.WithField("sentryDSN", dsn).WithField("sentryOpts", opts).Errorf("Sentry initialization failed %v\n", err)
	}
}

// RecoverWithSentry to recover error and send error to sentry
func RecoverWithSentry() {
	err := recover()
	if err != nil {
		sentry.CurrentHub().Recover(err)
		sentry.Flush(time.Second * 3)
	}
}

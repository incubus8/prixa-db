package util

import "github.com/microcosm-cc/bluemonday"

// SanitizeText sanitizes text from any undesired free text input
// This should be commonly used to prevent any XSS input attack
func SanitizeText(input string) string {
	policy := bluemonday.UGCPolicy()
	sanitizeText := policy.Sanitize(input)
	return sanitizeText
}

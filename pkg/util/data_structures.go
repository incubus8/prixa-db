package util

import (
	"encoding/json"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

// IsEmptyBody checks array of JSON formatted array of bytes
// if all values are empty string,
// the JSON should be string values
func IsEmptyBody(body []byte) (bool, error) {
	aMap := map[string]string{}
	err := json.Unmarshal(body, &aMap)
	if err != nil {
		return false, err
	}

	numberOfElements := len(aMap)
	counter := 0
	for _, value := range aMap {
		if value == "" {
			counter++
		}
	}
	return (counter == numberOfElements), nil
}

// StringIn checks whether in array there's a specific string
// returns true if found, false otherwise
func StringIn(array []string, item string) bool {
	for _, s := range array {
		if s == item {
			return true
		}
	}

	return false
}

// Intersect returns unique intersection of sets between two slices
func Intersect(first []string, second []string) []string {
	intersected := []string{}
	cached := map[string]string{}

	for _, fi := range first {
		for index, sec := range second {
			if fi == sec {
				// make sure it is unique
				cached[sec] = fmt.Sprintf("%v", index)
			}
		}
	}

	for key := range cached {
		intersected = append(intersected, key)
	}
	return intersected
}

// LogrusFieldsToMapString creates map from logrus.Fields to map[string]string
func LogrusFieldsToMapString(src logrus.Fields) map[string]string {
	mapString := map[string]string{}

	for key, value := range src {
		strKey := cast.ToString(key)
		strValue := cast.ToString(value)

		mapString[strKey] = strValue
	}
	return mapString
}

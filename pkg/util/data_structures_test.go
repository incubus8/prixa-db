package util

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestLogrusFieldsToMapString(t *testing.T) {
	v := logrus.Fields{
		"a": "1",
		"b": 1,
		"c": nil,
		"":  "",
	}
	res := LogrusFieldsToMapString(v)
	assert.Equal(t, v["a"], res["a"])
	assert.Equal(t, "1", res["b"])
	assert.Equal(t, "", res["c"])
	assert.Equal(t, "", res[""])
}

/**
 * IsEmptyBody
 */

func TestIsEmptyBodyTrue(t *testing.T) {
	someJSON := []byte(`{ "user": "", "ssn": "" }`)
	result, _ := IsEmptyBody(someJSON)
	assert.Equal(t, result, true)
}

func TestIsEmptyBodyFalse(t *testing.T) {
	someJSON := []byte(`{ "user": "maradona", "ssn": "" }`)
	result, _ := IsEmptyBody(someJSON)
	assert.Equal(t, result, false)
}

/**
 * Intersect
 */
func TestIntersectNormal(t *testing.T) {
	source := []string{"aku", "juga"}
	itemsTest := []string{"kamu", "jika", "aku", "dirimu", "juga"}

	for _, s := range Intersect(source, itemsTest) {
		has := false
		for _, e := range source {
			if e == s {
				has = true
			}
		}
		assert.Equal(t, has, true)
	}
}

func TestIntersectNoDuplicates(t *testing.T) {
	source := []string{"aku", "juga"}
	itemsTest := []string{"kamu", "juga", "jika", "aku", "aku", "dirimu", "juga"}

	for _, s := range Intersect(source, itemsTest) {
		has := false
		for _, e := range source {
			if e == s {
				has = true
			}
		}
		assert.Equal(t, has, true)
	}
}

func TestContainsNoIntersectionWithEmptySlice(t *testing.T) {
	source := []string{"aku", "juga"}
	itemsTest := []string{}

	assert.Equal(t, Intersect(source, itemsTest), []string{})
}

func TestContainsNoIntersectionWithWords(t *testing.T) {
	source := []string{"aku", "juga"}
	itemsTest := []string{"kamu", "dia"}

	assert.Equal(t, Intersect(source, itemsTest), []string{})
}

/**
 * StringIn
 */

func TestStringInTrue(t *testing.T) {
	res := StringIn([]string{"dia", "dirinya"}, "dia")
	assert.Equal(t, res, true)
}

func TestStringInFalse(t *testing.T) {
	res := StringIn([]string{"dia", "dirinya"}, "kami")
	assert.Equal(t, res, false)
}

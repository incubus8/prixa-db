package config

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2/google"

	"github.com/hashicorp/consul/api"

	"prixa.ai/prixa-db/pkg/log"
	"prixa.ai/prixa-db/pkg/services/stackdriver"
	"prixa.ai/prixa-db/pkg/util"
)

// setFromEnvFile gets config from .env
func setFromEnvFile() error {
	if err := godotenv.Load(); err != nil {
		logrus.WithError(err).Println("No .env file found")
		return err
	}
	return nil
}

type consul struct {
	ctx context.Context

	name   string
	client *api.Client
}

// NewConsulManager sets consul as configuration manager
func NewConsulManager(ctx context.Context) (ConfigurationManager, error) {
	_ = setFromEnvFile()

	root := os.Getenv("HOSTNAME")
	consul, err := setConsulConfig(root)
	if err != nil {
		logrus.WithError(err).Errorln("Error connecting to Consul")
		return nil, err
	}

	consul.ctx = ctx

	return consul, nil
}

// setConsulConfig creates config from consul top name key
func setConsulConfig(name string) (*consul, error) {
	if name == "" {
		return nil, errors.New("consul name is empty")
	}

	dcName := os.Getenv("CONSUL_DC_NAME")
	if dcName == "" {
		return nil, errors.New("consul data center name is empty")
	}

	// Get a new client
	config := api.DefaultConfig()
	config.Datacenter = dcName

	client, err := api.NewClient(config)
	if err != nil {
		return nil, err
	}

	return &consul{name: name, client: client}, nil
}

// GetConfiguration get consul value from path
func (c *consul) GetConfiguration(configPath string) (json.RawMessage, error) {
	// Get a handle to the KV API
	kv := c.client.KV()

	// Lookup the pair
	pair, _, err := kv.Get(c.name+configPath, nil)
	if err != nil {
		return nil, err
	}

	if pair == nil {
		return nil, fmt.Errorf("can't get file %v in consul", c.name+configPath)
	}

	return pair.Value, nil
}

// LoadEnvVariable to get config from consul and set to env variable
func (c *consul) LoadEnvVariable(configPath string) error {
	consulValue, err := c.GetConfiguration(configPath)
	if err != nil {
		logrus.WithField("path", configPath).WithError(err).Errorln("error loading env from consul")
		return err
	}

	var data map[string]string
	err = json.Unmarshal(consulValue, &data)
	if err != nil {
		logrus.WithField("consulValue", string(consulValue)).WithError(err).Errorln("failed to unmarshal consul value")
		return err
	}

	for k, v := range data {
		err = os.Setenv(k, v)
		if err != nil {
			logrus.WithField("key", k).WithField("value", v).WithError(err).Warningln("failed to set env variable")
			continue
		}
	}
	return nil
}

func defaultAuthScopes() []string {
	return []string{
		"https://www.googleapis.com/auth/cloud-platform",
		"https://www.googleapis.com/auth/trace.append",
		"https://www.googleapis.com/auth/logging.write",
	}
}

func (c *consul) LoadStackdriverCredentials() (*google.Credentials, error) {
	// Get a handle to the KV API
	kv := c.client.KV()

	// Lookup the pair
	pair, _, err := kv.Get(c.name+"/stackdriver-sa", nil)
	if err != nil {
		return nil, err
	}

	if pair == nil {
		return nil, fmt.Errorf("can't get file %v/stackdriver-sa in consul", c.name)
	}

	creds, err := google.CredentialsFromJSON(c.ctx, pair.Value, defaultAuthScopes()...)
	if err != nil {
		return nil, fmt.Errorf("can't load Google SA JSON: %v", err)
	}

	err = os.Setenv("GOOGLE_PROJECT_ID", creds.ProjectID)
	if err != nil {
		return nil, fmt.Errorf("can't set GOOGLE_PROJECT_ID env: %v", err)
	}

	if os.Getenv("GOOGLE_STACKDRIVER_LOG_NAME") == "" {
		err = os.Setenv("GOOGLE_STACKDRIVER_LOG_NAME", creds.ProjectID)
		if err != nil {
			return nil, fmt.Errorf("can't set GOOGLE_STACKDRIVER_LOG_NAME env: %v", err)
		}
	}

	return creds, nil
}

// Init is
func (c *consul) Init(baseConfig string) error {
	if err := c.LoadEnvVariable("/" + baseConfig); err != nil {
		logrus.Warnf("can't load from Consul: %v", err)
	}
	if err := c.SetCrispPartnerWebsiteID("/crisp"); err != nil {
		logrus.Warnf("can't load from Consul: %v", err)
	}

	creds, err := c.LoadStackdriverCredentials()
	if err != nil {
		return fmt.Errorf("cannot load stackdriver credentials: %v", err)
	}

	sdSvc := stackdriver.NewService(c.ctx, creds)

	lc, err := sdSvc.Logging()
	if err != nil {
		return err
	}

	ec, err := sdSvc.ErrorReporting()
	if err != nil {
		return err
	}

	util.InitSentry()
	log.Setup()
	log.AddSentryHook()
	log.AddStackdriverLoggingHook(lc, ec)

	// Trace
	if err := sdSvc.Trace(); err != nil {
		return fmt.Errorf("cannot load stackdriver trace: %v", err)
	}

	return nil
}

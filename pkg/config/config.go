package config

import (
	"encoding/json"

	"golang.org/x/oauth2/google"
)

// ConfigurationManager is
type ConfigurationManager interface {
	LoadEnvVariable(configPath string) error
	GetConfiguration(configPath string) (json.RawMessage, error)
	LoadStackdriverCredentials() (*google.Credentials, error)
	SetCrispPartnerWebsiteID(configPath string) error
	Init(baseConfig string) error
}

package config

import (
	"encoding/json"

	"github.com/sirupsen/logrus"
)

// PartnerWebsiteID defines a map[string]string which, the key is the pid and appid, joins by ":" and the value is the websiteID from consul
var PartnerWebsiteID map[string]string

// SetCrispPartnerWebsiteID is
func (c *consul) SetCrispPartnerWebsiteID(configPath string) error {
	consulValue, err := c.GetConfiguration(configPath)
	if err != nil {
		logrus.WithField("path", configPath).WithError(err).Errorln("error loading env from consul")
		return err
	}

	err = json.Unmarshal(consulValue, &PartnerWebsiteID)
	if err != nil {
		logrus.WithField("consulValue", string(consulValue)).WithError(err).Errorln("failed to unmarshal consul value")
		return err
	}
	return nil
}

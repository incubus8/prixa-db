package bot

import "strings"

// InsuranceQuestionType represents type
type InsuranceQuestionType string

const (
	haveInsurance       InsuranceQuestionType = "haveInsurance"
	planToHaveInsurance InsuranceQuestionType = "planToHaveInsurance"
)

var insuranceQuestionTypes = []InsuranceQuestionType{haveInsurance, planToHaveInsurance}

// InsuranceTexts map containing text for question about insurance
func InsuranceTexts(bot *Bot) map[InsuranceQuestionType]string {
	return map[InsuranceQuestionType]string{
		haveInsurance:       bot.Messages["askHaveInsurance"],
		planToHaveInsurance: bot.Messages["askPlanToHaveInsurance"],
	}
}

func (bot *Bot) processAskInsurance(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	if session.CurrentIndexInsurance == len(insuranceQuestionTypes) {
		session.State = PreChiefComplaint
		return session, result, nil
	}

	index := session.CurrentIndexInsurance
	insuranceTexts := InsuranceTexts(bot)

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: insuranceTexts[insuranceQuestionTypes[index]],
	})

	result.Action = Action{
		Type: ActionTypeButtons,
		Value: []ButtonValue{
			{
				Label: "Ya",
				Value: string(answerYes),
				Tag:   string(answerYes),
			},
			{
				Label: "Tidak",
				Value: string(answerNo),
				Tag:   string(answerNo),
			},
		},
	}
	result.EventName = string(insuranceQuestionTypes[index])

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		ProgressBar:                session.ProgressBar,
		State:                      ConfirmInsurance,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processConfirmInsurance(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	reply := param.reply
	result := param.result

	value := reply.Value.(string)
	index := session.CurrentIndexInsurance

	progressBar := session.ProgressBar
	progressBar = UpdateCurrentPercentage(progressBar, AddendPercentage)

	answer := strings.EqualFold(value, string(answerYes))

	userInformation := session.UserInformation
	if insuranceQuestionTypes[index] == haveInsurance {
		userInformation.Insurance.HaveInsurance = answer
	} else if insuranceQuestionTypes[index] == planToHaveInsurance {
		userInformation.Insurance.PlanToHaveInsurance = answer
	}

	currentIndexInsurance := session.CurrentIndexInsurance
	currentIndexInsurance++
	if currentIndexInsurance < len(insuranceQuestionTypes) {
		if insuranceQuestionTypes[currentIndexInsurance] == planToHaveInsurance && answer {
			currentIndexInsurance++
		}
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            userInformation,
		CurrentIndexInsurance:      currentIndexInsurance,
		State:                      AskInsurance,
		ProgressBar:                progressBar,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

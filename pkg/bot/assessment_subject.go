package bot

const (
	// answerSelfAssessment defines the answer for self assessment
	answerSelfAssessment = "Diri sendiri"
	// AnswerNotSelfAssessment defines the answer for assessment that meant for others
	AnswerNotSelfAssessment = "Orang lain"
)

func (bot *Bot) processAskAssessmentSubject(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["askAssessmentSubject"],
	})
	result.Action = Action{
		Type: ActionTypeButtons,
		Value: []ButtonValue{
			{
				Label: string(answerSelfAssessment),
				Value: string(answerSelfAssessment),
			},
			{
				Label: string(AnswerNotSelfAssessment),
				Value: string(AnswerNotSelfAssessment),
			},
		},
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		ProgressBar:                session.ProgressBar,
		State:                      AskPreCondition,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

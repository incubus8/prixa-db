package bot

import (
	"fmt"
)

const (
	// ReplyTypeResume represents user "resume" reply
	ReplyTypeResume = "resume"
	// ActionTypeData represents user "data" reply
	ActionTypeData = "data"
	// ActionTypeText represents user "text" reply
	ActionTypeText = "text"
	// ActionTypeButtons represents user "button" reply
	ActionTypeButtons = "button"
	// MessageTypeText represents user "text" message
	MessageTypeText = "text"
)

// Message contains information sent to user.
// Supported types: text
// Message.Description contains string that contains description of the message
// Message.Media contains url from the media, whether it's a video, image, etc
// Message.Source contains the source of the media to credit the owner
type Message struct {
	Type        string      `json:"type"`
	Value       interface{} `json:"value"`
	Explanation string      `json:"explanation"`
	Media       string      `json:"media"`
	Source      string      `json:"source"`
}

// ButtonValue contains information for the button action for users
// ButtonValue.Description contains string that contains description of the button action
type ButtonValue struct {
	Label       string      `json:"label"`
	Value       interface{} `json:"value"`
	Tag         interface{} `json:"tag"`
	Description string      `json:"description"`
}

// Action contains possible action that can be used by user.
// Supported types: button, text, select
type Action struct {
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
}

// Result contains object passed to user.
type Result struct {
	ID          string      `json:"id"`
	ReplyID     string      `json:"replyId"`
	Messages    []Message   `json:"messages"`
	Action      interface{} `json:"action"`
	EventName   string      `json:"eventname"`
	EventLog    interface{} `json:"eventlog"`
	SymptomID   string      `json:"symptomId"`
	PrixaAPIKey string      `json:"prixaAPIKey,omitempty"`
}

// Reply contains user reply
type Reply struct {
	ID    string      `json:"id"`
	Type  string      `json:"type"`
	Value interface{} `json:"value"`
	Tag   interface{} `json:"tag"`
}

// GreetMessage returns greeting with user's first name
func GreetMessage(name string, bot *Bot) string {
	if name == "" {
		return bot.Messages["greeting"]
	}
	return fmt.Sprintf(bot.Messages["greetingName"], name)
}

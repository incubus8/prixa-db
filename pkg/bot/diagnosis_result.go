package bot

import diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"

// DiagnosisResData is the diagnosis result from user assessment
type DiagnosisResData struct {
	Key  string                              `json:"key"`
	Data *diagnosticGRPC.DiagnosisResultData `json:"data"`
}

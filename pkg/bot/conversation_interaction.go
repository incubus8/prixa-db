package bot

import (
	"os"
	"time"

	"prixa.ai/prixa-db/pkg/messaging"
	"prixa.ai/prixa-db/pkg/util"
)

func (bot *Bot) publishConversationInteraction(prevSessionResult SessionResult, result Result, session Session, reply Reply) {
	result.PrixaAPIKey = os.Getenv("X_PRIXA_API_KEY")
	defer func() {
		result.PrixaAPIKey = ""
	}()

	ci := newConversationInteraction(prevSessionResult, result, session, reply)
	ciKey, _ := os.LookupEnv("RABBITMQ_CONVERSATION_QUEUE")

	bot.Publisher.Publish(ci, ciKey, "")
	bot.Publisher.Publish(ci, messaging.QueueNalarEvent, "")
}

// ConversationInteraction represents end to end conversation
type ConversationInteraction struct {
	ID             string      `json:"id"`
	SessionID      string      `json:"session_id"`
	PreviousState  string      `json:"previous_state"`
	PreviousResult interface{} `json:"previous_result"`
	NextState      string      `json:"next_state"`
	Reply          interface{} `json:"reply"`
	Result         interface{} `json:"result"`
	Timestamp      time.Time   `json:"timestamp"`
	PartnerID      string      `json:"partnerID"`
	ApplicationID  string      `json:"applicationID"`
	Latitude       float32     `json:"latitude"`
	Longitude      float32     `json:"longitude"`
	CovidScore     float32     `json:"covidScore"`
	UID            string      `json:"uid"`
}

func newConversationInteraction(prevSessionResult SessionResult, result Result, session Session, reply Reply) ConversationInteraction {
	return ConversationInteraction{
		ID:             util.GetRandomID(),
		SessionID:      session.ID,
		PreviousState:  string(prevSessionResult.Session.State),
		PreviousResult: prevSessionResult.Result,
		NextState:      string(session.State),
		Reply:          reply,
		Result:         result,
		Timestamp:      time.Now(),
		PartnerID:      session.PId,
		ApplicationID:  session.AppID,
		Latitude:       session.Latitude,
		Longitude:      session.Longitude,
		CovidScore:     session.Covid19Data.Score,
		UID:            session.UID,
	}
}

package bot

import (
	"errors"
	"fmt"

	meili "github.com/meilisearch/meilisearch-go"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/messaging"

	"prixa.ai/prixa-db/pkg/util"
)

// State of FSM
type State string

// answer for question
type answer string

const (
	// Initial describes fsm initial state
	Initial State = "initial"
	// AskAssessmentSubject asks who the assessment for
	AskAssessmentSubject State = "askAssessmentSubject"
	// InitialUserInfo describes fsm initialize user info state
	InitialUserInfo State = "initialUserInfo"
	// PreChiefComplaint describes fsm pre-chief complaint state
	PreChiefComplaint State = "preChiefComplaint"
	// AskChiefComplaint describes fsm asking chief complaint state
	AskChiefComplaint State = "askChiefComplaint"
	// ShowComplaint describes fsm show chief complaint state
	ShowComplaint State = "showComplaint"
	// ChooseComplaint describes fsm choose chief complaint state
	ChooseComplaint State = "chooseComplaint"
	// AskSymptom describes fsm asking symptom state
	AskSymptom State = "askSymptom"
	// DoSymptom describes fsm answer symptom state
	DoSymptom State = "doSymptom"
	// AskPropType describes fsm initial state
	AskPropType State = "askPropType"
	// DoPropType describes fsm do prop type question state
	DoPropType State = "doPropType"
	// Diagnosis describes fsm diagnosis state
	Diagnosis State = "diagnosis"
	// Fin describes fsm final state, no further state after this state
	Fin State = "fin"
	// AskInsurance describes fsm asking insurance state
	AskInsurance State = "askInsurance"
	// ConfirmInsurance describes fsm confirm (answer) insurance state
	ConfirmInsurance State = "confirmInsurance"

	// AskPreCondition describes fsm ask precondition state
	AskPreCondition State = "askPrecondition"
	// ConfirmPreCondition describes fsm confirm (answer) precondition state
	ConfirmPreCondition State = "confirmPrecondition"
	// Resume describes fsm resume state
	Resume State = "resume"
	// AskCovid19Tracing state is the state when the bot asks the user about user contact history and local transmission
	AskCovid19Tracing State = "askCovid19Tracing"
	// ConfirmCovid19Tracing state is process users answers on AskCovid19Tracing state
	ConfirmCovid19Tracing State = "processCovid19Tracing"

	// ProfileTypeGender types gender
	ProfileTypeGender = "gender"
	// ProfileTypeAge types age
	ProfileTypeAge = "age"
	// ProfileTypePregnancy types pregnancy
	ProfileTypePregnancy = "pregnancy"
	// ProfileTypeHistory types history
	ProfileTypeHistory = "history"
	// ProfileTypeBmi types BMI
	ProfileTypeBmi = "bmi"
	// ProfileTypeChronicDiseases is
	ProfileTypeChronicDiseases = "chronic diseases"
	// ProfileTypeContactHistoryCovid19 is
	ProfileTypeContactHistoryCovid19 = "contact history COVID19"
	// ProfileTypeLocalTransmission is
	ProfileTypeLocalTransmission = "local transmission COVID19"
	// ProfileTypeHabitRecord is
	ProfileTypeHabitRecord = "riwayat kebiasaan"
	// profileNameAdult types adult
	profileNameAdult = "adult"
	// profileNameChild types child
	profileNameChild = "child"
	// profileNameGeriatrics types geriatrics
	profileNameGeriatrics = "geriatrics"
	// profileNameFemale types female
	profileNameFemale = "female"

	answerYes     answer = "yes"
	answerNo      answer = "no"
	answerUnknown answer = "unknown"
)

// NewBot creates new bot
// TODO: remove redis client implementation to just use interface
func NewBot(core *core.Core, messages map[string]string, publisher messaging.Publisher) *Bot {
	return &Bot{
		core:      core,
		Messages:  messages,
		Publisher: publisher,
	}
}

// Bot contains core data and messages to be replied to user
type Bot struct {
	core      *core.Core
	Messages  map[string]string
	Publisher messaging.Publisher
}

// GetPreconditionsName is to get precondition name indo from a profileTypeMap
func (bot *Bot) GetPreconditionsName(profileTypeMap map[string]map[string]db.Profile) []string {
	var nameIndo []string
	for _, mapProfile := range profileTypeMap {
		for _, profile := range mapProfile {
			nameIndo = append(nameIndo, profile.NameIndo)
		}
	}

	return nameIndo
}

// CreateResult create diagnosis result from data in Session
func (bot *Bot) CreateResult(session Session) DiagnosisResult {
	symptoms := make([]SymptomDesc, 0, len(session.Evidences))
	for _, evidence := range session.Evidences {
		symptoms = append(symptoms, bot.EvidenceToSymptomDesc(evidence))
	}

	return DiagnosisResult{
		User:        session.UserInformation,
		UserDetails: session.UserInformation.GetDetails(),
		Symptoms:    symptoms,
		Profiles:    bot.core.Meta.GetProfiles(session.ProfileIds),
		Diseases:    bot.core.Diagnose(session.Evidences, core.Setting{Profile: session.ProfileIds, Threshold: 0.15}),
	}
}

// EvidenceToSymptomDesc convert evidence to SymptomDesc
func (bot *Bot) EvidenceToSymptomDesc(evidence db.Evidence) SymptomDesc {
	symptom := bot.core.Symptoms.Dict[evidence.SymptomID]
	return SymptomDesc{
		SymptomID:      symptom.ID,
		SymptomName:    symptom.NameIndo,
		Chief:          evidence.IsChiefComplaint,
		Answer:         evidence.Answer,
		SymptomsTriage: bot.GetSymptomTriage(symptom),
		PropNames:      bot.GetPropNames(evidence),
	}
}

// GetPropNames get NameIndo of the properties, from Evidences
func (bot *Bot) GetPropNames(evidence db.Evidence) []string {
	res := make([]string, 0, len(evidence.Props))
	for _, pair := range evidence.Props {
		prop := bot.core.Meta.Props[pair.Prop]
		res = append(res, prop.NameIndo)
	}

	return res
}

// GetSymptomTriage get symptoms' Triages
func (bot *Bot) GetSymptomTriage(symptom db.Symptom) []string {
	res := make([]string, 0, len(symptom.SymptomsTriage))
	for _, id := range symptom.SymptomsTriage {
		symptomTriage := bot.core.Meta.SymptomTriage[id]
		res = append(res, symptomTriage.Name)
	}

	return res
}

// DiagnosisResult represents diagnostic result
type DiagnosisResult struct {
	User        db.User                 `json:"user"`
	UserDetails map[string]interface{}  `json:"user_details"`
	Profiles    []db.Profile            `json:"profiles"`
	Symptoms    []SymptomDesc           `json:"symptoms"`
	Diseases    []core.PotentialDisease `json:"diseases"`
}

// SymptomDesc represents symptoms information
type SymptomDesc struct {
	SymptomID      string   `json:"symptom_id"`
	SymptomName    string   `json:"symptom_name"`
	PropNames      []string `json:"prop_names"`
	Chief          bool     `json:"chief"`
	SymptomsTriage []string `json:"symptoms_triage"`
	Answer         string   `json:"answer"`
}

type fsmProcessParam struct {
	session  Session
	reply    Reply
	result   Result
	version  Version
	msClient *meili.Client
}

type fsmProcessHandler func(fsmProcessParam) (Session, Result, error)

func (bot *Bot) mapStateProcessFSMHandler() map[State]fsmProcessHandler {
	return map[State]fsmProcessHandler{
		InitialUserInfo:       bot.processInitialUserInformation,
		Initial:               bot.processInit,
		AskAssessmentSubject:  bot.processAskAssessmentSubject,
		AskPreCondition:       bot.processAskPrecondition,
		ConfirmPreCondition:   bot.processConfirmPrecondition,
		PreChiefComplaint:     bot.processPreChiefComplaint,
		AskChiefComplaint:     bot.processAskChiefComplaint,
		ShowComplaint:         bot.processShowComplaint,
		ChooseComplaint:       bot.processChooseComplaint,
		AskSymptom:            bot.processAskSymptom,
		DoSymptom:             bot.processSymptom,
		AskPropType:           bot.processAskPropType,
		DoPropType:            bot.processPropType,
		Diagnosis:             bot.processDiagnosis,
		AskInsurance:          bot.processAskInsurance,
		ConfirmInsurance:      bot.processConfirmInsurance,
		Resume:                bot.processResume,
		AskCovid19Tracing:     bot.processAskCovid19Tracing,
		ConfirmCovid19Tracing: bot.processConfirmCovid19Tracing,
	}
}

// Process processes conversation
func (bot *Bot) Process(sessionResult SessionResult, reply Reply, version Version, msClient *meili.Client) (Session, Result, string, error) {
	var fsmHandler fsmProcessHandler
	var ok bool
	var err error
	var nextSession Session

	result := Result{
		ID:      util.GenUID(),
		ReplyID: reply.ID,
	}

	if reply.Type == "result" {
		result.Messages = reply.Value.(Result).Messages
	}

	session := sessionResult.Session
	if reply.Type == ReplyTypeResume {
		fsmHandler, ok = bot.mapStateProcessFSMHandler()[Resume]
	} else {
		fsmHandler, ok = bot.mapStateProcessFSMHandler()[session.State]
	}
	if ok {
		nextSession, result, err = fsmHandler(
			fsmProcessParam{
				session:  session,
				reply:    reply,
				result:   result,
				version:  version,
				msClient: msClient,
			},
		)
	}
	bot.publishConversationInteraction(sessionResult, result, nextSession, reply)

	// if next state is diagnose or no symptom is found
	if nextSession.State != "" && nextSession.State != Fin && result.Action == nil {
		sr := SessionResult{
			Session: nextSession,
			Result:  result,
		}
		return bot.Process(sr, Reply{
			ID:    util.GenUID(),
			Type:  "result",
			Value: result,
		}, version, msClient)
	}

	return nextSession, result, string(session.State), err
}

func (bot *Bot) processInit(param fsmProcessParam) (Session, Result, error) {
	/*
		Processing of greeting and showing start button replied by user click button
	*/
	session := param.session
	version := param.version
	result := param.result

	sessionProcess := session
	if version == Version2 {
		sessionProcess.State = AskAssessmentSubject
	}
	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: GreetMessage(session.UserInformation.FirstName(), bot),
	})

	result.Action = Action{
		Type: ActionTypeButtons,
		Value: []ButtonValue{
			{
				Label: bot.Messages["startAssessment"],
				Value: true,
			},
		},
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		State:                      sessionProcess.State,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processInitialUserInformation(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	sessionProcess := session
	if session.UserInformation.IsEmpty() {
		sessionProcess.State = Initial
		sessionProcess.ProfileTypes = ProfileTypesWithoutUser
		return sessionProcess, result, nil
	}

	if sessionProcess.ProfileTypeMap == nil {
		sessionProcess.ProfileTypeMap = map[string]map[string]db.Profile{}
	}

	// gender
	typename, ID, profile := GetGenderProfile(session.UserInformation.Gender, bot.core.Meta)
	if typename != "" && ID != "" {
		sessionProcess.ProfileTypeMap[typename] = map[string]db.Profile{}
		sessionProcess.ProfileTypeMap[typename][ID] = profile
	}

	// age
	typename, ID, profile = GetAgeProfile(session.UserInformation.Age(), bot.core.Meta)
	if typename != "" && ID != "" {
		sessionProcess.ProfileTypeMap[typename] = map[string]db.Profile{}
		sessionProcess.ProfileTypeMap[typename][ID] = profile
	}

	// bmi
	typename, ID, profile = GetBMIProfile(session.UserInformation.Weight, sessionProcess.UserInformation.Height, bot.core.Meta)
	if typename != "" && ID != "" {
		sessionProcess.ProfileTypeMap[typename] = map[string]db.Profile{}
		sessionProcess.ProfileTypeMap[typename][ID] = profile
	}

	sessionProcess.State = Initial

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      sessionProcess.State,
		ProfileTypes:               sessionProcess.ProfileTypes,
		ProfileTypeMap:             sessionProcess.ProfileTypeMap,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

var resumeStateMapper = map[State]State{
	ConfirmPreCondition:   AskPreCondition,
	ConfirmInsurance:      AskInsurance,
	ConfirmCovid19Tracing: AskCovid19Tracing,
	AskChiefComplaint:     PreChiefComplaint,
	ShowComplaint:         AskChiefComplaint,
	ChooseComplaint:       AskChiefComplaint,
	DoSymptom:             AskSymptom,
	DoPropType:            AskPropType,
	Fin:                   Diagnosis,
}

func (bot *Bot) processResume(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	session.State = resumeStateMapper[session.State]
	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      session.State,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, param.result, nil
}

func (bot *Bot) processPreChiefComplaint(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["preChiefComplaint"],
	})
	result.Action = Action{
		Type: ActionTypeButtons,
		Value: []ButtonValue{
			{
				Label: "Mulai",
				Value: "Mulai",
			},
		},
	}
	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      AskChiefComplaint,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processAskChiefComplaint(param fsmProcessParam) (Session, Result, error) {
	/*
		Processing of asking chief complaint replied by user text
	*/
	result := param.result
	session := param.session

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["askChiefComplaint"],
	})
	result.Action = Action{
		Type:  ActionTypeText,
		Value: bot.Messages["complaintPlaceholder"],
	}
	result.EventName = string(ShowComplaint)
	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      ShowComplaint,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processShowComplaint(param fsmProcessParam) (Session, Result, error) {
	/*
		Processing of searching complaint by user reply then asking confirmation for chief complaint
	*/
	session := param.session
	result := param.result
	reply := param.reply

	var state State

	text, ok := reply.Value.(string)
	if !ok {
		err := errors.New("reply is not text")
		logrus.WithError(err).WithField("session", session).WithField("result", result).Errorln()
		return Session{}, Result{}, err
	}

	var outputNL []string
	potentialSymps := getPotentialSymptoms(bot, text, session.ProfileIds, param.msClient)

	state = ChooseComplaint
	if len(potentialSymps) < 1 {
		result.Messages = append(result.Messages, Message{
			Type:  MessageTypeText,
			Value: bot.Messages["askChiefComplaint"],
		})
		session.State = AskChiefComplaint
		return session, result, nil
	}
	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["chooseComplaint"],
	})

	buttonValues := getButtonValues(potentialSymps)

	result.Action = Action{
		Type:  ActionTypeButtons,
		Value: buttonValues,
	}

	result.EventLog = outputNL

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      state,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func getPotentialSymptoms(bot *Bot, keyword string, profileIDs []string, msClient *meili.Client) db.Symptoms {
	symptomIds := searchSymptomIDFromMeilisearch(keyword, msClient)
	potentialSymptom := getAndFilterPotentialSymptomData(symptomIds, bot.core.Symptoms, bot.core.Meta, profileIDs)
	return potentialSymptom
}

func searchSymptomIDFromMeilisearch(keyword string, msClient *meili.Client) []string {
	table := "symptoms"
	if core.IsForCovid19() {
		table = fmt.Sprintf("covid_%v", table)
	} else {
		table = fmt.Sprintf("nalar_%v", table)
	}
	resp, err := msClient.Search(table).Search(meili.SearchRequest{
		Query: keyword,
	})
	if err != nil {
		return []string{}
	}
	symptomIDs := []string{}
	fmt.Println(resp, err)
	for _, hit := range resp.Hits {
		meiliSympData := cast.ToStringMap(hit)
		if len(cast.ToSlice(meiliSympData["keyword"])) == 0 {
			continue
		}
		record := db.MapToRecord(meiliSympData)
		symptomIDs = append(symptomIDs, record.ID)
	}
	return symptomIDs
}

func getAndFilterPotentialSymptomData(symptomIds []string, symptomTable db.SymptomTable, metaTable db.MetaTable, profileIDs []string) db.Symptoms {
	var symptoms db.Symptoms
	profiles := metaTable.GetProfiles(profileIDs)
	for _, id := range symptomIds {
		symptom, ok := symptomTable.GetSymptomDataByID(id)
		if ok && metaTable.MatchProfile(symptom.PreConditions, profiles) {
			symptoms = append(symptoms, symptom)
		}
	}
	return symptoms
}

func getButtonValues(symptoms db.Symptoms) (buttonValues []ButtonValue) {
	for _, potentialSymp := range symptoms {
		buttonVal := ButtonValue{
			Label:       potentialSymp.NameIndo,
			Value:       potentialSymp.ID,
			Tag:         potentialSymp.Name,
			Description: potentialSymp.Description,
		}

		buttonValues = append(buttonValues, buttonVal)
	}

	buttonValues = append(buttonValues, ButtonValue{
		Label: "Keluhan saya tidak ada dalam pilihan",
		Value: false,
		Tag:   "ComplaintNotInOptions",
	})

	return buttonValues
}

func (bot *Bot) processChooseComplaint(param fsmProcessParam) (Session, Result, error) {
	/*
		Processing of user chosen complaint or if user said "Tidak ada"
	*/
	session := param.session
	reply := param.reply
	result := param.result

	selectedSymp, ok := reply.Value.(string)
	progressBar := session.ProgressBar

	var state State
	currentSymptom := session.CurrentSymptom

	var evidences db.Evidences
	if ok && selectedSymp != "false" {
		evidences = db.FillEvidencesfromSymp(selectedSymp)
		state = AskPropType
		currentSymptom = selectedSymp
		progressBar = UpdateCurrentPercentage(progressBar, AddendPercentage)
		if core.IsForCovid19() {
			session.Covid19Data.Symptom = bot.core.Symptoms.Dict[currentSymptom]
			session.Covid19Data = session.Covid19Data.GetCovid19Score(bot.core.Symptoms.Dict[currentSymptom].ScoringType)
		}
	} else {
		result.Messages = append(result.Messages, Message{
			Type:  MessageTypeText,
			Value: bot.Messages["askChiefComplaint"],
		})
		state = AskChiefComplaint
		evidences = nil
	}
	result.EventName = string(ChooseComplaint)

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  evidences,
		ProgressBar:                progressBar,
		CurrentSymptom:             currentSymptom,
		State:                      state,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processAskSymptom(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	progressBar := session.ProgressBar
	potentialSymptoms := bot.core.Consult(session.Evidences, core.Setting{
		MaxResult: 0,
		Profile:   session.ProfileIds,
	})

	nPotentialSymptoms := len(potentialSymptoms)
	progressBar = UpdateProgressBar(progressBar, nPotentialSymptoms, ZeroNPropTypes, string(session.State))

	state := Diagnosis
	currentSymptom := session.CurrentSymptom
	if len(session.Evidences) < 30 && len(potentialSymptoms) > 0 {
		potentialSymptom := potentialSymptoms[0]

		result.Messages = append(result.Messages, Message{
			Type:        MessageTypeText,
			Value:       potentialSymptom.Question,
			Explanation: potentialSymptom.Explanation,
			Media:       potentialSymptom.Media,
			Source:      potentialSymptom.Source,
		})

		result.Action = Action{
			Type: ActionTypeButtons,
			Value: []ButtonValue{
				{
					Label: "Ya",
					Value: string(answerYes),
					Tag:   string(answerYes),
				},
				{
					Label: "Tidak",
					Value: string(answerNo),
					Tag:   string(answerNo),
				},
				{
					Label: "Tidak Tahu",
					Value: string(answerUnknown),
					Tag:   string(answerUnknown),
				},
			},
		}

		result.SymptomID = potentialSymptom.SID
		result.EventName = "Symptom"
		state = DoSymptom
		currentSymptom = potentialSymptom.ID
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                progressBar,
		CurrentSymptom:             currentSymptom,
		State:                      state,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processSymptom(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	reply := param.reply
	result := param.result

	answer, ok := reply.Value.(string)
	if !ok {
		return Session{}, Result{}, errors.New("reply is not text")
	}

	evidences := session.Evidences.Append(db.Evidence{
		SymptomID: session.CurrentSymptom,
		Answer:    answer,
	})

	currentSymptom := session.CurrentSymptom
	var state State
	if answer == string(answerYes) {
		state = AskPropType
		if core.IsForCovid19() {
			session.Covid19Data.Symptom = bot.core.Symptoms.Dict[currentSymptom]
			session.Covid19Data = session.Covid19Data.GetCovid19Score(bot.core.Symptoms.Dict[currentSymptom].ScoringType)
		}
	} else {
		state = AskSymptom
		currentSymptom = ""
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		ProgressBar:                session.ProgressBar,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  evidences,
		CurrentSymptom:             currentSymptom,
		State:                      state,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processAskPropType(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	sessionProcess := session

	progressBar := session.ProgressBar
	potentialSymptoms := bot.core.Consult(session.Evidences, core.Setting{
		MaxResult: 0,
		Profile:   session.ProfileIds,
	})

	if len(potentialSymptoms) > 0 && potentialSymptoms[0].ID == session.CurrentSymptom {
		potentialSymptom := potentialSymptoms[0]
		propType := potentialSymptom.PropTypes[0]

		nPotentialSymptoms := len(potentialSymptoms)
		nPropTypes := len(potentialSymptom.PropTypes)

		progressBar = UpdateProgressBar(progressBar, nPotentialSymptoms, nPropTypes, string(session.State))

		result.Messages = append(result.Messages, Message{
			Type:  MessageTypeText,
			Value: propType.Question,
		})

		var actionValues []ButtonValue
		for _, option := range propType.Options {
			actionValues = append(actionValues, ButtonValue{
				Label: option.Name,
				Value: option.PropID,
				Tag:   option.Name,
			})
		}

		result.Action = Action{
			Type:  ActionTypeButtons,
			Value: actionValues,
		}
		result.EventName = "properties"
		sessionProcess.State = DoPropType
		sessionProcess.CurrentSymptom = potentialSymptom.ID
		sessionProcess.CurrentPropType = propType.TypeID
	} else {
		sessionProcess.State = AskSymptom
		sessionProcess.CurrentSymptom = ""
		sessionProcess.CurrentPropType = ""
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		Evidences:                  session.Evidences,
		ProgressBar:                progressBar,
		CurrentPropType:            sessionProcess.CurrentPropType,
		CurrentSymptom:             sessionProcess.CurrentSymptom,
		State:                      sessionProcess.State,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processPropType(param fsmProcessParam) (Session, Result, error) {
	reply := param.reply
	session := param.session
	result := param.result

	answer, ok := reply.Value.(string)

	if !ok {
		err := errors.New("reply is not text")
		logrus.WithError(err).Errorln()
		return Session{}, Result{}, err
	}

	evidences := session.Evidences
	evidences = evidences.Append(db.Evidence{
		SymptomID: session.CurrentSymptom,
		Props: []db.PropertyPair{
			{
				Prop: answer,
				Type: session.CurrentPropType,
			},
		},
	})

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		ProgressBar:                session.ProgressBar,
		Evidences:                  evidences,
		State:                      AskPropType,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

// processDiagnosis returns diagnosis result
func (bot *Bot) processDiagnosis(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	session.State = Fin
	diagnosis := bot.CreateResult(session)
	progressBar := UpdateProgressBar(session.ProgressBar, ZeroNPotentialSymptoms, ZeroNPropTypes, string(session.State))

	if len(diagnosis.Diseases) < 1 {
		result.Messages = append(result.Messages, Message{
			Type:  MessageTypeText,
			Value: bot.Messages["unknownAssessment"],
		})
		result.Action = Action{
			Value: "",
		}
		session.State = Fin
		session.ProgressBar = progressBar
		return session, result, nil
	}

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["endAssessment"],
	})
	result.Action = Action{
		Type:  ActionTypeData,
		Value: diagnosis,
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		State:                      Fin,
		ProgressBar:                progressBar,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

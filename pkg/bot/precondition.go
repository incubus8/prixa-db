package bot

import (
	"errors"
	"fmt"
	"strings"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/db"
)

// PreconditionDetail hold mapped precondition object from Airtable
type PreconditionDetail struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// PreconditionAge represents year and month
type PreconditionAge struct {
	Year  int `json:"year"`
	Month int `json:"month"`
}

// PreconditionBmi represents height and weight
type PreconditionBmi struct {
	Height float64 `json:"height"`
	Weight float64 `json:"weight"`
}

// PreconditionData holds preconditions data
type PreconditionData struct {
	Gender              PreconditionDetail   `json:"gender"`
	Age                 PreconditionAge      `json:"age"`
	BMI                 PreconditionBmi      `json:"bmi"`
	History             []PreconditionDetail `json:"history"`
	Pregnancy           PreconditionDetail   `json:"pregnancy"`
	ChronicDisease      []PreconditionDetail `json:"chronic diseases"`
	ContactHistoryCovid []PreconditionDetail `json:"contact history COVID19"`
	LocalTransmission   []PreconditionDetail `json:"local transmission COVID19"`
	HabitRecord         []PreconditionDetail `json:"riwayat kebiasaan"`
}

type profileTypeFunc func(profileType string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile

var preconditionHandlers = map[string]profileTypeFunc{
	ProfileTypeGender:                preconditionGenderHandler,
	ProfileTypeAge:                   preconditionAgeHandler,
	ProfileTypeBmi:                   preconditonBmiHandler,
	ProfileTypePregnancy:             preconditionPregnancyHandler,
	ProfileTypeHistory:               preconditonHistoryHandler,
	ProfileTypeChronicDiseases:       preconditionCommonHandler,
	ProfileTypeContactHistoryCovid19: preconditionCommonHandler,
	ProfileTypeLocalTransmission:     preconditionCommonHandler,
	ProfileTypeHabitRecord:           preconditionCommonHandler,
}

// PreconditionType represents bot type
const PreconditionType string = "Preconditions"

func (bot *Bot) processAskPrecondition(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	result := param.result

	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["askPrecondition"],
	})
	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["askPreconditionInfo"],
	})

	var progressBar ProgressBar
	// check if nextSession.ProgressBar is already initialized.
	if IsProgressBarInitialized(session.ProgressBar) {
		progressBar = session.ProgressBar
	} else {
		progressBar = InitProgressBar()
	}

	actionPrecondition := make(map[string][]db.Profile)
	for _, profileType := range session.ProfileTypes {
		profiles := bot.core.Meta.ProfilesByType(profileType)
		actionPrecondition[profileType] = profiles
	}
	result.Action = Action{
		Type:  PreconditionType,
		Value: actionPrecondition,
	}
	result.EventName = string(AskPreCondition)

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,
		UserInformation:            session.UserInformation,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		State:                      ConfirmPreCondition,
		ProgressBar:                progressBar,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func (bot *Bot) processConfirmPrecondition(param fsmProcessParam) (Session, Result, error) {
	session := param.session
	reply := param.reply
	result := param.result

	// check if nextSession.ProgressBar is already initialized.
	if !IsProgressBarInitialized(session.ProgressBar) {
		session.ProgressBar = InitProgressBar()
	}

	preconditionData, ok := reply.Value.(PreconditionData)
	if !ok {
		return Session{}, Result{}, errors.New("reply is not Precondition Data")
	}
	gender, ok := bot.core.Meta.Profiles[preconditionData.Gender.ID]
	if !ok {
		return Session{}, Result{}, fmt.Errorf("meta profiles are not available for gender IDs %v", preconditionData.Gender.ID)
	}

	profileTypeMap := map[string]map[string]db.Profile{}
	for _, profileType := range session.ProfileTypes {
		preconditionHandler, ok := preconditionHandlers[profileType]
		if ok {
			profileTypeMap[profileType] = map[string]db.Profile{}
			profileMap := preconditionHandler(profileType, preconditionData, bot.core.Meta)
			if len(profileMap) > 0 {
				if core.IsForCovid19() {
					session.Covid19Data.ProfileMap = profileMap
					session.Covid19Data = session.Covid19Data.GetCovid19Score(profileType)
				}
				profileTypeMap[profileType] = profileMap
			}
		}
	}
	result.EventName = string(ConfirmPreCondition)

	nextState := AskChiefComplaint
	if core.IsForCovid19() {
		nextState = AskCovid19Tracing
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		CurrentPropType:            session.CurrentPropType,
		CurrentSymptom:             session.CurrentSymptom,
		Evidences:                  session.Evidences,
		ProfileTypes:               session.ProfileTypes,

		UserInformation: db.User{
			Gender:   gender.Name,
			Height:   preconditionData.BMI.Height,
			Weight:   preconditionData.BMI.Weight,
			AgeYear:  preconditionData.Age.Year,
			AgeMonth: preconditionData.Age.Month,
		},
		ProfileTypeMap: profileTypeMap,
		ProfileIds:     getProfileIdsFromProfileTypeMap(profileTypeMap),
		State:          nextState,
		ProgressBar:    UpdateCurrentPercentage(session.ProgressBar, AddendPercentagePrecondition),
		Latitude:       session.Latitude,
		Longitude:      session.Longitude,
		Covid19Data:    session.Covid19Data,
		UID:            session.UID,
	}, result, nil
}

func preconditionGenderHandler(profileType string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	profile := meta.Profiles[preconditionData.Gender.ID]
	profileMap := map[string]db.Profile{}
	profileMap[preconditionData.Gender.ID] = profile
	return profileMap
}

func preconditionAgeHandler(profileType string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	_, id, profile := GetAgeProfile(preconditionData.Age.Year, meta)
	profileMap := map[string]db.Profile{}
	profileMap[id] = profile
	return profileMap
}

func preconditonBmiHandler(profileType string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	bmiProfiles := meta.ProfilesByType(ProfileTypeBmi)
	profile := GetProfileBmi(preconditionData.BMI.Weight, preconditionData.BMI.Height, bmiProfiles)
	profileMap := map[string]db.Profile{}
	profileMap[profile.ID] = profile
	return profileMap
}

func preconditionPregnancyHandler(_ string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	age := preconditionData.Age.Year
	gender := meta.Profiles[preconditionData.Gender.ID]
	profileMap := map[string]db.Profile{}
	if strings.EqualFold(gender.Name, profileNameFemale) && age >= 17 && age <= 65 {
		profile, ok := meta.Profiles[preconditionData.Pregnancy.ID]
		if ok {
			profileMap[profile.ID] = profile
		}
	}
	return profileMap
}

func preconditonHistoryHandler(_ string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	profileMap := map[string]db.Profile{}
	for _, history := range preconditionData.History {
		profile, ok := meta.Profiles[history.ID]
		if ok {
			profileMap[history.ID] = profile
		}
	}
	return profileMap
}

// GetPreconditionDetails is
func GetPreconditionDetails(profileType string, preconditionData PreconditionData) []PreconditionDetail {
	preconditionDetailMap := map[string][]PreconditionDetail{
		ProfileTypeChronicDiseases:       preconditionData.ChronicDisease,
		ProfileTypeContactHistoryCovid19: preconditionData.ContactHistoryCovid,
		ProfileTypeLocalTransmission:     preconditionData.LocalTransmission,
		ProfileTypeHabitRecord:           preconditionData.HabitRecord,
	}

	return preconditionDetailMap[profileType]
}

func preconditionCommonHandler(profileType string, preconditionData PreconditionData, meta db.MetaTable) map[string]db.Profile {
	profileMap := map[string]db.Profile{}
	precondsData := GetPreconditionDetails(profileType, preconditionData)
	for _, data := range precondsData {
		profile, ok := meta.Profiles[data.ID]
		if ok {
			profileMap[data.ID] = profile
		}
	}
	return profileMap
}

func getProfileIdsFromProfileTypeMap(profileTypeMap map[string]map[string]db.Profile) []string {
	var profileIds []string
	for _, profiles := range profileTypeMap {
		for id := range profiles {
			profileIds = append(profileIds, id)
		}
	}
	return profileIds
}

package bot

const (
	// AddendPercentage Constant Value for preconditions questions until input chief complaint where CurrentPercentage will be add by constant value.
	// The static value used for these steps (preconditions until input chief complaint) is 4
	// Preconditions steps until input chief complaint are using constant value because these states can be modify and can be added with more states,
	//     which we don't know how many states will be add in the future.
	AddendPercentage float64 = 4
	// AddendPercentagePrecondition adds percentage during precondition
	AddendPercentagePrecondition float64 = 20
	// ZeroNPotentialSymptoms used for cases where State doesn't have nPotentialSymptoms or nPropTypes
	ZeroNPotentialSymptoms int = 0
	// ZeroNPropTypes used for n-prop type questions
	ZeroNPropTypes int = 0
)

// ProgressBar is
// LimitPercentage contains the value that limit the progress bar percentage
// CurrentPercentage contains the current percentage value that will be updated for every question
// PrevNPotentialSymptoms contains the number of potential value from the previous UpdateProgressBar
// PrevNPropType contains the number of prop type from the previous UpdateProgressBar
// PreviousState contains the previous state from UpdateProgressBar
// CurrentState contains the current state from UpdateProgressBar
type ProgressBar struct {
	LimitPercentage        float64 `json:"limitPercentage"`
	CurrentPercentage      float64 `json:"currentPercentage"`
	PrevNPotentialSymptoms float64 `json:"prevNPotentialSymptoms"`
	PrevPercentageRate     float64 `json:"prevPercentageRate"`
	PrevNPropType          float64 `json:"prevNPropType"`
	PreviousState          string  `json:"previousState"`
	CurrentState           string  `json:"currentState"`
}

// InitProgressBar initiates the struct for progress bar
func InitProgressBar() ProgressBar {
	progress := ProgressBar{}
	progress.CurrentState = "INIT"
	progress.PreviousState = "INIT"
	progress.LimitPercentage = float64(100)
	progress = SetCurrentPercentage(progress, float64(0))
	return progress
}

// SetCurrentPercentage is setting the value of CurrentPercentage
// val contains the value to be set for CurrentPercentage
func SetCurrentPercentage(progress ProgressBar, val float64) ProgressBar {
	progress.CurrentPercentage = val

	return progress
}

// IsProgressBarInitialized is checking if the given variable is already initialized
func IsProgressBarInitialized(progress ProgressBar) bool {
	return progress != ProgressBar{}
}

// UpdateCurrentPercentage is updating the CurrentPercentage value
// addedPercentage contains the value to be added for the CurrentPercentage
func UpdateCurrentPercentage(progress ProgressBar, addedPercentage float64) ProgressBar {
	progress.CurrentPercentage += addedPercentage

	return progress
}

// UpdateProgressBar is updating the ProgressBar based on the number of Potential symptoms (nPotentialSymptoms)
// and the number of Prop Type(nPropTypes)
func UpdateProgressBar(progress ProgressBar, nPotentialSymptoms int, nPropTypes int, state string) ProgressBar {
	progress.CurrentState = state

	// handle if nPropTypes value is 0, which will be an error because nPropTypes act as denominator
	if nPropTypes == 0 {
		nPropTypes = 1
	}

	// handle if nPotentialSymptoms is 0, which will be an error because nPotentialSymptoms act as denominator
	if nPotentialSymptoms == 0 {
		nPotentialSymptoms = 1
	}

	if progress.PrevNPotentialSymptoms > float64(nPotentialSymptoms) {
		progress.CurrentPercentage += (progress.PrevNPotentialSymptoms - float64(nPotentialSymptoms)) * progress.PrevPercentageRate
	}

	remainingPercentage := progress.LimitPercentage - progress.CurrentPercentage

	percentagePerSymptomProgress := remainingPercentage / float64(nPotentialSymptoms)

	var addedPercentage float64

	if (progress.PreviousState == "INIT" && progress.CurrentState == "askPropType") || progress.CurrentState == "fin" {
		addedPercentage = percentagePerSymptomProgress / float64(nPropTypes)
		progress.PrevNPropType = float64(nPropTypes)
	} else if progress.PreviousState == "askSymptom" && progress.CurrentState == "askPropType" {
		progress.CurrentPercentage -= percentagePerSymptomProgress
		addedPercentage = percentagePerSymptomProgress / float64(nPropTypes)
		progress.PrevNPropType = float64(nPropTypes)
	} else if progress.PreviousState == "askPropType" && progress.CurrentState == "askPropType" {
		addedPercentage = percentagePerSymptomProgress / progress.PrevNPropType
	} else if progress.PreviousState == "askPropType" && progress.CurrentState == "askSymptom" {
		addedPercentage = percentagePerSymptomProgress
	}

	progress.CurrentPercentage += addedPercentage
	progress.PrevNPotentialSymptoms = float64(nPotentialSymptoms)
	progress.PreviousState = progress.CurrentState
	progress.PrevPercentageRate = percentagePerSymptomProgress

	return progress
}

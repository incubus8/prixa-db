package bot

import (
	"context"
	"strings"

	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"

	"prixa.ai/prixa-db/pkg/core"
	"prixa.ai/prixa-db/pkg/db"
	"prixa.ai/prixa-db/pkg/util"
)

// Session holds session data
type Session struct {
	ID                         string                           `json:"id"`
	PId                        string                           `json:"pId"`
	AppID                      string                           `json:"appId"`
	State                      State                            `json:"state"`
	UserInformation            db.User                          `json:"userInformation"`
	CurrentSymptom             string                           `json:"currentSymptom"`
	CurrentPropType            string                           `json:"currentPropType"`
	Evidences                  db.Evidences                     `json:"evidences"`
	ProfileTypes               []string                         `json:"profileTypes"`
	ProfileTypeMap             map[string]map[string]db.Profile `json:"profileTypeMap"`
	ProfileIds                 []string                         `json:"profileIds"`
	CurrentIndexProfile        int                              `json:"currentIndexProfile"`
	CurrentIndexProfileBmi     int                              `json:"currentIndexProfileBmi"`
	CurrentIndexProfileHistory int                              `json:"currentIndexProfileHistory"`
	ProgressBar                ProgressBar                      `json:"progressBar"`
	CurrentIndexInsurance      int                              `json:"currentIndexInsurance"`
	Latitude                   float32                          `json:"latitude"`
	Longitude                  float32                          `json:"longitude"`
	Covid19Data                *core.Covid19Data                `json:"covid19Data"`
	UID                        string                           `json:"uid"`
}

// NewSession creates new session with initial state
func NewSession(ctx context.Context) Session {
	incomingMds := metautils.ExtractIncoming(ctx)

	return Session{
		ID:           util.GetRandomID(),
		State:        Initial,
		PId:          incomingMds.Get("x-partner-id"),
		AppID:        incomingMds.Get("x-partner-app-id"),
		ProfileTypes: ProfileTypesWithoutUser,
		Covid19Data: &core.Covid19Data{
			Score:                                0.0,
			IsEmergencyRespiratorySympPresent:    false,
			IsNonEmergencyRespiratorySympPresent: false,
		},
	}
}

// NewSessionWithUser creates new session with initial user information state at the start
func NewSessionWithUser(ctx context.Context, user db.User) Session {
	sess := NewSession(ctx)
	sess.State = InitialUserInfo
	sess.UserInformation = user
	sess.ProfileTypes = []string{}

	return sess
}

// GetGender is to print user's gender
func (s *Session) GetGender() string {
	gender := s.UserInformation.Gender
	if gender != "" {
		if strings.EqualFold(gender, "male") {
			return "Laki-laki"
		} else if strings.EqualFold(gender, "female") {
			return "Perempuan"
		}
		return gender
	}

	for _, gender := range s.ProfileTypeMap[ProfileTypeGender] {
		return gender.NameIndo
	}

	return ""
}

// PrintAge prints user's age
func (s *Session) PrintAge() string {
	// We need to implement this correctly. If we get this from UserInformation.Age(), the result will be "291".
	// This is due to improper handling on birthday information.
	for _, age := range s.ProfileTypeMap[ProfileTypeAge] {
		return age.NameIndo
	}

	return ""
}

// GetWeight is to get weight
func (s *Session) GetWeight() float64 {
	return s.UserInformation.Weight
}

// GetHeight is to get height
func (s *Session) GetHeight() float64 {
	return s.UserInformation.Height
}

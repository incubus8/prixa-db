package bot

// DiseaseAccuracy represents disease accuracy parameter
type DiseaseAccuracy string

const (
	diseaseAccuracyHigh   DiseaseAccuracy = "high"
	diseaseAccuracyMedium DiseaseAccuracy = "medium"
	diseaseAccuracyLow    DiseaseAccuracy = "low"
)

const (
	upperBound = 0.25
	lowerBound = 0.15
)

// GetDiseaseAccuracy gets diseases accuracy in string
// Score threshold is hardcoded to 0.25 as upper limit and 0.15 as lower limit
func GetDiseaseAccuracy(score float64) DiseaseAccuracy {
	if score > upperBound {
		return diseaseAccuracyHigh
	}

	if score > lowerBound {
		return diseaseAccuracyMedium
	}

	return diseaseAccuracyLow
}

var translateAccuracy = map[DiseaseAccuracy]string{
	diseaseAccuracyHigh:   "Kemungkinan Tinggi",
	diseaseAccuracyMedium: "Kemungkinan Sedang",
	diseaseAccuracyLow:    "Kemungkinan Rendah",
}

var accuracyColor = map[DiseaseAccuracy]string{
	diseaseAccuracyHigh:   `#0146ab`,
	diseaseAccuracyMedium: `#55B9E4`,
}

package bot

import (
	"bytes"
	"strings"
	"text/template"

	diagnosticGRPC "github.com/prixa-ai/prixa-proto/proto/diagnostic/v1"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/db"
)

type sep string

const (
	semicolon sep = "; "
	comma     sep = ", "
	listTag   sep = "<li>"
)

type printDiseaseData struct {
	DiseaseName      string
	DiseaseDesc      string
	DiseaseAccuracy  string
	AccuracyColor    string
	DiseasePrognosis string
	DiseaseTriage    string
}

// PrintDiseases is to print Prixa's diseases result
func (bot *Bot) PrintDiseases(diseases []*diagnosticGRPC.PotentialDisease) (diseasesStr string) {
	t, err := template.ParseFiles("./data/html/emailprinter_disease.html")
	if err != nil {
		logrus.WithError(err).Fatalln("no such file exist")
	}
	content := bytes.Buffer{}
	for _, disease := range diseases {
		accuracy := GetDiseaseAccuracy(cast.ToFloat64(disease.Score))
		err := t.Execute(&content, printDiseaseData{
			DiseaseName:      disease.Name,
			DiseaseDesc:      disease.Description,
			DiseaseAccuracy:  translateAccuracy[accuracy],
			AccuracyColor:    accuracyColor[accuracy],
			DiseasePrognosis: disease.Prognosis,
			DiseaseTriage:    disease.Triage.Description,
		})
		if err != nil {
			logrus.WithError(err).Errorln()
			return diseasesStr
		}
	}
	return content.String()
}

// printSymptoms is a helper to print symptoms from evidences
func (bot *Bot) printSymptoms(evidences db.Evidences, answer answer, sep sep) (printSymptoms string) {
	allSymptoms := []string{}
	for _, evidence := range evidences {
		if evidence.Answer == string(answer) {
			desc := bot.EvidenceToSymptomDesc(evidence)
			propsStr := ""
			propNames := bot.GetPropNames(evidence)
			if len(propNames) > 0 {
				propsStr = strings.Join(propNames, string(comma))
				allSymptoms = append(allSymptoms, desc.SymptomName+string(comma)+propsStr)
				continue
			}
			allSymptoms = append(allSymptoms, desc.SymptomName)
		}
	}

	if len(allSymptoms) == 0 {
		return "-"
	}
	printSymptoms = strings.Join(allSymptoms, string(sep))
	if sep == listTag {
		printSymptoms = strings.ReplaceAll(printSymptoms, string(sep), "</li>"+string(sep))
		printSymptoms = "<ul><li>" + printSymptoms + "</li></ul>"
	}

	return printSymptoms
}

// PrintPreconditionNames prints preconditions name
func (bot *Bot) PrintPreconditionNames(names []string) string {
	preconditions := ""
	for _, name := range names {
		preconditions += name + string(comma)
	}
	if len(preconditions) == 0 {
		return "-"
	}
	return preconditions
}

// PrintMainSymptoms prints main symptoms from evidences
func (bot *Bot) PrintMainSymptoms(evidences db.Evidences) string {
	return bot.printSymptoms(evidences.ChiefOnly(), answerYes, comma)
}

// PrintKnownSymptoms prints known symptoms (whose user select YES / Ya) from evidences
func (bot *Bot) PrintKnownSymptoms(evidences db.Evidences) string {
	return bot.printSymptoms(evidences.WithoutChief(), answerYes, listTag)
}

// PrintNotKnownSymptoms prints known symptoms (whose user select NO / Tidak) from evidences
func (bot *Bot) PrintNotKnownSymptoms(evidences db.Evidences) string {
	return bot.printSymptoms(evidences.WithoutChief(), answerNo, listTag)
}

// PrintUnknownSymptoms prints known symptoms (whose user select Unknown / Tidak tahu) from evidences
func (bot *Bot) PrintUnknownSymptoms(evidences db.Evidences) string {
	return bot.printSymptoms(evidences.WithoutChief(), answerUnknown, listTag)
}

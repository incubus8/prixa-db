package bot

import (
	"strings"

	"prixa.ai/prixa-db/pkg/db"
)

// This is helper for Profile related to Bot and Session

// ProfileTypesWithoutUser is the profile types when engine is started without predefined user data
var ProfileTypesWithoutUser = []string{
	ProfileTypeGender,
	ProfileTypeAge,
	ProfileTypePregnancy,
	ProfileTypeHistory,
	ProfileTypeBmi,
	ProfileTypeChronicDiseases,
	ProfileTypeContactHistoryCovid19,
	ProfileTypeLocalTransmission,
	ProfileTypeHabitRecord,
}

// GetGenderProfile return typename=male/female, profileID, and Profile object from
// user information
func GetGenderProfile(gender string, meta db.MetaTable) (string, string, db.Profile) {
	typename := ProfileTypeGender
	genders := meta.ProfilesByType(typename)
	for _, genderProfile := range genders {
		if strings.EqualFold(gender, genderProfile.Name) {
			return typename, genderProfile.ID, genderProfile
		}
	}

	return "", "", db.Profile{}
}

// GetBMIProfile return type profile typename, profile ID, and age Profile
// given weight and height
func GetBMIProfile(weight, height float64, meta db.MetaTable) (string, string, db.Profile) {
	if height == 0 && weight == 0 {
		return "", "", db.Profile{}
	}
	isObese := db.IsObeseWithWeightHeight(weight, height)
	typename := ProfileTypeBmi
	bmiProfiles := meta.ProfilesByType(typename)
	bmiProfile := bmiProfiles[0]
	if isObese {
		return typename, bmiProfile.ID, bmiProfile
	}
	return "", "", db.Profile{}
}

// GetAgeProfile returns profile typename, profile ID, and age Profile
// given age
func GetAgeProfile(age int, meta db.MetaTable) (string, string, db.Profile) {
	typename := ProfileTypeAge
	if age <= 0 {
		profile := meta.ProfileByName(profileNameChild)
		return typename, profile.ID, profile
	}

	ageProfiles := meta.ProfilesByType(typename)
	for _, profile := range ageProfiles {
		if ageName(age) == strings.ToLower(profile.Name) {
			return typename, profile.ID, profile
		}
	}

	return "", "", db.Profile{}
}

func ageName(age int) string {
	if age < 17 {
		return profileNameChild
	} else if age >= 17 && age <= 65 {
		return profileNameAdult
	} else {
		return profileNameGeriatrics
	}
}

// GetProfileBmi gets BMI from weight and height according to BMI profiles (Airtable database)
func GetProfileBmi(weight float64, height float64, bmiProfiles []db.Profile) db.Profile {
	bmiIndex := db.CalculateBMI(weight, height)
	for _, profile := range bmiProfiles {
		if !strings.EqualFold(profile.Type, ProfileTypeBmi) {
			continue
		}

		if bmiIndex >= profile.CustomValue1 && bmiIndex < profile.CustomValue2 {
			return profile
		}
	}
	return db.Profile{}
}

package bot

// SessionResult wrap Session and Results
type SessionResult struct {
	Session Session `json:"session"`
	Result  Result  `json:"result"`
}

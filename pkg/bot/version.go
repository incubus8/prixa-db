package bot

// Version tracks bot version
type Version string

// Version2 bots version 2
const Version2 Version = "v0.2"

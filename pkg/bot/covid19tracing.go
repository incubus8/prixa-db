package bot

import (
	"os"
	"strconv"
	"strings"

	"github.com/spf13/cast"
	"prixa.ai/prixa-db/pkg/core"
)

// processAskCovid19Tracing is
func (bot *Bot) processAskCovid19Tracing(param fsmProcessParam) (Session, Result, error) {
	/*
	* This state ask users about user contact history and user local transmission possibility
	 */
	session := param.session
	result := param.result
	result.Messages = append(result.Messages, Message{
		Type:  MessageTypeText,
		Value: bot.Messages["covid19Tracing"],
	})
	result.Action = Action{
		Type: ActionTypeButtons,
		Value: []ButtonValue{
			{
				Label: "Ya",
				Value: string(answerYes),
				Tag:   string(answerYes),
			},
			{
				Label: "Tidak",
				Value: string(answerNo),
				Tag:   string(answerNo),
			},
			{
				Label: "Tidak Tahu",
				Value: string(answerUnknown),
				Tag:   string(answerUnknown),
			},
			{
				Label: "Ya",
				Value: string(answerYes),
				Tag:   string(answerYes),
			},
			{
				Label: "Tidak",
				Value: string(answerNo),
				Tag:   string(answerNo),
			},
		},
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      ConfirmCovid19Tracing,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

func getCovid19TracingAnswers(reply interface{}) (string, string) {
	answers := strings.Split(cast.ToString(reply), ",")
	if len(answers) == 1 {
		return answers[0], ""
	}
	return answers[0], answers[1]
}

// processConfirmCovid19Tracing is
func (bot *Bot) processConfirmCovid19Tracing(param fsmProcessParam) (Session, Result, error) {
	/*
	* This state confirm users user contact history and user local transmission possibility
	 */
	session := param.session
	result := param.result
	contactHistory, localTransmission := getCovid19TracingAnswers(param.reply.Value)
	if strings.EqualFold(contactHistory, string(answerYes)) {
		session.Covid19Data.Score += 2.0
	}
	if strings.EqualFold(localTransmission, string(answerYes)) {
		session.Covid19Data.Score += 1.0
	}

	nextState := AskChiefComplaint
	enabled, _ := strconv.Atoi(os.Getenv("ENABLED_INSURANCE_STATE"))
	if enabled == core.StatusEnable {
		nextState = AskInsurance
	}

	return Session{
		ID:                         session.ID,
		PId:                        session.PId,
		AppID:                      session.AppID,
		CurrentIndexProfile:        session.CurrentIndexProfile,
		CurrentIndexProfileBmi:     session.CurrentIndexProfileBmi,
		CurrentIndexProfileHistory: session.CurrentIndexProfileHistory,
		ProfileTypes:               session.ProfileTypes,
		ProfileTypeMap:             session.ProfileTypeMap,
		ProfileIds:                 session.ProfileIds,
		UserInformation:            session.UserInformation,
		CurrentIndexInsurance:      session.CurrentIndexInsurance,
		CurrentPropType:            session.CurrentPropType,
		Evidences:                  session.Evidences,
		ProgressBar:                session.ProgressBar,
		CurrentSymptom:             session.CurrentSymptom,
		State:                      nextState,
		Latitude:                   session.Latitude,
		Longitude:                  session.Longitude,
		Covid19Data:                session.Covid19Data,
		UID:                        session.UID,
	}, result, nil
}

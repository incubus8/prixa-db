package clickhouse

import (
	"database/sql"
	"errors"
	"os"

	"github.com/ClickHouse/clickhouse-go"
	"github.com/sirupsen/logrus"
)

// NewClient is
func NewClient() (*sql.DB, error) {
	url, _ := os.LookupEnv("CLICKHOUSE_URL")
	if url == "" {
		return nil, errors.New("clickhouse URL not found")
	}

	connect, err := sql.Open("clickhouse", url)
	if err != nil {
		return nil, err
	}

	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			logrus.WithFields(logrus.Fields{
				"exception_code":    exception.Code,
				"exception_message": exception.Message,
			}).Info()
		} else {
			logrus.WithError(err).Error()
		}
		return nil, err
	}

	return connect, nil
}

#
#   Copyright (c) 2020 The NALAR Authors. All rights reserved.
#

PROJECT_NAME=prixa-db
PROJECT_REPO=gitlab.com/prixa-ai/prixa-db

FAIL_ON_STDOUT := awk '{ print } END { if (NR > 0) { exit 1 } }'

CURDIR := $(shell pwd)

GO := GO111MODULE=on go

GCP_CONTAINER_REGISTRY?=gcr.io
GCP_PROJECT_ID?=prixa-dev
IMAGE_NAME=$(GCP_CONTAINER_REGISTRY)/$(GCP_PROJECT_ID)/$(PROJECT_NAME)

GIT_COMMIT=$(shell git rev-parse --short HEAD)
GIT_TAG=$(shell git describe --abbrev=0 --tags --always --match "v*")

FILES := $(shell find . -name "*.go")

LDFLAGS += -X "$(PROJECT_REPO).PrixaDBReleaseVersion=$(shell git describe --abbrev=0 --tags --always --match 'v*')"
LDFLAGS += -X "$(PROJECT_REPO).PrixaDBBuildTS=$(shell date -u '+%Y-%m-%d %I:%M:%S')"
LDFLAGS += -X "$(PROJECT_REPO).PrixaDBGitHash=$(shell git rev-parse HEAD)"
LDFLAGS += -X "$(PROJECT_REPO).PrixaDBGitBranch=$(shell git rev-parse --abbrev-ref HEAD)"

IMAGE_TAG=$(GIT_TAG)-$(GIT_COMMIT)

all: build

vendor:
	$(GO) mod vendor

# Install the check tools.
check-setup: tools/bin/revive tools/bin/gosec tools/bin/errcheck tools/bin/golangci-lint

check: unparam unconvert prealloc nakedret fmt gofumpt gofumports errcheck gocyclo lint check-static staticcheck vet

fmt:
	@echo "gofmt (simplify)"
	@gofmt -s -l -w $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

gofumpt: tools/bin/gofumpt
	@echo "gofumpt (simplify)"
	@tools/bin/gofumpt -s -l -w $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

gofumports: tools/bin/gofumports
	@echo "gofumports"
	@tools/bin/gofumports -l -w $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

unparam: tools/bin/unparam
	@echo "unparam"
	@tools/bin/unparam -exported ./... 2>&1 | $(FAIL_ON_STDOUT)

unconvert: tools/bin/unconvert
	@echo "unconvert"
	@tools/bin/unconvert -v ./... 2>&1 | $(FAIL_ON_STDOUT)

gosec: tools/bin/gosec
	@echo "gosec"
	@tools/bin/gosec ./...

check-static: tools/bin/golangci-lint
	@tools/bin/golangci-lint run -v --disable-all --deadline=3m \
	  --enable=misspell \
	  --enable=ineffassign \
	  ./...

lint: tools/bin/revive
	@echo "Linting..."
	@tools/bin/revive -formatter friendly -config $(CURDIR)/tools/check/revive.toml $(FILES)

pylint: 
	pip3 install pylint
	pip3 install autopep8
	python3 pylinter.py

errcheck: tools/bin/errcheck
	@echo "errcheck"
	@GO111MODULE=on tools/bin/errcheck -exclude ./tools/check/errcheck_excludes.txt -ignoretests ./...

gocyclo: tools/bin/gocyclo
	@echo "cyclomatic complexities"
	@tools/bin/gocyclo -over 13 $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

prealloc: tools/bin/prealloc
	@echo "prealloc"
	@tools/bin/prealloc -simple -rangeloops -forloops $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

nakedret: tools/bin/nakedret
	@echo "nakedret"
	@tools/bin/nakedret -l 31 $(FILES) 2>&1 | $(FAIL_ON_STDOUT)

goconst: tools/bin/goconst
	@echo "goconst"
	@tools/bin/goconst -ignore "yacc|\.pb\." ./...

staticcheck: tools/bin/staticcheck
	@echo "staticcheck"
	@tools/bin/staticcheck -go 1.14 ./...

build:
	$(GO) mod download
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '-s -w $(LDFLAGS)' -o $(PROJECT_NAME) ./cmd/nalar/main.go
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '-s -w $(LDFLAGS)' -o $(PROJECT_NAME)-nalar-ingestion ./cmd/nalar-ingestion/main.go
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '-s -w $(LDFLAGS)' -o $(PROJECT_NAME)-nalar-enrichment ./cmd/nalar-enrichment/main.go
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '-s -w $(LDFLAGS)' -o $(PROJECT_NAME)-hospital-ingestion ./cmd/hospital-ingestion/main.go
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '-s -w $(LDFLAGS)' -o $(PROJECT_NAME)-siloam-ingestion ./cmd/siloam-ingestion/main.go

docker-build:
	docker pull $(IMAGE_NAME):latest || true
	docker build --cache-from $(IMAGE_NAME):latest -t $(IMAGE_NAME):$(IMAGE_TAG) .
	docker tag $(IMAGE_NAME):$(IMAGE_TAG) $(IMAGE_NAME):latest

docker-push:
	docker push $(IMAGE_NAME):$(IMAGE_TAG)
	docker push $(IMAGE_NAME):latest

docker: docker-build

e2e-nalar:
	cd e2e/nalar && py.test

vet:
	@echo "Vet..."
	$(GO) vet -all ./... 2>&1 | $(FAIL_ON_STDOUT)

test:
	$(GO) test ./pkg/core
	$(GO) test ./pkg/db
	$(GO) test ./pkg/util

clean:
	rm -rf ./$(PROJECT_NAME)
	rm -fr tools/bin

helm-debug:
	helm template --debug -f .helm/values.yaml .helm

helm-install:
	helm upgrade \
		-n prixa \
		--install \
		--force \
		-f .helm/values.yaml \
		prixa-db \
		.helm

helm-uninstall:
	helm uninstall prixa-db -n prixa

tools/bin/gofumpt: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/gofumpt mvdan.cc/gofumpt

tools/bin/gofumports: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/gofumports mvdan.cc/gofumpt/gofumports

tools/bin/unparam: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/unparam mvdan.cc/unparam

tools/bin/megacheck: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/megacheck honnef.co/go/tools/cmd/megacheck

tools/bin/revive: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/revive github.com/mgechev/revive

tools/bin/gosec: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/gosec github.com/securego/gosec/cmd/gosec

tools/bin/errcheck: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/errcheck github.com/kisielk/errcheck

tools/bin/failpoint-ctl: go.mod
	$(GO) build -o $@ github.com/pingcap/failpoint/failpoint-ctl

tools/bin/misspell:tools/check/go.mod
	$(GO) get -u github.com/client9/misspell/cmd/misspell

tools/bin/ineffassign:tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/ineffassign github.com/gordonklaus/ineffassign
tools/bin/golangci-lint:
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s -- -b ./tools/bin v1.21.0

tools/bin/gocyclo: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/gocyclo github.com/fzipp/gocyclo

tools/bin/prealloc: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/prealloc github.com/alexkohler/prealloc

tools/bin/nakedret: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/nakedret github.com/alexkohler/nakedret

tools/bin/goconst: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/goconst github.com/jgautheron/goconst/cmd/goconst

tools/bin/staticcheck: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/staticcheck honnef.co/go/tools/cmd/staticcheck

tools/bin/unconvert: tools/check/go.mod
	cd tools/check; \
	$(GO) build -o ../bin/unconvert github.com/mdempsky/unconvert

build-consumer-engine:
	$(GO) mod download
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '$(LDFLAGS)' -o $(PROJECT_NAME) ./cmd/data-consumer/main.go

run-consumer-engine:
	go run ./cmd/data-consumer/main.go

build-nalar-ingestion:
	$(GO) mod download
	CGO_ENABLED=0 $(GO) build -a -installsuffix cgo -ldflags '$(LDFLAGS)' -o $(PROJECT_NAME) ./cmd/nalar-ingestion/main.go

run-airtable-to-rabbitmq:
	go run ./cmd/nalar-ingestion/main.go --yaml=.\cmd\nalar-ingestion\airtabletorabbitmq.yaml	

run-rabbitmq-to-elasticsearch:
	go run ./cmd/nalar-ingestion/main.go --yaml=.\cmd\nalar-ingestion\rabbitmqtoelasticsearch.yaml

run-remove-outdated-data-in-elasticsearch:
	go run ./cmd/nalar-ingestion/main.go --yaml=.\cmd\nalar-ingestion\remve-outdated-data-elasticsearch.yaml	

run-rabbitmq-to-postgres:
	go run ./cmd/nalar-ingestion/main.go --yaml=.\cmd\nalar-ingestion\rabbitmqtopostgres.yaml

store-layers: ## Generate layers for the store
	cd pkg/store && $(GO) generate $(GOFLAGS) .

export GOBIN ?= $(PWD)/bin

check-mockery:
	env GO111MODULE=off go get -u github.com/vektra/mockery/... \

filestore-mocks:
	$(GOBIN)/mockery -dir pkg/services/filestore -all -output pkg/services/filestore/mocks -note 'Regenerate this file using `make filestore-mocks`.'

store-mocks:
	$(GOBIN)/mockery -dir store -all -output pkg/store/storetest/mocks -note 'Regenerate this file using `make store-mocks`.'

.PHONY: build clean vet test docker

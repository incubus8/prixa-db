module gitlab.com/prixa-ai/prixa-db/_tools

go 1.14

require (
	github.com/alexkohler/nakedret v1.0.0 // indirect
	github.com/alexkohler/prealloc v0.0.0-20200410114645-50faa068e017 // indirect
	github.com/fzipp/gocyclo v0.0.0-20150627053110-6acd4345c835 // indirect
	github.com/jgautheron/goconst v0.0.0-20200227150835-cda7ea3bf591 // indirect
	github.com/kisielk/errcheck v1.2.0 // indirect
	github.com/mdempsky/unconvert v0.0.0-20200228143138-95ecdbfc0b5f // indirect
	github.com/mgechev/revive v1.0.2 // indirect
	github.com/securego/gosec v0.0.0-20200316084457-7da9f46445fd // indirect
	honnef.co/go/tools v0.0.1-2020.1.3 // indirect
	mvdan.cc/gofumpt v0.0.0-20200412215918-a91da47f375c // indirect
	mvdan.cc/unparam v0.0.0-20200314162735-0ac8026f7d06 // indirect
)
